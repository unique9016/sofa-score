package com.score.cricket.Models.AppMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

import java.util.List;

public class Matches_Example {

    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("rows")
    @Expose
    private Integer rows;
    @SerializedName("tournaments")
    @Expose
    private List<Matches_Tournament> tournaments = null;

    public Matches_Example() {
    }

    public Matches_Example(AppSport sport, Integer rows, List<Matches_Tournament> tournaments) {
        super();
        this.sport = sport;
        this.rows = rows;
        this.tournaments = tournaments;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public List<Matches_Tournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<Matches_Tournament> tournaments) {
        this.tournaments = tournaments;
    }

}