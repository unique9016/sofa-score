package com.score.cricket.Models.AppMatches.AppMatches_Innings;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Innings_BowlingLine {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("over")
    @Expose
    private Double over;
    @SerializedName("maiden")
    @Expose
    private Integer maiden;
    @SerializedName("run")
    @Expose
    private Integer run;
    @SerializedName("wicket")
    @Expose
    private Integer wicket;
    @SerializedName("wide")
    @Expose
    private Integer wide;
    @SerializedName("noBall")
    @Expose
    private Integer noBall;


    public Innings_BowlingLine() {
    }

    /**
     *
     * @param over
     * @param maiden
     * @param noBall
     * @param wide
     * @param name
     * @param run
     * @param id
     * @param wicket
     */
    public Innings_BowlingLine(Integer id, String name, Double over, Integer maiden, Integer run, Integer wicket, Integer wide, Integer noBall) {
        super();
        this.id = id;
        this.name = name;
        this.over = over;
        this.maiden = maiden;
        this.run = run;
        this.wicket = wicket;
        this.wide = wide;
        this.noBall = noBall;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getOver() {
        return over;
    }

    public void setOver(Double over) {
        this.over = over;
    }

    public Integer getMaiden() {
        return maiden;
    }

    public void setMaiden(Integer maiden) {
        this.maiden = maiden;
    }

    public Integer getRun() {
        return run;
    }

    public void setRun(Integer run) {
        this.run = run;
    }

    public Integer getWicket() {
        return wicket;
    }

    public void setWicket(Integer wicket) {
        this.wicket = wicket;
    }

    public Integer getWide() {
        return wide;
    }

    public void setWide(Integer wide) {
        this.wide = wide;
    }

    public Integer getNoBall() {
        return noBall;
    }

    public void setNoBall(Integer noBall) {
        this.noBall = noBall;
    }

}