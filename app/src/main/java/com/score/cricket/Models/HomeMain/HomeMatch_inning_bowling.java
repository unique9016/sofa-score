package com.score.cricket.Models.HomeMain;

public class HomeMatch_inning_bowling {
    String bowlername;
    int maiden,run,wicket,noball,wide;
    Double ER,over;

    public HomeMatch_inning_bowling() {
    }


    public HomeMatch_inning_bowling(String bowlername, int maiden, int run, int wicket, int noball, int wide, Double ER, Double over) {
        this.bowlername = bowlername;
        this.maiden = maiden;
        this.run = run;
        this.wicket = wicket;
        this.noball = noball;
        this.wide = wide;
        this.ER = ER;
        this.over = over;
    }

    public String getBowlername() {
        return bowlername;
    }

    public void setBowlername(String bowlername) {
        this.bowlername = bowlername;
    }

    public int getMaiden() {
        return maiden;
    }

    public void setMaiden(int maiden) {
        this.maiden = maiden;
    }

    public int getRun() {
        return run;
    }

    public void setRun(int run) {
        this.run = run;
    }

    public int getWicket() {
        return wicket;
    }

    public void setWicket(int wicket) {
        this.wicket = wicket;
    }

    public int getNoball() {
        return noball;
    }

    public void setNoball(int noball) {
        this.noball = noball;
    }

    public int getWide() {
        return wide;
    }

    public void setWide(int wide) {
        this.wide = wide;
    }

    public Double getER() {
        return ER;
    }

    public void setER(Double ER) {
        this.ER = ER;
    }

    public Double getOver() {
        return over;
    }

    public void setOver(Double over) {
        this.over = over;
    }
}
