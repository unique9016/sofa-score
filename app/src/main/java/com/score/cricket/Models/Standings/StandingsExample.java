package com.score.cricket.Models.Standings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

import java.util.List;

public class StandingsExample {

    @SerializedName("tableRows")
    @Expose
    private List<StandingsTableRow> tableRows = null;
    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("category")
    @Expose
    private StandingsCategory category;
    @SerializedName("tournament")
    @Expose
    private StandingsTournament tournament;
    @SerializedName("season")
    @Expose
    private StandingsSeason season;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("descriptions")
    @Expose
    private List<Object> descriptions = null;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("updatedAtFormated")
    @Expose
    private String updatedAtFormated;
    @SerializedName("updatedAtTimestamp")
    @Expose
    private Integer updatedAtTimestamp;
    @SerializedName("totalPointsFields")
    @Expose
    private List<String> totalPointsFields = null;
    @SerializedName("homePointsFields")
    @Expose
    private List<Object> homePointsFields = null;
    @SerializedName("awayPointsFields")
    @Expose
    private List<Object> awayPointsFields = null;
    @SerializedName("totalTableKeys")
    @Expose
    private StandingsTotalTableKeys totalTableKeys;
    @SerializedName("homeTableKeys")
    @Expose
    private List<Object> homeTableKeys = null;
    @SerializedName("awayTableKeys")
    @Expose
    private List<Object> awayTableKeys = null;
    @SerializedName("filteredTableKeys")
    @Expose
    private StandingsFilteredTableKeys filteredTableKeys;
    @SerializedName("filteredHomeTableKeys")
    @Expose
    private List<Object> filteredHomeTableKeys = null;
    @SerializedName("filteredAwayTableKeys")
    @Expose
    private List<Object> filteredAwayTableKeys = null;
    @SerializedName("round")
    @Expose
    private Integer round;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("tableKeysOrder")
    @Expose
    private List<String> tableKeysOrder = null;
    @SerializedName("homeTableKeysOrder")
    @Expose
    private List<Object> homeTableKeysOrder = null;
    @SerializedName("awayTableKeysOrder")
    @Expose
    private List<Object> awayTableKeysOrder = null;
    @SerializedName("filteredTableKeysOrder")
    @Expose
    private List<String> filteredTableKeysOrder = null;
    @SerializedName("filteredHomeTableKeysOrder")
    @Expose
    private List<Object> filteredHomeTableKeysOrder = null;
    @SerializedName("filteredAwayTableKeysOrder")
    @Expose
    private List<Object> filteredAwayTableKeysOrder = null;
    @SerializedName("isLive")
    @Expose
    private Boolean isLive;
    @SerializedName("hasHomeAwayStandings")
    @Expose
    private Boolean hasHomeAwayStandings;
    @SerializedName("promotions")
    @Expose
    private List<Object> promotions = null;

    
    public StandingsExample() {
    }

    /**
     *
     * @param awayTableKeysOrder
     * @param homeTableKeys
     * @param awayPointsFields
     * @param updatedAtFormated
     * @param tournament
     * @param filteredHomeTableKeysOrder
     * @param descriptions
     * @param tableKeysOrder
     * @param isLive
     * @param filteredTableKeysOrder
     * @param season
     * @param id
     * @param hasHomeAwayStandings
     * @param updatedAt
     * @param homePointsFields
     * @param filteredAwayTableKeys
     * @param totalTableKeys
     * @param totalPointsFields
     * @param awayTableKeys
     * @param updatedAtTimestamp
     * @param filteredHomeTableKeys
     * @param homeTableKeysOrder
     * @param promotions
     * @param round
     * @param name
     * @param filteredTableKeys
     * @param category
     * @param sport
     * @param tableRows
     * @param filteredAwayTableKeysOrder
     */
    public StandingsExample(List<StandingsTableRow> tableRows, AppSport sport, StandingsCategory category, StandingsTournament tournament, StandingsSeason season, String name, List<Object> descriptions, String updatedAt, String updatedAtFormated, Integer updatedAtTimestamp, List<String> totalPointsFields, List<Object> homePointsFields, List<Object> awayPointsFields, StandingsTotalTableKeys totalTableKeys, List<Object> homeTableKeys, List<Object> awayTableKeys, StandingsFilteredTableKeys filteredTableKeys, List<Object> filteredHomeTableKeys, List<Object> filteredAwayTableKeys, Integer round, Integer id, List<String> tableKeysOrder, List<Object> homeTableKeysOrder, List<Object> awayTableKeysOrder, List<String> filteredTableKeysOrder, List<Object> filteredHomeTableKeysOrder, List<Object> filteredAwayTableKeysOrder, Boolean isLive, Boolean hasHomeAwayStandings, List<Object> promotions) {
        super();
        this.tableRows = tableRows;
        this.sport = sport;
        this.category = category;
        this.tournament = tournament;
        this.season = season;
        this.name = name;
        this.descriptions = descriptions;
        this.updatedAt = updatedAt;
        this.updatedAtFormated = updatedAtFormated;
        this.updatedAtTimestamp = updatedAtTimestamp;
        this.totalPointsFields = totalPointsFields;
        this.homePointsFields = homePointsFields;
        this.awayPointsFields = awayPointsFields;
        this.totalTableKeys = totalTableKeys;
        this.homeTableKeys = homeTableKeys;
        this.awayTableKeys = awayTableKeys;
        this.filteredTableKeys = filteredTableKeys;
        this.filteredHomeTableKeys = filteredHomeTableKeys;
        this.filteredAwayTableKeys = filteredAwayTableKeys;
        this.round = round;
        this.id = id;
        this.tableKeysOrder = tableKeysOrder;
        this.homeTableKeysOrder = homeTableKeysOrder;
        this.awayTableKeysOrder = awayTableKeysOrder;
        this.filteredTableKeysOrder = filteredTableKeysOrder;
        this.filteredHomeTableKeysOrder = filteredHomeTableKeysOrder;
        this.filteredAwayTableKeysOrder = filteredAwayTableKeysOrder;
        this.isLive = isLive;
        this.hasHomeAwayStandings = hasHomeAwayStandings;
        this.promotions = promotions;
    }

    public List<StandingsTableRow> getTableRows() {
        return tableRows;
    }

    public void setTableRows(List<StandingsTableRow> tableRows) {
        this.tableRows = tableRows;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public StandingsCategory getCategory() {
        return category;
    }

    public void setCategory(StandingsCategory category) {
        this.category = category;
    }

    public StandingsTournament getTournament() {
        return tournament;
    }

    public void setTournament(StandingsTournament tournament) {
        this.tournament = tournament;
    }

    public StandingsSeason getSeason() {
        return season;
    }

    public void setSeason(StandingsSeason season) {
        this.season = season;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<Object> descriptions) {
        this.descriptions = descriptions;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAtFormated() {
        return updatedAtFormated;
    }

    public void setUpdatedAtFormated(String updatedAtFormated) {
        this.updatedAtFormated = updatedAtFormated;
    }

    public Integer getUpdatedAtTimestamp() {
        return updatedAtTimestamp;
    }

    public void setUpdatedAtTimestamp(Integer updatedAtTimestamp) {
        this.updatedAtTimestamp = updatedAtTimestamp;
    }

    public List<String> getTotalPointsFields() {
        return totalPointsFields;
    }

    public void setTotalPointsFields(List<String> totalPointsFields) {
        this.totalPointsFields = totalPointsFields;
    }

    public List<Object> getHomePointsFields() {
        return homePointsFields;
    }

    public void setHomePointsFields(List<Object> homePointsFields) {
        this.homePointsFields = homePointsFields;
    }

    public List<Object> getAwayPointsFields() {
        return awayPointsFields;
    }

    public void setAwayPointsFields(List<Object> awayPointsFields) {
        this.awayPointsFields = awayPointsFields;
    }

    public StandingsTotalTableKeys getTotalTableKeys() {
        return totalTableKeys;
    }

    public void setTotalTableKeys(StandingsTotalTableKeys totalTableKeys) {
        this.totalTableKeys = totalTableKeys;
    }

    public List<Object> getHomeTableKeys() {
        return homeTableKeys;
    }

    public void setHomeTableKeys(List<Object> homeTableKeys) {
        this.homeTableKeys = homeTableKeys;
    }

    public List<Object> getAwayTableKeys() {
        return awayTableKeys;
    }

    public void setAwayTableKeys(List<Object> awayTableKeys) {
        this.awayTableKeys = awayTableKeys;
    }

    public StandingsFilteredTableKeys getFilteredTableKeys() {
        return filteredTableKeys;
    }

    public void setFilteredTableKeys(StandingsFilteredTableKeys filteredTableKeys) {
        this.filteredTableKeys = filteredTableKeys;
    }

    public List<Object> getFilteredHomeTableKeys() {
        return filteredHomeTableKeys;
    }

    public void setFilteredHomeTableKeys(List<Object> filteredHomeTableKeys) {
        this.filteredHomeTableKeys = filteredHomeTableKeys;
    }

    public List<Object> getFilteredAwayTableKeys() {
        return filteredAwayTableKeys;
    }

    public void setFilteredAwayTableKeys(List<Object> filteredAwayTableKeys) {
        this.filteredAwayTableKeys = filteredAwayTableKeys;
    }

    public Integer getRound() {
        return round;
    }

    public void setRound(Integer round) {
        this.round = round;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getTableKeysOrder() {
        return tableKeysOrder;
    }

    public void setTableKeysOrder(List<String> tableKeysOrder) {
        this.tableKeysOrder = tableKeysOrder;
    }

    public List<Object> getHomeTableKeysOrder() {
        return homeTableKeysOrder;
    }

    public void setHomeTableKeysOrder(List<Object> homeTableKeysOrder) {
        this.homeTableKeysOrder = homeTableKeysOrder;
    }

    public List<Object> getAwayTableKeysOrder() {
        return awayTableKeysOrder;
    }

    public void setAwayTableKeysOrder(List<Object> awayTableKeysOrder) {
        this.awayTableKeysOrder = awayTableKeysOrder;
    }

    public List<String> getFilteredTableKeysOrder() {
        return filteredTableKeysOrder;
    }

    public void setFilteredTableKeysOrder(List<String> filteredTableKeysOrder) {
        this.filteredTableKeysOrder = filteredTableKeysOrder;
    }

    public List<Object> getFilteredHomeTableKeysOrder() {
        return filteredHomeTableKeysOrder;
    }

    public void setFilteredHomeTableKeysOrder(List<Object> filteredHomeTableKeysOrder) {
        this.filteredHomeTableKeysOrder = filteredHomeTableKeysOrder;
    }

    public List<Object> getFilteredAwayTableKeysOrder() {
        return filteredAwayTableKeysOrder;
    }

    public void setFilteredAwayTableKeysOrder(List<Object> filteredAwayTableKeysOrder) {
        this.filteredAwayTableKeysOrder = filteredAwayTableKeysOrder;
    }

    public Boolean getIsLive() {
        return isLive;
    }

    public void setIsLive(Boolean isLive) {
        this.isLive = isLive;
    }

    public Boolean getHasHomeAwayStandings() {
        return hasHomeAwayStandings;
    }

    public void setHasHomeAwayStandings(Boolean hasHomeAwayStandings) {
        this.hasHomeAwayStandings = hasHomeAwayStandings;
    }

    public List<Object> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<Object> promotions) {
        this.promotions = promotions;
    }

}