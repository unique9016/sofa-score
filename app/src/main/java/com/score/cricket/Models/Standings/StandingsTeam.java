package com.score.cricket.Models.Standings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StandingsTeam {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("national")
    @Expose
    private Boolean national;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("gender")
    @Expose
    private String gender;

    
    public StandingsTeam() {
    }

    /**
     *
     * @param gender
     * @param name
     * @param national
     * @param id
     * @param shortName
     * @param slug
     */
    public StandingsTeam(String name, String slug, Boolean national, Integer id, String shortName, String gender) {
        super();
        this.name = name;
        this.slug = slug;
        this.national = national;
        this.id = id;
        this.shortName = shortName;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Boolean getNational() {
        return national;
    }

    public void setNational(Boolean national) {
        this.national = national;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}