package com.score.cricket.Models.AppMatches.AppMatches_Lineup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lineup_PlayerColor {

    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("outline")
    @Expose
    private String outline;


    public Lineup_PlayerColor() {
    }

    public Lineup_PlayerColor(String number, String outline) {
        super();
        this.number = number;
        this.outline = outline;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOutline() {
        return outline;
    }

    public void setOutline(String outline) {
        this.outline = outline;
    }

}