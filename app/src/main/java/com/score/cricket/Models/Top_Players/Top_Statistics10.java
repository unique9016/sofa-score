
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics10 {

    @SerializedName("accuratePasses")
    @Expose
    private Integer accuratePasses;
    @SerializedName("accuratePassesPercentage")
    @Expose
    private Double accuratePassesPercentage;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;


    public Top_Statistics10() {
    }

    public Top_Statistics10(Integer accuratePasses, Double accuratePassesPercentage, Integer id, String type, Integer appearances) {
        super();
        this.accuratePasses = accuratePasses;
        this.accuratePassesPercentage = accuratePassesPercentage;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getAccuratePasses() {
        return accuratePasses;
    }

    public void setAccuratePasses(Integer accuratePasses) {
        this.accuratePasses = accuratePasses;
    }

    public Double getAccuratePassesPercentage() {
        return accuratePassesPercentage;
    }

    public void setAccuratePassesPercentage(Double accuratePassesPercentage) {
        this.accuratePassesPercentage = accuratePassesPercentage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
