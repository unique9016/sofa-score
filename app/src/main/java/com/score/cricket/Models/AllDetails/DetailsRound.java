package com.score.cricket.Models.AllDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailsRound {

    @SerializedName("roundName")
    @Expose
    private String roundName;
    @SerializedName("roundSlug")
    @Expose
    private String roundSlug;
    @SerializedName("createdAtTimestamp")
    @Expose
    private Integer createdAtTimestamp;

    
    public DetailsRound() {
    }

    public DetailsRound(String roundName, String roundSlug, Integer createdAtTimestamp) {
        super();
        this.roundName = roundName;
        this.roundSlug = roundSlug;
        this.createdAtTimestamp = createdAtTimestamp;
    }

    public String getRoundName() {
        return roundName;
    }

    public void setRoundName(String roundName) {
        this.roundName = roundName;
    }

    public String getRoundSlug() {
        return roundSlug;
    }

    public void setRoundSlug(String roundSlug) {
        this.roundSlug = roundSlug;
    }

    public Integer getCreatedAtTimestamp() {
        return createdAtTimestamp;
    }

    public void setCreatedAtTimestamp(Integer createdAtTimestamp) {
        this.createdAtTimestamp = createdAtTimestamp;
    }

}