
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics3 {

    @SerializedName("penaltiesTaken")
    @Expose
    private Integer penaltiesTaken;
    @SerializedName("penaltyGoals")
    @Expose
    private Integer penaltyGoals;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;


    public Top_Statistics3() {
    }

    public Top_Statistics3(Integer penaltiesTaken, Integer penaltyGoals, Integer id, String type, Integer appearances) {
        super();
        this.penaltiesTaken = penaltiesTaken;
        this.penaltyGoals = penaltyGoals;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getPenaltiesTaken() {
        return penaltiesTaken;
    }

    public void setPenaltiesTaken(Integer penaltiesTaken) {
        this.penaltiesTaken = penaltiesTaken;
    }

    public Integer getPenaltyGoals() {
        return penaltyGoals;
    }

    public void setPenaltyGoals(Integer penaltyGoals) {
        this.penaltyGoals = penaltyGoals;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
