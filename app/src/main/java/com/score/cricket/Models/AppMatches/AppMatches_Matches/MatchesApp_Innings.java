package com.score.cricket.Models.AppMatches.AppMatches_Matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchesApp_Innings {

    @SerializedName("inning1")
    @Expose
    private MatchesApp_Inning1 inning1;
    @SerializedName("inning2")
    @Expose
    private MatchesApp_Inning2 inning2;

    
    public MatchesApp_Innings() {
    }

    
    public MatchesApp_Innings(MatchesApp_Inning1 inning1, MatchesApp_Inning2 inning2) {
        super();
        this.inning1 = inning1;
        this.inning2 = inning2;
    }

    public MatchesApp_Inning1 getInning1() {
        return inning1;
    }

    public void setInning1(MatchesApp_Inning1 inning1) {
        this.inning1 = inning1;
    }

    public MatchesApp_Inning2 getInning2() {
        return inning2;
    }

    public void setInning2(MatchesApp_Inning2 inning2) {
        this.inning2 = inning2;
    }

}