package com.score.cricket.Models.HomeMain;

import java.util.ArrayList;

public class HomeMatcheGroupInfo {

    private String name,flage;
    private ArrayList<HomeMatcheChildInfo> list = new ArrayList<HomeMatcheChildInfo>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlage() {
        return flage;
    }

    public void setFlage(String flage) {
        this.flage = flage;
    }

    public ArrayList<HomeMatcheChildInfo> getProductList() {
        return list;
    }

    public void setProductList(ArrayList<HomeMatcheChildInfo> productList) {
        this.list = productList;
    }

}