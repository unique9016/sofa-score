package com.score.cricket.Models.TeamDetails.FeaturedMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeaturedAwayScore {

    @SerializedName("current")
    @Expose
    private Integer current;
    @SerializedName("display")
    @Expose
    private Integer display;
    @SerializedName("innings")
    @Expose
    private FeaturedInnings innings;
    @SerializedName("hits")
    @Expose
    private Integer hits;
    @SerializedName("errors")
    @Expose
    private Integer errors;


    public FeaturedAwayScore() {
    }

    
    public FeaturedAwayScore(Integer current, Integer display, FeaturedInnings innings, Integer hits, Integer errors) {
        super();
        this.current = current;
        this.display = display;
        this.innings = innings;
        this.hits = hits;
        this.errors = errors;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getDisplay() {
        return display;
    }

    public void setDisplay(Integer display) {
        this.display = display;
    }

    public FeaturedInnings getInnings() {
        return innings;
    }

    public void setInnings(FeaturedInnings innings) {
        this.innings = innings;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    public Integer getErrors() {
        return errors;
    }

    public void setErrors(Integer errors) {
        this.errors = errors;
    }

}