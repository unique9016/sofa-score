
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics4 {

    @SerializedName("goalsAssistsSum")
    @Expose
    private Integer goalsAssistsSum;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;

    
    public Top_Statistics4() {
    }

    public Top_Statistics4(Integer goalsAssistsSum, Integer id, String type, Integer appearances) {
        super();
        this.goalsAssistsSum = goalsAssistsSum;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getGoalsAssistsSum() {
        return goalsAssistsSum;
    }

    public void setGoalsAssistsSum(Integer goalsAssistsSum) {
        this.goalsAssistsSum = goalsAssistsSum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
