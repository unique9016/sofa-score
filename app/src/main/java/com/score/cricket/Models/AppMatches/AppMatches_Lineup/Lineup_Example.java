package com.score.cricket.Models.AppMatches.AppMatches_Lineup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lineup_Example {

    @SerializedName("confirmed")
    @Expose
    private Boolean confirmed;
    @SerializedName("home")
    @Expose
    private Lineup_Home home;
    @SerializedName("away")
    @Expose
    private Lineup_Away away;

    
    public Lineup_Example() {
    }

    public Lineup_Example(Boolean confirmed, Lineup_Home home, Lineup_Away away) {
        super();
        this.confirmed = confirmed;
        this.home = home;
        this.away = away;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Lineup_Home getHome() {
        return home;
    }

    public void setHome(Lineup_Home home) {
        this.home = home;
    }

    public Lineup_Away getAway() {
        return away;
    }

    public void setAway(Lineup_Away away) {
        this.away = away;
    }

}