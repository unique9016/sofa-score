package com.score.cricket.Models.LeaguesUi.LeagueAllName;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeaguesGroup {

    @SerializedName("uniqueTournaments")
    @Expose
    private List<LeaguesUniqueTournament> uniqueTournaments = null;

    public LeaguesGroup() {
    }

    public LeaguesGroup(List<LeaguesUniqueTournament> uniqueTournaments) {
        super();
        this.uniqueTournaments = uniqueTournaments;
    }

    public List<LeaguesUniqueTournament> getUniqueTournaments() {
        return uniqueTournaments;
    }

    public void setUniqueTournaments(List<LeaguesUniqueTournament> uniqueTournaments) {
        this.uniqueTournaments = uniqueTournaments;
    }

}
