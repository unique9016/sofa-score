package com.score.cricket.Models.TeamDetails.TeamSquad;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

public class TeamSquadTeam {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("national")
    @Expose
    private Boolean national;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("teamColors")
    @Expose
    private TeamSquadTeamColors teamColors;


    public TeamSquadTeam() {
    }

    /**
     *
     * @param name
     * @param national
     * @param fullName
     * @param id
     * @param sport
     * @param slug
     * @param teamColors
     */
    public TeamSquadTeam(String name, String slug, AppSport sport, Boolean national, Integer id, String fullName, TeamSquadTeamColors teamColors) {
        super();
        this.name = name;
        this.slug = slug;
        this.sport = sport;
        this.national = national;
        this.id = id;
        this.fullName = fullName;
        this.teamColors = teamColors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Boolean getNational() {
        return national;
    }

    public void setNational(Boolean national) {
        this.national = national;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public TeamSquadTeamColors getTeamColors() {
        return teamColors;
    }

    public void setTeamColors(TeamSquadTeamColors teamColors) {
        this.teamColors = teamColors;
    }

}