package com.score.cricket.Models.TeamDetails.FeaturedMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeaturedChanges {

    @SerializedName("changes")
    @Expose
    private List<String> changes = null;
    @SerializedName("changeTimestamp")
    @Expose
    private Integer changeTimestamp;

    
    public FeaturedChanges() {
    }

    
    public FeaturedChanges(List<String> changes, Integer changeTimestamp) {
        super();
        this.changes = changes;
        this.changeTimestamp = changeTimestamp;
    }

    public List<String> getChanges() {
        return changes;
    }

    public void setChanges(List<String> changes) {
        this.changes = changes;
    }

    public Integer getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Integer changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

}
