
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_Country {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("flag")
    @Expose
    private String flag;

    
    public Details_Country() {
    }

    /**
     * 
     * @param flag
     * @param name
     */
    public Details_Country(String name, String flag) {
        super();
        this.name = name;
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

}
