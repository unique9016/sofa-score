package com.score.cricket.Models.TeamDetails.TeamSquad;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamSquadExample {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("team")
    @Expose
    private TeamSquadTeam team;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("marketValueCurrency")
    @Expose
    private String marketValueCurrency;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("hasImage")
    @Expose
    private Boolean hasImage;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("dateOfBirthFormated")
    @Expose
    private String dateOfBirthFormated;
    @SerializedName("dateOfBirthTimestamp")
    @Expose
    private Integer dateOfBirthTimestamp;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("nationalityIOC")
    @Expose
    private String nationalityIOC;
    @SerializedName("playerInfo")
    @Expose
    private TeamSquadPlayerInfo playerInfo;
    @SerializedName("shirtNumber")
    @Expose
    private Integer shirtNumber;
    @SerializedName("position")
    @Expose
    private String position;

    
    public TeamSquadExample() {
    }

    /**
     *
     * @param marketValueCurrency
     * @param flag
     * @param shirtNumber
     * @param playerInfo
     * @param hasImage
     * @param dateOfBirthFormated
     * @param team
     * @param dateOfBirthTimestamp
     * @param userCount
     * @param nationality
     * @param name
     * @param nationalityIOC
     * @param id
     * @param position
     * @param shortName
     * @param slug
     * @param age
     */
    public TeamSquadExample(String name, String slug, String shortName, TeamSquadTeam team, Integer userCount, Integer id, String marketValueCurrency, String nationality, Boolean hasImage, Integer age, String dateOfBirthFormated, Integer dateOfBirthTimestamp, String flag, String nationalityIOC, TeamSquadPlayerInfo playerInfo, Integer shirtNumber, String position) {
        super();
        this.name = name;
        this.slug = slug;
        this.shortName = shortName;
        this.team = team;
        this.userCount = userCount;
        this.id = id;
        this.marketValueCurrency = marketValueCurrency;
        this.nationality = nationality;
        this.hasImage = hasImage;
        this.age = age;
        this.dateOfBirthFormated = dateOfBirthFormated;
        this.dateOfBirthTimestamp = dateOfBirthTimestamp;
        this.flag = flag;
        this.nationalityIOC = nationalityIOC;
        this.playerInfo = playerInfo;
        this.shirtNumber = shirtNumber;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public TeamSquadTeam getTeam() {
        return team;
    }

    public void setTeam(TeamSquadTeam team) {
        this.team = team;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMarketValueCurrency() {
        return marketValueCurrency;
    }

    public void setMarketValueCurrency(String marketValueCurrency) {
        this.marketValueCurrency = marketValueCurrency;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Boolean getHasImage() {
        return hasImage;
    }

    public void setHasImage(Boolean hasImage) {
        this.hasImage = hasImage;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDateOfBirthFormated() {
        return dateOfBirthFormated;
    }

    public void setDateOfBirthFormated(String dateOfBirthFormated) {
        this.dateOfBirthFormated = dateOfBirthFormated;
    }

    public Integer getDateOfBirthTimestamp() {
        return dateOfBirthTimestamp;
    }

    public void setDateOfBirthTimestamp(Integer dateOfBirthTimestamp) {
        this.dateOfBirthTimestamp = dateOfBirthTimestamp;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNationalityIOC() {
        return nationalityIOC;
    }

    public void setNationalityIOC(String nationalityIOC) {
        this.nationalityIOC = nationalityIOC;
    }

    public TeamSquadPlayerInfo getPlayerInfo() {
        return playerInfo;
    }

    public void setPlayerInfo(TeamSquadPlayerInfo playerInfo) {
        this.playerInfo = playerInfo;
    }

    public Integer getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(Integer shirtNumber) {
        this.shirtNumber = shirtNumber;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}