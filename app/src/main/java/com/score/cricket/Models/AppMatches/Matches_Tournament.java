package com.score.cricket.Models.AppMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Matches_Tournament {

    @SerializedName("tournament")
    @Expose
    private Matches_Tournament_ tournament;
    @SerializedName("category")
    @Expose
    private Matches_Category category;
    @SerializedName("season")
    @Expose
    private Matches_Season season;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;
    @SerializedName("hasEventPlayerHeatMap")
    @Expose
    private Object hasEventPlayerHeatMap;
    @SerializedName("hasBoxScore")
    @Expose
    private Object hasBoxScore;
    @SerializedName("events")
    @Expose
    private List<Matches_Event> events = null;


    public Matches_Tournament() {
    }

    public Matches_Tournament(Matches_Tournament_ tournament, Matches_Category category, Matches_Season season, Boolean hasEventPlayerStatistics, Object hasEventPlayerHeatMap, Object hasBoxScore, List<Matches_Event> events) {
        super();
        this.tournament = tournament;
        this.category = category;
        this.season = season;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
        this.hasBoxScore = hasBoxScore;
        this.events = events;
    }

    public Matches_Tournament_ getTournament() {
        return tournament;
    }

    public void setTournament(Matches_Tournament_ tournament) {
        this.tournament = tournament;
    }

    public Matches_Category getCategory() {
        return category;
    }

    public void setCategory(Matches_Category category) {
        this.category = category;
    }

    public Matches_Season getSeason() {
        return season;
    }

    public void setSeason(Matches_Season season) {
        this.season = season;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public Object getHasEventPlayerHeatMap() {
        return hasEventPlayerHeatMap;
    }

    public void setHasEventPlayerHeatMap(Object hasEventPlayerHeatMap) {
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
    }

    public Object getHasBoxScore() {
        return hasBoxScore;
    }

    public void setHasBoxScore(Object hasBoxScore) {
        this.hasBoxScore = hasBoxScore;
    }

    public List<Matches_Event> getEvents() {
        return events;
    }

    public void setEvents(List<Matches_Event> events) {
        this.events = events;
    }

}