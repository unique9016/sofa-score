package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamChanges_ {

    @SerializedName("changeTimestamp")
    @Expose
    private Integer changeTimestamp;


    public TeamChanges_() {
    }

    public TeamChanges_(Integer changeTimestamp) {
        super();
        this.changeTimestamp = changeTimestamp;
    }

    public Integer getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Integer changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

}