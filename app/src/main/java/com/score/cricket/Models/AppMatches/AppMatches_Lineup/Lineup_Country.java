package com.score.cricket.Models.AppMatches.AppMatches_Lineup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lineup_Country {

    @SerializedName("alpha2")
    @Expose
    private String alpha2;
    @SerializedName("name")
    @Expose
    private String name;

    
    public Lineup_Country() {
    }

    public Lineup_Country(String alpha2, String name) {
        super();
        this.alpha2 = alpha2;
        this.name = name;
    }

    public String getAlpha2() {
        return alpha2;
    }

    public void setAlpha2(String alpha2) {
        this.alpha2 = alpha2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}