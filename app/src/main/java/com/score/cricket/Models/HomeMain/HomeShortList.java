package com.score.cricket.Models.HomeMain;

import com.score.cricket.Listener.ListnerItems;

public class HomeShortList implements ListnerItems {
    String index,name,total,wl,point;
    int id;

    public HomeShortList() {
    }

    public HomeShortList(String index, String name, String total, String wl, String point, int id) {
        this.index = index;
        this.name = name;
        this.total = total;
        this.wl = wl;
        this.point = point;
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getWl() {
        return wl;
    }

    public void setWl(String wl) {
        this.wl = wl;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getItemType() {
        return 0;
    }
}
