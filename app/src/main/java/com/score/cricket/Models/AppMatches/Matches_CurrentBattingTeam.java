package com.score.cricket.Models.AppMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Matches_CurrentBattingTeam {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("nameCode")
    @Expose
    private String nameCode;
    @SerializedName("national")
    @Expose
    private Boolean national;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("subTeams")
    @Expose
    private List<Object> subTeams = null;
    @SerializedName("teamColors")
    @Expose
    private Matches_TeamColors__ teamColors;


    public Matches_CurrentBattingTeam() {
    }

    
    public Matches_CurrentBattingTeam(String name, String slug, String shortName, String nameCode, Boolean national, Integer id, List<Object> subTeams, Matches_TeamColors__ teamColors) {
        super();
        this.name = name;
        this.slug = slug;
        this.shortName = shortName;
        this.nameCode = nameCode;
        this.national = national;
        this.id = id;
        this.subTeams = subTeams;
        this.teamColors = teamColors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getNameCode() {
        return nameCode;
    }

    public void setNameCode(String nameCode) {
        this.nameCode = nameCode;
    }

    public Boolean getNational() {
        return national;
    }

    public void setNational(Boolean national) {
        this.national = national;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Object> getSubTeams() {
        return subTeams;
    }

    public void setSubTeams(List<Object> subTeams) {
        this.subTeams = subTeams;
    }

    public Matches_TeamColors__ getTeamColors() {
        return teamColors;
    }

    public void setTeamColors(Matches_TeamColors__ teamColors) {
        this.teamColors = teamColors;
    }

}