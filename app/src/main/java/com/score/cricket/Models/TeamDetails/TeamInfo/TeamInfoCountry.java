package com.score.cricket.Models.TeamDetails.TeamInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamInfoCountry {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("flag")
    @Expose
    private String flag;

    
    public TeamInfoCountry() {
    }

    /**
     *
     * @param flag
     * @param name
     */
    public TeamInfoCountry(String name, String flag) {
        super();
        this.name = name;
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

}