package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayerTeamColors {

    @SerializedName("primary")
    @Expose
    private String primary;
    @SerializedName("secondary")
    @Expose
    private String secondary;
    @SerializedName("text")
    @Expose
    private String text;


    public PlayerTeamColors() {
    }

    
    public PlayerTeamColors(String primary, String secondary, String text) {
        super();
        this.primary = primary;
        this.secondary = secondary;
        this.text = text;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getSecondary() {
        return secondary;
    }

    public void setSecondary(String secondary) {
        this.secondary = secondary;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}