package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TeamCategory {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mcc")
    @Expose
    private List<Integer> mcc = null;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("flag")
    @Expose
    private String flag;


    public TeamCategory() {
    }

    
    public TeamCategory(String name, List<Integer> mcc, Integer id, String flag) {
        super();
        this.name = name;
        this.mcc = mcc;
        this.id = id;
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getMcc() {
        return mcc;
    }

    public void setMcc(List<Integer> mcc) {
        this.mcc = mcc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

}