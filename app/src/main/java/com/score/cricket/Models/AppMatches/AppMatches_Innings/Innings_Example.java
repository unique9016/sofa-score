package com.score.cricket.Models.AppMatches.AppMatches_Innings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Innings_Example {

    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("battingTeam")
    @Expose
    private Innings_BattingTeam battingTeam;
    @SerializedName("bowlingTeam")
    @Expose
    private Innings_BowlingTeam bowlingTeam;
    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("wickets")
    @Expose
    private Integer wickets;
    @SerializedName("overs")
    @Expose
    private Double overs;
    @SerializedName("extra")
    @Expose
    private Integer extra;
    @SerializedName("wide")
    @Expose
    private Integer wide;
    @SerializedName("noBall")
    @Expose
    private Integer noBall;
    @SerializedName("bye")
    @Expose
    private Integer bye;
    @SerializedName("legBye")
    @Expose
    private Integer legBye;
    @SerializedName("penalty")
    @Expose
    private Integer penalty;
    @SerializedName("isInningDeclare")
    @Expose
    private Integer isInningDeclare;
    @SerializedName("currentBatsmanId")
    @Expose
    private Integer currentBatsmanId;
    @SerializedName("currentBowlerId")
    @Expose
    private Integer currentBowlerId;
    @SerializedName("battingLine")
    @Expose
    private List<Innings_BattingLine> battingLine = null;
    @SerializedName("bowlingLine")
    @Expose
    private List<Innings_BowlingLine> bowlingLine = null;
    @SerializedName("partnerships")
    @Expose
    private List<Innings_Partnership> partnerships = null;


    public Innings_Example() {
    }

    /**
     *
     * @param battingTeam
     * @param wide
     * @param penalty
     * @param currentBowlerId
     * @param legBye
     * @param battingLine
     * @param overs
     * @param bye
     * @param noBall
     * @param number
     * @param score
     * @param bowlingLine
     * @param bowlingTeam
     * @param partnerships
     * @param extra
     * @param isInningDeclare
     * @param wickets
     * @param currentBatsmanId
     */
    public Innings_Example(Integer number, Innings_BattingTeam battingTeam, Innings_BowlingTeam bowlingTeam, Integer score, Integer wickets, Double overs, Integer extra, Integer wide, Integer noBall, Integer bye, Integer legBye, Integer penalty, Integer isInningDeclare, Integer currentBatsmanId, Integer currentBowlerId, List<Innings_BattingLine> battingLine, List<Innings_BowlingLine> bowlingLine, List<Innings_Partnership> partnerships) {
        super();
        this.number = number;
        this.battingTeam = battingTeam;
        this.bowlingTeam = bowlingTeam;
        this.score = score;
        this.wickets = wickets;
        this.overs = overs;
        this.extra = extra;
        this.wide = wide;
        this.noBall = noBall;
        this.bye = bye;
        this.legBye = legBye;
        this.penalty = penalty;
        this.isInningDeclare = isInningDeclare;
        this.currentBatsmanId = currentBatsmanId;
        this.currentBowlerId = currentBowlerId;
        this.battingLine = battingLine;
        this.bowlingLine = bowlingLine;
        this.partnerships = partnerships;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Innings_BattingTeam getBattingTeam() {
        return battingTeam;
    }

    public void setBattingTeam(Innings_BattingTeam battingTeam) {
        this.battingTeam = battingTeam;
    }

    public Innings_BowlingTeam getBowlingTeam() {
        return bowlingTeam;
    }

    public void setBowlingTeam(Innings_BowlingTeam bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getWickets() {
        return wickets;
    }

    public void setWickets(Integer wickets) {
        this.wickets = wickets;
    }

    public Double getOvers() {
        return overs;
    }

    public void setOvers(Double overs) {
        this.overs = overs;
    }

    public Integer getExtra() {
        return extra;
    }

    public void setExtra(Integer extra) {
        this.extra = extra;
    }

    public Integer getWide() {
        return wide;
    }

    public void setWide(Integer wide) {
        this.wide = wide;
    }

    public Integer getNoBall() {
        return noBall;
    }

    public void setNoBall(Integer noBall) {
        this.noBall = noBall;
    }

    public Integer getBye() {
        return bye;
    }

    public void setBye(Integer bye) {
        this.bye = bye;
    }

    public Integer getLegBye() {
        return legBye;
    }

    public void setLegBye(Integer legBye) {
        this.legBye = legBye;
    }

    public Integer getPenalty() {
        return penalty;
    }

    public void setPenalty(Integer penalty) {
        this.penalty = penalty;
    }

    public Integer getIsInningDeclare() {
        return isInningDeclare;
    }

    public void setIsInningDeclare(Integer isInningDeclare) {
        this.isInningDeclare = isInningDeclare;
    }

    public Integer getCurrentBatsmanId() {
        return currentBatsmanId;
    }

    public void setCurrentBatsmanId(Integer currentBatsmanId) {
        this.currentBatsmanId = currentBatsmanId;
    }

    public Integer getCurrentBowlerId() {
        return currentBowlerId;
    }

    public void setCurrentBowlerId(Integer currentBowlerId) {
        this.currentBowlerId = currentBowlerId;
    }

    public List<Innings_BattingLine> getBattingLine() {
        return battingLine;
    }

    public void setBattingLine(List<Innings_BattingLine> battingLine) {
        this.battingLine = battingLine;
    }

    public List<Innings_BowlingLine> getBowlingLine() {
        return bowlingLine;
    }

    public void setBowlingLine(List<Innings_BowlingLine> bowlingLine) {
        this.bowlingLine = bowlingLine;
    }

    public List<Innings_Partnership> getPartnerships() {
        return partnerships;
    }

    public void setPartnerships(List<Innings_Partnership> partnerships) {
        this.partnerships = partnerships;
    }

}