package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamTournament_ {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("uniqueId")
    @Expose
    private Integer uniqueId;
    @SerializedName("uniqueName")
    @Expose
    private String uniqueName;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;
    @SerializedName("hasRounds")
    @Expose
    private Boolean hasRounds;

    
    public TeamTournament_() {
    }

    
    public TeamTournament_(String name, Integer id, Integer uniqueId, String uniqueName, Boolean hasEventPlayerStatistics, Boolean hasRounds) {
        super();
        this.name = name;
        this.id = id;
        this.uniqueId = uniqueId;
        this.uniqueName = uniqueName;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
        this.hasRounds = hasRounds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public Boolean getHasRounds() {
        return hasRounds;
    }

    public void setHasRounds(Boolean hasRounds) {
        this.hasRounds = hasRounds;
    }

}