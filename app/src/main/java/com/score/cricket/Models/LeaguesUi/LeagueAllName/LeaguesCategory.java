package com.score.cricket.Models.LeaguesUi.LeagueAllName;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

public class LeaguesCategory {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("flag")
    @Expose
    private String flag;

    public LeaguesCategory() {
    }

    public LeaguesCategory(String name, String slug, AppSport sport, Integer id, String flag) {
        super();
        this.name = name;
        this.slug = slug;
        this.sport = sport;
        this.id = id;
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

}
