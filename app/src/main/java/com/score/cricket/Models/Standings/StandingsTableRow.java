package com.score.cricket.Models.Standings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StandingsTableRow {

    @SerializedName("team")
    @Expose
    private StandingsTeam team;
    @SerializedName("isLive")
    @Expose
    private Boolean isLive;
    @SerializedName("descriptions")
    @Expose
    private List<Object> descriptions = null;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("totalFields")
    @Expose
    private StandingsTotalFields totalFields;
    @SerializedName("totalForm")
    @Expose
    private List<String> totalForm = null;
    @SerializedName("homeForm")
    @Expose
    private List<String> homeForm = null;
    @SerializedName("awayForm")
    @Expose
    private List<String> awayForm = null;
    @SerializedName("id")
    @Expose
    private Integer id;

    
    public StandingsTableRow() {
    }

    /**
     *
     * @param isLive
     * @param totalFields
     * @param totalForm
     * @param awayForm
     * @param team
     * @param position
     * @param id
     * @param descriptions
     * @param homeForm
     * @param points
     */
    public StandingsTableRow(StandingsTeam team, Boolean isLive, List<Object> descriptions, String position, String points, StandingsTotalFields totalFields, List<String> totalForm, List<String> homeForm, List<String> awayForm, Integer id) {
        super();
        this.team = team;
        this.isLive = isLive;
        this.descriptions = descriptions;
        this.position = position;
        this.points = points;
        this.totalFields = totalFields;
        this.totalForm = totalForm;
        this.homeForm = homeForm;
        this.awayForm = awayForm;
        this.id = id;
    }

    public StandingsTeam getTeam() {
        return team;
    }

    public void setTeam(StandingsTeam team) {
        this.team = team;
    }

    public Boolean getIsLive() {
        return isLive;
    }

    public void setIsLive(Boolean isLive) {
        this.isLive = isLive;
    }

    public List<Object> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<Object> descriptions) {
        this.descriptions = descriptions;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public StandingsTotalFields getTotalFields() {
        return totalFields;
    }

    public void setTotalFields(StandingsTotalFields totalFields) {
        this.totalFields = totalFields;
    }

    public List<String> getTotalForm() {
        return totalForm;
    }

    public void setTotalForm(List<String> totalForm) {
        this.totalForm = totalForm;
    }

    public List<String> getHomeForm() {
        return homeForm;
    }

    public void setHomeForm(List<String> homeForm) {
        this.homeForm = homeForm;
    }

    public List<String> getAwayForm() {
        return awayForm;
    }

    public void setAwayForm(List<String> awayForm) {
        this.awayForm = awayForm;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}