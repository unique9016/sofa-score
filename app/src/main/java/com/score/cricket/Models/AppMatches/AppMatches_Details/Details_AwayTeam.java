
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_AwayTeam {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("nameCode")
    @Expose
    private String nameCode;
    @SerializedName("national")
    @Expose
    private Boolean national;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("subTeams")
    @Expose
    private List<Object> subTeams = null;
    @SerializedName("teamColors")
    @Expose
    private Details_TeamColors teamColors;

    
    public Details_AwayTeam() {
    }

    /**
     * 
     * @param gender
     * @param nameCode
     * @param name
     * @param national
     * @param id
     * @param shortName
     * @param subTeams
     * @param slug
     * @param teamColors
     */
    public Details_AwayTeam(String name, String slug, String shortName, String gender, String nameCode, Boolean national, Integer id, List<Object> subTeams, Details_TeamColors teamColors) {
        super();
        this.name = name;
        this.slug = slug;
        this.shortName = shortName;
        this.gender = gender;
        this.nameCode = nameCode;
        this.national = national;
        this.id = id;
        this.subTeams = subTeams;
        this.teamColors = teamColors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNameCode() {
        return nameCode;
    }

    public void setNameCode(String nameCode) {
        this.nameCode = nameCode;
    }

    public Boolean getNational() {
        return national;
    }

    public void setNational(Boolean national) {
        this.national = national;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Object> getSubTeams() {
        return subTeams;
    }

    public void setSubTeams(List<Object> subTeams) {
        this.subTeams = subTeams;
    }

    public Details_TeamColors getTeamColors() {
        return teamColors;
    }

    public void setTeamColors(Details_TeamColors teamColors) {
        this.teamColors = teamColors;
    }

}
