package com.score.cricket.Models.AppMatches.AppMatches_Matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MatchesApp_AwayTeam {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("subTeams")
    @Expose
    private List<Object> subTeams = null;

    
    public MatchesApp_AwayTeam() {
    }

    /**
     *
     * @param name
     * @param id
     * @param subTeams
     */
    public MatchesApp_AwayTeam(String name, Integer id, List<Object> subTeams) {
        super();
        this.name = name;
        this.id = id;
        this.subTeams = subTeams;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Object> getSubTeams() {
        return subTeams;
    }

    public void setSubTeams(List<Object> subTeams) {
        this.subTeams = subTeams;
    }

}