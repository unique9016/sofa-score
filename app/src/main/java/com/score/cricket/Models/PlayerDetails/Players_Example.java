package com.score.cricket.Models.PlayerDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Players_Example {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("team")
    @Expose
    private Players_Team team;
    @SerializedName("shirtNumber")
    @Expose
    private Integer shirtNumber;
    @SerializedName("statistics")
    @Expose
    private List<Object> statistics = null;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("marketValueCurrency")
    @Expose
    private String marketValueCurrency;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("hasImage")
    @Expose
    private Boolean hasImage;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("dateOfBirthFormated")
    @Expose
    private String dateOfBirthFormated;
    @SerializedName("dateOfBirthTimestamp")
    @Expose
    private Integer dateOfBirthTimestamp;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("nationalityIOC")
    @Expose
    private String nationalityIOC;
    @SerializedName("playerInfo")
    @Expose
    private Players_PlayerInfo playerInfo;

    
    public Players_Example() {
    }

    /**
     *
     * @param marketValueCurrency
     * @param flag
     * @param shirtNumber
     * @param playerInfo
     * @param hasImage
     * @param dateOfBirthFormated
     * @param team
     * @param dateOfBirthTimestamp
     * @param userCount
     * @param nationality
     * @param name
     * @param nationalityIOC
     * @param id
     * @param shortName
     * @param slug
     * @param age
     * @param statistics
     */
    public Players_Example(String name, String slug, String shortName, Players_Team team, Integer shirtNumber, List<Object> statistics, Integer userCount, Integer id, String marketValueCurrency, String nationality, Boolean hasImage, Integer age, String dateOfBirthFormated, Integer dateOfBirthTimestamp, String flag, String nationalityIOC, Players_PlayerInfo playerInfo) {
        super();
        this.name = name;
        this.slug = slug;
        this.shortName = shortName;
        this.team = team;
        this.shirtNumber = shirtNumber;
        this.statistics = statistics;
        this.userCount = userCount;
        this.id = id;
        this.marketValueCurrency = marketValueCurrency;
        this.nationality = nationality;
        this.hasImage = hasImage;
        this.age = age;
        this.dateOfBirthFormated = dateOfBirthFormated;
        this.dateOfBirthTimestamp = dateOfBirthTimestamp;
        this.flag = flag;
        this.nationalityIOC = nationalityIOC;
        this.playerInfo = playerInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Players_Team getTeam() {
        return team;
    }

    public void setTeam(Players_Team team) {
        this.team = team;
    }

    public Integer getShirtNumber() {
        return shirtNumber;
    }

    public void setShirtNumber(Integer shirtNumber) {
        this.shirtNumber = shirtNumber;
    }

    public List<Object> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<Object> statistics) {
        this.statistics = statistics;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMarketValueCurrency() {
        return marketValueCurrency;
    }

    public void setMarketValueCurrency(String marketValueCurrency) {
        this.marketValueCurrency = marketValueCurrency;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Boolean getHasImage() {
        return hasImage;
    }

    public void setHasImage(Boolean hasImage) {
        this.hasImage = hasImage;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getDateOfBirthFormated() {
        return dateOfBirthFormated;
    }

    public void setDateOfBirthFormated(String dateOfBirthFormated) {
        this.dateOfBirthFormated = dateOfBirthFormated;
    }

    public Integer getDateOfBirthTimestamp() {
        return dateOfBirthTimestamp;
    }

    public void setDateOfBirthTimestamp(Integer dateOfBirthTimestamp) {
        this.dateOfBirthTimestamp = dateOfBirthTimestamp;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getNationalityIOC() {
        return nationalityIOC;
    }

    public void setNationalityIOC(String nationalityIOC) {
        this.nationalityIOC = nationalityIOC;
    }

    public Players_PlayerInfo getPlayerInfo() {
        return playerInfo;
    }

    public void setPlayerInfo(Players_PlayerInfo playerInfo) {
        this.playerInfo = playerInfo;
    }

}