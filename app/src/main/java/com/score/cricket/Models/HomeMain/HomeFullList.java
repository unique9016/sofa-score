package com.score.cricket.Models.HomeMain;

import com.score.cricket.Listener.ListnerItems;

public class HomeFullList implements ListnerItems {
    String index,name,total,win,draw,loss,points,netrunrate,noresult;
    int id;

    public HomeFullList() {
    }

    public HomeFullList(String index, String name, String total, String win, String draw, String loss, String points, String netrunrate, String noresult, int id) {
        this.index = index;
        this.name = name;
        this.total = total;
        this.win = win;
        this.draw = draw;
        this.loss = loss;
        this.points = points;
        this.netrunrate = netrunrate;
        this.noresult = noresult;
        this.id = id;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getWin() {
        return win;
    }

    public void setWin(String win) {
        this.win = win;
    }

    public String getDraw() {
        return draw;
    }

    public void setDraw(String draw) {
        this.draw = draw;
    }

    public String getLoss() {
        return loss;
    }

    public void setLoss(String loss) {
        this.loss = loss;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getNetrunrate() {
        return netrunrate;
    }

    public void setNetrunrate(String netrunrate) {
        this.netrunrate = netrunrate;
    }

    public String getNoresult() {
        return noresult;
    }

    public void setNoresult(String noresult) {
        this.noresult = noresult;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getItemType() {
        return 0;
    }
}
