package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TeamTournament {

    @SerializedName("tournament")
    @Expose
    private TeamTournament_ tournament;
    @SerializedName("category")
    @Expose
    private TeamCategory category;
    @SerializedName("season")
    @Expose
    private TeamSeason season;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;
    @SerializedName("hasEventPlayerHeatMap")
    @Expose
    private Object hasEventPlayerHeatMap;
    @SerializedName("hasBoxScore")
    @Expose
    private Object hasBoxScore;
    @SerializedName("events")
    @Expose
    private List<TeamEvent> events = null;


    public TeamTournament() {
    }

    
    public TeamTournament(TeamTournament_ tournament, TeamCategory category, TeamSeason season, Boolean hasEventPlayerStatistics, Object hasEventPlayerHeatMap, Object hasBoxScore, List<TeamEvent> events) {
        super();
        this.tournament = tournament;
        this.category = category;
        this.season = season;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
        this.hasBoxScore = hasBoxScore;
        this.events = events;
    }

    public TeamTournament_ getTournament() {
        return tournament;
    }

    public void setTournament(TeamTournament_ tournament) {
        this.tournament = tournament;
    }

    public TeamCategory getCategory() {
        return category;
    }

    public void setCategory(TeamCategory category) {
        this.category = category;
    }

    public TeamSeason getSeason() {
        return season;
    }

    public void setSeason(TeamSeason season) {
        this.season = season;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public Object getHasEventPlayerHeatMap() {
        return hasEventPlayerHeatMap;
    }

    public void setHasEventPlayerHeatMap(Object hasEventPlayerHeatMap) {
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
    }

    public Object getHasBoxScore() {
        return hasBoxScore;
    }

    public void setHasBoxScore(Object hasBoxScore) {
        this.hasBoxScore = hasBoxScore;
    }

    public List<TeamEvent> getEvents() {
        return events;
    }

    public void setEvents(List<TeamEvent> events) {
        this.events = events;
    }

}