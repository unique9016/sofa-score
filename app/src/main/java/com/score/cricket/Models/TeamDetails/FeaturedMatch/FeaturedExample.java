package com.score.cricket.Models.TeamDetails.FeaturedMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

import java.util.List;

public class FeaturedExample {

    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("rows")
    @Expose
    private Integer rows;
    @SerializedName("tournaments")
    @Expose
    private List<FeaturedTournament> tournaments = null;

    
    public FeaturedExample() {
    }

    /**
     *
     * @param tournaments
     * @param rows
     * @param sport
     */
    public FeaturedExample(AppSport sport, Integer rows, List<FeaturedTournament> tournaments) {
        super();
        this.sport = sport;
        this.rows = rows;
        this.tournaments = tournaments;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public List<FeaturedTournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<FeaturedTournament> tournaments) {
        this.tournaments = tournaments;
    }

}