
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_Venue {

    @SerializedName("city")
    @Expose
    private Details_City city;
    @SerializedName("stadium")
    @Expose
    private Details_Stadium stadium;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("hasImage")
    @Expose
    private Boolean hasImage;
    @SerializedName("country")
    @Expose
    private Details_Country country;

    
    public Details_Venue() {
    }

    /**
     * 
     * @param country
     * @param city
     * @param stadium
     * @param hasImage
     * @param id
     */
    public Details_Venue(Details_City city, Details_Stadium stadium, Integer id, Boolean hasImage, Details_Country country) {
        super();
        this.city = city;
        this.stadium = stadium;
        this.id = id;
        this.hasImage = hasImage;
        this.country = country;
    }

    public Details_City getCity() {
        return city;
    }

    public void setCity(Details_City city) {
        this.city = city;
    }

    public Details_Stadium getStadium() {
        return stadium;
    }

    public void setStadium(Details_Stadium stadium) {
        this.stadium = stadium;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getHasImage() {
        return hasImage;
    }

    public void setHasImage(Boolean hasImage) {
        this.hasImage = hasImage;
    }

    public Details_Country getCountry() {
        return country;
    }

    public void setCountry(Details_Country country) {
        this.country = country;
    }

}
