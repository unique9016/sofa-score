
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics8 {

    @SerializedName("bigChancesMissed")
    @Expose
    private Integer bigChancesMissed;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;

    
    public Top_Statistics8() {
    }

    public Top_Statistics8(Integer bigChancesMissed, Integer id, String type, Integer appearances) {
        super();
        this.bigChancesMissed = bigChancesMissed;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getBigChancesMissed() {
        return bigChancesMissed;
    }

    public void setBigChancesMissed(Integer bigChancesMissed) {
        this.bigChancesMissed = bigChancesMissed;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
