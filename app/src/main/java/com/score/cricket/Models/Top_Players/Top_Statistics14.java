
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics14 {

    @SerializedName("penaltyWon")
    @Expose
    private Integer penaltyWon;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;

    
    public Top_Statistics14() {
    }

    public Top_Statistics14(Integer penaltyWon, Integer id, String type, Integer appearances) {
        super();
        this.penaltyWon = penaltyWon;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getPenaltyWon() {
        return penaltyWon;
    }

    public void setPenaltyWon(Integer penaltyWon) {
        this.penaltyWon = penaltyWon;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
