package com.score.cricket.Models.AppMatches.AppMatches_Matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchesApp_Event {

    @SerializedName("status")
    @Expose
    private MatchesApp_Status status;
    @SerializedName("winnerCode")
    @Expose
    private Integer winnerCode;
    @SerializedName("homeTeam")
    @Expose
    private MatchesApp_HomeTeam homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private MatchesApp_AwayTeam awayTeam;
    @SerializedName("homeScore")
    @Expose
    private MatchesApp_HomeScore homeScore;
    @SerializedName("awayScore")
    @Expose
    private MatchesApp_AwayScore awayScore;
    @SerializedName("changes")
    @Expose
    private MatchesApp_Changes changes;
    @SerializedName("hasHighlights")
    @Expose
    private Boolean hasHighlights;
    @SerializedName("hasHighlightsStream")
    @Expose
    private Boolean hasHighlightsStream;
    @SerializedName("hasGlobalHighlights")
    @Expose
    private Boolean hasGlobalHighlights;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("startTimestamp")
    @Expose
    private Integer startTimestamp;
    @SerializedName("statusDescription")
    @Expose
    private String statusDescription;
    @SerializedName("webUrl")
    @Expose
    private String webUrl;
    @SerializedName("hasTime")
    @Expose
    private Boolean hasTime;
    @SerializedName("resultOnly")
    @Expose
    private Boolean resultOnly;


    public MatchesApp_Event() {
    }

    /**
     *
     * @param note
     * @param resultOnly
     * @param homeScore
     * @param awayTeam
     * @param changes
     * @param hasHighlightsStream
     * @param hasTime
     * @param winnerCode
     * @param hasGlobalHighlights
     * @param statusDescription
     * @param awayScore
     * @param webUrl
     * @param homeTeam
     * @param id
     * @param hasHighlights
     * @param startTimestamp
     * @param status
     */
    public MatchesApp_Event(MatchesApp_Status status, Integer winnerCode, MatchesApp_HomeTeam homeTeam, MatchesApp_AwayTeam awayTeam, MatchesApp_HomeScore homeScore, MatchesApp_AwayScore awayScore, MatchesApp_Changes changes, Boolean hasHighlights, Boolean hasHighlightsStream, Boolean hasGlobalHighlights, Integer id, String note, Integer startTimestamp, String statusDescription, String webUrl, Boolean hasTime, Boolean resultOnly) {
        super();
        this.status = status;
        this.winnerCode = winnerCode;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
        this.changes = changes;
        this.hasHighlights = hasHighlights;
        this.hasHighlightsStream = hasHighlightsStream;
        this.hasGlobalHighlights = hasGlobalHighlights;
        this.id = id;
        this.note = note;
        this.startTimestamp = startTimestamp;
        this.statusDescription = statusDescription;
        this.webUrl = webUrl;
        this.hasTime = hasTime;
        this.resultOnly = resultOnly;
    }

    public MatchesApp_Status getStatus() {
        return status;
    }

    public void setStatus(MatchesApp_Status status) {
        this.status = status;
    }

    public Integer getWinnerCode() {
        return winnerCode;
    }

    public void setWinnerCode(Integer winnerCode) {
        this.winnerCode = winnerCode;
    }

    public MatchesApp_HomeTeam getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(MatchesApp_HomeTeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    public MatchesApp_AwayTeam getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(MatchesApp_AwayTeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    public MatchesApp_HomeScore getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(MatchesApp_HomeScore homeScore) {
        this.homeScore = homeScore;
    }

    public MatchesApp_AwayScore getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(MatchesApp_AwayScore awayScore) {
        this.awayScore = awayScore;
    }

    public MatchesApp_Changes getChanges() {
        return changes;
    }

    public void setChanges(MatchesApp_Changes changes) {
        this.changes = changes;
    }

    public Boolean getHasHighlights() {
        return hasHighlights;
    }

    public void setHasHighlights(Boolean hasHighlights) {
        this.hasHighlights = hasHighlights;
    }

    public Boolean getHasHighlightsStream() {
        return hasHighlightsStream;
    }

    public void setHasHighlightsStream(Boolean hasHighlightsStream) {
        this.hasHighlightsStream = hasHighlightsStream;
    }

    public Boolean getHasGlobalHighlights() {
        return hasGlobalHighlights;
    }

    public void setHasGlobalHighlights(Boolean hasGlobalHighlights) {
        this.hasGlobalHighlights = hasGlobalHighlights;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Integer startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Boolean getHasTime() {
        return hasTime;
    }

    public void setHasTime(Boolean hasTime) {
        this.hasTime = hasTime;
    }

    public Boolean getResultOnly() {
        return resultOnly;
    }

    public void setResultOnly(Boolean resultOnly) {
        this.resultOnly = resultOnly;
    }

}