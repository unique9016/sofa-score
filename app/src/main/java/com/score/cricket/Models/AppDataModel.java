package com.score.cricket.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppDataModel {

    @SerializedName("review_popup_count")
    @Expose
    private Integer review_popup_count;

    public Integer getReview_popup_count() {
        if (review_popup_count==null){
            return 2;
        }
        return review_popup_count;
    }

    public void setReview_popup_count(Integer review_popup_count) {
        this.review_popup_count = review_popup_count;
    }


    @SerializedName("direct_review_enable")
    @Expose
    private Integer direct_review_enable;

    public Integer getDirect_review_enable() {
        if (direct_review_enable==null){
            return 1;
        }
        return direct_review_enable;
    }

    public void setDirect_review_enable(Integer direct_review_enable) {
        this.direct_review_enable = direct_review_enable;
    }

    @SerializedName("showFavorite")
    @Expose
    private Boolean showFavorite;

    public Boolean getShowFavorite() {
        if (showFavorite==null){
            return true;
        }
        return showFavorite;
    }

    public void setShowFavorite(Boolean showFavorite) {
        this.showFavorite = showFavorite;
    }

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("banner_id")
    @Expose
    private String bannerid;
    @SerializedName("show_banner")
    @Expose
    private Integer banner;
    @SerializedName("native_id")
    @Expose
    private String nativeid;
    @SerializedName("show_native")
    @Expose
    private Integer _native;
    @SerializedName("interstitial_id")
    @Expose
    private String interstitialid;
    @SerializedName("show_interstitial")
    @Expose
    private Integer interstitial;
    @SerializedName("open_app_id")
    @Expose
    private String openadid;
    @SerializedName("show_open_app")
    @Expose
    private Integer openad;
    @SerializedName("rewardadid")
    @Expose
    private String rewardadid;
    @SerializedName("rewardad")
    @Expose
    private Integer rewardad;
    @SerializedName("adtype")
    @Expose
    private String adtype;
    @SerializedName("is_show_ads")
    @Expose
    private Integer isAdEnable;
    @SerializedName("in_appreview")
    @Expose
    private Integer inAppreview;
    @SerializedName("review")
    @Expose
    private Integer review;
    @SerializedName("isactive")
    @Expose
    private Integer isactive;
    @SerializedName("interstitial_count")
    @Expose
    private Integer ads_per_click;
    @SerializedName("show_interstitial_exit")
    @Expose
    private Integer exit_ad_enable;
    @SerializedName("ads_per_seassion")
    @Expose
    private Integer ads_per_session;
    @SerializedName("relocation")
    @Expose
    private String transfer_link;

    @SerializedName("app_open_count")
    @Expose
    private Integer app_open_count;
    @SerializedName("show_open_app_splash")
    @Expose
    private Integer is_splash_on;
    @SerializedName("splash_time")
    @Expose
    private Integer splash_time;

    public Integer getApp_open_count() {
        if (app_open_count==null){
            return 3;
        }
        return app_open_count;
    }

    public void setApp_open_count(Integer app_open_count) {
        this.app_open_count = app_open_count;
    }

    public Integer getIs_splash_on() {
        if (is_splash_on==null){
            return 1;
        }
        return is_splash_on;
    }

    public Integer getSplash_time() {
        if (splash_time==null){
            return 10;
        }
        return splash_time;
    }

    public void setSplash_time(Integer splash_time) {
        this.splash_time = splash_time;
    }

    public void setIs_splash_on(Integer is_splash_on) {
        this.is_splash_on = is_splash_on;
    }

    public String getTransfer_link() {
        return transfer_link;
    }

    public void setTransfer_link(String transfer_link) {
        this.transfer_link = transfer_link;
    }

    public Integer get_native() {
        return _native;
    }

    public void set_native(Integer _native) {
        this._native = _native;
    }

    public Integer getExit_ad_enable() {
        return exit_ad_enable;
    }

    public void setExit_ad_enable(Integer exit_ad_enable) {
        this.exit_ad_enable = exit_ad_enable;
    }

    public Integer getAds_per_session() {
        return ads_per_session;
    }

    public void setAds_per_session(Integer ads_per_session) {
        this.ads_per_session = ads_per_session;
    }

    public Integer getAds_per_click() {
        return ads_per_click;
    }

    public void setAds_per_click(Integer ads_per_click) {
        this.ads_per_click = ads_per_click;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBannerid() {
        return bannerid;
    }

    public void setBannerid(String bannerid) {
        this.bannerid = bannerid;
    }

    public Integer getBanner() {
        return banner;
    }

    public void setBanner(Integer banner) {
        this.banner = banner;
    }

    public String getNativeid() {
        return nativeid;
    }

    public void setNativeid(String nativeid) {
        this.nativeid = nativeid;
    }

    public Integer getNative() {
        return _native;
    }

    public void setNative(Integer _native) {
        this._native = _native;
    }

    public String getInterstitialid() {
        return interstitialid;
    }

    public void setInterstitialid(String interstitialid) {
        this.interstitialid = interstitialid;
    }

    public Integer getInterstitial() {
        return interstitial;
    }

    public void setInterstitial(Integer interstitial) {
        this.interstitial = interstitial;
    }

    public String getOpenadid() {
        return openadid;
    }

    public void setOpenadid(String openadid) {
        this.openadid = openadid;
    }

    public Integer getOpenad() {
        return openad;
    }

    public void setOpenad(Integer openad) {
        this.openad = openad;
    }

    public String getRewardadid() {
        return rewardadid;
    }

    public void setRewardadid(String rewardadid) {
        this.rewardadid = rewardadid;
    }

    public Integer getRewardad() {
        return rewardad;
    }

    public void setRewardad(Integer rewardad) {
        this.rewardad = rewardad;
    }

    public String getAdtype() {
        return adtype;
    }

    public void setAdtype(String adtype) {
        this.adtype = adtype;
    }

    public Integer getIsAdEnable() {
        return isAdEnable;
    }

    public void setIsAdEnable(Integer isAdEnable) {
        this.isAdEnable = isAdEnable;
    }

    public Integer getInAppreview() {
        return inAppreview;
    }

    public void setInAppreview(Integer inAppreview) {
        this.inAppreview = inAppreview;
    }

    public Integer getReview() {
        return review;
    }

    public void setReview(Integer review) {
        this.review = review;
    }

    public Integer getIsactive() {
        return isactive;
    }

    public void setIsactive(Integer isactive) {
        this.isactive = isactive;
    }

    @SerializedName("LogoLink")
    @Expose
    private String LogoLink;
    @SerializedName("LogoLink_2")
    @Expose
    private String LogoLink_2;
    @SerializedName("LeaguesLink")
    @Expose
    private String LeaguesLink;
    @SerializedName("getPlayer")
    @Expose
    private String getPlayer;
    @SerializedName("getMatch")
    @Expose
    private String getMatch;
    @SerializedName("getLeagueName")
    @Expose
    private String getLeagueName;

    public String getLogoLink() {
        return LogoLink;
    }

    public void setLogoLink(String logoLink) {
        LogoLink = logoLink;
    }

    public String getLogoLink_2() {
        return LogoLink_2;
    }

    public void setLogoLink_2(String logoLink_2) {
        LogoLink_2 = logoLink_2;
    }

    public String getLeaguesLink() {
        return LeaguesLink;
    }

    public void setLeaguesLink(String leaguesLink) {
        LeaguesLink = leaguesLink;
    }

    public String getGetPlayer() {
        return getPlayer;
    }

    public void setGetPlayer(String getPlayer) {
        this.getPlayer = getPlayer;
    }

    public String getGetMatch() {
        return getMatch;
    }

    public void setGetMatch(String getMatch) {
        this.getMatch = getMatch;
    }

    public String getGetLeagueName() {
        return getLeagueName;
    }

    public void setGetLeagueName(String getLeagueName) {
        this.getLeagueName = getLeagueName;
    }
}
