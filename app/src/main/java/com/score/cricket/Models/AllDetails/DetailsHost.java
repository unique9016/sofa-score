package com.score.cricket.Models.AllDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailsHost {

    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("flag")
    @Expose
    private String flag;


    public DetailsHost() {
    }

    public DetailsHost(String country, String flag) {
        super();
        this.country = country;
        this.flag = flag;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

}