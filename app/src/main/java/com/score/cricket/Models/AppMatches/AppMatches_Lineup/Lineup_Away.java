package com.score.cricket.Models.AppMatches.AppMatches_Lineup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Lineup_Away {

    @SerializedName("players")
    @Expose
    private List<Lineup_Player__> players = null;
    @SerializedName("playerColor")
    @Expose
    private Lineup_PlayerColor playerColor;
    @SerializedName("goalkeeperColor")
    @Expose
    private Lineup_GoalkeeperColor goalkeeperColor;


    public Lineup_Away() {
    }

    public Lineup_Away(List<Lineup_Player__> players, Lineup_PlayerColor playerColor, Lineup_GoalkeeperColor goalkeeperColor) {
        super();
        this.players = players;
        this.playerColor = playerColor;
        this.goalkeeperColor = goalkeeperColor;
    }

    public List<Lineup_Player__> getPlayers() {
        return players;
    }

    public void setPlayers(List<Lineup_Player__> players) {
        this.players = players;
    }

    public Lineup_PlayerColor getPlayerColor() {
        return playerColor;
    }

    public void setPlayerColor(Lineup_PlayerColor playerColor) {
        this.playerColor = playerColor;
    }

    public Lineup_GoalkeeperColor getGoalkeeperColor() {
        return goalkeeperColor;
    }

    public void setGoalkeeperColor(Lineup_GoalkeeperColor goalkeeperColor) {
        this.goalkeeperColor = goalkeeperColor;
    }

}