package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlayerAwayTeam {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;
    @SerializedName("nameCode")
    @Expose
    private String nameCode;
    @SerializedName("national")
    @Expose
    private Boolean national;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("subTeams")
    @Expose
    private List<Object> subTeams = null;
    @SerializedName("teamColors")
    @Expose
    private PlayerTeamColors teamColors;
    @SerializedName("gender")
    @Expose
    private String gender;


    public PlayerAwayTeam() {
    }

    public PlayerAwayTeam(String name, String slug, String shortName, Integer userCount, String nameCode, Boolean national, Integer type, Integer id, List<Object> subTeams, PlayerTeamColors teamColors, String gender) {
        super();
        this.name = name;
        this.slug = slug;
        this.shortName = shortName;
        this.userCount = userCount;
        this.nameCode = nameCode;
        this.national = national;
        this.type = type;
        this.id = id;
        this.subTeams = subTeams;
        this.teamColors = teamColors;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public String getNameCode() {
        return nameCode;
    }

    public void setNameCode(String nameCode) {
        this.nameCode = nameCode;
    }

    public Boolean getNational() {
        return national;
    }

    public void setNational(Boolean national) {
        this.national = national;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Object> getSubTeams() {
        return subTeams;
    }

    public void setSubTeams(List<Object> subTeams) {
        this.subTeams = subTeams;
    }

    public PlayerTeamColors getTeamColors() {
        return teamColors;
    }

    public void setTeamColors(PlayerTeamColors teamColors) {
        this.teamColors = teamColors;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}