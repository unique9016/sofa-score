
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_HomeTeam_ {

    @SerializedName("avgRating")
    @Expose
    private Object avgRating;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("form")
    @Expose
    private List<String> form = null;

    
    public Details_HomeTeam_() {
    }

    
    public Details_HomeTeam_(Object avgRating, String position, String points, List<String> form) {
        super();
        this.avgRating = avgRating;
        this.position = position;
        this.points = points;
        this.form = form;
    }

    public Object getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(Object avgRating) {
        this.avgRating = avgRating;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public List<String> getForm() {
        return form;
    }

    public void setForm(List<String> form) {
        this.form = form;
    }

}
