package com.score.cricket.Models.AppTournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TournamentsUniqueTournament {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("category")
    @Expose
    private TournamentsCategory category;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;
    @SerializedName("id")
    @Expose
    private Integer id;

    
    public TournamentsUniqueTournament() {
    }

    /**
     *
     * @param userCount
     * @param name
     * @param id
     * @param category
     * @param slug
     */
    public TournamentsUniqueTournament(String name, String slug, TournamentsCategory category, Integer userCount, Integer id) {
        super();
        this.name = name;
        this.slug = slug;
        this.category = category;
        this.userCount = userCount;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public TournamentsCategory getCategory() {
        return category;
    }

    public void setCategory(TournamentsCategory category) {
        this.category = category;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}