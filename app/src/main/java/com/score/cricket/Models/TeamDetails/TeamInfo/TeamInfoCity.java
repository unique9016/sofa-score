package com.score.cricket.Models.TeamDetails.TeamInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamInfoCity {

    @SerializedName("name")
    @Expose
    private String name;


    public TeamInfoCity() {
    }

    /**
     *
     * @param name
     */
    public TeamInfoCity(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}