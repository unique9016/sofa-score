package com.score.cricket.Models.AllDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

import java.util.List;

public class DetailsFeaturedMatches {

    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("rows")
    @Expose
    private Integer rows;
    @SerializedName("tournaments")
    @Expose
    private List<DetailsTournament> tournaments = null;


    public DetailsFeaturedMatches() {
    }

    public DetailsFeaturedMatches(AppSport sport, Integer rows, List<DetailsTournament> tournaments) {
        super();
        this.sport = sport;
        this.rows = rows;
        this.tournaments = tournaments;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public List<DetailsTournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<DetailsTournament> tournaments) {
        this.tournaments = tournaments;
    }

}