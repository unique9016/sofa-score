
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_Changes {

    @SerializedName("changes")
    @Expose
    private List<String> changes = null;
    @SerializedName("changeTimestamp")
    @Expose
    private Integer changeTimestamp;


    public Details_Changes() {
    }

    /**
     * 
     * @param changeTimestamp
     * @param changes
     */
    public Details_Changes(List<String> changes, Integer changeTimestamp) {
        super();
        this.changes = changes;
        this.changeTimestamp = changeTimestamp;
    }

    public List<String> getChanges() {
        return changes;
    }

    public void setChanges(List<String> changes) {
        this.changes = changes;
    }

    public Integer getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Integer changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

}
