package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayerInnings {

    @SerializedName("inning1")
    @Expose
    private PlayerInning1 inning1;
    @SerializedName("inning2")
    @Expose
    private PlayerInning2 inning2;


    public PlayerInnings() {
    }

    
    public PlayerInnings(PlayerInning1 inning1, PlayerInning2 inning2) {
        super();
        this.inning1 = inning1;
        this.inning2 = inning2;
    }

    public PlayerInning1 getInning1() {
        return inning1;
    }

    public void setInning1(PlayerInning1 inning1) {
        this.inning1 = inning1;
    }

    public PlayerInning2 getInning2() {
        return inning2;
    }

    public void setInning2(PlayerInning2 inning2) {
        this.inning2 = inning2;
    }

}