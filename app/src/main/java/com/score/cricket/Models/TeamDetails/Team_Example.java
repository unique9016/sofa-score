package com.score.cricket.Models.TeamDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Team_Example {

    @SerializedName("eventId")
    @Expose
    private Integer eventId;
    @SerializedName("points")
    @Expose
    private Integer points;
    @SerializedName("winFlag")
    @Expose
    private String winFlag;
    @SerializedName("opponent")
    @Expose
    private Team_Opponent opponent;

    
    public Team_Example() {
    }

    /**
     *
     * @param eventId
     * @param opponent
     * @param winFlag
     * @param points
     */
    public Team_Example(Integer eventId, Integer points, String winFlag, Team_Opponent opponent) {
        super();
        this.eventId = eventId;
        this.points = points;
        this.winFlag = winFlag;
        this.opponent = opponent;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getWinFlag() {
        return winFlag;
    }

    public void setWinFlag(String winFlag) {
        this.winFlag = winFlag;
    }

    public Team_Opponent getOpponent() {
        return opponent;
    }

    public void setOpponent(Team_Opponent opponent) {
        this.opponent = opponent;
    }

}