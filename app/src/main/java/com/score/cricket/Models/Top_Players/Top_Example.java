
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Example {

    @SerializedName("topPlayers")
    @Expose
    private TopPlayers topPlayers;

    
    public Top_Example() {
    }
    
    public Top_Example(TopPlayers topPlayers) {
        super();
        this.topPlayers = topPlayers;
    }

    public TopPlayers getTopPlayers() {
        return topPlayers;
    }

    public void setTopPlayers(TopPlayers topPlayers) {
        this.topPlayers = topPlayers;
    }

}
