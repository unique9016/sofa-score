
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics12 {

    @SerializedName("accurateLongBalls")
    @Expose
    private Integer accurateLongBalls;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;

    
    public Top_Statistics12() {
    }

    public Top_Statistics12(Integer accurateLongBalls, Integer id, String type, Integer appearances) {
        super();
        this.accurateLongBalls = accurateLongBalls;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getAccurateLongBalls() {
        return accurateLongBalls;
    }

    public void setAccurateLongBalls(Integer accurateLongBalls) {
        this.accurateLongBalls = accurateLongBalls;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
