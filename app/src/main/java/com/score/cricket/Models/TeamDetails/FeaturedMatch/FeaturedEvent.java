package com.score.cricket.Models.TeamDetails.FeaturedMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeaturedEvent {

    @SerializedName("status")
    @Expose
    private FeaturedStatus status;
    @SerializedName("winnerCode")
    @Expose
    private Integer winnerCode;
    @SerializedName("homeTeam")
    @Expose
    private FeaturedHomeTeam homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private FeaturedAwayTeam awayTeam;
    @SerializedName("homeScore")
    @Expose
    private FeaturedHomeScore homeScore;
    @SerializedName("awayScore")
    @Expose
    private FeaturedAwayScore awayScore;
    @SerializedName("time")
    @Expose
    private FeaturedTime time;
    @SerializedName("changes")
    @Expose
    private FeaturedChanges changes;
    @SerializedName("hasHighlights")
    @Expose
    private Boolean hasHighlights;
    @SerializedName("hasHighlightsStream")
    @Expose
    private Boolean hasHighlightsStream;
    @SerializedName("hasGlobalHighlights")
    @Expose
    private Boolean hasGlobalHighlights;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("startTimestamp")
    @Expose
    private Integer startTimestamp;
    @SerializedName("statusDescription")
    @Expose
    private String statusDescription;
    @SerializedName("webUrl")
    @Expose
    private String webUrl;
    @SerializedName("hasTime")
    @Expose
    private Boolean hasTime;
    @SerializedName("resultOnly")
    @Expose
    private Boolean resultOnly;


    public FeaturedEvent() {
    }

    /**
     *
     * @param note
     * @param resultOnly
     * @param homeScore
     * @param awayTeam
     * @param changes
     * @param hasHighlightsStream
     * @param hasTime
     * @param winnerCode
     * @param hasGlobalHighlights
     * @param statusDescription
     * @param awayScore
     * @param webUrl
     * @param homeTeam
     * @param time
     * @param id
     * @param hasHighlights
     * @param startTimestamp
     * @param status
     */
    public FeaturedEvent(FeaturedStatus status, Integer winnerCode, FeaturedHomeTeam homeTeam, FeaturedAwayTeam awayTeam, FeaturedHomeScore homeScore, FeaturedAwayScore awayScore, FeaturedTime time, FeaturedChanges changes, Boolean hasHighlights, Boolean hasHighlightsStream, Boolean hasGlobalHighlights, Integer id, String note, Integer startTimestamp, String statusDescription, String webUrl, Boolean hasTime, Boolean resultOnly) {
        super();
        this.status = status;
        this.winnerCode = winnerCode;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
        this.time = time;
        this.changes = changes;
        this.hasHighlights = hasHighlights;
        this.hasHighlightsStream = hasHighlightsStream;
        this.hasGlobalHighlights = hasGlobalHighlights;
        this.id = id;
        this.note = note;
        this.startTimestamp = startTimestamp;
        this.statusDescription = statusDescription;
        this.webUrl = webUrl;
        this.hasTime = hasTime;
        this.resultOnly = resultOnly;
    }

    public FeaturedStatus getStatus() {
        return status;
    }

    public void setStatus(FeaturedStatus status) {
        this.status = status;
    }

    public Integer getWinnerCode() {
        return winnerCode;
    }

    public void setWinnerCode(Integer winnerCode) {
        this.winnerCode = winnerCode;
    }

    public FeaturedHomeTeam getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(FeaturedHomeTeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    public FeaturedAwayTeam getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(FeaturedAwayTeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    public FeaturedHomeScore getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(FeaturedHomeScore homeScore) {
        this.homeScore = homeScore;
    }

    public FeaturedAwayScore getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(FeaturedAwayScore awayScore) {
        this.awayScore = awayScore;
    }

    public FeaturedTime getTime() {
        return time;
    }

    public void setTime(FeaturedTime time) {
        this.time = time;
    }

    public FeaturedChanges getChanges() {
        return changes;
    }

    public void setChanges(FeaturedChanges changes) {
        this.changes = changes;
    }

    public Boolean getHasHighlights() {
        return hasHighlights;
    }

    public void setHasHighlights(Boolean hasHighlights) {
        this.hasHighlights = hasHighlights;
    }

    public Boolean getHasHighlightsStream() {
        return hasHighlightsStream;
    }

    public void setHasHighlightsStream(Boolean hasHighlightsStream) {
        this.hasHighlightsStream = hasHighlightsStream;
    }

    public Boolean getHasGlobalHighlights() {
        return hasGlobalHighlights;
    }

    public void setHasGlobalHighlights(Boolean hasGlobalHighlights) {
        this.hasGlobalHighlights = hasGlobalHighlights;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Integer startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Boolean getHasTime() {
        return hasTime;
    }

    public void setHasTime(Boolean hasTime) {
        this.hasTime = hasTime;
    }

    public Boolean getResultOnly() {
        return resultOnly;
    }

    public void setResultOnly(Boolean resultOnly) {
        this.resultOnly = resultOnly;
    }

}