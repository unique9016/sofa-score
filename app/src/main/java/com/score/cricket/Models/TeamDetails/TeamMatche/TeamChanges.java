package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TeamChanges {

    @SerializedName("changes")
    @Expose
    private List<String> changes = null;
    @SerializedName("changeTimestamp")
    @Expose
    private Integer changeTimestamp;


    public TeamChanges() {
    }

    
    public TeamChanges(List<String> changes, Integer changeTimestamp) {
        super();
        this.changes = changes;
        this.changeTimestamp = changeTimestamp;
    }

    public List<String> getChanges() {
        return changes;
    }

    public void setChanges(List<String> changes) {
        this.changes = changes;
    }

    public Integer getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Integer changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

}