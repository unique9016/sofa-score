package com.score.cricket.Models.HomeMain;

import java.util.ArrayList;

public class HomeGroupInfo {

    private String name,flagename;
    private ArrayList<HomeChildInfo> list = new ArrayList<HomeChildInfo>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlagename() {
        return flagename;
    }

    public void setFlagename(String flagename) {
        this.flagename = flagename;
    }

    public ArrayList<HomeChildInfo> getProductList() {
        return list;
    }

    public void setProductList(ArrayList<HomeChildInfo> productList) {
        this.list = productList;
    }

}