package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayerUniqueTournament {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("category")
    @Expose
    private PlayerCategory category;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;

    
    public PlayerUniqueTournament() {
    }

    public PlayerUniqueTournament(String name, String slug, PlayerCategory category, Integer userCount, Integer id, Boolean hasEventPlayerStatistics) {
        super();
        this.name = name;
        this.slug = slug;
        this.category = category;
        this.userCount = userCount;
        this.id = id;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public PlayerCategory getCategory() {
        return category;
    }

    public void setCategory(PlayerCategory category) {
        this.category = category;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

}
