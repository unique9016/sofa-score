package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayerHomeScore {

    @SerializedName("current")
    @Expose
    private Integer current;
    @SerializedName("display")
    @Expose
    private Integer display;
    @SerializedName("innings")
    @Expose
    private PlayerInnings innings;

    
    public PlayerHomeScore() {
    }

    /**
     *
     * @param current
     * @param display
     * @param innings
     */
    public PlayerHomeScore(Integer current, Integer display, PlayerInnings innings) {
        super();
        this.current = current;
        this.display = display;
        this.innings = innings;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getDisplay() {
        return display;
    }

    public void setDisplay(Integer display) {
        this.display = display;
    }

    public PlayerInnings getInnings() {
        return innings;
    }

    public void setInnings(PlayerInnings innings) {
        this.innings = innings;
    }

}