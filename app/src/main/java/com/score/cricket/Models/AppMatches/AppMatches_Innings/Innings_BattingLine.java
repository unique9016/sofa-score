package com.score.cricket.Models.AppMatches.AppMatches_Innings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Innings_BattingLine {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("balls")
    @Expose
    private Integer balls;
    @SerializedName("s4")
    @Expose
    private Integer s4;
    @SerializedName("s6")
    @Expose
    private Integer s6;
    @SerializedName("wicketTypeId")
    @Expose
    private Integer wicketTypeId;
    @SerializedName("wicketTypeName")
    @Expose
    private String wicketTypeName;
    @SerializedName("fowScore")
    @Expose
    private Integer fowScore;
    @SerializedName("fowOver")
    @Expose
    private Double fowOver;
    @SerializedName("wicketBowler")
    @Expose
    private Innings_WicketBowler wicketBowler;
    @SerializedName("wicketCatch")
    @Expose
    private Innings_WicketCatch wicketCatch;

    
    public Innings_BattingLine() {
    }

    /**
     *
     * @param score
     * @param s4
     * @param s6
     * @param balls
     * @param wicketBowler
     * @param name
     * @param fowScore
     * @param fowOver
     * @param wicketCatch
     * @param id
     * @param wicketTypeName
     * @param wicketTypeId
     */
    public Innings_BattingLine(Integer id, String name, Integer score, Integer balls, Integer s4, Integer s6, Integer wicketTypeId, String wicketTypeName, Integer fowScore, Double fowOver, Innings_WicketBowler wicketBowler, Innings_WicketCatch wicketCatch) {
        super();
        this.id = id;
        this.name = name;
        this.score = score;
        this.balls = balls;
        this.s4 = s4;
        this.s6 = s6;
        this.wicketTypeId = wicketTypeId;
        this.wicketTypeName = wicketTypeName;
        this.fowScore = fowScore;
        this.fowOver = fowOver;
        this.wicketBowler = wicketBowler;
        this.wicketCatch = wicketCatch;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getBalls() {
        return balls;
    }

    public void setBalls(Integer balls) {
        this.balls = balls;
    }

    public Integer getS4() {
        return s4;
    }

    public void setS4(Integer s4) {
        this.s4 = s4;
    }

    public Integer getS6() {
        return s6;
    }

    public void setS6(Integer s6) {
        this.s6 = s6;
    }

    public Integer getWicketTypeId() {
        return wicketTypeId;
    }

    public void setWicketTypeId(Integer wicketTypeId) {
        this.wicketTypeId = wicketTypeId;
    }

    public String getWicketTypeName() {
        return wicketTypeName;
    }

    public void setWicketTypeName(String wicketTypeName) {
        this.wicketTypeName = wicketTypeName;
    }

    public int getFowScore() {
        return fowScore;
    }

    public void setFowScore(Integer fowScore) {
        this.fowScore = fowScore;
    }

    public Double getFowOver() {
        return fowOver;
    }

    public void setFowOver(Double fowOver) {
        this.fowOver = fowOver;
    }

    public Innings_WicketBowler getWicketBowler() {
        return wicketBowler;
    }

    public void setWicketBowler(Innings_WicketBowler wicketBowler) {
        this.wicketBowler = wicketBowler;
    }

    public Innings_WicketCatch getWicketCatch() {
        return wicketCatch;
    }

    public void setWicketCatch(Innings_WicketCatch wicketCatch) {
        this.wicketCatch = wicketCatch;
    }

}