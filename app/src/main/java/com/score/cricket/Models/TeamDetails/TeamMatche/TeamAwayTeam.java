package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TeamAwayTeam {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("national")
    @Expose
    private Boolean national;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("subTeams")
    @Expose
    private List<Object> subTeams = null;

    
    public TeamAwayTeam() {
    }

    public TeamAwayTeam(String name, String slug, String gender, Boolean national, Integer id, List<Object> subTeams) {
        super();
        this.name = name;
        this.slug = slug;
        this.gender = gender;
        this.national = national;
        this.id = id;
        this.subTeams = subTeams;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Boolean getNational() {
        return national;
    }

    public void setNational(Boolean national) {
        this.national = national;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Object> getSubTeams() {
        return subTeams;
    }

    public void setSubTeams(List<Object> subTeams) {
        this.subTeams = subTeams;
    }

}