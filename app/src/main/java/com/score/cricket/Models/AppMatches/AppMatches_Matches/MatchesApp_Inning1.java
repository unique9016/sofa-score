package com.score.cricket.Models.AppMatches.AppMatches_Matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchesApp_Inning1 {

    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("wickets")
    @Expose
    private Integer wickets;
    @SerializedName("overs")
    @Expose
    private Double overs;
    @SerializedName("inningDeclare")
    @Expose
    private Integer inningDeclare;
    @SerializedName("runRate")
    @Expose
    private Double runRate;


    public MatchesApp_Inning1() {
    }

    
    public MatchesApp_Inning1(Integer score, Integer wickets, Double overs, Integer inningDeclare, Double runRate) {
        super();
        this.score = score;
        this.wickets = wickets;
        this.overs = overs;
        this.inningDeclare = inningDeclare;
        this.runRate = runRate;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getWickets() {
        return wickets;
    }

    public void setWickets(Integer wickets) {
        this.wickets = wickets;
    }

    public Double getOvers() {
        return overs;
    }

    public void setOvers(Double overs) {
        this.overs = overs;
    }

    public Integer getInningDeclare() {
        return inningDeclare;
    }

    public void setInningDeclare(Integer inningDeclare) {
        this.inningDeclare = inningDeclare;
    }

    public Double getRunRate() {
        return runRate;
    }

    public void setRunRate(Double runRate) {
        this.runRate = runRate;
    }

}