
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics18 {

    @SerializedName("possessionLost")
    @Expose
    private Integer possessionLost;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;


    public Top_Statistics18() {
    }

    public Top_Statistics18(Integer possessionLost, Integer id, String type, Integer appearances) {
        super();
        this.possessionLost = possessionLost;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getPossessionLost() {
        return possessionLost;
    }

    public void setPossessionLost(Integer possessionLost) {
        this.possessionLost = possessionLost;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
