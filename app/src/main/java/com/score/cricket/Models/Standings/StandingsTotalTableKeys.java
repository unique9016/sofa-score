package com.score.cricket.Models.Standings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StandingsTotalTableKeys {

    @SerializedName("matchesTotal")
    @Expose
    private String matchesTotal;
    @SerializedName("winsTotal")
    @Expose
    private String winsTotal;
    @SerializedName("drawsTotal")
    @Expose
    private String drawsTotal;
    @SerializedName("lossesTotal")
    @Expose
    private String lossesTotal;
    @SerializedName("pointsTotal")
    @Expose
    private String pointsTotal;
    @SerializedName("noResultTotal")
    @Expose
    private String noResultTotal;
    @SerializedName("netRunRateTotal")
    @Expose
    private String netRunRateTotal;

    
    public StandingsTotalTableKeys() {
    }

    /**
     *
     * @param winsTotal
     * @param netRunRateTotal
     * @param drawsTotal
     * @param noResultTotal
     * @param matchesTotal
     * @param lossesTotal
     * @param pointsTotal
     */
    public StandingsTotalTableKeys(String matchesTotal, String winsTotal, String drawsTotal, String lossesTotal, String pointsTotal, String noResultTotal, String netRunRateTotal) {
        super();
        this.matchesTotal = matchesTotal;
        this.winsTotal = winsTotal;
        this.drawsTotal = drawsTotal;
        this.lossesTotal = lossesTotal;
        this.pointsTotal = pointsTotal;
        this.noResultTotal = noResultTotal;
        this.netRunRateTotal = netRunRateTotal;
    }

    public String getMatchesTotal() {
        return matchesTotal;
    }

    public void setMatchesTotal(String matchesTotal) {
        this.matchesTotal = matchesTotal;
    }

    public String getWinsTotal() {
        return winsTotal;
    }

    public void setWinsTotal(String winsTotal) {
        this.winsTotal = winsTotal;
    }

    public String getDrawsTotal() {
        return drawsTotal;
    }

    public void setDrawsTotal(String drawsTotal) {
        this.drawsTotal = drawsTotal;
    }

    public String getLossesTotal() {
        return lossesTotal;
    }

    public void setLossesTotal(String lossesTotal) {
        this.lossesTotal = lossesTotal;
    }

    public String getPointsTotal() {
        return pointsTotal;
    }

    public void setPointsTotal(String pointsTotal) {
        this.pointsTotal = pointsTotal;
    }

    public String getNoResultTotal() {
        return noResultTotal;
    }

    public void setNoResultTotal(String noResultTotal) {
        this.noResultTotal = noResultTotal;
    }

    public String getNetRunRateTotal() {
        return netRunRateTotal;
    }

    public void setNetRunRateTotal(String netRunRateTotal) {
        this.netRunRateTotal = netRunRateTotal;
    }

}