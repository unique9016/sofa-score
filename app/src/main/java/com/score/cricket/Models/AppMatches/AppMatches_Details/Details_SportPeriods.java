
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_SportPeriods {

    @SerializedName("current")
    @Expose
    private String current;
    @SerializedName("inning1")
    @Expose
    private String inning1;
    @SerializedName("inning2")
    @Expose
    private String inning2;
    @SerializedName("inning3")
    @Expose
    private String inning3;
    @SerializedName("inning4")
    @Expose
    private String inning4;

    
    public Details_SportPeriods() {
    }

    /**
     * 
     * @param current
     * @param inning3
     * @param inning4
     * @param inning1
     * @param inning2
     */
    public Details_SportPeriods(String current, String inning1, String inning2, String inning3, String inning4) {
        super();
        this.current = current;
        this.inning1 = inning1;
        this.inning2 = inning2;
        this.inning3 = inning3;
        this.inning4 = inning4;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public String getInning1() {
        return inning1;
    }

    public void setInning1(String inning1) {
        this.inning1 = inning1;
    }

    public String getInning2() {
        return inning2;
    }

    public void setInning2(String inning2) {
        this.inning2 = inning2;
    }

    public String getInning3() {
        return inning3;
    }

    public void setInning3(String inning3) {
        this.inning3 = inning3;
    }

    public String getInning4() {
        return inning4;
    }

    public void setInning4(String inning4) {
        this.inning4 = inning4;
    }

}
