package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppMatches.Matches_CurrentBatsman;
import com.score.cricket.Models.AppMatches.Matches_CurrentBattingTeam;

public class Details_Event {

    @SerializedName("customId")
    @Expose
    private String customId;
    @SerializedName("status")
    @Expose
    private Details_Status status;
    @SerializedName("winnerCode")
    @Expose
    private Integer winnerCode;
    @SerializedName("venue")
    @Expose
    private Details_Venue venue;
    @SerializedName("referee")
    @Expose
    private Details_Referee referee;
    @SerializedName("homeTeam")
    @Expose
    private Details_HomeTeam homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private Details_AwayTeam awayTeam;
    @SerializedName("homeScore")
    @Expose
    private Details_HomeScore homeScore;
    @SerializedName("awayScore")
    @Expose
    private Details_AwayScore awayScore;
    @SerializedName("time")
    @Expose
    private Details_Time time;
    @SerializedName("changes")
    @Expose
    private Details_Changes changes;
    @SerializedName("hasHighlights")
    @Expose
    private Boolean hasHighlights;
    @SerializedName("hasHighlightsStream")
    @Expose
    private Boolean hasHighlightsStream;
    @SerializedName("hasGlobalHighlights")
    @Expose
    private Boolean hasGlobalHighlights;
    @SerializedName("manOfMatch")
    @Expose
    private Details_ManOfMatch manOfMatch;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("umpire1")
    @Expose
    private String umpire1;
    @SerializedName("umpire2")
    @Expose
    private String umpire2;
    @SerializedName("tvUmpire")
    @Expose
    private String tvUmpire;
    @SerializedName("tossWin")
    @Expose
    private String tossWin;
    @SerializedName("tossDecision")
    @Expose
    private String tossDecision;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("startTimestamp")
    @Expose
    private Integer startTimestamp;
    @SerializedName("statusDescription")
    @Expose
    private String statusDescription;
    @SerializedName("sportPeriods")
    @Expose
    private Details_SportPeriods sportPeriods;
    @SerializedName("webUrl")
    @Expose
    private String webUrl;
    @SerializedName("fbPostId")
    @Expose
    private String fbPostId;
    @SerializedName("hasTime")
    @Expose
    private Boolean hasTime;
    @SerializedName("resultOnly")
    @Expose
    private Boolean resultOnly;
    @SerializedName("showEventNote")
    @Expose
    private Boolean showEventNote;
    @SerializedName("teamsForm")
    @Expose
    private Details_TeamsForm teamsForm;
    @SerializedName("bet365Links")
    @Expose
    private Details_Bet365Links bet365Links;

    @SerializedName("currentBowler")
    @Expose
    private Matches_CurrentBatsman currentBowler;
    @SerializedName("currentBatsman")
    @Expose
    private Matches_CurrentBatsman currentBatsman;
    @SerializedName("currentBattingTeam")
    @Expose
    private Matches_CurrentBattingTeam currentBattingTeam;
    @SerializedName("currentBowlingTeam")
    @Expose
    private Matches_CurrentBattingTeam currentBowlingTeam;

    /**
     * No args constructor for use in serialization
     */
    public Details_Event() {
    }

    public Details_Event(String customId, Details_Status status, Integer winnerCode, Details_Venue venue, Details_Referee referee, Details_HomeTeam homeTeam, Details_AwayTeam awayTeam, Details_HomeScore homeScore, Details_AwayScore awayScore, Details_Time time, Details_Changes changes, Boolean hasHighlights, Boolean hasHighlightsStream, Boolean hasGlobalHighlights, Details_ManOfMatch manOfMatch, Integer id, String umpire1, String umpire2, String tvUmpire, String tossWin, String tossDecision, String note, Integer startTimestamp, String statusDescription, Details_SportPeriods sportPeriods, String webUrl, String fbPostId, Boolean hasTime, Boolean resultOnly, Boolean showEventNote, Details_TeamsForm teamsForm, Details_Bet365Links bet365Links, Matches_CurrentBatsman currentBowler, Matches_CurrentBatsman currentBatsman, Matches_CurrentBattingTeam currentBattingTeam, Matches_CurrentBattingTeam currentBowlingTeam) {
        this.customId = customId;
        this.status = status;
        this.winnerCode = winnerCode;
        this.venue = venue;
        this.referee = referee;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
        this.time = time;
        this.changes = changes;
        this.hasHighlights = hasHighlights;
        this.hasHighlightsStream = hasHighlightsStream;
        this.hasGlobalHighlights = hasGlobalHighlights;
        this.manOfMatch = manOfMatch;
        this.id = id;
        this.umpire1 = umpire1;
        this.umpire2 = umpire2;
        this.tvUmpire = tvUmpire;
        this.tossWin = tossWin;
        this.tossDecision = tossDecision;
        this.note = note;
        this.startTimestamp = startTimestamp;
        this.statusDescription = statusDescription;
        this.sportPeriods = sportPeriods;
        this.webUrl = webUrl;
        this.fbPostId = fbPostId;
        this.hasTime = hasTime;
        this.resultOnly = resultOnly;
        this.showEventNote = showEventNote;
        this.teamsForm = teamsForm;
        this.bet365Links = bet365Links;
        this.currentBowler = currentBowler;
        this.currentBatsman = currentBatsman;
        this.currentBattingTeam = currentBattingTeam;
        this.currentBowlingTeam = currentBowlingTeam;
    }

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public Details_Status getStatus() {
        return status;
    }

    public void setStatus(Details_Status status) {
        this.status = status;
    }

    public Integer getWinnerCode() {
        return winnerCode;
    }

    public void setWinnerCode(Integer winnerCode) {
        this.winnerCode = winnerCode;
    }

    public Details_Venue getVenue() {
        return venue;
    }

    public void setVenue(Details_Venue venue) {
        this.venue = venue;
    }

    public Details_Referee getReferee() {
        return referee;
    }

    public void setReferee(Details_Referee referee) {
        this.referee = referee;
    }

    public Details_HomeTeam getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Details_HomeTeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Details_AwayTeam getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Details_AwayTeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Details_HomeScore getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Details_HomeScore homeScore) {
        this.homeScore = homeScore;
    }

    public Details_AwayScore getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(Details_AwayScore awayScore) {
        this.awayScore = awayScore;
    }

    public Details_Time getTime() {
        return time;
    }

    public void setTime(Details_Time time) {
        this.time = time;
    }

    public Details_Changes getChanges() {
        return changes;
    }

    public void setChanges(Details_Changes changes) {
        this.changes = changes;
    }

    public Boolean getHasHighlights() {
        return hasHighlights;
    }

    public void setHasHighlights(Boolean hasHighlights) {
        this.hasHighlights = hasHighlights;
    }

    public Boolean getHasHighlightsStream() {
        return hasHighlightsStream;
    }

    public void setHasHighlightsStream(Boolean hasHighlightsStream) {
        this.hasHighlightsStream = hasHighlightsStream;
    }

    public Boolean getHasGlobalHighlights() {
        return hasGlobalHighlights;
    }

    public void setHasGlobalHighlights(Boolean hasGlobalHighlights) {
        this.hasGlobalHighlights = hasGlobalHighlights;
    }

    public Details_ManOfMatch getManOfMatch() {
        return manOfMatch;
    }

    public void setManOfMatch(Details_ManOfMatch manOfMatch) {
        this.manOfMatch = manOfMatch;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUmpire1() {
        return umpire1;
    }

    public void setUmpire1(String umpire1) {
        this.umpire1 = umpire1;
    }

    public String getUmpire2() {
        return umpire2;
    }

    public void setUmpire2(String umpire2) {
        this.umpire2 = umpire2;
    }

    public String getTvUmpire() {
        return tvUmpire;
    }

    public void setTvUmpire(String tvUmpire) {
        this.tvUmpire = tvUmpire;
    }

    public String getTossWin() {
        return tossWin;
    }

    public void setTossWin(String tossWin) {
        this.tossWin = tossWin;
    }

    public String getTossDecision() {
        return tossDecision;
    }

    public void setTossDecision(String tossDecision) {
        this.tossDecision = tossDecision;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Integer startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public Details_SportPeriods getSportPeriods() {
        return sportPeriods;
    }

    public void setSportPeriods(Details_SportPeriods sportPeriods) {
        this.sportPeriods = sportPeriods;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getFbPostId() {
        return fbPostId;
    }

    public void setFbPostId(String fbPostId) {
        this.fbPostId = fbPostId;
    }

    public Boolean getHasTime() {
        return hasTime;
    }

    public void setHasTime(Boolean hasTime) {
        this.hasTime = hasTime;
    }

    public Boolean getResultOnly() {
        return resultOnly;
    }

    public void setResultOnly(Boolean resultOnly) {
        this.resultOnly = resultOnly;
    }

    public Boolean getShowEventNote() {
        return showEventNote;
    }

    public void setShowEventNote(Boolean showEventNote) {
        this.showEventNote = showEventNote;
    }

    public Details_TeamsForm getTeamsForm() {
        return teamsForm;
    }

    public void setTeamsForm(Details_TeamsForm teamsForm) {
        this.teamsForm = teamsForm;
    }

    public Details_Bet365Links getBet365Links() {
        return bet365Links;
    }

    public void setBet365Links(Details_Bet365Links bet365Links) {
        this.bet365Links = bet365Links;
    }

    public Matches_CurrentBatsman getCurrentBowler() {
        return currentBowler;
    }

    public void setCurrentBowler(Matches_CurrentBatsman currentBowler) {
        this.currentBowler = currentBowler;
    }

    public Matches_CurrentBatsman getCurrentBatsman() {
        return currentBatsman;
    }

    public void setCurrentBatsman(Matches_CurrentBatsman currentBatsman) {
        this.currentBatsman = currentBatsman;
    }

    public Matches_CurrentBattingTeam getCurrentBattingTeam() {
        return currentBattingTeam;
    }

    public void setCurrentBattingTeam(Matches_CurrentBattingTeam currentBattingTeam) {
        this.currentBattingTeam = currentBattingTeam;
    }

    public Matches_CurrentBattingTeam getCurrentBowlingTeam() {
        return currentBowlingTeam;
    }

    public void setCurrentBowlingTeam(Matches_CurrentBattingTeam currentBowlingTeam) {
        this.currentBowlingTeam = currentBowlingTeam;
    }
}
