
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_HomeScore {

    @SerializedName("current")
    @Expose
    private Integer current;
    @SerializedName("display")
    @Expose
    private Integer display;
    @SerializedName("innings")
    @Expose
    private Details_Innings innings;
    @SerializedName("hits")
    @Expose
    private Integer hits;
    @SerializedName("errors")
    @Expose
    private Integer errors;

    
    public Details_HomeScore() {
    }


    public Details_HomeScore(Integer current, Integer display, Details_Innings innings, Integer hits, Integer errors) {
        super();
        this.current = current;
        this.display = display;
        this.innings = innings;
        this.hits = hits;
        this.errors = errors;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getDisplay() {
        return display;
    }

    public void setDisplay(Integer display) {
        this.display = display;
    }

    public Details_Innings getInnings() {
        return innings;
    }

    public void setInnings(Details_Innings innings) {
        this.innings = innings;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    public Integer getErrors() {
        return errors;
    }

    public void setErrors(Integer errors) {
        this.errors = errors;
    }

}
