package com.score.cricket.Models.HomeMain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeMatch_inning_wicket {

    @SerializedName("batsmen")
    @Expose
    private String batsmen;
    @SerializedName("index")
    @Expose
    private Integer index;
    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("over")
    @Expose
    private Double over;

    public HomeMatch_inning_wicket() {
    }

    public HomeMatch_inning_wicket(String batsmen, Integer index, Integer score, Double over) {
        this.batsmen = batsmen;
        this.index = index;
        this.score = score;
        this.over = over;
    }

    public String getBatsmen() {
        return batsmen;
    }

    public void setBatsmen(String batsmen) {
        this.batsmen = batsmen;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Double getOver() {
        return over;
    }

    public void setOver(Double over) {
        this.over = over;
    }
}
