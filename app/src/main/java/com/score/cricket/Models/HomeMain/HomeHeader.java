package com.score.cricket.Models.HomeMain;

import com.score.cricket.Listener.ListnerItems;

public class HomeHeader implements ListnerItems {
    String name,flage;

    public HomeHeader(String name, String flage) {
        this.name = name;
        this.flage = flage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFlage() {
        return flage;
    }

    public void setFlage(String flage) {
        this.flage = flage;
    }

    @Override
    public int getItemType() {
        return ListnerItems.TYPE_HEADER;
    }
}