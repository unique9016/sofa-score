
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_City {

    @SerializedName("name")
    @Expose
    private String name;


    public Details_City() {
    }

    /**
     * 
     * @param name
     */
    public Details_City(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
