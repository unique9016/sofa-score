package com.score.cricket.Models.AppMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Matches_Tournament_ {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order")
    @Expose
    private Integer order;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("uniqueId")
    @Expose
    private Integer uniqueId;
    @SerializedName("uniqueName")
    @Expose
    private String uniqueName;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;
    @SerializedName("hasRounds")
    @Expose
    private Boolean hasRounds;


    public Matches_Tournament_() {
    }

    /**
     *
     * @param uniqueName
     * @param hasRounds
     * @param name
     * @param hasEventPlayerStatistics
     * @param id
     * @param uniqueId
     * @param order
     */
    public Matches_Tournament_(String name, Integer order, Integer id, Integer uniqueId, String uniqueName, Boolean hasEventPlayerStatistics, Boolean hasRounds) {
        super();
        this.name = name;
        this.order = order;
        this.id = id;
        this.uniqueId = uniqueId;
        this.uniqueName = uniqueName;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
        this.hasRounds = hasRounds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public Boolean getHasRounds() {
        return hasRounds;
    }

    public void setHasRounds(Boolean hasRounds) {
        this.hasRounds = hasRounds;
    }

}