package com.score.cricket.Models.AppTournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TournamentsExample {

    @SerializedName("seasons")
    @Expose
    private List<TournamentsSeason> seasons = null;


    public TournamentsExample() {
    }

    /**
     *
     * @param seasons
     */
    public TournamentsExample(List<TournamentsSeason> seasons) {
        super();
        this.seasons = seasons;
    }

    public List<TournamentsSeason> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<TournamentsSeason> seasons) {
        this.seasons = seasons;
    }

}
