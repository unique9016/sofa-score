package com.score.cricket.Models.Standings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StandingsFilteredTableKeys {

    @SerializedName("matchesTotal")
    @Expose
    private String matchesTotal;
    @SerializedName("winsLossesTotal")
    @Expose
    private String winsLossesTotal;
    @SerializedName("pointsTotal")
    @Expose
    private String pointsTotal;

    
    public StandingsFilteredTableKeys() {
    }

    /**
     *
     * @param winsLossesTotal
     * @param matchesTotal
     * @param pointsTotal
     */
    public StandingsFilteredTableKeys(String matchesTotal, String winsLossesTotal, String pointsTotal) {
        super();
        this.matchesTotal = matchesTotal;
        this.winsLossesTotal = winsLossesTotal;
        this.pointsTotal = pointsTotal;
    }

    public String getMatchesTotal() {
        return matchesTotal;
    }

    public void setMatchesTotal(String matchesTotal) {
        this.matchesTotal = matchesTotal;
    }

    public String getWinsLossesTotal() {
        return winsLossesTotal;
    }

    public void setWinsLossesTotal(String winsLossesTotal) {
        this.winsLossesTotal = winsLossesTotal;
    }

    public String getPointsTotal() {
        return pointsTotal;
    }

    public void setPointsTotal(String pointsTotal) {
        this.pointsTotal = pointsTotal;
    }

}