package com.score.cricket.Models.AppMatches.AppMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MatchesExample {

    @SerializedName("categories")
    @Expose
    private List<MatchesCategory> categories = null;

    public MatchesExample() {
    }

    public MatchesExample(List<MatchesCategory> categories) {
        super();
        this.categories = categories;
    }

    public List<MatchesCategory> getCategories() {
        return categories;
    }

    public void setCategories(List<MatchesCategory> categories) {
        this.categories = categories;
    }

}