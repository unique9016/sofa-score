package com.score.cricket.Models.LeaguesUi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MainLeague {


        @SerializedName("categories")
        @Expose
        private List<LeaguesUiCategory> categories = null;

        public MainLeague() {
        }

        public MainLeague(List<LeaguesUiCategory> categories) {
            super();
            this.categories = categories;
        }

        public List<LeaguesUiCategory> getCategories() {
            return categories;
        }

        public void setCategories(List<LeaguesUiCategory> categories) {
            this.categories = categories;
        }

}
