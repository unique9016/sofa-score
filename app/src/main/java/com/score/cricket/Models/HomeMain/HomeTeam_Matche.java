package com.score.cricket.Models.HomeMain;

import com.score.cricket.Listener.ListnerItems;

public class HomeTeam_Matche implements ListnerItems {
    String hometeam,awayteam,homescore,awayscore,homewicket,awaywicket,homeover,awayover,date,time,status;
    int id,winnercode;

    public HomeTeam_Matche() {
    }

    public HomeTeam_Matche(String hometeam, String awayteam, String homescore, String awayscore, String homewicket, String awaywicket, String homeover, String awayover, String date, String time, String status, int id, int winnercode) {
        this.hometeam = hometeam;
        this.awayteam = awayteam;
        this.homescore = homescore;
        this.awayscore = awayscore;
        this.homewicket = homewicket;
        this.awaywicket = awaywicket;
        this.homeover = homeover;
        this.awayover = awayover;
        this.date = date;
        this.time = time;
        this.status = status;
        this.id = id;
        this.winnercode = winnercode;
    }


    public String getHometeam() {
        return hometeam;
    }

    public void setHometeam(String hometeam) {
        this.hometeam = hometeam;
    }

    public String getAwayteam() {
        return awayteam;
    }

    public void setAwayteam(String awayteam) {
        this.awayteam = awayteam;
    }

    public String getHomescore() {
        return homescore;
    }

    public void setHomescore(String homescore) {
        this.homescore = homescore;
    }

    public String getAwayscore() {
        return awayscore;
    }

    public void setAwayscore(String awayscore) {
        this.awayscore = awayscore;
    }

    public String getHomewicket() {
        return homewicket;
    }

    public void setHomewicket(String homewicket) {
        this.homewicket = homewicket;
    }

    public String getAwaywicket() {
        return awaywicket;
    }

    public void setAwaywicket(String awaywicket) {
        this.awaywicket = awaywicket;
    }

    public String getHomeover() {
        return homeover;
    }

    public void setHomeover(String homeover) {
        this.homeover = homeover;
    }

    public String getAwayover() {
        return awayover;
    }

    public void setAwayover(String awayover) {
        this.awayover = awayover;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWinnercode() {
        return winnercode;
    }

    public void setWinnercode(int winnercode) {
        this.winnercode = winnercode;
    }

    @Override
    public int getItemType() {
        return 0;
    }
}
