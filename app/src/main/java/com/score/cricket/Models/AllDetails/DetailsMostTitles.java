package com.score.cricket.Models.AllDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailsMostTitles {

    @SerializedName("teams")
    @Expose
    private List<DetailsTeam> teams = null;
    @SerializedName("count")
    @Expose
    private String count;


    public DetailsMostTitles() {
    }

    public DetailsMostTitles(List<DetailsTeam> teams, String count) {
        super();
        this.teams = teams;
        this.count = count;
    }

    public List<DetailsTeam> getTeams() {
        return teams;
    }

    public void setTeams(List<DetailsTeam> teams) {
        this.teams = teams;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

}