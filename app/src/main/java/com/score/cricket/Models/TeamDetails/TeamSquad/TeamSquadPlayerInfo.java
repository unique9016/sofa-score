package com.score.cricket.Models.TeamDetails.TeamSquad;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamSquadPlayerInfo {

    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("batting")
    @Expose
    private String batting;
    @SerializedName("bowling")
    @Expose
    private String bowling;


    public TeamSquadPlayerInfo() {
    }

    /**
     *
     * @param batting
     * @param role
     * @param bowling
     */
    public TeamSquadPlayerInfo(String role, String batting, String bowling) {
        super();
        this.role = role;
        this.batting = batting;
        this.bowling = bowling;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBatting() {
        return batting;
    }

    public void setBatting(String batting) {
        this.batting = batting;
    }

    public String getBowling() {
        return bowling;
    }

    public void setBowling(String bowling) {
        this.bowling = bowling;
    }

}