package com.score.cricket.Models.AppTournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TournamentsTournament {

    @SerializedName("uniqueTournaments")
    @Expose
    private List<TournamentsUniqueTournament> uniqueTournaments = null;
    @SerializedName("topUniqueTournamentIds")
    @Expose
    private List<Integer> topUniqueTournamentIds = null;

    
    public TournamentsTournament() {
    }

    /**
     *
     * @param uniqueTournaments
     * @param topUniqueTournamentIds
     */
    public TournamentsTournament(List<TournamentsUniqueTournament> uniqueTournaments, List<Integer> topUniqueTournamentIds) {
        super();
        this.uniqueTournaments = uniqueTournaments;
        this.topUniqueTournamentIds = topUniqueTournamentIds;
    }

    public List<TournamentsUniqueTournament> getUniqueTournaments() {
        return uniqueTournaments;
    }

    public void setUniqueTournaments(List<TournamentsUniqueTournament> uniqueTournaments) {
        this.uniqueTournaments = uniqueTournaments;
    }

    public List<Integer> getTopUniqueTournamentIds() {
        return topUniqueTournamentIds;
    }

    public void setTopUniqueTournamentIds(List<Integer> topUniqueTournamentIds) {
        this.topUniqueTournamentIds = topUniqueTournamentIds;
    }

}