package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamExample {

    @SerializedName("last")
    @Expose
    private TeamLast last;
    @SerializedName("next")
    @Expose
    private TeamNext next;

    
    public TeamExample() {
    }

    public TeamExample(TeamLast last, TeamNext next) {
        super();
        this.last = last;
        this.next = next;
    }

    public TeamLast getLast() {
        return last;
    }

    public void setLast(TeamLast last) {
        this.last = last;
    }

    public TeamNext getNext() {
        return next;
    }

    public void setNext(TeamNext next) {
        this.next = next;
    }

}