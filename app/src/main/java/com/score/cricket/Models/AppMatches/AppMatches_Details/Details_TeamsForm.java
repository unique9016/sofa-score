
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_TeamsForm {

    @SerializedName("homeTeam")
    @Expose
    private Details_HomeTeam_ homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private Details_AwayTeam_ awayTeam;
    @SerializedName("pointsLabel")
    @Expose
    private String pointsLabel;

    
    public Details_TeamsForm() {
    }


    public Details_TeamsForm(Details_HomeTeam_ homeTeam, Details_AwayTeam_ awayTeam, String pointsLabel) {
        super();
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.pointsLabel = pointsLabel;
    }

    public Details_HomeTeam_ getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Details_HomeTeam_ homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Details_AwayTeam_ getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Details_AwayTeam_ awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getPointsLabel() {
        return pointsLabel;
    }

    public void setPointsLabel(String pointsLabel) {
        this.pointsLabel = pointsLabel;
    }

}
