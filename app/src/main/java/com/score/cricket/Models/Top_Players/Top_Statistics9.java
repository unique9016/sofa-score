
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics9 {

    @SerializedName("bigChancesCreated")
    @Expose
    private Integer bigChancesCreated;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;

    
    public Top_Statistics9() {
    }

    public Top_Statistics9(Integer bigChancesCreated, Integer id, String type, Integer appearances) {
        super();
        this.bigChancesCreated = bigChancesCreated;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getBigChancesCreated() {
        return bigChancesCreated;
    }

    public void setBigChancesCreated(Integer bigChancesCreated) {
        this.bigChancesCreated = bigChancesCreated;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
