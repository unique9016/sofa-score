package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

import java.util.List;

public class TeamNext {

    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("rows")
    @Expose
    private Integer rows;
    @SerializedName("tournaments")
    @Expose
    private List<TeamTournament> tournaments = null;

    
    public TeamNext() {
    }

    public TeamNext(AppSport sport, Integer rows, List<TeamTournament> tournaments) {
        super();
        this.sport = sport;
        this.rows = rows;
        this.tournaments = tournaments;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public List<TeamTournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<TeamTournament> tournaments) {
        this.tournaments = tournaments;
    }

}