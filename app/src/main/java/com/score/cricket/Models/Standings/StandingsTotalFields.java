package com.score.cricket.Models.Standings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StandingsTotalFields {

    @SerializedName("matchesTotal")
    @Expose
    private String matchesTotal;
    @SerializedName("winsTotal")
    @Expose
    private String winsTotal;
    @SerializedName("drawsTotal")
    @Expose
    private String drawsTotal;
    @SerializedName("lossesTotal")
    @Expose
    private String lossesTotal;
    @SerializedName("winsLossesTotal")
    @Expose
    private String winsLossesTotal;
    @SerializedName("goalsTotal")
    @Expose
    private String goalsTotal;
    @SerializedName("pctGoalTotal")
    @Expose
    private String pctGoalTotal;
    @SerializedName("scoreDiffFormattedTotal")
    @Expose
    private String scoreDiffFormattedTotal;
    @SerializedName("pointsTotal")
    @Expose
    private String pointsTotal;
    @SerializedName("percentageTotal")
    @Expose
    private String percentageTotal;
    @SerializedName("gamesBehindTotal")
    @Expose
    private String gamesBehindTotal;
    @SerializedName("overtimeWinsTotal")
    @Expose
    private String overtimeWinsTotal;
    @SerializedName("overtimeLossesTotal")
    @Expose
    private String overtimeLossesTotal;
    @SerializedName("penaltyWinsTotal")
    @Expose
    private String penaltyWinsTotal;
    @SerializedName("penaltyLossesTotal")
    @Expose
    private String penaltyLossesTotal;
    @SerializedName("overtimeAndPenaltyWinsTotal")
    @Expose
    private String overtimeAndPenaltyWinsTotal;
    @SerializedName("threePointsTotal")
    @Expose
    private String threePointsTotal;
    @SerializedName("twoPointsTotal")
    @Expose
    private String twoPointsTotal;
    @SerializedName("onePointTotal")
    @Expose
    private String onePointTotal;
    @SerializedName("noResultTotal")
    @Expose
    private String noResultTotal;
    @SerializedName("netRunRateTotal")
    @Expose
    private String netRunRateTotal;
    @SerializedName("streakTotal")
    @Expose
    private String streakTotal;

    
    public StandingsTotalFields() {
    }

    /**
     *
     * @param twoPointsTotal
     * @param winsTotal
     * @param winsLossesTotal
     * @param overtimeWinsTotal
     * @param drawsTotal
     * @param lossesTotal
     * @param threePointsTotal
     * @param scoreDiffFormattedTotal
     * @param penaltyLossesTotal
     * @param overtimeAndPenaltyWinsTotal
     * @param gamesBehindTotal
     * @param overtimeLossesTotal
     * @param netRunRateTotal
     * @param onePointTotal
     * @param goalsTotal
     * @param pctGoalTotal
     * @param noResultTotal
     * @param matchesTotal
     * @param pointsTotal
     * @param streakTotal
     * @param percentageTotal
     * @param penaltyWinsTotal
     */
    public StandingsTotalFields(String matchesTotal, String winsTotal, String drawsTotal, String lossesTotal, String winsLossesTotal, String goalsTotal, String pctGoalTotal, String scoreDiffFormattedTotal, String pointsTotal, String percentageTotal, String gamesBehindTotal, String overtimeWinsTotal, String overtimeLossesTotal, String penaltyWinsTotal, String penaltyLossesTotal, String overtimeAndPenaltyWinsTotal, String threePointsTotal, String twoPointsTotal, String onePointTotal, String noResultTotal, String netRunRateTotal, String streakTotal) {
        super();
        this.matchesTotal = matchesTotal;
        this.winsTotal = winsTotal;
        this.drawsTotal = drawsTotal;
        this.lossesTotal = lossesTotal;
        this.winsLossesTotal = winsLossesTotal;
        this.goalsTotal = goalsTotal;
        this.pctGoalTotal = pctGoalTotal;
        this.scoreDiffFormattedTotal = scoreDiffFormattedTotal;
        this.pointsTotal = pointsTotal;
        this.percentageTotal = percentageTotal;
        this.gamesBehindTotal = gamesBehindTotal;
        this.overtimeWinsTotal = overtimeWinsTotal;
        this.overtimeLossesTotal = overtimeLossesTotal;
        this.penaltyWinsTotal = penaltyWinsTotal;
        this.penaltyLossesTotal = penaltyLossesTotal;
        this.overtimeAndPenaltyWinsTotal = overtimeAndPenaltyWinsTotal;
        this.threePointsTotal = threePointsTotal;
        this.twoPointsTotal = twoPointsTotal;
        this.onePointTotal = onePointTotal;
        this.noResultTotal = noResultTotal;
        this.netRunRateTotal = netRunRateTotal;
        this.streakTotal = streakTotal;
    }

    public String getMatchesTotal() {
        return matchesTotal;
    }

    public void setMatchesTotal(String matchesTotal) {
        this.matchesTotal = matchesTotal;
    }

    public String getWinsTotal() {
        return winsTotal;
    }

    public void setWinsTotal(String winsTotal) {
        this.winsTotal = winsTotal;
    }

    public String getDrawsTotal() {
        return drawsTotal;
    }

    public void setDrawsTotal(String drawsTotal) {
        this.drawsTotal = drawsTotal;
    }

    public String getLossesTotal() {
        return lossesTotal;
    }

    public void setLossesTotal(String lossesTotal) {
        this.lossesTotal = lossesTotal;
    }

    public String getWinsLossesTotal() {
        return winsLossesTotal;
    }

    public void setWinsLossesTotal(String winsLossesTotal) {
        this.winsLossesTotal = winsLossesTotal;
    }

    public String getGoalsTotal() {
        return goalsTotal;
    }

    public void setGoalsTotal(String goalsTotal) {
        this.goalsTotal = goalsTotal;
    }

    public String getPctGoalTotal() {
        return pctGoalTotal;
    }

    public void setPctGoalTotal(String pctGoalTotal) {
        this.pctGoalTotal = pctGoalTotal;
    }

    public String getScoreDiffFormattedTotal() {
        return scoreDiffFormattedTotal;
    }

    public void setScoreDiffFormattedTotal(String scoreDiffFormattedTotal) {
        this.scoreDiffFormattedTotal = scoreDiffFormattedTotal;
    }

    public String getPointsTotal() {
        return pointsTotal;
    }

    public void setPointsTotal(String pointsTotal) {
        this.pointsTotal = pointsTotal;
    }

    public String getPercentageTotal() {
        return percentageTotal;
    }

    public void setPercentageTotal(String percentageTotal) {
        this.percentageTotal = percentageTotal;
    }

    public String getGamesBehindTotal() {
        return gamesBehindTotal;
    }

    public void setGamesBehindTotal(String gamesBehindTotal) {
        this.gamesBehindTotal = gamesBehindTotal;
    }

    public String getOvertimeWinsTotal() {
        return overtimeWinsTotal;
    }

    public void setOvertimeWinsTotal(String overtimeWinsTotal) {
        this.overtimeWinsTotal = overtimeWinsTotal;
    }

    public String getOvertimeLossesTotal() {
        return overtimeLossesTotal;
    }

    public void setOvertimeLossesTotal(String overtimeLossesTotal) {
        this.overtimeLossesTotal = overtimeLossesTotal;
    }

    public String getPenaltyWinsTotal() {
        return penaltyWinsTotal;
    }

    public void setPenaltyWinsTotal(String penaltyWinsTotal) {
        this.penaltyWinsTotal = penaltyWinsTotal;
    }

    public String getPenaltyLossesTotal() {
        return penaltyLossesTotal;
    }

    public void setPenaltyLossesTotal(String penaltyLossesTotal) {
        this.penaltyLossesTotal = penaltyLossesTotal;
    }

    public String getOvertimeAndPenaltyWinsTotal() {
        return overtimeAndPenaltyWinsTotal;
    }

    public void setOvertimeAndPenaltyWinsTotal(String overtimeAndPenaltyWinsTotal) {
        this.overtimeAndPenaltyWinsTotal = overtimeAndPenaltyWinsTotal;
    }

    public String getThreePointsTotal() {
        return threePointsTotal;
    }

    public void setThreePointsTotal(String threePointsTotal) {
        this.threePointsTotal = threePointsTotal;
    }

    public String getTwoPointsTotal() {
        return twoPointsTotal;
    }

    public void setTwoPointsTotal(String twoPointsTotal) {
        this.twoPointsTotal = twoPointsTotal;
    }

    public String getOnePointTotal() {
        return onePointTotal;
    }

    public void setOnePointTotal(String onePointTotal) {
        this.onePointTotal = onePointTotal;
    }

    public String getNoResultTotal() {
        return noResultTotal;
    }

    public void setNoResultTotal(String noResultTotal) {
        this.noResultTotal = noResultTotal;
    }

    public String getNetRunRateTotal() {
        return netRunRateTotal;
    }

    public void setNetRunRateTotal(String netRunRateTotal) {
        this.netRunRateTotal = netRunRateTotal;
    }

    public String getStreakTotal() {
        return streakTotal;
    }

    public void setStreakTotal(String streakTotal) {
        this.streakTotal = streakTotal;
    }

}