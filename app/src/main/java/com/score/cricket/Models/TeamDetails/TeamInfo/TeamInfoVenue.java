package com.score.cricket.Models.TeamDetails.TeamInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamInfoVenue {

    @SerializedName("city")
    @Expose
    private TeamInfoCity city;
    @SerializedName("stadium")
    @Expose
    private TeamInfoStadium stadium;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("hasImage")
    @Expose
    private Boolean hasImage;
    @SerializedName("country")
    @Expose
    private TeamInfoCountry country;

    
    public TeamInfoVenue() {
    }

    /**
     *
     * @param country
     * @param city
     * @param stadium
     * @param hasImage
     * @param id
     */
    public TeamInfoVenue(TeamInfoCity city, TeamInfoStadium stadium, Integer id, Boolean hasImage, TeamInfoCountry country) {
        super();
        this.city = city;
        this.stadium = stadium;
        this.id = id;
        this.hasImage = hasImage;
        this.country = country;
    }

    public TeamInfoCity getCity() {
        return city;
    }

    public void setCity(TeamInfoCity city) {
        this.city = city;
    }

    public TeamInfoStadium getStadium() {
        return stadium;
    }

    public void setStadium(TeamInfoStadium stadium) {
        this.stadium = stadium;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getHasImage() {
        return hasImage;
    }

    public void setHasImage(Boolean hasImage) {
        this.hasImage = hasImage;
    }

    public TeamInfoCountry getCountry() {
        return country;
    }

    public void setCountry(TeamInfoCountry country) {
        this.country = country;
    }

}