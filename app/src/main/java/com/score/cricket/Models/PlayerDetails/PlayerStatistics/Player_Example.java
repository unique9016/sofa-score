package com.score.cricket.Models.PlayerDetails.PlayerStatistics;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Player_Example {

    @SerializedName("groupName")
    @Expose
    private String groupName;
    @SerializedName("statisticsItems")
    @Expose
    private List<Player_StatisticsItem> statisticsItems = null;


    public Player_Example() {
    }

    /**
     *
     * @param groupName
     * @param statisticsItems
     */
    public Player_Example(String groupName, List<Player_StatisticsItem> statisticsItems) {
        super();
        this.groupName = groupName;
        this.statisticsItems = statisticsItems;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Player_StatisticsItem> getStatisticsItems() {
        return statisticsItems;
    }

    public void setStatisticsItems(List<Player_StatisticsItem> statisticsItems) {
        this.statisticsItems = statisticsItems;
    }

}