package com.score.cricket.Models.HomeMain;

public class HomePlayer_Statistic_BattingList {
    String name,value;

    public HomePlayer_Statistic_BattingList() {
    }

    public HomePlayer_Statistic_BattingList(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
