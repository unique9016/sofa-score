package com.score.cricket.Models.HomeMain;

public class HomePlayer_Staticstics_Spinner {
    String name;
    int position;

    public HomePlayer_Staticstics_Spinner() {
    }

    public HomePlayer_Staticstics_Spinner(String name, int position) {
        this.name = name;
        this.position = position;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
