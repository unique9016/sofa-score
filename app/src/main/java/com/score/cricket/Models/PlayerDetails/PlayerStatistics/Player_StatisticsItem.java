package com.score.cricket.Models.PlayerDetails.PlayerStatistics;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Player_StatisticsItem {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;


    public Player_StatisticsItem() {
    }

    /**
     *
     * @param name
     * @param value
     */
    public Player_StatisticsItem(String name, String value) {
        super();
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}