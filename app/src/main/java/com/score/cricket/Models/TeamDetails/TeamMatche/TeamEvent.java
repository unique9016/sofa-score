package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamEvent {

    @SerializedName("status")
    @Expose
    private TeamStatus status;
    @SerializedName("winnerCode")
    @Expose
    private Integer winnerCode;
    @SerializedName("homeTeam")
    @Expose
    private TeamHomeTeam homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private TeamAwayTeam awayTeam;
    @SerializedName("homeScore")
    @Expose
    private TeamHomeScore homeScore;
    @SerializedName("awayScore")
    @Expose
    private TeamAwayScore awayScore;
    @SerializedName("time")
    @Expose
    private TeamTime time;
    @SerializedName("changes")
    @Expose
    private TeamChanges changes;
    @SerializedName("hasHighlights")
    @Expose
    private Boolean hasHighlights;
    @SerializedName("hasHighlightsStream")
    @Expose
    private Boolean hasHighlightsStream;
    @SerializedName("hasGlobalHighlights")
    @Expose
    private Boolean hasGlobalHighlights;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("startTimestamp")
    @Expose
    private Integer startTimestamp;
    @SerializedName("statusDescription")
    @Expose
    private String statusDescription;
    @SerializedName("webUrl")
    @Expose
    private String webUrl;
    @SerializedName("hasTime")
    @Expose
    private Boolean hasTime;
    @SerializedName("resultOnly")
    @Expose
    private Boolean resultOnly;

    
    public TeamEvent() {
    }

    /**
     *
     * @param note
     * @param resultOnly
     * @param homeScore
     * @param awayTeam
     * @param changes
     * @param hasHighlightsStream
     * @param hasTime
     * @param winnerCode
     * @param hasGlobalHighlights
     * @param statusDescription
     * @param awayScore
     * @param webUrl
     * @param homeTeam
     * @param time
     * @param id
     * @param hasHighlights
     * @param startTimestamp
     * @param status
     */
    public TeamEvent(TeamStatus status, Integer winnerCode, TeamHomeTeam homeTeam, TeamAwayTeam awayTeam, TeamHomeScore homeScore, TeamAwayScore awayScore, TeamTime time, TeamChanges changes, Boolean hasHighlights, Boolean hasHighlightsStream, Boolean hasGlobalHighlights, Integer id, String note, Integer startTimestamp, String statusDescription, String webUrl, Boolean hasTime, Boolean resultOnly) {
        super();
        this.status = status;
        this.winnerCode = winnerCode;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
        this.time = time;
        this.changes = changes;
        this.hasHighlights = hasHighlights;
        this.hasHighlightsStream = hasHighlightsStream;
        this.hasGlobalHighlights = hasGlobalHighlights;
        this.id = id;
        this.note = note;
        this.startTimestamp = startTimestamp;
        this.statusDescription = statusDescription;
        this.webUrl = webUrl;
        this.hasTime = hasTime;
        this.resultOnly = resultOnly;
    }

    public TeamStatus getStatus() {
        return status;
    }

    public void setStatus(TeamStatus status) {
        this.status = status;
    }

    public Integer getWinnerCode() {
        return winnerCode;
    }

    public void setWinnerCode(Integer winnerCode) {
        this.winnerCode = winnerCode;
    }

    public TeamHomeTeam getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(TeamHomeTeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    public TeamAwayTeam getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(TeamAwayTeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    public TeamHomeScore getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(TeamHomeScore homeScore) {
        this.homeScore = homeScore;
    }

    public TeamAwayScore getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(TeamAwayScore awayScore) {
        this.awayScore = awayScore;
    }

    public TeamTime getTime() {
        return time;
    }

    public void setTime(TeamTime time) {
        this.time = time;
    }

    public TeamChanges getChanges() {
        return changes;
    }

    public void setChanges(TeamChanges changes) {
        this.changes = changes;
    }

    public Boolean getHasHighlights() {
        return hasHighlights;
    }

    public void setHasHighlights(Boolean hasHighlights) {
        this.hasHighlights = hasHighlights;
    }

    public Boolean getHasHighlightsStream() {
        return hasHighlightsStream;
    }

    public void setHasHighlightsStream(Boolean hasHighlightsStream) {
        this.hasHighlightsStream = hasHighlightsStream;
    }

    public Boolean getHasGlobalHighlights() {
        return hasGlobalHighlights;
    }

    public void setHasGlobalHighlights(Boolean hasGlobalHighlights) {
        this.hasGlobalHighlights = hasGlobalHighlights;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Integer startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Boolean getHasTime() {
        return hasTime;
    }

    public void setHasTime(Boolean hasTime) {
        this.hasTime = hasTime;
    }

    public Boolean getResultOnly() {
        return resultOnly;
    }

    public void setResultOnly(Boolean resultOnly) {
        this.resultOnly = resultOnly;
    }

}