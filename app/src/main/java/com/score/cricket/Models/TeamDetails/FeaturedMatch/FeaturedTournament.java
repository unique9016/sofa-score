package com.score.cricket.Models.TeamDetails.FeaturedMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeaturedTournament {

    @SerializedName("tournament")
    @Expose
    private FeaturedTournament_ tournament;
    @SerializedName("category")
    @Expose
    private FeaturedCategory category;
    @SerializedName("season")
    @Expose
    private FeaturedSeason season;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;
    @SerializedName("hasEventPlayerHeatMap")
    @Expose
    private Object hasEventPlayerHeatMap;
    @SerializedName("hasBoxScore")
    @Expose
    private Object hasBoxScore;
    @SerializedName("events")
    @Expose
    private List<FeaturedEvent> events = null;


    public FeaturedTournament() {
    }

    
    public FeaturedTournament(FeaturedTournament_ tournament, FeaturedCategory category, FeaturedSeason season, Boolean hasEventPlayerStatistics, Object hasEventPlayerHeatMap, Object hasBoxScore, List<FeaturedEvent> events) {
        super();
        this.tournament = tournament;
        this.category = category;
        this.season = season;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
        this.hasBoxScore = hasBoxScore;
        this.events = events;
    }

    public FeaturedTournament_ getTournament() {
        return tournament;
    }

    public void setTournament(FeaturedTournament_ tournament) {
        this.tournament = tournament;
    }

    public FeaturedCategory getCategory() {
        return category;
    }

    public void setCategory(FeaturedCategory category) {
        this.category = category;
    }

    public FeaturedSeason getSeason() {
        return season;
    }

    public void setSeason(FeaturedSeason season) {
        this.season = season;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public Object getHasEventPlayerHeatMap() {
        return hasEventPlayerHeatMap;
    }

    public void setHasEventPlayerHeatMap(Object hasEventPlayerHeatMap) {
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
    }

    public Object getHasBoxScore() {
        return hasBoxScore;
    }

    public void setHasBoxScore(Object hasBoxScore) {
        this.hasBoxScore = hasBoxScore;
    }

    public List<FeaturedEvent> getEvents() {
        return events;
    }

    public void setEvents(List<FeaturedEvent> events) {
        this.events = events;
    }

}