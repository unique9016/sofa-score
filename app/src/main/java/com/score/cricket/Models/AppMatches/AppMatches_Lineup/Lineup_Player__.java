package com.score.cricket.Models.AppMatches.AppMatches_Lineup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lineup_Player__ {

    @SerializedName("player")
    @Expose
    private Lineup_Player_ player;
    @SerializedName("substitute")
    @Expose
    private Boolean substitute;
    @SerializedName("position")
    @Expose
    private String position;

    
    public Lineup_Player__() {
    }

    public Lineup_Player__(Lineup_Player_ player, Boolean substitute, String position) {
        super();
        this.player = player;
        this.substitute = substitute;
        this.position = position;
    }

    public Lineup_Player_ getPlayer() {
        return player;
    }

    public void setPlayer(Lineup_Player_ player) {
        this.player = player;
    }

    public Boolean getSubstitute() {
        return substitute;
    }

    public void setSubstitute(Boolean substitute) {
        this.substitute = substitute;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}