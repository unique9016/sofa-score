package com.score.cricket.Models.SingleMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MatchTournament {

    @SerializedName("tournament")
    @Expose
    private MatchTournament_ tournament;
    @SerializedName("category")
    @Expose
    private MatchCategory category;
    @SerializedName("season")
    @Expose
    private MatchSeason season;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;
    @SerializedName("hasEventPlayerHeatMap")
    @Expose
    private Object hasEventPlayerHeatMap;
    @SerializedName("hasBoxScore")
    @Expose
    private Object hasBoxScore;
    @SerializedName("events")
    @Expose
    private List<MatchEvent> events = null;


    public MatchTournament() {
    }

    
    public MatchTournament(MatchTournament_ tournament, MatchCategory category, MatchSeason season, Boolean hasEventPlayerStatistics, Object hasEventPlayerHeatMap, Object hasBoxScore, List<MatchEvent> events) {
        super();
        this.tournament = tournament;
        this.category = category;
        this.season = season;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
        this.hasBoxScore = hasBoxScore;
        this.events = events;
    }

    public MatchTournament_ getTournament() {
        return tournament;
    }

    public void setTournament(MatchTournament_ tournament) {
        this.tournament = tournament;
    }

    public MatchCategory getCategory() {
        return category;
    }

    public void setCategory(MatchCategory category) {
        this.category = category;
    }

    public MatchSeason getSeason() {
        return season;
    }

    public void setSeason(MatchSeason season) {
        this.season = season;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public Object getHasEventPlayerHeatMap() {
        return hasEventPlayerHeatMap;
    }

    public void setHasEventPlayerHeatMap(Object hasEventPlayerHeatMap) {
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
    }

    public Object getHasBoxScore() {
        return hasBoxScore;
    }

    public void setHasBoxScore(Object hasBoxScore) {
        this.hasBoxScore = hasBoxScore;
    }

    public List<MatchEvent> getEvents() {
        return events;
    }

    public void setEvents(List<MatchEvent> events) {
        this.events = events;
    }

}