package com.score.cricket.Models.AllDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailsTeamOfTheWeek {

    @SerializedName("seasonId")
    @Expose
    private Integer seasonId;
    @SerializedName("rounds")
    @Expose
    private List<DetailsRound> rounds = null;


    public DetailsTeamOfTheWeek() {
    }

    public DetailsTeamOfTheWeek(Integer seasonId, List<DetailsRound> rounds) {
        super();
        this.seasonId = seasonId;
        this.rounds = rounds;
    }

    public Integer getSeasonId() {
        return seasonId;
    }

    public void setSeasonId(Integer seasonId) {
        this.seasonId = seasonId;
    }

    public List<DetailsRound> getRounds() {
        return rounds;
    }

    public void setRounds(List<DetailsRound> rounds) {
        this.rounds = rounds;
    }

}