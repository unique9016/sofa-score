package com.score.cricket.Models.HomeMain;

import com.score.cricket.Listener.ListnerItems;

public class HomeMatcheChildInfo implements ListnerItems {

    String name, hometeam, awayteam, homescore, awayscore, homewicket, awaywicket, homeover, awayover, date, time, status, matchstatus, currentBattingTeam;
    int id, seasonid, tournamentid, winnercode, position, main_id;
    long timestamp;

    public HomeMatcheChildInfo() {
    }

    public HomeMatcheChildInfo(String name, String hometeam, String awayteam, String homescore, String awayscore, String homewicket, String awaywicket, String homeover, String awayover, String date, String time, String status, String matchstatus, String currentBattingTeam, int id, int seasonid, int tournamentid, int winnercode, int position, int main_id, long timestamp) {
        this.name = name;
        this.hometeam = hometeam;
        this.awayteam = awayteam;
        this.homescore = homescore;
        this.awayscore = awayscore;
        this.homewicket = homewicket;
        this.awaywicket = awaywicket;
        this.homeover = homeover;
        this.awayover = awayover;
        this.date = date;
        this.time = time;
        this.status = status;
        this.matchstatus = matchstatus;
        this.currentBattingTeam = currentBattingTeam;
        this.id = id;
        this.seasonid = seasonid;
        this.tournamentid = tournamentid;
        this.winnercode = winnercode;
        this.position = position;
        this.main_id = main_id;
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHometeam() {
        return hometeam;
    }

    public void setHometeam(String hometeam) {
        this.hometeam = hometeam;
    }

    public String getAwayteam() {
        return awayteam;
    }

    public void setAwayteam(String awayteam) {
        this.awayteam = awayteam;
    }

    public String getHomescore() {
        return homescore;
    }

    public void setHomescore(String homescore) {
        this.homescore = homescore;
    }

    public String getAwayscore() {
        return awayscore;
    }

    public void setAwayscore(String awayscore) {
        this.awayscore = awayscore;
    }

    public String getHomewicket() {
        return homewicket;
    }

    public void setHomewicket(String homewicket) {
        this.homewicket = homewicket;
    }

    public String getAwaywicket() {
        return awaywicket;
    }

    public void setAwaywicket(String awaywicket) {
        this.awaywicket = awaywicket;
    }

    public String getHomeover() {
        return homeover;
    }

    public void setHomeover(String homeover) {
        this.homeover = homeover;
    }

    public String getAwayover() {
        return awayover;
    }

    public void setAwayover(String awayover) {
        this.awayover = awayover;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMatchstatus() {
        return matchstatus;
    }

    public void setMatchstatus(String matchstatus) {
        this.matchstatus = matchstatus;
    }

    public String getCurrentBattingTeam() {
        return currentBattingTeam;
    }

    public void setCurrentBattingTeam(String currentBattingTeam) {
        this.currentBattingTeam = currentBattingTeam;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeasonid() {
        return seasonid;
    }

    public void setSeasonid(int seasonid) {
        this.seasonid = seasonid;
    }

    public int getTournamentid() {
        return tournamentid;
    }

    public void setTournamentid(int tournamentid) {
        this.tournamentid = tournamentid;
    }

    public int getWinnercode() {
        return winnercode;
    }

    public void setWinnercode(int winnercode) {
        this.winnercode = winnercode;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getMain_id() {
        return main_id;
    }

    public void setMain_id(int main_id) {
        this.main_id = main_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public int getItemType() {
        return 0;
    }
}