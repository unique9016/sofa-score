package com.score.cricket.Models.AppMatches.AppMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MatchesCategory {

    @SerializedName("category")
    @Expose
    private MatchesCategory_ category;
    @SerializedName("totalEvents")
    @Expose
    private Integer totalEvents;
    @SerializedName("totalVideos")
    @Expose
    private Integer totalVideos;
    @SerializedName("totalEventPlayerStatistics")
    @Expose
    private Integer totalEventPlayerStatistics;
    @SerializedName("uniqueTournamentIds")
    @Expose
    private List<Integer> uniqueTournamentIds = null;


    public MatchesCategory() {
    }

    /**
     *
     * @param totalEvents
     * @param totalEventPlayerStatistics
     * @param totalVideos
     * @param category
     * @param uniqueTournamentIds
     */
    public MatchesCategory(MatchesCategory_ category, Integer totalEvents, Integer totalVideos, Integer totalEventPlayerStatistics, List<Integer> uniqueTournamentIds) {
        super();
        this.category = category;
        this.totalEvents = totalEvents;
        this.totalVideos = totalVideos;
        this.totalEventPlayerStatistics = totalEventPlayerStatistics;
        this.uniqueTournamentIds = uniqueTournamentIds;
    }

    public MatchesCategory_ getCategory() {
        return category;
    }

    public void setCategory(MatchesCategory_ category) {
        this.category = category;
    }

    public Integer getTotalEvents() {
        return totalEvents;
    }

    public void setTotalEvents(Integer totalEvents) {
        this.totalEvents = totalEvents;
    }

    public Integer getTotalVideos() {
        return totalVideos;
    }

    public void setTotalVideos(Integer totalVideos) {
        this.totalVideos = totalVideos;
    }

    public Integer getTotalEventPlayerStatistics() {
        return totalEventPlayerStatistics;
    }

    public void setTotalEventPlayerStatistics(Integer totalEventPlayerStatistics) {
        this.totalEventPlayerStatistics = totalEventPlayerStatistics;
    }

    public List<Integer> getUniqueTournamentIds() {
        return uniqueTournamentIds;
    }

    public void setUniqueTournamentIds(List<Integer> uniqueTournamentIds) {
        this.uniqueTournamentIds = uniqueTournamentIds;
    }

}