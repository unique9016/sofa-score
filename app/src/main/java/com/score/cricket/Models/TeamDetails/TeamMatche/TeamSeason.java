package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamSeason {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("id")
    @Expose
    private Integer id;


    public TeamSeason() {
    }

    
    public TeamSeason(String name, String year, Integer id) {
        super();
        this.name = name;
        this.year = year;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}