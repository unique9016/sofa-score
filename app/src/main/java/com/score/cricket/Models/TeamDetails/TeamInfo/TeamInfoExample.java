package com.score.cricket.Models.TeamDetails.TeamInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

import java.util.List;

public class TeamInfoExample {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;
    @SerializedName("venue")
    @Expose
    private TeamInfoVenue venue;
    @SerializedName("nameCode")
    @Expose
    private String nameCode;
    @SerializedName("national")
    @Expose
    private Boolean national;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("countryIOC")
    @Expose
    private String countryIOC;
    @SerializedName("countryISO")
    @Expose
    private String countryISO;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("teamColors")
    @Expose
    private TeamInfoTeamColors teamColors;
    @SerializedName("tournamentInfo")
    @Expose
    private List<TeamInfoTournamentInfo> tournamentInfo = null;
    @SerializedName("totalPlayers")
    @Expose
    private Integer totalPlayers;
    @SerializedName("country")
    @Expose
    private String country;

    
    public TeamInfoExample() {
    }

    /**
     *
     * @param venue
     * @param country
     * @param flag
     * @param totalPlayers
     * @param fullName
     * @param teamColors
     * @param tournamentInfo
     * @param userCount
     * @param nameCode
     * @param countryISO
     * @param name
     * @param national
     * @param id
     * @param shortName
     * @param sport
     * @param countryIOC
     * @param slug
     */
    public TeamInfoExample(String name, String slug, AppSport sport, Integer userCount, TeamInfoVenue venue, String nameCode, Boolean national, Integer id, String shortName, String countryIOC, String countryISO, String fullName, String flag, TeamInfoTeamColors teamColors,
                           List<TeamInfoTournamentInfo> tournamentInfo, Integer totalPlayers, String country) {
        super();
        this.name = name;
        this.slug = slug;
        this.sport = sport;
        this.userCount = userCount;
        this.venue = venue;
        this.nameCode = nameCode;
        this.national = national;
        this.id = id;
        this.shortName = shortName;
        this.countryIOC = countryIOC;
        this.countryISO = countryISO;
        this.fullName = fullName;
        this.flag = flag;
        this.teamColors = teamColors;
        this.tournamentInfo = tournamentInfo;
        this.totalPlayers = totalPlayers;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public TeamInfoVenue getVenue() {
        return venue;
    }

    public void setVenue(TeamInfoVenue venue) {
        this.venue = venue;
    }

    public String getNameCode() {
        return nameCode;
    }

    public void setNameCode(String nameCode) {
        this.nameCode = nameCode;
    }

    public Boolean getNational() {
        return national;
    }

    public void setNational(Boolean national) {
        this.national = national;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getCountryIOC() {
        return countryIOC;
    }

    public void setCountryIOC(String countryIOC) {
        this.countryIOC = countryIOC;
    }

    public String getCountryISO() {
        return countryISO;
    }

    public void setCountryISO(String countryISO) {
        this.countryISO = countryISO;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public TeamInfoTeamColors getTeamColors() {
        return teamColors;
    }

    public void setTeamColors(TeamInfoTeamColors teamColors) {
        this.teamColors = teamColors;
    }

    public List<TeamInfoTournamentInfo> getTournamentInfo() {
        return tournamentInfo;
    }

    public void setTournamentInfo(List<TeamInfoTournamentInfo> tournamentInfo) {
        this.tournamentInfo = tournamentInfo;
    }

    public Integer getTotalPlayers() {
        return totalPlayers;
    }

    public void setTotalPlayers(Integer totalPlayers) {
        this.totalPlayers = totalPlayers;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}