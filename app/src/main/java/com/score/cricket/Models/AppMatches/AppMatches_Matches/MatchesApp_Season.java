package com.score.cricket.Models.AppMatches.AppMatches_Matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchesApp_Season {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("id")
    @Expose
    private Integer id;


    public MatchesApp_Season() {
    }

    
    public MatchesApp_Season(String name, String year, Integer id) {
        super();
        this.name = name;
        this.year = year;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}