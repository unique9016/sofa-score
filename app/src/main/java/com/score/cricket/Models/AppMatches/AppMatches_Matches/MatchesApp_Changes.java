package com.score.cricket.Models.AppMatches.AppMatches_Matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MatchesApp_Changes {

    @SerializedName("changes")
    @Expose
    private List<String> changes = null;
    @SerializedName("changeTimestamp")
    @Expose
    private Integer changeTimestamp;


    public MatchesApp_Changes() {
    }

    
    public MatchesApp_Changes(List<String> changes, Integer changeTimestamp) {
        super();
        this.changes = changes;
        this.changeTimestamp = changeTimestamp;
    }

    public List<String> getChanges() {
        return changes;
    }

    public void setChanges(List<String> changes) {
        this.changes = changes;
    }

    public Integer getChangeTimestamp() {
        return changeTimestamp;
    }

    public void setChangeTimestamp(Integer changeTimestamp) {
        this.changeTimestamp = changeTimestamp;
    }

}