package com.score.cricket.Models.AllDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailsTournament {

    @SerializedName("tournament")
    @Expose
    private DetailsTournament_ tournament;
    @SerializedName("category")
    @Expose
    private DetailsCategory category;
    @SerializedName("season")
    @Expose
    private DetailsSeason season;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;
    @SerializedName("hasEventPlayerHeatMap")
    @Expose
    private Object hasEventPlayerHeatMap;
    @SerializedName("hasBoxScore")
    @Expose
    private Object hasBoxScore;
    @SerializedName("events")
    @Expose
    private List<DetailsEvent> events = null;


    public DetailsTournament() {
    }


    public DetailsTournament(DetailsTournament_ tournament, DetailsCategory category, DetailsSeason season, Boolean hasEventPlayerStatistics, Object hasEventPlayerHeatMap, Object hasBoxScore, List<DetailsEvent> events) {
        super();
        this.tournament = tournament;
        this.category = category;
        this.season = season;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
        this.hasBoxScore = hasBoxScore;
        this.events = events;
    }

    public DetailsTournament_ getTournament() {
        return tournament;
    }

    public void setTournament(DetailsTournament_ tournament) {
        this.tournament = tournament;
    }

    public DetailsCategory getCategory() {
        return category;
    }

    public void setCategory(DetailsCategory category) {
        this.category = category;
    }

    public DetailsSeason getSeason() {
        return season;
    }

    public void setSeason(DetailsSeason season) {
        this.season = season;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public Object getHasEventPlayerHeatMap() {
        return hasEventPlayerHeatMap;
    }

    public void setHasEventPlayerHeatMap(Object hasEventPlayerHeatMap) {
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
    }

    public Object getHasBoxScore() {
        return hasBoxScore;
    }

    public void setHasBoxScore(Object hasBoxScore) {
        this.hasBoxScore = hasBoxScore;
    }

    public List<DetailsEvent> getEvents() {
        return events;
    }

    public void setEvents(List<DetailsEvent> events) {
        this.events = events;
    }

}