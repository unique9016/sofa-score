package com.score.cricket.Models.HomeMain;

public class HomeMatch_inning_partnership {

    String player1,player2;
    int index,score,ball;

    public HomeMatch_inning_partnership() {
    }

    public HomeMatch_inning_partnership(String player1, String player2, int index, int score, int ball) {
        this.player1 = player1;
        this.player2 = player2;
        this.index = index;
        this.score = score;
        this.ball = ball;
    }

    public String getPlayer1() {
        return player1;
    }

    public void setPlayer1(String player1) {
        this.player1 = player1;
    }

    public String getPlayer2() {
        return player2;
    }

    public void setPlayer2(String player2) {
        this.player2 = player2;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getBall() {
        return ball;
    }

    public void setBall(int ball) {
        this.ball = ball;
    }
}
