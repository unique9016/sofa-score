package com.score.cricket.Models.AllDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailsMain {

    @SerializedName("host")
    @Expose
    private DetailsHost host;
    @SerializedName("primaryColor")
    @Expose
    private String primaryColor;
    @SerializedName("secondaryColor")
    @Expose
    private String secondaryColor;
    @SerializedName("endDate")
    @Expose
    private Object endDate;
    @SerializedName("featuredMatches")
    @Expose
    private DetailsFeaturedMatches featuredMatches;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;

    @SerializedName("titleHolder")
    @Expose
    private DetailsTitleHolder titleHolder;
    @SerializedName("titleHolderTitles")
    @Expose
    private Integer titleHolderTitles;
    @SerializedName("mostTitles")
    @Expose
    private DetailsMostTitles mostTitles;
    @SerializedName("facts")
    @Expose
    private List<DetailsFact> facts = null;
    @SerializedName("startDate")
    @Expose
    private Integer startDate;
    @SerializedName("teamOfTheWeek")
    @Expose
    private DetailsTeamOfTheWeek teamOfTheWeek;




    public DetailsMain() {
    }


    public DetailsMain(DetailsHost host, String primaryColor, String secondaryColor, Object endDate, DetailsFeaturedMatches featuredMatches, Integer userCount, DetailsTitleHolder titleHolder, Integer titleHolderTitles, DetailsMostTitles mostTitles, List<DetailsFact> facts, Integer startDate, DetailsTeamOfTheWeek teamOfTheWeek) {
        this.host = host;
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
        this.endDate = endDate;
        this.featuredMatches = featuredMatches;
        this.userCount = userCount;
        this.titleHolder = titleHolder;
        this.titleHolderTitles = titleHolderTitles;
        this.mostTitles = mostTitles;
        this.facts = facts;
        this.startDate = startDate;
        this.teamOfTheWeek = teamOfTheWeek;
    }

    public DetailsHost getHost() {
        return host;
    }

    public void setHost(DetailsHost host) {
        this.host = host;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public String getSecondaryColor() {
        return secondaryColor;
    }

    public void setSecondaryColor(String secondaryColor) {
        this.secondaryColor = secondaryColor;
    }

    public Object getEndDate() {
        return endDate;
    }

    public void setEndDate(Object endDate) {
        this.endDate = endDate;
    }

    public DetailsFeaturedMatches getFeaturedMatches() {
        return featuredMatches;
    }

    public void setFeaturedMatches(DetailsFeaturedMatches featuredMatches) {
        this.featuredMatches = featuredMatches;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public DetailsTitleHolder getTitleHolder() {
        return titleHolder;
    }

    public void setTitleHolder(DetailsTitleHolder titleHolder) {
        this.titleHolder = titleHolder;
    }

    public Integer getTitleHolderTitles() {
        return titleHolderTitles;
    }

    public void setTitleHolderTitles(Integer titleHolderTitles) {
        this.titleHolderTitles = titleHolderTitles;
    }

    public DetailsMostTitles getMostTitles() {
        return mostTitles;
    }

    public void setMostTitles(DetailsMostTitles mostTitles) {
        this.mostTitles = mostTitles;
    }

    public List<DetailsFact> getFacts() {
        return facts;
    }

    public void setFacts(List<DetailsFact> facts) {
        this.facts = facts;
    }

    public Integer getStartDate() {
        return startDate;
    }

    public void setStartDate(Integer startDate) {
        this.startDate = startDate;
    }

    public DetailsTeamOfTheWeek getTeamOfTheWeek() {
        return teamOfTheWeek;
    }

    public void setTeamOfTheWeek(DetailsTeamOfTheWeek teamOfTheWeek) {
        this.teamOfTheWeek = teamOfTheWeek;
    }
}