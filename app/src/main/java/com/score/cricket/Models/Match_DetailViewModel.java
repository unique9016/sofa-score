package com.score.cricket.Models;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.score.cricket.Models.AppMatches.AppMatches_Details.Details_Example;
import com.score.cricket.SubScreen.FragMatchInformation2;


public class Match_DetailViewModel extends ViewModel {

    public FragMatchInformation2.Match_DetailRepository newRepository;
    private MutableLiveData<Details_Example> mutableLiveData;

    public void init() {
        if (mutableLiveData != null) {
            return;
        }
        newRepository = FragMatchInformation2.Match_DetailRepository.getInstance();
        mutableLiveData = newRepository.getNews();

    }

    public LiveData<Details_Example> getNewsRepository() {
        return mutableLiveData;
    }
}
