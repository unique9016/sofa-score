package com.score.cricket.Models.SingleMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

import java.util.List;

public class MatchExample {

    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("rows")
    @Expose
    private Integer rows;
    @SerializedName("tournaments")
    @Expose
    private List<MatchTournament> tournaments = null;


    public MatchExample() {
    }

    /**
     *
     * @param tournaments
     * @param rows
     * @param sport
     */
    public MatchExample(AppSport sport, Integer rows, List<MatchTournament> tournaments) {
        super();
        this.sport = sport;
        this.rows = rows;
        this.tournaments = tournaments;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public List<MatchTournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<MatchTournament> tournaments) {
        this.tournaments = tournaments;
    }

}