
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics24 {

    @SerializedName("cleanSheet")
    @Expose
    private Integer cleanSheet;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;


    public Top_Statistics24() {
    }

    public Top_Statistics24(Integer cleanSheet, Integer id, String type, Integer appearances) {
        super();
        this.cleanSheet = cleanSheet;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getCleanSheet() {
        return cleanSheet;
    }

    public void setCleanSheet(Integer cleanSheet) {
        this.cleanSheet = cleanSheet;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
