package com.score.cricket.Models.AllDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailsAwayScore {
    @SerializedName("current")
    @Expose
    private Integer current;
    @SerializedName("display")
    @Expose
    private Integer display;
    @SerializedName("innings")
    @Expose
    private DetailsInnings innings;
    @SerializedName("hits")
    @Expose
    private Integer hits;
    @SerializedName("errors")
    @Expose
    private Integer errors;
    @SerializedName("period1")
    @Expose
    private Integer period1;
    @SerializedName("normaltime")
    @Expose
    private Integer normaltime;

    public DetailsAwayScore() {
    }

    public DetailsAwayScore(Integer current, Integer display, DetailsInnings innings, Integer hits, Integer errors, Integer period1, Integer normaltime) {
        this.current = current;
        this.display = display;
        this.innings = innings;
        this.hits = hits;
        this.errors = errors;
        this.period1 = period1;
        this.normaltime = normaltime;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getDisplay() {
        return display;
    }

    public void setDisplay(Integer display) {
        this.display = display;
    }

    public DetailsInnings getInnings() {
        return innings;
    }

    public void setInnings(DetailsInnings innings) {
        this.innings = innings;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    public Integer getErrors() {
        return errors;
    }

    public void setErrors(Integer errors) {
        this.errors = errors;
    }

    public Integer getPeriod1() {
        return period1;
    }

    public void setPeriod1(Integer period1) {
        this.period1 = period1;
    }

    public Integer getNormaltime() {
        return normaltime;
    }

    public void setNormaltime(Integer normaltime) {
        this.normaltime = normaltime;
    }
}