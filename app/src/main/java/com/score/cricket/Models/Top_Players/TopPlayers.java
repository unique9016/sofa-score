
package com.score.cricket.Models.Top_Players;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopPlayers {

    @SerializedName("rating")
    @Expose
    private List<Top_Rating> rating = null;
    @SerializedName("goals")
    @Expose
    private List<Top_Goal> goals = null;
    @SerializedName("assists")
    @Expose
    private List<Top_Assist> assists = null;
    @SerializedName("penaltyGoals")
    @Expose
    private List<Top_PenaltyGoal> penaltyGoals = null;
    @SerializedName("goalsAssistsSum")
    @Expose
    private List<Top_GoalsAssistsSum> goalsAssistsSum = null;
    @SerializedName("freeKickGoal")
    @Expose
    private List<Top_FreeKickGoal> freeKickGoal = null;
    @SerializedName("scoringFrequency")
    @Expose
    private List<Top_ScoringFrequency> scoringFrequency = null;
    @SerializedName("totalShots")
    @Expose
    private List<Top_TotalShot> totalShots = null;
    @SerializedName("bigChancesMissed")
    @Expose
    private List<Top_BigChancesMissed> bigChancesMissed = null;
    @SerializedName("bigChancesCreated")
    @Expose
    private List<Top_BigChancesCreated> bigChancesCreated = null;
    @SerializedName("accuratePasses")
    @Expose
    private List<Top_AccuratePass> accuratePasses = null;
    @SerializedName("keyPasses")
    @Expose
    private List<Top_KeyPass> keyPasses = null;
    @SerializedName("accurateLongBalls")
    @Expose
    private List<Top_AccurateLongBall> accurateLongBalls = null;
    @SerializedName("successfulDribbles")
    @Expose
    private List<Top_SuccessfulDribble> successfulDribbles = null;
    @SerializedName("penaltyWon")
    @Expose
    private List<Top_PenaltyWon> penaltyWon = null;
    @SerializedName("tackles")
    @Expose
    private List<Top_Tackle> tackles = null;
    @SerializedName("interceptions")
    @Expose
    private List<Top_Interception> interceptions = null;
    @SerializedName("clearances")
    @Expose
    private List<Top_Clearance> clearances = null;
    @SerializedName("possessionLost")
    @Expose
    private List<Top_PossessionLost> possessionLost = null;
    @SerializedName("yellowCards")
    @Expose
    private List<Top_YellowCard> yellowCards = null;
    @SerializedName("redCards")
    @Expose
    private List<Top_RedCard> redCards = null;
    @SerializedName("saves")
    @Expose
    private List<Top_Safe> saves = null;
    @SerializedName("mostConceded")
    @Expose
    private List<Top_MostConceded> mostConceded = null;
    @SerializedName("leastConceded")
    @Expose
    private List<Top_LeastConceded> leastConceded = null;
    @SerializedName("cleanSheet")
    @Expose
    private List<Top_CleanSheet> cleanSheet = null;


    public TopPlayers() {
    }

    public TopPlayers(List<Top_Rating> rating, List<Top_Goal> goals, List<Top_Assist> assists, List<Top_PenaltyGoal> penaltyGoals, List<Top_GoalsAssistsSum> goalsAssistsSum, List<Top_FreeKickGoal> freeKickGoal, List<Top_ScoringFrequency> scoringFrequency, List<Top_TotalShot> totalShots, List<Top_BigChancesMissed> bigChancesMissed, List<Top_BigChancesCreated> bigChancesCreated, List<Top_AccuratePass> accuratePasses, List<Top_KeyPass> keyPasses, List<Top_AccurateLongBall> accurateLongBalls, List<Top_SuccessfulDribble> successfulDribbles, List<Top_PenaltyWon> penaltyWon, List<Top_Tackle> tackles, List<Top_Interception> interceptions, List<Top_Clearance> clearances, List<Top_PossessionLost> possessionLost, List<Top_YellowCard> yellowCards, List<Top_RedCard> redCards, List<Top_Safe> saves, List<Top_MostConceded> mostConceded, List<Top_LeastConceded> leastConceded, List<Top_CleanSheet> cleanSheet) {
        super();
        this.rating = rating;
        this.goals = goals;
        this.assists = assists;
        this.penaltyGoals = penaltyGoals;
        this.goalsAssistsSum = goalsAssistsSum;
        this.freeKickGoal = freeKickGoal;
        this.scoringFrequency = scoringFrequency;
        this.totalShots = totalShots;
        this.bigChancesMissed = bigChancesMissed;
        this.bigChancesCreated = bigChancesCreated;
        this.accuratePasses = accuratePasses;
        this.keyPasses = keyPasses;
        this.accurateLongBalls = accurateLongBalls;
        this.successfulDribbles = successfulDribbles;
        this.penaltyWon = penaltyWon;
        this.tackles = tackles;
        this.interceptions = interceptions;
        this.clearances = clearances;
        this.possessionLost = possessionLost;
        this.yellowCards = yellowCards;
        this.redCards = redCards;
        this.saves = saves;
        this.mostConceded = mostConceded;
        this.leastConceded = leastConceded;
        this.cleanSheet = cleanSheet;
    }

    public List<Top_Rating> getRating() {
        return rating;
    }

    public void setRating(List<Top_Rating> rating) {
        this.rating = rating;
    }

    public List<Top_Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Top_Goal> goals) {
        this.goals = goals;
    }

    public List<Top_Assist> getAssists() {
        return assists;
    }

    public void setAssists(List<Top_Assist> assists) {
        this.assists = assists;
    }

    public List<Top_PenaltyGoal> getPenaltyGoals() {
        return penaltyGoals;
    }

    public void setPenaltyGoals(List<Top_PenaltyGoal> penaltyGoals) {
        this.penaltyGoals = penaltyGoals;
    }

    public List<Top_GoalsAssistsSum> getGoalsAssistsSum() {
        return goalsAssistsSum;
    }

    public void setGoalsAssistsSum(List<Top_GoalsAssistsSum> goalsAssistsSum) {
        this.goalsAssistsSum = goalsAssistsSum;
    }

    public List<Top_FreeKickGoal> getFreeKickGoal() {
        return freeKickGoal;
    }

    public void setFreeKickGoal(List<Top_FreeKickGoal> freeKickGoal) {
        this.freeKickGoal = freeKickGoal;
    }

    public List<Top_ScoringFrequency> getScoringFrequency() {
        return scoringFrequency;
    }

    public void setScoringFrequency(List<Top_ScoringFrequency> scoringFrequency) {
        this.scoringFrequency = scoringFrequency;
    }

    public List<Top_TotalShot> getTotalShots() {
        return totalShots;
    }

    public void setTotalShots(List<Top_TotalShot> totalShots) {
        this.totalShots = totalShots;
    }

    public List<Top_BigChancesMissed> getBigChancesMissed() {
        return bigChancesMissed;
    }

    public void setBigChancesMissed(List<Top_BigChancesMissed> bigChancesMissed) {
        this.bigChancesMissed = bigChancesMissed;
    }

    public List<Top_BigChancesCreated> getBigChancesCreated() {
        return bigChancesCreated;
    }

    public void setBigChancesCreated(List<Top_BigChancesCreated> bigChancesCreated) {
        this.bigChancesCreated = bigChancesCreated;
    }

    public List<Top_AccuratePass> getAccuratePasses() {
        return accuratePasses;
    }

    public void setAccuratePasses(List<Top_AccuratePass> accuratePasses) {
        this.accuratePasses = accuratePasses;
    }

    public List<Top_KeyPass> getKeyPasses() {
        return keyPasses;
    }

    public void setKeyPasses(List<Top_KeyPass> keyPasses) {
        this.keyPasses = keyPasses;
    }

    public List<Top_AccurateLongBall> getAccurateLongBalls() {
        return accurateLongBalls;
    }

    public void setAccurateLongBalls(List<Top_AccurateLongBall> accurateLongBalls) {
        this.accurateLongBalls = accurateLongBalls;
    }

    public List<Top_SuccessfulDribble> getSuccessfulDribbles() {
        return successfulDribbles;
    }

    public void setSuccessfulDribbles(List<Top_SuccessfulDribble> successfulDribbles) {
        this.successfulDribbles = successfulDribbles;
    }

    public List<Top_PenaltyWon> getPenaltyWon() {
        return penaltyWon;
    }

    public void setPenaltyWon(List<Top_PenaltyWon> penaltyWon) {
        this.penaltyWon = penaltyWon;
    }

    public List<Top_Tackle> getTackles() {
        return tackles;
    }

    public void setTackles(List<Top_Tackle> tackles) {
        this.tackles = tackles;
    }

    public List<Top_Interception> getInterceptions() {
        return interceptions;
    }

    public void setInterceptions(List<Top_Interception> interceptions) {
        this.interceptions = interceptions;
    }

    public List<Top_Clearance> getClearances() {
        return clearances;
    }

    public void setClearances(List<Top_Clearance> clearances) {
        this.clearances = clearances;
    }

    public List<Top_PossessionLost> getPossessionLost() {
        return possessionLost;
    }

    public void setPossessionLost(List<Top_PossessionLost> possessionLost) {
        this.possessionLost = possessionLost;
    }

    public List<Top_YellowCard> getYellowCards() {
        return yellowCards;
    }

    public void setYellowCards(List<Top_YellowCard> yellowCards) {
        this.yellowCards = yellowCards;
    }

    public List<Top_RedCard> getRedCards() {
        return redCards;
    }

    public void setRedCards(List<Top_RedCard> redCards) {
        this.redCards = redCards;
    }

    public List<Top_Safe> getSaves() {
        return saves;
    }

    public void setSaves(List<Top_Safe> saves) {
        this.saves = saves;
    }

    public List<Top_MostConceded> getMostConceded() {
        return mostConceded;
    }

    public void setMostConceded(List<Top_MostConceded> mostConceded) {
        this.mostConceded = mostConceded;
    }

    public List<Top_LeastConceded> getLeastConceded() {
        return leastConceded;
    }

    public void setLeastConceded(List<Top_LeastConceded> leastConceded) {
        this.leastConceded = leastConceded;
    }

    public List<Top_CleanSheet> getCleanSheet() {
        return cleanSheet;
    }

    public void setCleanSheet(List<Top_CleanSheet> cleanSheet) {
        this.cleanSheet = cleanSheet;
    }

}
