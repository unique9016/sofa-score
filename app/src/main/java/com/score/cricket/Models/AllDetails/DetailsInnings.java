package com.score.cricket.Models.AllDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailsInnings {

    @SerializedName("inning1")
    @Expose
    private DetailsInning1 inning1;


    public DetailsInnings() {
    }

    public DetailsInnings(DetailsInning1 inning1) {
        super();
        this.inning1 = inning1;
    }

    public DetailsInning1 getInning1() {
        return inning1;
    }

    public void setInning1(DetailsInning1 inning1) {
        this.inning1 = inning1;
    }

}