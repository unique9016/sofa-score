package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayerTournament {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("category")
    @Expose
    private PlayerCategory category;
    @SerializedName("uniqueTournament")
    @Expose
    private PlayerUniqueTournament uniqueTournament;
    @SerializedName("id")
    @Expose
    private Integer id;

    
    public PlayerTournament() {
    }

    /**
     *
     * @param uniqueTournament
     * @param name
     * @param id
     * @param category
     * @param slug
     */
    public PlayerTournament(String name, String slug, PlayerCategory category, PlayerUniqueTournament uniqueTournament, Integer id) {
        super();
        this.name = name;
        this.slug = slug;
        this.category = category;
        this.uniqueTournament = uniqueTournament;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public PlayerCategory getCategory() {
        return category;
    }

    public void setCategory(PlayerCategory category) {
        this.category = category;
    }

    public PlayerUniqueTournament getUniqueTournament() {
        return uniqueTournament;
    }

    public void setUniqueTournament(PlayerUniqueTournament uniqueTournament) {
        this.uniqueTournament = uniqueTournament;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}