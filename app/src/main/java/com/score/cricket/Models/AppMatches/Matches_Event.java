package com.score.cricket.Models.AppMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Matches_Event {

    @SerializedName("status")
    @Expose
    private Matches_Status status;
    @SerializedName("winnerCode")
    @Expose
    private Integer winnerCode;
    @SerializedName("homeTeam")
    @Expose
    private Matches_HomeTeam homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private Matches_AwayTeam awayTeam;
    @SerializedName("homeScore")
    @Expose
    private Matches_HomeScore homeScore;
    @SerializedName("awayScore")
    @Expose
    private Matches_AwayScore awayScore;
    @SerializedName("time")
    @Expose
    private Matches_Time time;
    @SerializedName("changes")
    @Expose
    private Matches_Changes changes;
    @SerializedName("hasHighlights")
    @Expose
    private Boolean hasHighlights;
    @SerializedName("hasHighlightsStream")
    @Expose
    private Boolean hasHighlightsStream;
    @SerializedName("hasGlobalHighlights")
    @Expose
    private Boolean hasGlobalHighlights;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("startTimestamp")
    @Expose
    private Integer startTimestamp;
    @SerializedName("endTimestamp")
    @Expose
    private Integer endTimestamp;
    @SerializedName("statusDescription")
    @Expose
    private String statusDescription;
    @SerializedName("webUrl")
    @Expose
    private String webUrl;
    @SerializedName("hasTime")
    @Expose
    private Boolean hasTime;
    @SerializedName("resultOnly")
    @Expose
    private Boolean resultOnly;
    @SerializedName("currentBowler")
    @Expose
    private Matches_CurrentBatsman currentBowler;
    @SerializedName("currentBatsman")
    @Expose
    private Matches_CurrentBatsman currentBatsman;
    @SerializedName("currentBattingTeam")
    @Expose
    private Matches_CurrentBattingTeam currentBattingTeam;
    @SerializedName("currentBowlingTeam")
    @Expose
    private Matches_CurrentBattingTeam currentBowlingTeam;


    public Matches_Event() {
    }

    public Matches_Event(Matches_Status status, Integer winnerCode, Matches_HomeTeam homeTeam, Matches_AwayTeam awayTeam, Matches_HomeScore homeScore, Matches_AwayScore awayScore, Matches_Time time, Matches_Changes changes, Boolean hasHighlights, Boolean hasHighlightsStream, Boolean hasGlobalHighlights, Integer id, String note, Integer startTimestamp, Integer endTimestamp, String statusDescription, String webUrl, Boolean hasTime, Boolean resultOnly, Matches_CurrentBatsman currentBowler, Matches_CurrentBatsman currentBatsman, Matches_CurrentBattingTeam currentBattingTeam, Matches_CurrentBattingTeam currentBowlingTeam) {
        this.status = status;
        this.winnerCode = winnerCode;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
        this.time = time;
        this.changes = changes;
        this.hasHighlights = hasHighlights;
        this.hasHighlightsStream = hasHighlightsStream;
        this.hasGlobalHighlights = hasGlobalHighlights;
        this.id = id;
        this.note = note;
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        this.statusDescription = statusDescription;
        this.webUrl = webUrl;
        this.hasTime = hasTime;
        this.resultOnly = resultOnly;
        this.currentBowler = currentBowler;
        this.currentBatsman = currentBatsman;
        this.currentBattingTeam = currentBattingTeam;
        this.currentBowlingTeam = currentBowlingTeam;
    }

    public Matches_Status getStatus() {
        return status;
    }

    public void setStatus(Matches_Status status) {
        this.status = status;
    }

    public Integer getWinnerCode() {
        return winnerCode;
    }

    public void setWinnerCode(Integer winnerCode) {
        this.winnerCode = winnerCode;
    }

    public Matches_HomeTeam getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Matches_HomeTeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Matches_AwayTeam getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Matches_AwayTeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Matches_HomeScore getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(Matches_HomeScore homeScore) {
        this.homeScore = homeScore;
    }

    public Matches_AwayScore getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(Matches_AwayScore awayScore) {
        this.awayScore = awayScore;
    }

    public Matches_Time getTime() {
        return time;
    }

    public void setTime(Matches_Time time) {
        this.time = time;
    }

    public Matches_Changes getChanges() {
        return changes;
    }

    public void setChanges(Matches_Changes changes) {
        this.changes = changes;
    }

    public Boolean getHasHighlights() {
        return hasHighlights;
    }

    public void setHasHighlights(Boolean hasHighlights) {
        this.hasHighlights = hasHighlights;
    }

    public Boolean getHasHighlightsStream() {
        return hasHighlightsStream;
    }

    public void setHasHighlightsStream(Boolean hasHighlightsStream) {
        this.hasHighlightsStream = hasHighlightsStream;
    }

    public Boolean getHasGlobalHighlights() {
        return hasGlobalHighlights;
    }

    public void setHasGlobalHighlights(Boolean hasGlobalHighlights) {
        this.hasGlobalHighlights = hasGlobalHighlights;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Integer startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Integer getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(Integer endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Boolean getHasTime() {
        return hasTime;
    }

    public void setHasTime(Boolean hasTime) {
        this.hasTime = hasTime;
    }

    public Boolean getResultOnly() {
        return resultOnly;
    }

    public void setResultOnly(Boolean resultOnly) {
        this.resultOnly = resultOnly;
    }

    public Matches_CurrentBatsman getCurrentBowler() {
        return currentBowler;
    }

    public void setCurrentBowler(Matches_CurrentBatsman currentBowler) {
        this.currentBowler = currentBowler;
    }

    public Matches_CurrentBatsman getCurrentBatsman() {
        return currentBatsman;
    }

    public void setCurrentBatsman(Matches_CurrentBatsman currentBatsman) {
        this.currentBatsman = currentBatsman;
    }

    public Matches_CurrentBattingTeam getCurrentBattingTeam() {
        return currentBattingTeam;
    }

    public void setCurrentBattingTeam(Matches_CurrentBattingTeam currentBattingTeam) {
        this.currentBattingTeam = currentBattingTeam;
    }

    public Matches_CurrentBattingTeam getCurrentBowlingTeam() {
        return currentBowlingTeam;
    }

    public void setCurrentBowlingTeam(Matches_CurrentBattingTeam currentBowlingTeam) {
        this.currentBowlingTeam = currentBowlingTeam;
    }
}