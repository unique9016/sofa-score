package com.score.cricket.Models.TeamDetails.FeaturedMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeaturedInnings {

    @SerializedName("inning1")
    @Expose
    private FeaturedInning1 inning1;

    
    public FeaturedInnings() {
    }

    
    public FeaturedInnings(FeaturedInning1 inning1) {
        super();
        this.inning1 = inning1;
    }

    public FeaturedInning1 getInning1() {
        return inning1;
    }

    public void setInning1(FeaturedInning1 inning1) {
        this.inning1 = inning1;
    }

}
