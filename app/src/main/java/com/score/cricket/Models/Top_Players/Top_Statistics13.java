
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics13 {

    @SerializedName("successfulDribbles")
    @Expose
    private Integer successfulDribbles;
    @SerializedName("successfulDribblesPercentage")
    @Expose
    private Double successfulDribblesPercentage;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;

    
    public Top_Statistics13() {
    }

    public Top_Statistics13(Integer successfulDribbles, Double successfulDribblesPercentage, Integer id, String type, Integer appearances) {
        super();
        this.successfulDribbles = successfulDribbles;
        this.successfulDribblesPercentage = successfulDribblesPercentage;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getSuccessfulDribbles() {
        return successfulDribbles;
    }

    public void setSuccessfulDribbles(Integer successfulDribbles) {
        this.successfulDribbles = successfulDribbles;
    }

    public Double getSuccessfulDribblesPercentage() {
        return successfulDribblesPercentage;
    }

    public void setSuccessfulDribblesPercentage(Double successfulDribblesPercentage) {
        this.successfulDribblesPercentage = successfulDribblesPercentage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
