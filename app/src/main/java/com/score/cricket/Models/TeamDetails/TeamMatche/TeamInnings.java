package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamInnings {

    @SerializedName("inning1")
    @Expose
    private TeamInning1 inning1;
    @SerializedName("inning2")
    @Expose
    private TeamInning2 inning2;


    public TeamInnings() {
    }

    
    public TeamInnings(TeamInning1 inning1, TeamInning2 inning2) {
        super();
        this.inning1 = inning1;
        this.inning2 = inning2;
    }

    public TeamInning1 getInning1() {
        return inning1;
    }

    public void setInning1(TeamInning1 inning1) {
        this.inning1 = inning1;
    }

    public TeamInning2 getInning2() {
        return inning2;
    }

    public void setInning2(TeamInning2 inning2) {
        this.inning2 = inning2;
    }

}