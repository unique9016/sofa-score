package com.score.cricket.Models.AppMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Matches_Innings {

    @SerializedName("inning1")
    @Expose
    private Matches_Inning1 inning1;


    public Matches_Innings() {
    }

    
    public Matches_Innings(Matches_Inning1 inning1) {
        super();
        this.inning1 = inning1;
    }

    public Matches_Inning1 getInning1() {
        return inning1;
    }

    public void setInning1(Matches_Inning1 inning1) {
        this.inning1 = inning1;
    }

}
