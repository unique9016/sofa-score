package com.score.cricket.Models.HomeMain;

public class HomeMatch_Inning_items {
    String name;
    int id;

    public HomeMatch_Inning_items(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
