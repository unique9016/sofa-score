
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

public class Details_Game {

    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("rows")
    @Expose
    private Integer rows;
    @SerializedName("tournaments")
    @Expose
    private List<Details_Tournament> tournaments = null;


    public Details_Game() {
    }

    /**
     * 
     * @param tournaments
     * @param rows
     * @param sport
     */
    public Details_Game(AppSport sport, Integer rows, List<Details_Tournament> tournaments) {
        super();
        this.sport = sport;
        this.rows = rows;
        this.tournaments = tournaments;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public List<Details_Tournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<Details_Tournament> tournaments) {
        this.tournaments = tournaments;
    }

}
