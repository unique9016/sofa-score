
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_Vote {

    @SerializedName("vote1")
    @Expose
    private Integer vote1;
    @SerializedName("vote2")
    @Expose
    private Integer vote2;
    @SerializedName("voteX")
    @Expose
    private Integer voteX;
    @SerializedName("vote1Percentage")
    @Expose
    private String vote1Percentage;
    @SerializedName("voteXPercentage")
    @Expose
    private String voteXPercentage;
    @SerializedName("vote2Percentage")
    @Expose
    private String vote2Percentage;


    public Details_Vote() {
    }

    /**
     * 
     * @param vote1
     * @param voteXPercentage
     * @param vote1Percentage
     * @param vote2
     * @param voteX
     * @param vote2Percentage
     */
    public Details_Vote(Integer vote1, Integer vote2, Integer voteX, String vote1Percentage, String voteXPercentage, String vote2Percentage) {
        super();
        this.vote1 = vote1;
        this.vote2 = vote2;
        this.voteX = voteX;
        this.vote1Percentage = vote1Percentage;
        this.voteXPercentage = voteXPercentage;
        this.vote2Percentage = vote2Percentage;
    }

    public Integer getVote1() {
        return vote1;
    }

    public void setVote1(Integer vote1) {
        this.vote1 = vote1;
    }

    public Integer getVote2() {
        return vote2;
    }

    public void setVote2(Integer vote2) {
        this.vote2 = vote2;
    }

    public Integer getVoteX() {
        return voteX;
    }

    public void setVoteX(Integer voteX) {
        this.voteX = voteX;
    }

    public String getVote1Percentage() {
        return vote1Percentage;
    }

    public void setVote1Percentage(String vote1Percentage) {
        this.vote1Percentage = vote1Percentage;
    }

    public String getVoteXPercentage() {
        return voteXPercentage;
    }

    public void setVoteXPercentage(String voteXPercentage) {
        this.voteXPercentage = voteXPercentage;
    }

    public String getVote2Percentage() {
        return vote2Percentage;
    }

    public void setVote2Percentage(String vote2Percentage) {
        this.vote2Percentage = vote2Percentage;
    }

}
