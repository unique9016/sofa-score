
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_Bet365Links {

    @SerializedName("deeplink")
    @Expose
    private String deeplink;
    @SerializedName("joinlink")
    @Expose
    private String joinlink;

    
    public Details_Bet365Links() {
    }


    public Details_Bet365Links(String deeplink, String joinlink) {
        super();
        this.deeplink = deeplink;
        this.joinlink = joinlink;
    }

    public String getDeeplink() {
        return deeplink;
    }

    public void setDeeplink(String deeplink) {
        this.deeplink = deeplink;
    }

    public String getJoinlink() {
        return joinlink;
    }

    public void setJoinlink(String joinlink) {
        this.joinlink = joinlink;
    }

}
