package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayedForTeamMap {

    @SerializedName("7983375")
    @Expose
    private Integer _7983375;
    @SerializedName("8980002")
    @Expose
    private Integer _8980002;
    @SerializedName("8980016")
    @Expose
    private Integer _8980016;
    @SerializedName("8980021")
    @Expose
    private Integer _8980021;
    @SerializedName("8980521")
    @Expose
    private Integer _8980521;
    @SerializedName("8980526")
    @Expose
    private Integer _8980526;
    @SerializedName("8980523")
    @Expose
    private Integer _8980523;
    @SerializedName("8980547")
    @Expose
    private Integer _8980547;
    @SerializedName("8980552")
    @Expose
    private Integer _8980552;
    @SerializedName("8980558")
    @Expose
    private Integer _8980558;
    @SerializedName("8980563")
    @Expose
    private Integer _8980563;
    @SerializedName("8980567")
    @Expose
    private Integer _8980567;
    @SerializedName("8980570")
    @Expose
    private Integer _8980570;
    @SerializedName("8980575")
    @Expose
    private Integer _8980575;
    @SerializedName("8980578")
    @Expose
    private Integer _8980578;
    @SerializedName("9151199")
    @Expose
    private Integer _9151199;


    public PlayedForTeamMap() {
    }

    /**
     *
     * @param _8980521
     * @param _8980016
     * @param _7983375
     * @param _8980523
     * @param _8980567
     * @param _8980578
     * @param _8980002
     * @param _8980552
     * @param _8980563
     * @param _8980575
     * @param _8980570
     * @param _8980021
     * @param _9151199
     * @param _8980547
     * @param _8980558
     * @param _8980526
     */
    public PlayedForTeamMap(Integer _7983375, Integer _8980002, Integer _8980016, Integer _8980021, Integer _8980521, Integer _8980526, Integer _8980523, Integer _8980547, Integer _8980552, Integer _8980558, Integer _8980563, Integer _8980567, Integer _8980570, Integer _8980575, Integer _8980578, Integer _9151199) {
        super();
        this._7983375 = _7983375;
        this._8980002 = _8980002;
        this._8980016 = _8980016;
        this._8980021 = _8980021;
        this._8980521 = _8980521;
        this._8980526 = _8980526;
        this._8980523 = _8980523;
        this._8980547 = _8980547;
        this._8980552 = _8980552;
        this._8980558 = _8980558;
        this._8980563 = _8980563;
        this._8980567 = _8980567;
        this._8980570 = _8980570;
        this._8980575 = _8980575;
        this._8980578 = _8980578;
        this._9151199 = _9151199;
    }

    public Integer get7983375() {
        return _7983375;
    }

    public void set7983375(Integer _7983375) {
        this._7983375 = _7983375;
    }

    public Integer get8980002() {
        return _8980002;
    }

    public void set8980002(Integer _8980002) {
        this._8980002 = _8980002;
    }

    public Integer get8980016() {
        return _8980016;
    }

    public void set8980016(Integer _8980016) {
        this._8980016 = _8980016;
    }

    public Integer get8980021() {
        return _8980021;
    }

    public void set8980021(Integer _8980021) {
        this._8980021 = _8980021;
    }

    public Integer get8980521() {
        return _8980521;
    }

    public void set8980521(Integer _8980521) {
        this._8980521 = _8980521;
    }

    public Integer get8980526() {
        return _8980526;
    }

    public void set8980526(Integer _8980526) {
        this._8980526 = _8980526;
    }

    public Integer get8980523() {
        return _8980523;
    }

    public void set8980523(Integer _8980523) {
        this._8980523 = _8980523;
    }

    public Integer get8980547() {
        return _8980547;
    }

    public void set8980547(Integer _8980547) {
        this._8980547 = _8980547;
    }

    public Integer get8980552() {
        return _8980552;
    }

    public void set8980552(Integer _8980552) {
        this._8980552 = _8980552;
    }

    public Integer get8980558() {
        return _8980558;
    }

    public void set8980558(Integer _8980558) {
        this._8980558 = _8980558;
    }

    public Integer get8980563() {
        return _8980563;
    }

    public void set8980563(Integer _8980563) {
        this._8980563 = _8980563;
    }

    public Integer get8980567() {
        return _8980567;
    }

    public void set8980567(Integer _8980567) {
        this._8980567 = _8980567;
    }

    public Integer get8980570() {
        return _8980570;
    }

    public void set8980570(Integer _8980570) {
        this._8980570 = _8980570;
    }

    public Integer get8980575() {
        return _8980575;
    }

    public void set8980575(Integer _8980575) {
        this._8980575 = _8980575;
    }

    public Integer get8980578() {
        return _8980578;
    }

    public void set8980578(Integer _8980578) {
        this._8980578 = _8980578;
    }

    public Integer get9151199() {
        return _9151199;
    }

    public void set9151199(Integer _9151199) {
        this._9151199 = _9151199;
    }

}