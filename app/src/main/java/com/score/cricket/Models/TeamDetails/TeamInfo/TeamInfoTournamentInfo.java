package com.score.cricket.Models.TeamDetails.TeamInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamInfoTournamentInfo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("uniqueId")
    @Expose
    private Integer uniqueId;
    @SerializedName("uniqueName")
    @Expose
    private String uniqueName;


    public TeamInfoTournamentInfo() {
    }

    /**
     *
     * @param uniqueName
     * @param name
     * @param id
     * @param uniqueId
     */
    public TeamInfoTournamentInfo(Integer id, String name, Integer uniqueId, String uniqueName) {
        super();
        this.id = id;
        this.name = name;
        this.uniqueId = uniqueId;
        this.uniqueName = uniqueName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUniqueName() {
        return uniqueName;
    }

    public void setUniqueName(String uniqueName) {
        this.uniqueName = uniqueName;
    }

}