
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_Tournament {

    @SerializedName("tournament")
    @Expose
    private Details_Tournament_ tournament;
    @SerializedName("category")
    @Expose
    private Details_Category category;
    @SerializedName("season")
    @Expose
    private Details_Season season;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;
    @SerializedName("hasEventPlayerHeatMap")
    @Expose
    private Object hasEventPlayerHeatMap;
    @SerializedName("hasBoxScore")
    @Expose
    private Object hasBoxScore;
    @SerializedName("events")
    @Expose
    private List<Details_Event> events = null;

    
    public Details_Tournament() {
    }


    public Details_Tournament(Details_Tournament_ tournament, Details_Category category, Details_Season season, Boolean hasEventPlayerStatistics, Object hasEventPlayerHeatMap, Object hasBoxScore, List<Details_Event> events) {
        super();
        this.tournament = tournament;
        this.category = category;
        this.season = season;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
        this.hasBoxScore = hasBoxScore;
        this.events = events;
    }

    public Details_Tournament_ getTournament() {
        return tournament;
    }

    public void setTournament(Details_Tournament_ tournament) {
        this.tournament = tournament;
    }

    public Details_Category getCategory() {
        return category;
    }

    public void setCategory(Details_Category category) {
        this.category = category;
    }

    public Details_Season getSeason() {
        return season;
    }

    public void setSeason(Details_Season season) {
        this.season = season;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public Object getHasEventPlayerHeatMap() {
        return hasEventPlayerHeatMap;
    }

    public void setHasEventPlayerHeatMap(Object hasEventPlayerHeatMap) {
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
    }

    public Object getHasBoxScore() {
        return hasBoxScore;
    }

    public void setHasBoxScore(Object hasBoxScore) {
        this.hasBoxScore = hasBoxScore;
    }

    public List<Details_Event> getEvents() {
        return events;
    }

    public void setEvents(List<Details_Event> events) {
        this.events = events;
    }

}
