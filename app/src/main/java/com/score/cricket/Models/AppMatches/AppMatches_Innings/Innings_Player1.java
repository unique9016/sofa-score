package com.score.cricket.Models.AppMatches.AppMatches_Innings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Innings_Player1 {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("id")
    @Expose
    private Integer id;


    public Innings_Player1() {
    }

    
    public Innings_Player1(String name, String slug, String shortName, Integer id) {
        super();
        this.name = name;
        this.slug = slug;
        this.shortName = shortName;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}