package com.score.cricket.Models.AppMatches.AppMatches_Innings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Innings_Partnership {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("player1")
    @Expose
    private Innings_Player1 player1;
    @SerializedName("player2")
    @Expose
    private Innings_Player2 player2;
    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("balls")
    @Expose
    private Integer balls;


    public Innings_Partnership() {
    }


    public Innings_Partnership(Integer id, Innings_Player1 player1, Innings_Player2 player2, Integer score, Integer balls) {
        super();
        this.id = id;
        this.player1 = player1;
        this.player2 = player2;
        this.score = score;
        this.balls = balls;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Innings_Player1 getPlayer1() {
        return player1;
    }

    public void setPlayer1(Innings_Player1 player1) {
        this.player1 = player1;
    }

    public Innings_Player2 getPlayer2() {
        return player2;
    }

    public void setPlayer2(Innings_Player2 player2) {
        this.player2 = player2;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getBalls() {
        return balls;
    }

    public void setBalls(Integer balls) {
        this.balls = balls;
    }

}