package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlayerExample {

    @SerializedName("events")
    @Expose
    private List<PlayerEvent> events = null;
    @SerializedName("hasNextPage")
    @Expose
    private Boolean hasNextPage;
    @SerializedName("playedForTeamMap")
    @Expose
    private PlayedForTeamMap playedForTeamMap;
    @SerializedName("statisticsMap")
    @Expose
    private PlayerStatisticsMap statisticsMap;
    @SerializedName("incidentsMap")
    @Expose
    private PlayerIncidentsMap incidentsMap;
    @SerializedName("onBenchMap")
    @Expose
    private PlayerOnBenchMap onBenchMap;


    public PlayerExample() {
    }

    /**
     *
     * @param onBenchMap
     * @param playedForTeamMap
     * @param statisticsMap
     * @param hasNextPage
     * @param incidentsMap
     * @param events
     */
    public PlayerExample(List<PlayerEvent> events, Boolean hasNextPage, PlayedForTeamMap playedForTeamMap, PlayerStatisticsMap statisticsMap, PlayerIncidentsMap incidentsMap, PlayerOnBenchMap onBenchMap) {
        super();
        this.events = events;
        this.hasNextPage = hasNextPage;
        this.playedForTeamMap = playedForTeamMap;
        this.statisticsMap = statisticsMap;
        this.incidentsMap = incidentsMap;
        this.onBenchMap = onBenchMap;
    }

    public List<PlayerEvent> getEvents() {
        return events;
    }

    public void setEvents(List<PlayerEvent> events) {
        this.events = events;
    }

    public Boolean getHasNextPage() {
        return hasNextPage;
    }

    public void setHasNextPage(Boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public PlayedForTeamMap getPlayedForTeamMap() {
        return playedForTeamMap;
    }

    public void setPlayedForTeamMap(PlayedForTeamMap playedForTeamMap) {
        this.playedForTeamMap = playedForTeamMap;
    }

    public PlayerStatisticsMap getStatisticsMap() {
        return statisticsMap;
    }

    public void setStatisticsMap(PlayerStatisticsMap statisticsMap) {
        this.statisticsMap = statisticsMap;
    }

    public PlayerIncidentsMap getIncidentsMap() {
        return incidentsMap;
    }

    public void setIncidentsMap(PlayerIncidentsMap incidentsMap) {
        this.incidentsMap = incidentsMap;
    }

    public PlayerOnBenchMap getOnBenchMap() {
        return onBenchMap;
    }

    public void setOnBenchMap(PlayerOnBenchMap onBenchMap) {
        this.onBenchMap = onBenchMap;
    }

}