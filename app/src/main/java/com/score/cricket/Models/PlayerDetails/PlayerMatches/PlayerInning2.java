package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayerInning2 {

    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("wickets")
    @Expose
    private Integer wickets;
    @SerializedName("overs")
    @Expose
    private Double overs;
    @SerializedName("runRate")
    @Expose
    private Double runRate;
    @SerializedName("inningDeclare")
    @Expose
    private Integer inningDeclare;


    public PlayerInning2() {
    }

    
    public PlayerInning2(Integer score, Integer wickets, Double overs, Double runRate, Integer inningDeclare) {
        super();
        this.score = score;
        this.wickets = wickets;
        this.overs = overs;
        this.runRate = runRate;
        this.inningDeclare = inningDeclare;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getWickets() {
        return wickets;
    }

    public void setWickets(Integer wickets) {
        this.wickets = wickets;
    }

    public Double getOvers() {
        return overs;
    }

    public void setOvers(Double overs) {
        this.overs = overs;
    }

    public Double getRunRate() {
        return runRate;
    }

    public void setRunRate(Double runRate) {
        this.runRate = runRate;
    }

    public Integer getInningDeclare() {
        return inningDeclare;
    }

    public void setInningDeclare(Integer inningDeclare) {
        this.inningDeclare = inningDeclare;
    }

}