package com.score.cricket.Models.HomeMain;

public class HomeMatch_inning_batting {
    String batsmaname,cathername,bowlername,SR;
    int run,ball,four,six,wicketid;

    public HomeMatch_inning_batting() {
    }

    public HomeMatch_inning_batting(String batsmaname, String cathername, String bowlername, String SR, int run, int ball, int four, int six, int wicketid) {
        this.batsmaname = batsmaname;
        this.cathername = cathername;
        this.bowlername = bowlername;
        this.SR = SR;
        this.run = run;
        this.ball = ball;
        this.four = four;
        this.six = six;
        this.wicketid = wicketid;
    }

    public String getBatsmaname() {
        return batsmaname;
    }

    public void setBatsmaname(String batsmaname) {
        this.batsmaname = batsmaname;
    }

    public String getCathername() {
        return cathername;
    }

    public void setCathername(String cathername) {
        this.cathername = cathername;
    }

    public String getBowlername() {
        return bowlername;
    }

    public void setBowlername(String bowlername) {
        this.bowlername = bowlername;
    }

    public String getSR() {
        return SR;
    }

    public void setSR(String SR) {
        this.SR = SR;
    }

    public int getRun() {
        return run;
    }

    public void setRun(int run) {
        this.run = run;
    }

    public int getBall() {
        return ball;
    }

    public void setBall(int ball) {
        this.ball = ball;
    }

    public int getFour() {
        return four;
    }

    public void setFour(int four) {
        this.four = four;
    }

    public int getSix() {
        return six;
    }

    public void setSix(int six) {
        this.six = six;
    }

    public int getWicketid() {
        return wicketid;
    }

    public void setWicketid(int wicketid) {
        this.wicketid = wicketid;
    }
}
