package com.score.cricket.Models.AppMatches.AppMatches_Matches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MatchesApp_Tournament {

    @SerializedName("tournament")
    @Expose
    private MatchesApp_Tournament_ tournament;
    @SerializedName("category")
    @Expose
    private MatchesApp_Category category;
    @SerializedName("season")
    @Expose
    private MatchesApp_Season season;
    @SerializedName("hasEventPlayerStatistics")
    @Expose
    private Boolean hasEventPlayerStatistics;
    @SerializedName("hasEventPlayerHeatMap")
    @Expose
    private Object hasEventPlayerHeatMap;
    @SerializedName("hasBoxScore")
    @Expose
    private Object hasBoxScore;
    @SerializedName("events")
    @Expose
    private List<MatchesApp_Event> events = null;


    public MatchesApp_Tournament() {
    }

    
    public MatchesApp_Tournament(MatchesApp_Tournament_ tournament, MatchesApp_Category category, MatchesApp_Season season, Boolean hasEventPlayerStatistics, Object hasEventPlayerHeatMap, Object hasBoxScore, List<MatchesApp_Event> events) {
        super();
        this.tournament = tournament;
        this.category = category;
        this.season = season;
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
        this.hasBoxScore = hasBoxScore;
        this.events = events;
    }

    public MatchesApp_Tournament_ getTournament() {
        return tournament;
    }

    public void setTournament(MatchesApp_Tournament_ tournament) {
        this.tournament = tournament;
    }

    public MatchesApp_Category getCategory() {
        return category;
    }

    public void setCategory(MatchesApp_Category category) {
        this.category = category;
    }

    public MatchesApp_Season getSeason() {
        return season;
    }

    public void setSeason(MatchesApp_Season season) {
        this.season = season;
    }

    public Boolean getHasEventPlayerStatistics() {
        return hasEventPlayerStatistics;
    }

    public void setHasEventPlayerStatistics(Boolean hasEventPlayerStatistics) {
        this.hasEventPlayerStatistics = hasEventPlayerStatistics;
    }

    public Object getHasEventPlayerHeatMap() {
        return hasEventPlayerHeatMap;
    }

    public void setHasEventPlayerHeatMap(Object hasEventPlayerHeatMap) {
        this.hasEventPlayerHeatMap = hasEventPlayerHeatMap;
    }

    public Object getHasBoxScore() {
        return hasBoxScore;
    }

    public void setHasBoxScore(Object hasBoxScore) {
        this.hasBoxScore = hasBoxScore;
    }

    public List<MatchesApp_Event> getEvents() {
        return events;
    }

    public void setEvents(List<MatchesApp_Event> events) {
        this.events = events;
    }

}