package com.score.cricket.Models.PlayerDetails.PlayerMatches;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayerEvent {

    @SerializedName("tournament")
    @Expose
    private PlayerTournament tournament;
    @SerializedName("customId")
    @Expose
    private String customId;
    @SerializedName("status")
    @Expose
    private PlayerStatus status;
    @SerializedName("winnerCode")
    @Expose
    private Integer winnerCode;
    @SerializedName("homeTeam")
    @Expose
    private PlayerHomeTeam homeTeam;
    @SerializedName("awayTeam")
    @Expose
    private PlayerAwayTeam awayTeam;
    @SerializedName("homeScore")
    @Expose
    private PlayerHomeScore homeScore;
    @SerializedName("awayScore")
    @Expose
    private PlayerAwayScore awayScore;
    @SerializedName("changes")
    @Expose
    private PlayerChanges changes;
    @SerializedName("hasGlobalHighlights")
    @Expose
    private Boolean hasGlobalHighlights;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("startTimestamp")
    @Expose
    private Integer startTimestamp;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("periods")
    @Expose
    private PlayerPeriods periods;
    @SerializedName("finalResultOnly")
    @Expose
    private Boolean finalResultOnly;


    public PlayerEvent() {
    }

    /**
     *
     * @param note
     * @param homeScore
     * @param awayTeam
     * @param changes
     * @param finalResultOnly
     * @param tournament
     * @param customId
     * @param winnerCode
     * @param hasGlobalHighlights
     * @param awayScore
     * @param homeTeam
     * @param periods
     * @param id
     * @param startTimestamp
     * @param slug
     * @param status
     */
    public PlayerEvent(PlayerTournament tournament, String customId, PlayerStatus status, Integer winnerCode, PlayerHomeTeam homeTeam, PlayerAwayTeam awayTeam, PlayerHomeScore homeScore, PlayerAwayScore awayScore, PlayerChanges changes, Boolean hasGlobalHighlights, Integer id, String note, Integer startTimestamp, String slug, PlayerPeriods periods, Boolean finalResultOnly) {
        super();
        this.tournament = tournament;
        this.customId = customId;
        this.status = status;
        this.winnerCode = winnerCode;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
        this.changes = changes;
        this.hasGlobalHighlights = hasGlobalHighlights;
        this.id = id;
        this.note = note;
        this.startTimestamp = startTimestamp;
        this.slug = slug;
        this.periods = periods;
        this.finalResultOnly = finalResultOnly;
    }

    public PlayerTournament getTournament() {
        return tournament;
    }

    public void setTournament(PlayerTournament tournament) {
        this.tournament = tournament;
    }

    public String getCustomId() {
        return customId;
    }

    public void setCustomId(String customId) {
        this.customId = customId;
    }

    public PlayerStatus getStatus() {
        return status;
    }

    public void setStatus(PlayerStatus status) {
        this.status = status;
    }

    public Integer getWinnerCode() {
        return winnerCode;
    }

    public void setWinnerCode(Integer winnerCode) {
        this.winnerCode = winnerCode;
    }

    public PlayerHomeTeam getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(PlayerHomeTeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    public PlayerAwayTeam getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(PlayerAwayTeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    public PlayerHomeScore getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(PlayerHomeScore homeScore) {
        this.homeScore = homeScore;
    }

    public PlayerAwayScore getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(PlayerAwayScore awayScore) {
        this.awayScore = awayScore;
    }

    public PlayerChanges getChanges() {
        return changes;
    }

    public void setChanges(PlayerChanges changes) {
        this.changes = changes;
    }

    public Boolean getHasGlobalHighlights() {
        return hasGlobalHighlights;
    }

    public void setHasGlobalHighlights(Boolean hasGlobalHighlights) {
        this.hasGlobalHighlights = hasGlobalHighlights;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Integer startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public PlayerPeriods getPeriods() {
        return periods;
    }

    public void setPeriods(PlayerPeriods periods) {
        this.periods = periods;
    }

    public Boolean getFinalResultOnly() {
        return finalResultOnly;
    }

    public void setFinalResultOnly(Boolean finalResultOnly) {
        this.finalResultOnly = finalResultOnly;
    }

}