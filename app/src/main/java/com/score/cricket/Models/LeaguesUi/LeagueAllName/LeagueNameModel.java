package com.score.cricket.Models.LeaguesUi.LeagueAllName;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeagueNameModel {

    @SerializedName("groups")
    @Expose
    private List<LeaguesGroup> groups = null;

    public LeagueNameModel() {
    }

    public LeagueNameModel(List<LeaguesGroup> groups) {
        super();
        this.groups = groups;
    }

    public List<LeaguesGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<LeaguesGroup> groups) {
        this.groups = groups;
    }

}