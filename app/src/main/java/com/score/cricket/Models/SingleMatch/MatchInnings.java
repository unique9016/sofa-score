package com.score.cricket.Models.SingleMatch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchInnings {

    @SerializedName("inning1")
    @Expose
    private MatchInning1 inning1;
    @SerializedName("inning2")
    @Expose
    private MatchInning2 inning2;


    public MatchInnings() {
    }

    
    public MatchInnings(MatchInning1 inning1, MatchInning2 inning2) {
        super();
        this.inning1 = inning1;
        this.inning2 = inning2;
    }

    public MatchInning1 getInning1() {
        return inning1;
    }

    public void setInning1(MatchInning1 inning1) {
        this.inning1 = inning1;
    }

    public MatchInning2 getInning2() {
        return inning2;
    }

    public void setInning2(MatchInning2 inning2) {
        this.inning2 = inning2;
    }

}