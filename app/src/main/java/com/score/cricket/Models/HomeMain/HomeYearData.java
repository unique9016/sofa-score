package com.score.cricket.Models.HomeMain;

public class HomeYearData {
    String year;
    int id;

    public HomeYearData() {
    }

    public HomeYearData(String year, int id) {
        this.year = year;
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
