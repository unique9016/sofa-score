
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics22 {

    @SerializedName("goalsConceded")
    @Expose
    private Integer goalsConceded;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;

    
    public Top_Statistics22() {
    }

    public Top_Statistics22(Integer goalsConceded, Integer id, String type, Integer appearances) {
        super();
        this.goalsConceded = goalsConceded;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getGoalsConceded() {
        return goalsConceded;
    }

    public void setGoalsConceded(Integer goalsConceded) {
        this.goalsConceded = goalsConceded;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
