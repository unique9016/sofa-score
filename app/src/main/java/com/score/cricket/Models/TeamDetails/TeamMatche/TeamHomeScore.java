package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamHomeScore {

    @SerializedName("current")
    @Expose
    private Integer current;
    @SerializedName("display")
    @Expose
    private Integer display;
    @SerializedName("innings")
    @Expose
    private TeamInnings innings;
    @SerializedName("hits")
    @Expose
    private Integer hits;
    @SerializedName("errors")
    @Expose
    private Integer errors;


    public TeamHomeScore() {
    }

    
    public TeamHomeScore(Integer current, Integer display, TeamInnings innings, Integer hits, Integer errors) {
        super();
        this.current = current;
        this.display = display;
        this.innings = innings;
        this.hits = hits;
        this.errors = errors;
    }

    public Integer getCurrent() {
        return current;
    }

    public void setCurrent(Integer current) {
        this.current = current;
    }

    public Integer getDisplay() {
        return display;
    }

    public void setDisplay(Integer display) {
        this.display = display;
    }

    public TeamInnings getInnings() {
        return innings;
    }

    public void setInnings(TeamInnings innings) {
        this.innings = innings;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    public Integer getErrors() {
        return errors;
    }

    public void setErrors(Integer errors) {
        this.errors = errors;
    }

}