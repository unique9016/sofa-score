
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_BigChancesCreated {

    @SerializedName("statistics")
    @Expose
    private Top_Statistics9 statistics;
    @SerializedName("playedEnough")
    @Expose
    private Boolean playedEnough;
    @SerializedName("player")
    @Expose
    private Top_Player player;
    @SerializedName("team")
    @Expose
    private Top_Team team;

    
    public Top_BigChancesCreated() {
    }

    public Top_BigChancesCreated(Top_Statistics9 statistics, Boolean playedEnough, Top_Player player, Top_Team team) {
        super();
        this.statistics = statistics;
        this.playedEnough = playedEnough;
        this.player = player;
        this.team = team;
    }

    public Top_Statistics9 getStatistics() {
        return statistics;
    }

    public void setStatistics(Top_Statistics9 statistics) {
        this.statistics = statistics;
    }

    public Boolean getPlayedEnough() {
        return playedEnough;
    }

    public void setPlayedEnough(Boolean playedEnough) {
        this.playedEnough = playedEnough;
    }

    public Top_Player getPlayer() {
        return player;
    }

    public void setPlayer(Top_Player player) {
        this.player = player;
    }

    public Top_Team getTeam() {
        return team;
    }

    public void setTeam(Top_Team team) {
        this.team = team;
    }

}
