package com.score.cricket.Models.TeamDetails.TeamInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamInfoStadium {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("capacity")
    @Expose
    private Integer capacity;

    
    public TeamInfoStadium() {
    }

    /**
     *
     * @param name
     * @param capacity
     */
    public TeamInfoStadium(String name, Integer capacity) {
        super();
        this.name = name;
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

}