package com.score.cricket.Models.AppMatches.AppMatches_Lineup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lineup_Player_ {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("country")
    @Expose
    private Lineup_Country country;
    @SerializedName("marketValueCurrency")
    @Expose
    private String marketValueCurrency;
    @SerializedName("dateOfBirthTimestamp")
    @Expose
    private Integer dateOfBirthTimestamp;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("position")
    @Expose
    private String position;


    public Lineup_Player_() {
    }

    public Lineup_Player_(String name, String slug, String shortName, Integer userCount, Integer id, Lineup_Country country, String marketValueCurrency, Integer dateOfBirthTimestamp, String firstName, String lastName, String position) {
        super();
        this.name = name;
        this.slug = slug;
        this.shortName = shortName;
        this.userCount = userCount;
        this.id = id;
        this.country = country;
        this.marketValueCurrency = marketValueCurrency;
        this.dateOfBirthTimestamp = dateOfBirthTimestamp;
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Lineup_Country getCountry() {
        return country;
    }

    public void setCountry(Lineup_Country country) {
        this.country = country;
    }

    public String getMarketValueCurrency() {
        return marketValueCurrency;
    }

    public void setMarketValueCurrency(String marketValueCurrency) {
        this.marketValueCurrency = marketValueCurrency;
    }

    public Integer getDateOfBirthTimestamp() {
        return dateOfBirthTimestamp;
    }

    public void setDateOfBirthTimestamp(Integer dateOfBirthTimestamp) {
        this.dateOfBirthTimestamp = dateOfBirthTimestamp;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}