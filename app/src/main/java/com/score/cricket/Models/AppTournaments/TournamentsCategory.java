package com.score.cricket.Models.AppTournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.score.cricket.Models.AppSport;

public class TournamentsCategory {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("sport")
    @Expose
    private AppSport sport;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("flag")
    @Expose
    private String flag;
    @SerializedName("alpha2")
    @Expose
    private String alpha2;

    /**
     * No args constructor for use in serialization
     */
    public TournamentsCategory() {
    }

    /**
     * @param flag
     * @param name
     * @param alpha2
     * @param id
     * @param sport
     * @param slug
     */
    public TournamentsCategory(String name, String slug, AppSport sport, Integer id, String flag, String alpha2) {
        super();
        this.name = name;
        this.slug = slug;
        this.sport = sport;
        this.id = id;
        this.flag = flag;
        this.alpha2 = alpha2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public AppSport getSport() {
        return sport;
    }

    public void setSport(AppSport sport) {
        this.sport = sport;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAlpha2() {
        return alpha2;
    }

    public void setAlpha2(String alpha2) {
        this.alpha2 = alpha2;
    }
}