
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_Innings {

    @SerializedName("inning1")
    @Expose
    private Details_Inning1 inning1;


    public Details_Innings() {
    }


    public Details_Innings(Details_Inning1 inning1) {
        super();
        this.inning1 = inning1;
    }

    public Details_Inning1 getInning1() {
        return inning1;
    }

    public void setInning1(Details_Inning1 inning1) {
        this.inning1 = inning1;
    }

}
