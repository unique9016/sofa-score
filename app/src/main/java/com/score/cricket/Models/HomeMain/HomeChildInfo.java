package com.score.cricket.Models.HomeMain;

public class HomeChildInfo {

    private String sequence = "";
    private String name = "";
    private int id;

    public HomeChildInfo() {
    }

    public HomeChildInfo(String sequence, String name, int id) {
        this.sequence = sequence;
        this.name = name;
        this.id = id;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}