package com.score.cricket.Models.TeamDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Team_Opponent {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;


    public Team_Opponent() {
    }

    /**
     *
     * @param name
     * @param id
     */
    public Team_Opponent(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}