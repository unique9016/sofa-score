
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics5 {

    @SerializedName("shotFromSetPiece")
    @Expose
    private Integer shotFromSetPiece;
    @SerializedName("freeKickGoal")
    @Expose
    private Integer freeKickGoal;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;

    
    public Top_Statistics5() {
    }

    public Top_Statistics5(Integer shotFromSetPiece, Integer freeKickGoal, Integer id, String type, Integer appearances) {
        super();
        this.shotFromSetPiece = shotFromSetPiece;
        this.freeKickGoal = freeKickGoal;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getShotFromSetPiece() {
        return shotFromSetPiece;
    }

    public void setShotFromSetPiece(Integer shotFromSetPiece) {
        this.shotFromSetPiece = shotFromSetPiece;
    }

    public Integer getFreeKickGoal() {
        return freeKickGoal;
    }

    public void setFreeKickGoal(Integer freeKickGoal) {
        this.freeKickGoal = freeKickGoal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
