
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Team {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("shortName")
    @Expose
    private String shortName;
    @SerializedName("userCount")
    @Expose
    private Integer userCount;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("teamColors")
    @Expose
    private Top_TeamColors teamColors;

    
    public Top_Team() {
    }

    public Top_Team(String name, String slug, String shortName, Integer userCount, Integer type, Integer id, Top_TeamColors teamColors) {
        super();
        this.name = name;
        this.slug = slug;
        this.shortName = shortName;
        this.userCount = userCount;
        this.type = type;
        this.id = id;
        this.teamColors = teamColors;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Top_TeamColors getTeamColors() {
        return teamColors;
    }

    public void setTeamColors(Top_TeamColors teamColors) {
        this.teamColors = teamColors;
    }

}
