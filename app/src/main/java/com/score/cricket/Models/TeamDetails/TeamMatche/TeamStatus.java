package com.score.cricket.Models.TeamDetails.TeamMatche;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamStatus {

    @SerializedName("code")
    @Expose
    private Integer code;
    @SerializedName("type")
    @Expose
    private String type;

    
    public TeamStatus() {
    }

    
    public TeamStatus(Integer code, String type) {
        super();
        this.code = code;
        this.type = type;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}