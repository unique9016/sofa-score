
package com.score.cricket.Models.AppMatches.AppMatches_Details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details_Example {

    @SerializedName("createdByNickname")
    @Expose
    private Object createdByNickname;
    @SerializedName("hasStandings")
    @Expose
    private Boolean hasStandings;
    @SerializedName("hasCupTree")
    @Expose
    private Boolean hasCupTree;
    @SerializedName("hasIncidents")
    @Expose
    private Boolean hasIncidents;
    @SerializedName("hasHighlights")
    @Expose
    private Boolean hasHighlights;
    @SerializedName("hasHighlightsStream")
    @Expose
    private Boolean hasHighlightsStream;
    @SerializedName("game")
    @Expose
    private Details_Game game;
    @SerializedName("vote")
    @Expose
    private Details_Vote vote;
    @SerializedName("hasStatistics")
    @Expose
    private Boolean hasStatistics;
    @SerializedName("hasComments")
    @Expose
    private Boolean hasComments;
    @SerializedName("hasInnings")
    @Expose
    private Boolean hasInnings;
    @SerializedName("hasLineups")
    @Expose
    private Boolean hasLineups;
    @SerializedName("hasOdds")
    @Expose
    private Boolean hasOdds;
    @SerializedName("hasTvChannels")
    @Expose
    private Boolean hasTvChannels;
    @SerializedName("hasLineupsImage")
    @Expose
    private Boolean hasLineupsImage;

    
    public Details_Example() {
    }

    /**
     * 
     * @param hasStandings
     * @param game
     * @param hasComments
     * @param hasLineups
     * @param hasTvChannels
     * @param hasInnings
     * @param hasCupTree
     * @param hasStatistics
     * @param hasLineupsImage
     * @param hasHighlightsStream
     * @param createdByNickname
     * @param hasOdds
     * @param hasIncidents
     * @param hasHighlights
     * @param vote
     */
    public Details_Example(Object createdByNickname, Boolean hasStandings, Boolean hasCupTree, Boolean hasIncidents, Boolean hasHighlights, Boolean hasHighlightsStream, Details_Game game, Details_Vote vote, Boolean hasStatistics, Boolean hasComments, Boolean hasInnings, Boolean hasLineups, Boolean hasOdds, Boolean hasTvChannels, Boolean hasLineupsImage) {
        super();
        this.createdByNickname = createdByNickname;
        this.hasStandings = hasStandings;
        this.hasCupTree = hasCupTree;
        this.hasIncidents = hasIncidents;
        this.hasHighlights = hasHighlights;
        this.hasHighlightsStream = hasHighlightsStream;
        this.game = game;
        this.vote = vote;
        this.hasStatistics = hasStatistics;
        this.hasComments = hasComments;
        this.hasInnings = hasInnings;
        this.hasLineups = hasLineups;
        this.hasOdds = hasOdds;
        this.hasTvChannels = hasTvChannels;
        this.hasLineupsImage = hasLineupsImage;
    }

    public Object getCreatedByNickname() {
        return createdByNickname;
    }

    public void setCreatedByNickname(Object createdByNickname) {
        this.createdByNickname = createdByNickname;
    }

    public Boolean getHasStandings() {
        return hasStandings;
    }

    public void setHasStandings(Boolean hasStandings) {
        this.hasStandings = hasStandings;
    }

    public Boolean getHasCupTree() {
        return hasCupTree;
    }

    public void setHasCupTree(Boolean hasCupTree) {
        this.hasCupTree = hasCupTree;
    }

    public Boolean getHasIncidents() {
        return hasIncidents;
    }

    public void setHasIncidents(Boolean hasIncidents) {
        this.hasIncidents = hasIncidents;
    }

    public Boolean getHasHighlights() {
        return hasHighlights;
    }

    public void setHasHighlights(Boolean hasHighlights) {
        this.hasHighlights = hasHighlights;
    }

    public Boolean getHasHighlightsStream() {
        return hasHighlightsStream;
    }

    public void setHasHighlightsStream(Boolean hasHighlightsStream) {
        this.hasHighlightsStream = hasHighlightsStream;
    }

    public Details_Game getGame() {
        return game;
    }

    public void setGame(Details_Game game) {
        this.game = game;
    }

    public Details_Vote getVote() {
        return vote;
    }

    public void setVote(Details_Vote vote) {
        this.vote = vote;
    }

    public Boolean getHasStatistics() {
        return hasStatistics;
    }

    public void setHasStatistics(Boolean hasStatistics) {
        this.hasStatistics = hasStatistics;
    }

    public Boolean getHasComments() {
        return hasComments;
    }

    public void setHasComments(Boolean hasComments) {
        this.hasComments = hasComments;
    }

    public Boolean getHasInnings() {
        return hasInnings;
    }

    public void setHasInnings(Boolean hasInnings) {
        this.hasInnings = hasInnings;
    }

    public Boolean getHasLineups() {
        return hasLineups;
    }

    public void setHasLineups(Boolean hasLineups) {
        this.hasLineups = hasLineups;
    }

    public Boolean getHasOdds() {
        return hasOdds;
    }

    public void setHasOdds(Boolean hasOdds) {
        this.hasOdds = hasOdds;
    }

    public Boolean getHasTvChannels() {
        return hasTvChannels;
    }

    public void setHasTvChannels(Boolean hasTvChannels) {
        this.hasTvChannels = hasTvChannels;
    }

    public Boolean getHasLineupsImage() {
        return hasLineupsImage;
    }

    public void setHasLineupsImage(Boolean hasLineupsImage) {
        this.hasLineupsImage = hasLineupsImage;
    }

}
