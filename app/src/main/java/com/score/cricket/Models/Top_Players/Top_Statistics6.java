
package com.score.cricket.Models.Top_Players;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Top_Statistics6 {

    @SerializedName("scoringFrequency")
    @Expose
    private Integer scoringFrequency;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("appearances")
    @Expose
    private Integer appearances;


    public Top_Statistics6() {
    }

    public Top_Statistics6(Integer scoringFrequency, Integer id, String type, Integer appearances) {
        super();
        this.scoringFrequency = scoringFrequency;
        this.id = id;
        this.type = type;
        this.appearances = appearances;
    }

    public Integer getScoringFrequency() {
        return scoringFrequency;
    }

    public void setScoringFrequency(Integer scoringFrequency) {
        this.scoringFrequency = scoringFrequency;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getAppearances() {
        return appearances;
    }

    public void setAppearances(Integer appearances) {
        this.appearances = appearances;
    }

}
