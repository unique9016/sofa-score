package com.score.cricket.Retrofit;

import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.MainScreen.ActivityLaunch;

import me.linshen.retrofit2.adapter.LiveDataCallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestManager {

    static Retrofit retrofit;
    static Retrofit retrofi2;
    static Retrofit retrofi3;
    static Retrofit retrofi4;
    static Retrofit retrofi5;

    public static Retrofit getLeague(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(ActivityLaunch.LeaguesLink)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getPlayer(){
        String url = AdsHelper.getGet_Player();
        if(retrofi2 == null){
            retrofi2 = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofi2;
    }

    public static Retrofit getMatch(){
        String url = AdsHelper.getGet_Match();
        if(retrofi3 == null){
            retrofi3 = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofi3;
    }



    public static Retrofit getLeagueName(){
        String url = AdsHelper.getLeague_Name();
        if(retrofi4 == null){
            retrofi4 = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofi4;
    }

    public static Retrofit getLeague1(){
        String url = AdsHelper.getGet_Player();
        if(retrofi5 == null){
            retrofi5 = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                    .build();
        }
        return retrofi5;
    }
}
