package com.score.cricket.Retrofit;

import com.score.cricket.Models.AllDetails.DetailsMain;
import com.score.cricket.Models.AppMatches.AppMatches_Details.Details_Example;
import com.score.cricket.Models.AppMatches.AppMatches_Innings.Innings_Example;
import com.score.cricket.Models.AppMatches.AppMatches_Lineup.Lineup_Example;
import com.score.cricket.Models.AppMatches.AppMatches_Matches.MatchesApp_Example;
import com.score.cricket.Models.AppTournaments.TournamentsExample;
import com.score.cricket.Models.LeaguesUi.LeagueAllName.LeagueNameModel;
import com.score.cricket.Models.LeaguesUi.MainLeague;
import com.score.cricket.Models.AppMatches.AppMatch.MatchesExample;
import com.score.cricket.Models.AppMatches.Matches_Example;
import com.score.cricket.Models.PlayerDetails.PlayerMatches.PlayerExample;
import com.score.cricket.Models.PlayerDetails.PlayerStatistics.Player_Example;
import com.score.cricket.Models.PlayerDetails.Players_Example;
import com.score.cricket.Models.Standings.StandingsExample;
import com.score.cricket.Models.TeamDetails.FeaturedMatch.FeaturedExample;
import com.score.cricket.Models.TeamDetails.TeamInfo.TeamInfoExample;
import com.score.cricket.Models.TeamDetails.TeamMatche.TeamExample;
import com.score.cricket.Models.TeamDetails.TeamSquad.TeamSquadExample;
import com.score.cricket.Models.TeamDetails.Team_Example;

import org.joda.time.LocalDate;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface APIService {

    @GET("unique-tournament/{id}/seasons")
    Call<TournamentsExample> getYearly(@Path("id") int id);

    @GET("player/{playerid}/events/last/0")
    Call<PlayerExample> getPlayerMatches(@Path("playerid") int playerid);

    @GET("player/{playerid}/career-statistics")
    Call<List<Player_Example>> getPlayerStatistic(@Path("playerid") int playerid);

    @GET("player/{playerid}/details")
    Call<Players_Example> getPlayerDetails(@Path("playerid") int playerid);

    @GET("team/{teamid}/players")
    Call<List<TeamSquadExample>> getPlayerInfo(@Path("teamid") int teamid);

    @GET("team/{teamid}/details")
    Call<TeamInfoExample> getMatchInfo1(@Path("teamid") int teamid);

    @GET("team/{teamid}/events")
    Call<FeaturedExample> getFeaturedMatch(@Path("teamid") int teamid);

    @GET("team/{teamid}/performance")
    Call<List<Team_Example>> getTeamInfo(@Path("teamid") int teamid);

    @GET("{sport_id}/unique-tournaments")
    Call<LeagueNameModel> getLeagueName(@Path("sport_id") int sportid);

    @GET("{sport}/categories")
    Call<MainLeague> getLeague(@Path("sport") String sport);

    @GET("sport/{sportname}/{date}/19800/categories")
    Call<MatchesExample> getMatch(@Path("sportname") String sportname,
                                  @Path("date") LocalDate date);

    @GET("list/scheduled/{sportname}/{id}/{date}")
    Call<Matches_Example> getPlayer(@Path("sportname") String sportname,
                                    @Path("id" )int id,
                                    @Path("date") LocalDate date);

    @GET("unique-tournament/{id}/details")
    Call<DetailsMain> getMatch(@Path("id") int id);

    @GET("event/{id}/details")
    Call<Details_Example> getMatchInfo(@Path("id") int id);

    @GET("unique-tournament/{u_id}/season/{season_id}/standings")
    Call<List<StandingsExample>> getStanding(@Path("u_id") int u_id,
                                             @Path("season_id") int season_id);

    @GET("unique-tournament/{u_id}/season/{season_id}/events")
    Call<com.score.cricket.Models.SingleMatch.MatchExample> getevents(@Path("u_id") int u_id,
                                                                      @Path("season_id") int season_id);
    @GET("event/{id}/innings")
    Call<List<Innings_Example>> getinning(@Path("id") int id);

    @GET("event/{id}/head2head")
    Call<MatchesApp_Example> geth2h(@Path("id") int id);

    @GET("team/{id}/lastnext")
    Call<TeamExample> getmatchboard(@Path("id") int id);

    @GET("event/{eventid}/lineups")
    Call<Lineup_Example> getPlayer(@Path("eventid") int id);
}
