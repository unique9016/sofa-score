package com.score.cricket.SubScreen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.WorldCountryIdList;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.MainScreen.ActivityCrickrtMatchInformation;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.AllDetails.DetailsMain;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragMatchInformationBinding;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragMatchInformation extends Fragment implements IEventListener {
    private static int sportid;
    String mTeam1logo;
    String mTeam2logo;
    String mFlageName;
    int mDelay = 0;
    int mPeriod = 500;
    Timer mTimer = new Timer();
    WorldCountryIdList mCountry = new WorldCountryIdList();
    FrameLayout fl_adplaceholder;
    private TextView _leaguename, _cname, _seasoname, _teamname, _homescore, _awayscore, _countryname, _matchtime;
    private String mUrl, mTeam1, mTeam2, mFlaage;
    private int A, B, C;

    private int mDay, mPosition;
    private CardView mCardView;
    private LinearLayout _scorelayout, _matchlayout, _hostdivider, _hostlayout;
    private Context _context;
    private int mTeam_id1, mTeam_id2;
    private ImageView _imageView, _teamlogo1, _teamlogo2;
    private SwipeRefreshLayout _swipeRefreshLayout;
    private TextView _name, _name1, _name2, _name3, _name4, _name5,
            _value, _value1, _value2, _value3, _value4, _value5,
            _n1, _n2,
            _status1, _status2;
    private LinearLayout _facts_divider, _facts_layout,
            _f1_layout, _f2_layout, _f3_layout, _f4_layout, _f5_layout, _f6_layout,
            _info_divider, _info_layout, _info_layout1, _info_layout2;
    private String mName0, mName01, mName02, mName03, mName04, mName05,
            mValue0, mValue01, mValue02, mValue03, mValue04, mValue05;
    private ImageView _img1, _img2, _flage;
    private RelativeLayout _nodata;
    private int status = 1;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCountry.Createarray();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragMatchInformationBinding binding = FragMatchInformationBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        sportid = sharedPreferences.getInt("id", 0);
        mPosition = sharedPreferences.getInt("mPosition", 0);

        _context = getActivity();

        UIInitialize(binding);

        if (mPosition == 0) {
            _nodata.setVisibility(View.GONE);
            ApiCall();
            if (status == 1) {
                loadlogo();
            } else if (status == 0) {
                loadlogo();
            } else if (status == 100) {
                loadlogo();
            } else {
                loadlogo();
                LiveAPIData();
            }
            _swipeRefreshLayout.setOnRefreshListener(() -> {
                ApiCall();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        _swipeRefreshLayout.setRefreshing(false);
                    }
                }, 1000);
            });
        } else {
            _nodata.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1);
    }

    private void LiveAPIData() {

        mTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Log.d("LIVEDATA", "LiveData every 10 sec: ");
                ApiCall();
            }
        }, mDelay, mPeriod);
    }

    private void UIInitialize(FragMatchInformationBinding binding) {

        _nodata = binding.nodata;

        _leaguename = binding.leagname;
        _cname = binding.cname;
        _seasoname = binding.seasonname;
        _teamname = binding.teamname;
        _homescore = binding.homescore;
        _awayscore = binding.awayscore;
        _countryname = binding.countryname;
        _matchtime = binding.matchtime;

        _scorelayout = binding.scorelayout;
        _matchlayout = binding.matchlayout;

        _hostdivider = binding.hostDivider;
        _hostlayout = binding.hostLayout;

        mCardView = binding.cardview;

        _swipeRefreshLayout = binding.swiperefresh;

        _imageView = binding.leaguelogo;
        _flage = binding.flage;
        _teamlogo1 = binding.teamlogo1;
        _teamlogo2 = binding.teamlogo2;


        _info_divider = binding.infoDivider;
        _info_layout = binding.infoLayout;
        _info_layout1 = binding.infoLayout1;
        _info_layout2 = binding.infoLayout2;

        _img1 = binding.img1;
        _img2 = binding.img2;

        _n1 = binding.n1;
        _n2 = binding.n2;

        _status1 = binding.status1;
        _status2 = binding.status2;


        _facts_divider = binding.factsDivider;
        _facts_layout = binding.factsLayout;

        _f1_layout = binding.f1Layout;
        _f2_layout = binding.f2Layout;
        _f3_layout = binding.f3Layout;
        _f4_layout = binding.f4Layout;
        _f5_layout = binding.f5Layout;
        _f6_layout = binding.f6Layout;

        _name = binding.name;
        _value = binding.value;

        _name1 = binding.name1;
        _value1 = binding.value1;

        _name2 = binding.name2;
        _value2 = binding.value2;

        _name3 = binding.name3;
        _value3 = binding.value3;

        _name4 = binding.name4;
        _value4 = binding.value4;

        _name5 = binding.name5;
        _value5 = binding.value5;


    }

    @SuppressLint("SetTextI18n")
    public void ApiCall() {
        APIService apiService6 = RestManager.getLeague1().create(APIService.class);
        Call<DetailsMain> call6 = apiService6.getMatch(sportid);

        call6.enqueue(new Callback<DetailsMain>() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<DetailsMain> call, @NonNull Response<DetailsMain> response) {
                if (response.body() != null) {
                    synchronized (response.body()) {
                        try {

                            try {
                                status = response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getStatus().getCode();

                                mTeam_id1 = response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getHomeTeam().getId();
                                mTeam_id2 = response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getAwayTeam().getId();
                            } catch (Exception e) {
                                Log.d("Formation", "id error: " + e);
                            }

                            try {
                                mFlageName = response.body().getFeaturedMatches().getTournaments().get(0).getCategory().getFlag();
                                Log.d("FLAGE", "name: " + mFlageName);

                            } catch (Exception e) {

                            }

                            loadlogo();

                            try {
                                _leaguename.setText(response.body().getFeaturedMatches().getTournaments().get(0).getTournament().getUniqueName());

                            } catch (Exception e) {
                                Log.d("Formation", "league error: " + e);
                            }

                            try {
                                _cname.setText(response.body().getFeaturedMatches().getTournaments().get(0).getCategory().getName());

                            } catch (Exception e) {
                                Log.d("Formation", "category name error: " + e);
                            }

                            try {
                                _seasoname.setText(response.body().getFeaturedMatches().getTournaments().get(0).getTournament().getName());

                            } catch (Exception e) {
                                Log.d("Formation", "season name error: " + e);
                            }


                            try {
                                _teamname.setText(response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getHomeTeam().getName() +
                                        " - " + response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getAwayTeam().getName());

                            } catch (Exception e) {
                                Log.d("Formation", "team name error: " + e);
                            }


                            try {
                                _countryname.setText(response.body().getHost().getCountry());
                                _hostdivider.setVisibility(View.VISIBLE);
                                _hostlayout.setVisibility(View.VISIBLE);
                            } catch (Exception e) {
                                Log.d("Formation", "host error: " + e);
                                _hostdivider.setVisibility(View.GONE);
                                _hostlayout.setVisibility(View.GONE);
                            }


                            try {
                                SharedPreferences sharedPreferencess = getContext().getSharedPreferences("name", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferencess.edit();
                                editor.putInt("eventid", response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getId());
                                //editor.putInt("seasonid", response.body().getFeaturedMatches().getTournaments().get(0).getSeason().getId());
                                editor.putInt("homeiconid", mTeam_id1);
                                editor.putInt("awayiconid", mTeam_id2);
                                editor.apply();
                            } catch (Exception e) {
                                Log.d("Formation", "shareprefrencees error: " + e);

                            }

                            try {
                                getDate(response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getStartTimestamp());

                            } catch (Exception e) {
                                Log.d("Formation", "date error: " + e);
                            }


                            try {
                                getDateandTime(response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getStartTimestamp());

                            } catch (Exception e) {
                                Log.d("Formation", "date and time error: " + e);
                            }


                            try {
                                C = response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getId();

                            } catch (Exception e) {
                                Log.d("Formation", "event  error: " + e);
                            }

                            mCardView.setOnClickListener(v -> {
                                Intent intent = new Intent(getContext(), ActivityCrickrtMatchInformation.class);
                                if (AdsHelper.isNetworkConnected(requireActivity())) {
                                    MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent, false);
                                } else {
                                    startActivity(intent);
                                }
                            });

                            try {
                                if (response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getHomeScore().getCurrent().toString().isEmpty()) {


                                } else {
                                    Log.d("ABCDEF", "else condition");
                                    _scorelayout.setVisibility(View.VISIBLE);
                                    _matchlayout.setVisibility(View.INVISIBLE);


                                    _homescore.setText(response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getHomeScore().getCurrent().toString());
                                    _awayscore.setText(response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getAwayScore().getCurrent().toString());

                                    A = response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getHomeScore().getCurrent();
                                    B = response.body().getFeaturedMatches().getTournaments().get(0).getEvents().get(0).getAwayScore().getCurrent();


                                    if (A > B) {

                                        _homescore.setTextColor(ContextCompat.getColor(requireContext(), R.color.black));

                                    } else {

                                        _awayscore.setTextColor(ContextCompat.getColor(requireContext(), R.color.black));
                                    }
                                }
                            } catch (Exception e) {
                                Log.d("Formation", "score board: " + e);
                            }


                            try {
                                mName0 = response.body().getFacts().get(0).getName();
                                mValue0 = response.body().getFacts().get(0).getValue();

                                mName01 = response.body().getFacts().get(1).getName();
                                mValue01 = response.body().getFacts().get(1).getValue();

                                mName02 = response.body().getFacts().get(2).getName();
                                mValue02 = response.body().getFacts().get(2).getValue();

                                mName03 = response.body().getFacts().get(3).getName();
                                mValue03 = response.body().getFacts().get(3).getValue();

                                mName04 = response.body().getFacts().get(4).getName();
                                mValue04 = response.body().getFacts().get(4).getValue();

                                mName05 = response.body().getFacts().get(5).getName();
                                mValue05 = response.body().getFacts().get(5).getValue();
                            } catch (Exception e) {
                                Log.d("Formation", "facts error " + e);

                            }

                            try {
                                _n1.setText(response.body().getTitleHolder().getName());
                                _status1.setText("Title holder (" + response.body().getTitleHolderTitles().toString() + ")");
                                int team1id = response.body().getTitleHolder().getId();
                                mTeam1logo = ActivityLaunch.LogoLink_2 + team1id + "/logo";


                                _info_divider.setVisibility(View.VISIBLE);
                                _info_layout.setVisibility(View.VISIBLE);
                                _info_layout1.setVisibility(View.VISIBLE);


                            } catch (Exception e) {
                                Log.d("TAG", "onResponse:...... " + e);

                            }

                            try {
                                _n2.setText(response.body().getMostTitles().getTeams().get(0).getName());
                                _status2.setText("Most titles (" + response.body().getMostTitles().getCount() + ")");
                                int team2id = response.body().getMostTitles().getTeams().get(0).getId();
                                mTeam2logo = ActivityLaunch.LogoLink_2 + team2id + "/logo";


                                _info_divider.setVisibility(View.VISIBLE);
                                _info_layout.setVisibility(View.VISIBLE);
                                _info_layout2.setVisibility(View.VISIBLE);

                            } catch (Exception e) {
                                Log.d("TAG", "onResponse 2:...... " + e);
                            }


                        } catch (Exception e) {
                            Log.d("Formation", "main error: " + e);

                        }

                        if (mName0 == null && mName01 == null && mName02 == null && mName03 == null && mName04 == null && mName05 == null) {

                        } else {
                            if (mName0 == null) {

                            } else {
                                _f1_layout.setVisibility(View.VISIBLE);
                                _name.setText(mName0);
                                _value.setText(mValue0);
                            }

                            if (mName01 == null) {

                            } else {
                                _f2_layout.setVisibility(View.VISIBLE);
                                _name1.setText(mName01);
                                _value1.setText(mValue01);
                            }

                            if (mName02 == null) {

                            } else {
                                _f3_layout.setVisibility(View.VISIBLE);
                                _name2.setText(mName02);
                                _value2.setText(mValue02);
                            }

                            if (mName03 == null) {

                            } else {
                                _f4_layout.setVisibility(View.VISIBLE);
                                _name3.setText(mName03);
                                _value3.setText(mValue03);
                            }

                            if (mName04 == null) {

                            } else {
                                _f5_layout.setVisibility(View.VISIBLE);
                                _name4.setText(mName04);
                                _value4.setText(mValue04);
                            }

                            if (mName05 == null) {

                            } else {
                                _f6_layout.setVisibility(View.VISIBLE);
                                _name5.setText(mName05);
                                _value5.setText(mValue05);
                            }
                            _facts_divider.setVisibility(View.VISIBLE);
                            _facts_layout.setVisibility(View.VISIBLE);
                        }

                    }
                }
                if (status == 1) {
                    loadlogo();
                } else if (status == 0) {
                    loadlogo();
                } else if (status == 100) {
                    loadlogo();
                }

            }

            @Override
            public void onFailure(@NonNull Call<DetailsMain> call, @NonNull Throwable t) {

            }
        });

    }

    private void loadlogo() {
        try {
            int abs = mCountry.Createarray().get(mFlageName);
            Log.d("FLAGE", "loadlogo: " + abs);
            mFlaage = ActivityLaunch.LogoLink_2 + abs + "/logo";
            Glide.with(_context)
                    .load(mFlaage)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(_flage);

        } catch (Exception e) {

        }


        mUrl = ActivityLaunch.LogoLink + sportid + "/logo";
        mTeam1 = ActivityLaunch.LogoLink_2 + mTeam_id1 + "/logo";
        mTeam2 = ActivityLaunch.LogoLink_2 + mTeam_id2 + "/logo";

        Glide.with(_context)
                .load(mUrl)
                .centerCrop()
                .placeholder(R.drawable.ic_baseline_error_24)
                .into(_imageView);


        Glide.with(_context)
                .load(mTeam1)
                .centerCrop()
                .into(_teamlogo1);

        Glide.with(_context)
                .load(mTeam2)
                .centerCrop()
                .into(_teamlogo2);

        Glide.with(_context)
                .load(mTeam1logo)
                .centerCrop()
                .into(_img1);

        Glide.with(_context)
                .load(mTeam2logo)
                .centerCrop()
                .into(_img2);
    }

    @SuppressLint("SetTextI18n")
    private void getDateandTime(long time) {

        Date date2 = new Date(); // *1000 is to convert seconds to milliseconds
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyy "); // the format of your date
        sdf1.setTimeZone(TimeZone.getDefault());

        String currenttime = sdf1.format(date2);

        Log.d("PRASHANT", "simplmethod - time" + sdf1.format(date2));


        Date date = new Date(time * 1000); // *1000 is to convert seconds to milliseconds
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); // the format of your date
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf11 = new SimpleDateFormat("K:mm aa"); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault());

        String matchtimee = sdf.format(date);
        Log.d("PRASHANT", "match time" + matchtimee);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(currenttime);
            d2 = format.parse(matchtimee);

            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d2);

            Log.d("TIME", Days.daysBetween(dt1, dt2).getDays() + " days, ");


            int d = Days.daysBetween(dt1, dt2).getDays();

            Log.d("PRASHANT", "days :" + d);

            if (d == 0) {
                _matchtime.setText("ToDay " + sdf11.format(date));

            } else if (d == 1) {
                _matchtime.setText("Tommorow " + sdf11.format(date));
            } else {
                _matchtime.setText("In " + d + " Days");

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private String getDate(long time) {

        Date date2 = new Date();
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyy K:mm:ss aa ");
        sdf1.setTimeZone(TimeZone.getDefault());

        String currenttime = sdf1.format(date2);

        Log.d("PRASHANT", "simplmethod - time" + sdf1.format(date2));


        Date date = new Date(time * 1000);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy K:mm:ss aa ");
        sdf.setTimeZone(TimeZone.getDefault());

        String matchtime = sdf.format(date);
        Log.d("PRASHANT", "match time" + matchtime);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy K:mm:ss aa");

        Date d1 = null;
        Date d2 = null;

        try {

            d1 = format.parse(currenttime);
            d2 = format.parse(matchtime);

            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d2);

            Log.d("TIME", Days.daysBetween(dt1, dt2).getDays() + " days, ");
            Log.d("TIME", Hours.hoursBetween(dt1, dt2).getHours() % 24 + " hours, ");
            Log.d("TIME", Minutes.minutesBetween(dt1, dt2).getMinutes() % 60 + " minutes, ");
            Log.d("TIME", Seconds.secondsBetween(dt1, dt2).getSeconds() % 60 + " seconds.");
            int a = Hours.hoursBetween(dt1, dt2).getHours() % 24;
            int b = Minutes.minutesBetween(dt1, dt2).getMinutes() % 60;
            int c = Seconds.secondsBetween(dt1, dt2).getSeconds() % 60;
            mDay = Days.daysBetween(dt1, dt2).getDays();

            _matchlayout.setVisibility(View.VISIBLE);

        } catch (Exception e) {

        }

        if (mDay == 0) {

        }
        return sdf.format(date);
    }

    public static class NewsRepository {
        private static NewsRepository newsRepository;
        private final APIService newsApi;
        Call<DetailsMain> call6;


        public NewsRepository() {

            Log.d("SPORTID", "NewsRepository: " + sportid);

            newsApi = RestManager.getLeague1().create(APIService.class);
            call6 = newsApi.getMatch(sportid);
        }

        public static NewsRepository getInstance() {

            if (newsRepository == null) {
                newsRepository = new NewsRepository();
            }


            return newsRepository;
        }

        public MutableLiveData<DetailsMain> getNews() {
            MutableLiveData<DetailsMain> newsData = new MutableLiveData<>();

            call6.enqueue(new Callback<DetailsMain>() {
                @Override
                public void onResponse(Call<DetailsMain> call, Response<DetailsMain> response) {
                    if (response.isSuccessful()) {
                        newsData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<DetailsMain> call, Throwable t) {
                    newsData.setValue(null);
                }
            });

            return newsData;
        }
    }

}

