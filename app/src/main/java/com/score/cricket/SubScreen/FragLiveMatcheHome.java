package com.score.cricket.SubScreen;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Repeater.RepeaterMatchMain;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Models.HomeMain.HomeMatcheChildInfo;
import com.score.cricket.Models.HomeMain.HomeMatcheGroupInfo;
import com.score.cricket.Models.AppMatches.AppMatch.MatchesExample;
import com.score.cricket.Models.AppMatches.Matches_Example;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Helper.Logger;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.databinding.FragLineupBinding;
import com.score.cricket.databinding.FragMatchHomeBinding;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragLiveMatcheHome extends Frag_Base implements IEventListener {

    static ArrayList<Object> mIconarray = new ArrayList<>();
    private final LinkedHashMap<String, HomeMatcheGroupInfo> mSubjects = new LinkedHashMap<>();
    private final ArrayList<HomeMatcheGroupInfo> mDeptList = new ArrayList<>();
    LocalDate mLocalDate;
    String mCatName, mCompare;
    String s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11 = null, s12, mCurrentteam, mFlagename;
    RepeaterMatchMain listRepeater;
    int a, a1, a2, a3, a4, mMainId;
    DatePickerDialog mDatePickerDialog;
    LocalDate mLocalDate1;
    SharedPreferences sharedPreferences;
    private RecyclerView _recyclerView;
    private SwipeRefreshLayout _swipeRefreshLayout;
    FrameLayout _fl_adplaceholder;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(_fl_adplaceholder,requireActivity(),1), 500));
        }
        return eventState;
    }
    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragMatchHomeBinding binding = FragMatchHomeBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();
        _fl_adplaceholder = binding.flAdplaceholder;
        _recyclerView = binding.recyclerView;
        _swipeRefreshLayout = binding.swiperefresh;
        registerAdsListener();
        setHasOptionsMenu(true);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _recyclerView.setLayoutManager(new LinearLayoutManager(requireActivity()));

        MyApplication.getInstance().loadNativeFullAds(_fl_adplaceholder,requireActivity(),1);

        sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        mLocalDate = new LocalDate();
        Logger.AppLog("TOUR", "Date" + mLocalDate);
        String localdate = String.valueOf(mLocalDate);
        editor.putString("localdate", localdate);
        editor.apply();

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        mDatePickerDialog = new DatePickerDialog(getContext(),
                (datePicker, year1, month1, day) -> {
                    int m = month1 + 1;
                    String date = year1 + "-" + m + "-" + day;

                    editor.putString("localdate", date);
                    editor.apply();
                    ApiCall();
                }, year, month, dayOfMonth);

        listRepeater = new RepeaterMatchMain(requireActivity(), mDeptList);
        _recyclerView.setAdapter(listRepeater);
        ApiCall();

        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCall();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);
            
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.calender) {
            mDatePickerDialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("NewApi")
    private void ApiCall() {
        showProgressDialog(requireActivity());
        mIconarray.clear();
        mSubjects.clear();
        mDeptList.clear();

        mLocalDate1 = LocalDate.parse(sharedPreferences.getString("localdate", "1999-06-21"));

        APIService apiService = RestManager.getMatch().create(APIService.class);
        Call<MatchesExample> call = apiService.getMatch("cricket", mLocalDate1);

        call.enqueue(new Callback<MatchesExample>() {
            @Override
            public void onResponse(@NonNull Call<MatchesExample> call, @NonNull Response<MatchesExample> response) {
                mIconarray = new ArrayList<>();
                if (response.body() != null) {
                    synchronized (response.body().getCategories()) {
                        for (int i = 0; i < response.body().getCategories().size(); i++) {
                            a = response.body().getCategories().get(i).getCategory().getId();
                            mCatName = response.body().getCategories().get(i).getCategory().getName();
                            try {
                                mFlagename = response.body().getCategories().get(i).getCategory().getFlag();
                            } catch (Exception e) {
                                Logger.AppLog("FLAGE", "time error" + e);
                            }
                            LoadMatchInfo(a, mCatName, mFlagename);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<MatchesExample> call, @NonNull Throwable t) {
                Logger.AppLog("CRICKETSCORE", "respons-15error/15..." + t);
            }
        });
    }

    private void LoadMatchInfo(int d, String name, String mFlagename) {
        mLocalDate1 = LocalDate.parse(sharedPreferences.getString("localdate", "1999-06-21"));
        Logger.AppLog("CRICKETSCORE", "nwe date..." + mLocalDate1.toString());

        APIService apiService = RestManager.getPlayer().create(APIService.class);
        Call<Matches_Example> call = apiService.getPlayer("cricket", d, mLocalDate1);

        call.enqueue(new Callback<Matches_Example>() {
            @Override
            public void onResponse(@NonNull Call<Matches_Example> call, @NonNull Response<Matches_Example> response) {
                dismissProgressDialog(requireActivity());
                try {
                    if (response.body() != null) {
                        for (int j = 0; j < response.body().getTournaments().size(); j++) {

                            for (int i = 0; i < response.body().getTournaments().get(j).getEvents().size(); i++) {
                                try {
                                    long time = response.body().getTournaments().get(j).getEvents().get(i).getStartTimestamp();
                                    Date date = new Date(time * 1000);
                                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd.MMM"); // the format of your date
                                    sdf.setTimeZone(TimeZone.getDefault());

                                    Logger.AppLog("CRICKETSCORE", "time" + sdf.format(date));

                                    Date date1 = new Date(time * 1000);
                                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("K:mm aa"); // the format of your date
                                    sdf1.setTimeZone(TimeZone.getDefault());
                                    Logger.AppLog("CRICKETSCORE", "time" + sdf1.format(date1));

                                    try {
                                        s = response.body().getTournaments().get(j).getEvents().get(i).getHomeTeam().getName();
                                        Logger.AppLog("CRICKETSCORE", "time" + s);
                                    } catch (Exception e) {
                                        s = "null";
                                    }

                                    try {
                                        s1 = response.body().getTournaments().get(j).getEvents().get(i).getAwayTeam().getName();
                                        Logger.AppLog("CRICKETSCORE", "time" + s1);
                                    } catch (Exception e) {
                                        s1 = "null";
                                    }

                                    try {
                                        s2 = response.body().getTournaments().get(j).getEvents().get(i).getHomeScore().getDisplay().toString();
                                        Logger.AppLog("CRICKETSCORE", "time" + s2);
                                    } catch (Exception e) {
                                        s2 = "null";
                                    }

                                    try {
                                        s3 = response.body().getTournaments().get(j).getEvents().get(i).getAwayScore().getDisplay().toString();
                                        Logger.AppLog("CRICKETSCORE", "time" + s3);
                                    } catch (Exception e) {
                                        s3 = "null";
                                    }

                                    try {
                                        s4 = response.body().getTournaments().get(j).getEvents().get(i).getHomeScore().getInnings().getInning1().getWickets().toString();
                                        Logger.AppLog("CRICKETSCORE", "time" + s4);
                                    } catch (Exception e) {
                                        s4 = "null";
                                    }

                                    try {
                                        s5 = response.body().getTournaments().get(j).getEvents().get(i).getAwayScore().getInnings().getInning1().getWickets().toString();
                                        Logger.AppLog("CRICKETSCORE", "time" + s5);
                                    } catch (Exception e) {
                                        s5 = "null";
                                    }

                                    try {
                                        s6 = response.body().getTournaments().get(j).getEvents().get(i).getHomeScore().getInnings().getInning1().getOvers().toString();
                                        Logger.AppLog("CRICKETSCORE", "time" + s6);
                                    } catch (Exception e) {
                                        s6 = "null";
                                    }

                                    try {
                                        s7 = response.body().getTournaments().get(j).getEvents().get(i).getAwayScore().getInnings().getInning1().getOvers().toString();
                                        Logger.AppLog("CRICKETSCORE", "time" + s7);
                                    } catch (Exception e) {
                                        s7 = "null";
                                    }

                                    try {
                                        s8 = sdf.format(date);
                                    } catch (Exception e) {
                                        s8 = "null";
                                    }

                                    try {
                                        s9 = sdf1.format(date1);
                                    } catch (Exception e) {
                                        s9 = "null";
                                    }

                                    try {
                                        s10 = response.body().getTournaments().get(j).getEvents().get(i).getNote();
                                        Logger.AppLog("CRICKETSCORE", "time" + s10);
                                    } catch (Exception e) {
                                        s10 = "null";
                                    }

                                    try {
                                        if (s11 == null) {
                                            Logger.AppLog("COUNTRY..1", "time" + name);
                                            s11 = response.body().getTournaments().get(j).getTournament().getName();
                                            mCompare = s11;
                                        } else if (response.body().getTournaments().get(j).getTournament().getName().equals(mCompare)) {
                                            Logger.AppLog("COUNTRY..2", "time" + name);
                                            s11 = "null";
                                        } else {
                                            Logger.AppLog("COUNTRY..3", "time" + name);
                                            s11 = response.body().getTournaments().get(j).getTournament().getName();
                                            mCompare = s11;
                                        }

                                        Logger.AppLog("CRICKETSCORE", "time" + s11);
                                    } catch (Exception e) {
                                        s11 = "null";
                                    }

                                    try {
                                        a1 = response.body().getTournaments().get(j).getEvents().get(i).getId();
                                        Logger.AppLog("CRICKETSCORE", "time" + a1);
                                    } catch (Exception e) {
                                    }


                                    try {
                                        a2 = response.body().getTournaments().get(j).getSeason().getId();
                                        Logger.AppLog("CRICKETSCORE", "time" + a2);
                                    } catch (Exception e) {
                                    }

                                    try {
                                        a3 = response.body().getTournaments().get(j).getTournament().getUniqueId();
                                        Logger.AppLog("CRICKETSCORE", "time" + a2);
                                    } catch (Exception e) {
                                    }


                                    try {
                                        a4 = response.body().getTournaments().get(j).getEvents().get(i).getWinnerCode();
                                        Logger.AppLog("CRICKETSCORE", "time" + a2);
                                    } catch (Exception e) {
                                    }

                                    try {
                                        s12 = response.body().getTournaments().get(j).getEvents().get(i).getStatus().getType();
                                        Logger.AppLog("CRICKETSCORE", "time" + a2);
                                    } catch (Exception e) {
                                    }

                                    try {
                                        mCurrentteam = response.body().getTournaments().get(j).getEvents().get(i).getCurrentBattingTeam().getName();
                                        Logger.AppLog("CRICKETSCORE", "time" + a2);
                                    } catch (Exception e) {
                                        mCurrentteam = null;
                                    }

                                    try {
                                        mMainId = response.body().getTournaments().get(j).getCategory().getId();
                                        Logger.AppLog("CRICKETSCORE", "time" + a2);
                                    } catch (Exception e) {

                                    }


                                    loadata(name, s11, s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s12, mCurrentteam, a1, a2, a3, a4, i, mMainId, time, mFlagename);
                                    listRepeater.notifyDataSetChanged();


                                } catch (Exception e) {
                                    Logger.AppLog("CRICKETSCORE", "Error-" + e);
                                }

                            }

                        }

                    }
                } catch (Exception e) {
                    Logger.AppLog("CRICKETSCORE", "Error-" + e);
                }

            }

            @Override
            public void onFailure(@NonNull Call<Matches_Example> call, @NonNull Throwable t) {
                Logger.AppLog("CRICKETSCORE", "Error-" + t);
            }
        });

    }

    private int loadata(String name, String s11, String s, String s1, String s2, String s3, String s4, String s5, String s6, String s7, String s8, String s9, String s10, String s12, String mCurrentteam, int a1, int a2, int a3, int a4, int position, int mainid, long time, String mFlagename) {


        int groupPosition = 0;

        HomeMatcheGroupInfo headerInfo = mSubjects.get(name);
        if (headerInfo == null) {
            headerInfo = new HomeMatcheGroupInfo();
            headerInfo.setName(name);
            headerInfo.setFlage(mFlagename);
            mSubjects.put(name, headerInfo);
            mDeptList.add(headerInfo);
        }

        ArrayList<HomeMatcheChildInfo> productList = headerInfo.getProductList();
        int listSize = productList.size();
        listSize++;

        HomeMatcheChildInfo detailInfo = new HomeMatcheChildInfo();
        detailInfo.setName(s11);
        detailInfo.setHometeam(s);
        detailInfo.setAwayteam(s1);
        detailInfo.setHomescore(s2);
        detailInfo.setAwayscore(s3);
        detailInfo.setHomewicket(s4);
        detailInfo.setAwaywicket(s5);
        detailInfo.setHomeover(s6);
        detailInfo.setAwayover(s7);
        detailInfo.setDate(s8);
        detailInfo.setTime(s9);
        detailInfo.setStatus(s10);
        detailInfo.setMatchstatus(s12);
        detailInfo.setCurrentBattingTeam(mCurrentteam);
        detailInfo.setId(a1);
        detailInfo.setSeasonid(a2);
        detailInfo.setTournamentid(a3);
        detailInfo.setWinnercode(a4);
        detailInfo.setPosition(position);
        detailInfo.setMain_id(mainid);
        detailInfo.setTimestamp(time);
        productList.add(detailInfo);

        headerInfo.setProductList(productList);

        groupPosition = mDeptList.indexOf(headerInfo);
        return groupPosition;
    }
}

