package com.score.cricket.SubScreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.MainScreen.ActivityStringInfomation;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.databinding.FragBannerBinding;

public class FragBanner extends Fragment {
    ImageView _ivPoster;
    int mCurrPos;
    int[] mMyImageList = new int[]{R.drawable.ic_poster1, R.drawable.ic_poster2, R.drawable.ic_poster3, R.drawable.ic_poster4, R.drawable.ic_poster5,R.drawable.ic_poster6};

    public FragBanner() {
    }
    public static FragBanner newInstance(String param1, String param2) {
        FragBanner fragment = new FragBanner();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragBannerBinding binding = FragBannerBinding.inflate(LayoutInflater.from(requireActivity()),container,false);
        View view = binding.getRoot();
        assert getArguments() != null;
        mCurrPos = getArguments().getInt("CurrentPosition");
        _ivPoster = binding.ivPoster;
        _ivPoster.setImageResource(mMyImageList[mCurrPos]);

        _ivPoster.setOnClickListener(view1 -> {
            Intent intent = new Intent(requireActivity(), ActivityStringInfomation.class);
            intent.putExtra("currPos",mCurrPos);
            if (AdsHelper.isNetworkConnected(requireActivity())){
                MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent, false);
            }else {
                startActivity(intent);
            }
        });
        return view;
    }
}