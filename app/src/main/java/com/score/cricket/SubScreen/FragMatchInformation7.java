package com.score.cricket.SubScreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.Models.HomeMain.HomeTeam_Matche;
import com.score.cricket.Models.TeamDetails.TeamMatche.TeamExample;
import com.score.cricket.MyApplication;
import com.score.cricket.Repeater.RapaeterMatch;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragMatchInformation7Binding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragMatchInformation7 extends Fragment implements IEventListener {


    FrameLayout fl_adplaceholder;
    private int mSeasonid;
    private ArrayList<ListnerItems> mItems = new ArrayList<>();
    private Activity mContext;
    private RapaeterMatch repeater;
    private RecyclerView _recyclerView;
    private String s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10;
    private int a, b;
    private SwipeRefreshLayout _swipeRefreshLayout;

    public FragMatchInformation7() {
    }

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        mSeasonid = sharedPreferences.getInt("team_id", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragMatchInformation7Binding binding = FragMatchInformation7Binding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();

        mContext = getActivity();

        _recyclerView = binding.matcheRv1;
        _recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        _recyclerView.setLayoutManager(layoutManager);

        repeater = new RapaeterMatch(mContext, mItems);
        _recyclerView.setAdapter(repeater);

        ApiCall();

        _swipeRefreshLayout = binding.swiperefresh;

        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCall();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1);
    }

    private void ApiCall() {
        APIService service = RestManager.getPlayer().create(APIService.class);
        Call<TeamExample> call13 = service.getmatchboard(mSeasonid);

        call13.enqueue(new Callback<TeamExample>() {
            @Override
            public void onResponse(Call<TeamExample> call, Response<TeamExample> response) {

                mItems.clear();
                for (int j = 0; j < response.body().getLast().getTournaments().size(); j++) {
                    mItems.add(new HomeHeader(response.body().getLast().getTournaments().get(j).getTournament().getName().toString(),
                            response.body().getLast().getTournaments().get(j).getCategory().getFlag()));

                    for (int i = 0; i < response.body().getLast().getTournaments().get(j).getEvents().size(); i++) {

                        long time = response.body().getLast().getTournaments().get(j).getEvents().get(i).getStartTimestamp();

                        Date date = new Date(time * 1000);
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd.MMM");
                        sdf.setTimeZone(TimeZone.getDefault());

                        Log.d("XYZZZ", "time" + sdf.format(date));

                        Date date1 = new Date(time * 1000);
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("K:mm aa");
                        sdf1.setTimeZone(TimeZone.getDefault());
                        Log.d("XYZZZ", "time" + sdf1.format(date1));

                        try {
                            s = response.body().getLast().getTournaments().get(j).getEvents().get(i).getHomeTeam().getName().toString();
                        } catch (Exception e) {
                            s = null;
                        }

                        try {
                            s1 = response.body().getLast().getTournaments().get(j).getEvents().get(i).getAwayTeam().getName().toString();
                        } catch (Exception e) {
                            s1 = null;

                        }

                        try {
                            s2 = response.body().getLast().getTournaments().get(j).getEvents().get(i).getHomeScore().getDisplay().toString();

                        } catch (Exception e) {
                            s2 = null;
                        }

                        try {
                            s3 = response.body().getLast().getTournaments().get(j).getEvents().get(i).getAwayScore().getDisplay().toString();
                        } catch (Exception e) {
                            s3 = null;
                        }

                        try {
                            s4 = response.body().getLast().getTournaments().get(j).getEvents().get(i).getHomeScore().getInnings().getInning1().getWickets().toString();
                        } catch (Exception e) {
                            s4 = null;
                        }

                        try {
                            s5 = response.body().getLast().getTournaments().get(j).getEvents().get(i).getAwayScore().getInnings().getInning1().getWickets().toString();
                        } catch (Exception e) {
                            s5 = null;
                        }

                        try {
                            s6 = response.body().getLast().getTournaments().get(j).getEvents().get(i).getHomeScore().getInnings().getInning1().getOvers().toString();
                        } catch (Exception e) {
                            s6 = null;
                        }

                        try {
                            s7 = response.body().getLast().getTournaments().get(j).getEvents().get(i).getAwayScore().getInnings().getInning1().getOvers().toString();
                        } catch (Exception e) {
                            s7 = null;
                        }

                        try {
                            s8 = sdf.format(date);
                        } catch (Exception e) {
                            s8 = null;
                        }

                        try {
                            s9 = sdf1.format(date1);
                        } catch (Exception e) {
                            s9 = null;
                        }

                        try {
                            s10 = response.body().getLast().getTournaments().get(j).getEvents().get(i).getNote().toString();
                        } catch (Exception e) {
                            s10 = null;
                        }

                        try {
                            a = response.body().getLast().getTournaments().get(j).getEvents().get(i).getId();

                        } catch (Exception e) {
                            a = 0;

                        }

                        try {

                            b = response.body().getLast().getTournaments().get(j).getEvents().get(i).getWinnerCode();
                        } catch (Exception e) {
                            b = 0;
                        }

                        mItems.add(new HomeTeam_Matche(s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, a, b));
                        repeater.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<TeamExample> call, Throwable t) {
                Log.d("XYZZZ", "Error" + t.toString());
            }
        });
    }
}