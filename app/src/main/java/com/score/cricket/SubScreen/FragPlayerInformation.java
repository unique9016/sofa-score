package com.score.cricket.SubScreen;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Helper.WorldCountryIdList;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.PlayerDetails.Players_Example;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.databinding.FragMatchInningsInformationBinding;
import com.score.cricket.databinding.FragPlayerInformartionBinding;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragPlayerInformation extends Fragment implements IEventListener {

    private Context mContext;
    private int mPlayer_id;
    private TextView _teamname, _nationality, _age, _dob, _shirtnumber, _batting, _bowling, _role, _textView16, _textView17;
    private ImageView _country_logo, _teamlogo;
    private LinearLayout _batting_layout, _bowling_layout, _role_layout;
    private CardView _header_layout;

    private SwipeRefreshLayout _swipeRefreshLayout;
    FrameLayout fl_adplaceholder;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("name", MODE_PRIVATE);
        mPlayer_id = sharedPreferences.getInt("mPlayer_id", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragPlayerInformartionBinding binding = FragPlayerInformartionBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();

        UIInitialize(binding);
        ApiCall();

        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCall();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);
        });
        
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1);
    }


    private void UIInitialize(FragPlayerInformartionBinding binding) {

        _teamname = binding.teamname;
        _country_logo = binding.countrylogo;
        _teamlogo = binding.teamlogo;

        _swipeRefreshLayout = binding.swiperefresh;

        _nationality = binding.nationality;
        _age = binding.age;
        _dob = binding.dob;
        _shirtnumber = binding.shirtnumber;
        _batting = binding.batting;
        _bowling = binding.bowling;
        _role = binding.role;

        _textView16 = binding.textView16;
        _textView17 = binding.textview17;

        _header_layout = binding.headerLayout;
        _batting_layout = binding.battingLayout;
        _bowling_layout = binding.bowlingLayout;
        _role_layout = binding.roleLayout;

    }

    private void ApiCall() {

        APIService service = RestManager.getPlayer().create(APIService.class);
        Call<Players_Example> call21 = service.getPlayerDetails(mPlayer_id);

        call21.enqueue(new Callback<Players_Example>() {
            @Override
            public void onResponse(Call<Players_Example> call, Response<Players_Example> response) {

                int id = response.body().getTeam().getId();
                Log.d("FLAGE", "loadlogo: " + response.body().getFlag());
                try {
                    WorldCountryIdList country = new WorldCountryIdList();
                    int abs = country.Createarray().get(response.body().getFlag());
                    Log.d("FLAGE", "loadlogo: " + abs);
                    String flaage = ActivityLaunch.LogoLink_2 + abs + "/logo";
                    Glide.with(mContext)
                            .load(flaage)
                            .centerCrop()
                            .placeholder(R.drawable.ic_baseline_error_24)
                            .into(_country_logo);

                } catch (Exception e) {

                }
                
                String team2 = ActivityLaunch.LogoLink_2 + id + "/logo";

                Glide.with(mContext)
                        .load(team2)
                        .centerCrop()
                        .into(_teamlogo);

                try {
                    _teamname.setText(response.body().getTeam().getFullName().toString());
                } catch (Exception e) {
                    _header_layout.setVisibility(View.INVISIBLE);
                }


                try {
                    _nationality.setText(response.body().getNationality().toString());
                } catch (Exception e) {
                    _country_logo.setVisibility(View.INVISIBLE);
                    _nationality.setVisibility(View.INVISIBLE);
                    _textView16.setVisibility(View.INVISIBLE);
                }

                try {
                    _age.setText(response.body().getAge().toString());
                } catch (Exception e) {
                    _age.setVisibility(View.INVISIBLE);
                }

                try {
                    _shirtnumber.setText(response.body().getShirtNumber().toString());

                } catch (Exception e) {
                    _shirtnumber.setVisibility(View.INVISIBLE);
                    _textView17.setVisibility(View.INVISIBLE);
                }

                try {
                    long timestamp = response.body().getDateOfBirthTimestamp();
                    getDate(timestamp);

                } catch (Exception e) {
                    _dob.setVisibility(View.INVISIBLE);
                }

                try {
                    _batting.setText(response.body().getPlayerInfo().getBatting().toString());
                } catch (Exception e) {
                    _batting_layout.setVisibility(View.GONE);
                }

                try {
                    _bowling.setText(response.body().getPlayerInfo().getBowling().toString());

                } catch (Exception e) {
                    _bowling_layout.setVisibility(View.GONE);

                }

                try {
                    _role.setText(response.body().getPlayerInfo().getRole().toString());

                } catch (Exception e) {
                    _role_layout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Players_Example> call, Throwable t) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private String getDate(long time) {
        Date date = new Date(time * 1000);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd MMM YYYY ");
        sdf.setTimeZone(TimeZone.getDefault());

        Log.d("TIME", "time" + sdf.format(date));
        _dob.setText(sdf.format(date));
        return sdf.format(date);
        
    }
}