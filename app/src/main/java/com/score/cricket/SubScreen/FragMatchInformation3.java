package com.score.cricket.SubScreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.Models.HomeMain.HomeMatches_Matche;
import com.score.cricket.Models.AppMatches.AppMatches_Matches.MatchesApp_Example;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Repeater.RepeaterMatchList;
import com.score.cricket.databinding.FragMatchInformation2Binding;
import com.score.cricket.databinding.FragMatchInformation3Binding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragMatchInformation3 extends Fragment implements IEventListener {
    FrameLayout fl_adplaceholder;
    private int seasonid, id;
    private Activity mContext;
    private ArrayList<ListnerItems> mItems = new ArrayList<>();
    private RepeaterMatchList repater;
    private RecyclerView _recyclerView;
    private String s;
    private String s1;
    private String s2;
    private String s3;
    private String s4;
    private String s5;
    private String s6;
    private String s7;
    private String s8;
    private String s9;
    private String s10;
    private int a;
    private int b;
    private SwipeRefreshLayout _swipeRefreshLayout;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        seasonid = sharedPreferences.getInt("seasonid", 0);
        id = sharedPreferences.getInt("eventid", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragMatchInformation3Binding binding = FragMatchInformation3Binding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();

        mContext = getActivity();

        _swipeRefreshLayout = binding.swiperefresh;
        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCall();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);
        });


        _recyclerView = binding.matcheRv;
        _recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        _recyclerView.setLayoutManager(layoutManager);

        repater = new RepeaterMatchList(mItems, mContext);
        _recyclerView.setAdapter(repater);

        ApiCall();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1);
    }

    private void ApiCall() {
        APIService service = RestManager.getPlayer().create(APIService.class);
        Call<MatchesApp_Example> call12 = service.geth2h(id);


        call12.enqueue(new Callback<MatchesApp_Example>() {
            @Override
            public void onResponse(@NonNull Call<MatchesApp_Example>
                                           call, @NonNull Response<MatchesApp_Example> response) {
                mItems.clear();

                assert response.body() != null;
                for (int j = 0; j < response.body().getTournaments().size(); j++) {


                    mItems.add(new HomeHeader(response.body().getTournaments().get(j).getTournament().getName().toString(),
                            response.body().getTournaments().get(j).getCategory().getFlag().toString()));


                    for (int i = 0; i < response.body().getTournaments().get(j).getEvents().size(); i++) {
                        try {

                            Log.d("HTHGFDB", "..." + i + "..." + j + "..." + response.body().getTournaments().get(j).getEvents().get(i).getHomeTeam().getName().toString());
                            long time = response.body().getTournaments().get(j).getEvents().get(i).getStartTimestamp();

                            Date date = new Date(time * 1000);
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd.MMM");
                            sdf.setTimeZone(TimeZone.getDefault());

                            Log.d("XYZZZ", "time" + sdf.format(date));

                            Date date1 = new Date(time * 1000);
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("K:mm aa");
                            sdf1.setTimeZone(TimeZone.getDefault());
                            Log.d("XYZZZ", "time" + sdf1.format(date1));


                            try {
                                s = response.body().getTournaments().get(j).getEvents().get(i).getHomeTeam().getName().toString();
                            } catch (Exception e) {
                                s = null;
                            }

                            try {
                                s1 = response.body().getTournaments().get(j).getEvents().get(i).getAwayTeam().getName().toString();

                            } catch (Exception e) {
                                s1 = null;

                            }

                            try {
                                s2 = response.body().getTournaments().get(j).getEvents().get(i).getHomeScore().getDisplay().toString();

                            } catch (Exception e) {
                                s2 = null;

                            }

                            try {
                                s3 = response.body().getTournaments().get(j).getEvents().get(i).getAwayScore().getDisplay().toString();

                            } catch (Exception e) {
                                s3 = null;

                            }

                            try {
                                s4 = response.body().getTournaments().get(j).getEvents().get(i).getHomeScore().getInnings().getInning1().getWickets().toString();

                            } catch (Exception e) {
                                s4 = null;

                            }

                            try {
                                s5 = response.body().getTournaments().get(j).getEvents().get(i).getAwayScore().getInnings().getInning1().getWickets().toString();


                            } catch (Exception e) {
                                s5 = null;

                            }

                            try {
                                s6 = response.body().getTournaments().get(j).getEvents().get(i).getHomeScore().getInnings().getInning1().getOvers().toString();


                            } catch (Exception e) {
                                s6 = null;

                            }

                            try {
                                s7 = response.body().getTournaments().get(j).getEvents().get(i).getAwayScore().getInnings().getInning1().getOvers().toString();

                            } catch (Exception e) {
                                s7 = null;

                            }

                            try {
                                s8 = sdf.format(date);

                            } catch (Exception e) {
                                s8 = null;
                            }

                            try {
                                s9 = sdf1.format(date1);

                            } catch (Exception e) {
                                s9 = null;

                            }

                            try {
                                s10 = response.body().getTournaments().get(j).getEvents().get(i).getNote().toString();

                            } catch (Exception e) {
                                s10 = null;

                            }

                            try {
                                a = response.body().getTournaments().get(j).getEvents().get(i).getId();

                            } catch (Exception e) {
                                a = 0;

                            }
                            try {
                                b = response.body().getTournaments().get(j).getEvents().get(i).getWinnerCode();
                            } catch (Exception e) {

                            }


                            mItems.add(new HomeMatches_Matche(s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, a, b));
                            repater.notifyDataSetChanged();


                        } catch (Exception e) {
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MatchesApp_Example> call, Throwable t) {
                Log.d("XYZZZ", "Error" + t.toString());
            }
        });
    }
}