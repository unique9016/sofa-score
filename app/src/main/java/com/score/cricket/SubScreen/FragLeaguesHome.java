package com.score.cricket.SubScreen;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.score.cricket.Helper.Logger;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.Models.HomeMain.HomeChildInfo;
import com.score.cricket.Models.HomeMain.HomeGroupInfo;
import com.score.cricket.Models.LeaguesUi.LeagueAllName.LeagueNameModel;
import com.score.cricket.Models.LeaguesUi.MainLeague;
import com.score.cricket.MyApplication;
import com.score.cricket.Repeater.RepeaterLeaguesMain;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragLeaguesHomeBinding;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragLeaguesHome extends Frag_Base implements IEventListener {
    private static ArrayList<Object> mIconarray = new ArrayList<>();
    final long DELAY_MS = 500;
    final long PERIOD_MS = 3000;
    final long NUM_PAGES = 6;
    private final LinkedHashMap<String, HomeGroupInfo> mSubjects = new LinkedHashMap<>();
    private final LinkedHashMap<String, HomeGroupInfo> mSubjects1 = new LinkedHashMap<>();
    private final ArrayList<HomeGroupInfo> mDeptList = new ArrayList<>();
    private final ArrayList<HomeGroupInfo> mDeptList1 = new ArrayList<>();
    private final ArrayList<Object> mL_cat = new ArrayList<>();
    FrameLayout _fl_adplaceholder;
    ViewPager _vpPosters;
    int currentPage = 0;
    Timer timer;
    private String mS1, mS3, mS4;
    private int mS5, mS6;
    private String s;
    private RepeaterLeaguesMain leaguesRepeater1, leaguesRepeater2;
    private RecyclerView _recyclerView1, _recyclerView2;
    private String mFlagename;
    private String mFlagename1;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(_fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mL_cat.add("africa");
        mL_cat.add("asia");
        mL_cat.add("europe");
        mL_cat.add("north-and-central-america");
        mL_cat.add("oceania");
        mL_cat.add("south America");
        mL_cat.add("world");
        mL_cat.add("international");
        mL_cat.add("international Youth");
        mL_cat.add("international Clubs");
        mL_cat.add("south-america");

        mSubjects.clear();
        mSubjects1.clear();
        mDeptList.clear();
        mDeptList1.clear();
        LoadCategoriLeague();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragLeaguesHomeBinding binding = FragLeaguesHomeBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();
        _fl_adplaceholder = binding.flAdplaceholder;

        _vpPosters = binding.vpPosters;
        _recyclerView1 = binding.recyclerView1;
        _recyclerView2 = binding.recyclerView2;
        ViewPagerAdapter adapter = new ViewPagerAdapter(requireActivity().getSupportFragmentManager());
        _vpPosters.setAdapter(adapter);

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0;
                }
                _vpPosters.setCurrentItem(currentPage++, true);
            }
        };

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        _vpPosters.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        registerAdsListener();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MyApplication.getInstance().loadNativeFullAds(_fl_adplaceholder, requireActivity(), 1);

        _recyclerView1.setLayoutManager(new LinearLayoutManager(requireActivity()));
        _recyclerView2.setLayoutManager(new LinearLayoutManager(requireActivity()));
    }
    private void ExpandebleListview() {
        new Handler().postDelayed(() -> dismissProgressDialog(requireActivity()), 500);

        leaguesRepeater1 = new RepeaterLeaguesMain(requireActivity(), mDeptList);
        _recyclerView1.setAdapter(leaguesRepeater1);

        leaguesRepeater2 = new RepeaterLeaguesMain(requireActivity(), mDeptList1);
        _recyclerView2.setAdapter(leaguesRepeater2);
    }

    private void LoadCategoriLeague() {
        showProgressDialog(requireActivity());
        APIService apiService = RestManager.getLeague().create(APIService.class);
        Call<MainLeague> call = apiService.getLeague("cricket");

        call.enqueue(new Callback<MainLeague>() {
            @Override
            public void onResponse(@NonNull Call<MainLeague> call, @NonNull Response<MainLeague> response) {
                mIconarray = new ArrayList<>();
                if (response.body() != null) {
                    synchronized (response.body().getCategories()) {

                        for (int i = 0; i < response.body().getCategories().size(); i++) {
                            mIconarray.add(response.body().getCategories().get(i).getId());
                        }

                        for (int i = 0; i < mIconarray.size(); i++) {
                            int mID = (int) mIconarray.get(i);
                            LoadLeageName(mID);
                        }
                    }
                } else {
                    Logger.Toast(requireActivity(), "Something went Wrong!!");
                }
            }
            @Override
            public void onFailure(@NonNull Call<MainLeague> call, @NonNull Throwable t) {
                Logger.AppLog("ERROR", t.getMessage());
            }
        });

    }

    private void LoadLeageName(int d) {
        APIService apiService = RestManager.getLeagueName().create(APIService.class);
        Call<LeagueNameModel> call = apiService.getLeagueName(d);

        call.enqueue(new Callback<LeagueNameModel>() {
            @Override
            public void onResponse(@NonNull Call<LeagueNameModel> call, @NonNull Response<LeagueNameModel> response) {
                if (response.body() != null) {
                    for (int i = 0; i < response.body().getGroups().get(0).getUniqueTournaments().size(); i++) {

                        if (mL_cat.contains(response.body().getGroups().get(0).getUniqueTournaments().get(i).getCategory().getFlag())) {
                            mFlagename = response.body().getGroups().get(0).getUniqueTournaments().get(i).getCategory().getFlag();
                            s = response.body().getGroups().get(0).getUniqueTournaments().get(i).getCategory().getName();
                            mS1 = response.body().getGroups().get(0).getUniqueTournaments().get(i).getName();
                            mS5 = response.body().getGroups().get(0).getUniqueTournaments().get(i).getId();
                            addProduct(s, mS1, mS5, mFlagename);
                        } else {
                            mFlagename1 = response.body().getGroups().get(0).getUniqueTournaments().get(i).getCategory().getFlag();
                            mS3 = response.body().getGroups().get(0).getUniqueTournaments().get(i).getCategory().getName();
                            mS4 = response.body().getGroups().get(0).getUniqueTournaments().get(i).getName();
                            mS6 = response.body().getGroups().get(0).getUniqueTournaments().get(i).getId();
                            addProduct1(mS3, mS4, mS6, mFlagename1);
                        }
                        ExpandebleListview();
                    }

                } else {
                    Logger.AppLog("ERROR", "Response Fail");
                }

            }

            @Override
            public void onFailure(@NonNull Call<LeagueNameModel> call, @NonNull Throwable t) {
                Logger.AppLog("ERROR", t.getMessage());
            }
        });
    }

    private int addProduct(String department, String product, int id, String mFlagename) {

        int groupPosition = 0;

        HomeGroupInfo headerInfo = mSubjects.get(department);
        if (headerInfo == null) {
            headerInfo = new HomeGroupInfo();
            headerInfo.setName(department);
            headerInfo.setFlagename(mFlagename);
            mSubjects.put(department, headerInfo);
            mDeptList.add(headerInfo);
        }

        ArrayList<HomeChildInfo> productList = headerInfo.getProductList();
        int listSize = productList.size();
        listSize++;

        HomeChildInfo detailInfo = new HomeChildInfo();
        detailInfo.setSequence(String.valueOf(listSize));
        detailInfo.setName(product);
        detailInfo.setId(id);
        productList.add(detailInfo);
        headerInfo.setProductList(productList);

        groupPosition = mDeptList.indexOf(headerInfo);
        return groupPosition;
    }

    private int addProduct1(String department1, String product1, int id1, String mFlagename1) {
        int groupPosition1 = 0;

        HomeGroupInfo headerInfo1 = mSubjects1.get(department1);
        if (headerInfo1 == null) {
            headerInfo1 = new HomeGroupInfo();
            headerInfo1.setName(department1);
            headerInfo1.setFlagename(mFlagename1);
            mSubjects1.put(department1, headerInfo1);
            mDeptList1.add(headerInfo1);
        }

        ArrayList<HomeChildInfo> productList1 = headerInfo1.getProductList();
        int listSize = productList1.size();
        listSize++;

        HomeChildInfo detailInfo1 = new HomeChildInfo();
        detailInfo1.setSequence(String.valueOf(listSize));
        detailInfo1.setName(product1);
        detailInfo1.setId(id1);
        productList1.add(detailInfo1);
        headerInfo1.setProductList(productList1);

        groupPosition1 = mDeptList1.indexOf(headerInfo1);
        return groupPosition1;
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
        FragmentManager fragmentManager;

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            this.fragmentManager = fragmentManager;
        }

        @Override
        public Fragment getItem(int position) {
            FragBanner fragment = new FragBanner();
            Bundle bundle = new Bundle();
            bundle.putSerializable("CurrentPosition", position);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public int getCount() {
            return 6;
        }
    }
}