package com.score.cricket.SubScreen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Helper.Sorter.SortMode1;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.Models.AppMatches.AppMatches_Innings.Innings_Example;
import com.score.cricket.Models.HomeMain.HomeMatch_Inning_items;
import com.score.cricket.Models.HomeMain.HomeMatch_inning_batting;
import com.score.cricket.Models.HomeMain.HomeMatch_inning_bowling;
import com.score.cricket.Models.HomeMain.HomeMatch_inning_partnership;
import com.score.cricket.Models.HomeMain.HomeMatch_inning_wicket;
import com.score.cricket.MyApplication;
import com.score.cricket.Repeater.RepeaterBatting;
import com.score.cricket.Repeater.RepeaterBowling;
import com.score.cricket.Repeater.RepeaterPartners;
import com.score.cricket.Repeater.RepeaterSpin;
import com.score.cricket.Repeater.RepeaterWicket;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragMatchInningsInformationBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragMatchInningsInfomation extends Fragment implements IEventListener {

    FrameLayout fl_adplaceholder;
    FrameLayout fl_adplaceholder1;
    private Spinner spinner;
    private int id, I, K, L, M, N, O;
    private String A, B, C, D, E, F, G, H;
    private Double J;
    private ArrayList<HomeMatch_Inning_items> mCategories = new ArrayList<HomeMatch_Inning_items>();
    private ArrayList<Object> mBattinglist = new ArrayList<>();
    private ArrayList<Object> mBowlinglist = new ArrayList<>();
    private ArrayList<HomeMatch_inning_partnership> mPartnershiplist = new ArrayList<>();
    private ArrayList<HomeMatch_inning_wicket> mWicketlist1 = new ArrayList<>();
    private ArrayList<Object> mBattinglist1 = new ArrayList<>();
    private ArrayList<Object> mBowlinglist1 = new ArrayList<>();
    private ArrayList<HomeMatch_inning_partnership> mPartnershiplist1 = new ArrayList<>();
    private ArrayList<HomeMatch_inning_wicket> mWicketlist2 = new ArrayList<>();
    private RecyclerView _batting_rv, _bowling_rv, _wicket_rv, _partnership_rv;
    private Context mContext;
    private TextView _extra, _wide, _noball, _bye, _legbye, _penalty, _totalscore;
    private NestedScrollView _linearLayout;
    private SwipeRefreshLayout _swipeRefreshLayout;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1), 500));
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(fl_adplaceholder1, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        id = sharedPreferences.getInt("eventid", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragMatchInningsInformationBinding binding = FragMatchInningsInformationBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        fl_adplaceholder = binding.flAdplaceholder;
        fl_adplaceholder1 = binding.flAdplaceholder1;
        registerAdsListener();

        mContext = getActivity();

        UIInitialize(binding);

        ApiCall(view);

        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCall(view);
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1);
        MyApplication.getInstance().loadNativeAds(fl_adplaceholder1, requireActivity(), 1);
    }

    private void UIInitialize(FragMatchInningsInformationBinding binding) {

        _linearLayout = binding.inningLayout;
        _batting_rv = binding.batingRv;
        _batting_rv.setHasFixedSize(true);
        _bowling_rv = binding.bowlingRv;
        _bowling_rv.setHasFixedSize(true);
        _swipeRefreshLayout = binding.swiperefresh;
        _wicket_rv = binding.wicketRv;
        _wicket_rv.setHasFixedSize(true);
        _partnership_rv = binding.patnershipRv;
        _partnership_rv.setHasFixedSize(true);
        _extra = binding.extra;
        _wide = binding.wide;
        _noball = binding.noball;
        _bye = binding.bye;
        _legbye = binding.legby;
        _penalty = binding.penalty;
        _totalscore = binding.totalscore;
        spinner = binding.spinner;
    }

    private void ApiCall(View view) {
        APIService service = RestManager.getPlayer().create(APIService.class);
        Call<List<Innings_Example>> call11 = service.getinning(id);

        call11.enqueue(new Callback<List<Innings_Example>>() {
            @SuppressLint({"DefaultLocale", "SetTextI18n", "NewApi"})
            @Override
            public void onResponse(Call<List<Innings_Example>> call, Response<List<Innings_Example>> response) {
                assert response.body() != null;
                mCategories.clear();
                if (response.body() != null) {
                    _linearLayout.setVisibility(View.VISIBLE);
                    try {
                        try {
                            G = response.body().get(0).getBattingTeam().getName().toString();
                            mCategories.add(new HomeMatch_Inning_items(G, response.body().get(0).getBattingTeam().getId()));
                        } catch (Exception e) {
                        }
                        try {
                            H = response.body().get(1).getBattingTeam().getName().toString();
                            mCategories.add(new HomeMatch_Inning_items(H, response.body().get(1).getBattingTeam().getId()));
                        } catch (Exception e) {
                        }
                    } catch (Exception e) {
                    }

                    mBattinglist = new ArrayList<>();
                    mBowlinglist = new ArrayList<>();
                    mPartnershiplist = new ArrayList<>();
                    mWicketlist1 = new ArrayList<>();
                    mBattinglist1 = new ArrayList<>();
                    mBowlinglist1 = new ArrayList<>();
                    mPartnershiplist1 = new ArrayList<>();
                    mWicketlist2 = new ArrayList<>();


                    try {
                        for (int i = 0; i < response.body().get(0).getBattingLine().size(); i++) {

                            try {
                                A = response.body().get(0).getBattingLine().get(i).getName().toString();
                            } catch (Exception e) {
                                A = null;
                            }
                            try {
                                B = response.body().get(0).getBattingLine().get(i).getWicketCatch().getName().toString();
                            } catch (Exception e) {
                                B = null;
                            }
                            try {
                                C = response.body().get(0).getBattingLine().get(i).getWicketBowler().getName().toString();
                            } catch (Exception e) {
                                C = null;
                            }
                            try {
                                N = response.body().get(0).getBattingLine().get(i).getWicketTypeId();
                            } catch (Exception e) {
                                C = null;
                            }

                            try {
                                mBattinglist.add(new HomeMatch_inning_batting(A, B, C, null,
                                        response.body().get(0).getBattingLine().get(i).getScore(),
                                        response.body().get(0).getBattingLine().get(i).getBalls(),
                                        response.body().get(0).getBattingLine().get(i).getS4(),
                                        response.body().get(0).getBattingLine().get(i).getS6(),
                                        N));
                            } catch (Exception e) {
                                Log.d("INNINGS", e.toString());
                            }

                            try {
                                _extra.setText("Extra: " + response.body().get(0).getExtra());
                            } catch (Exception e) {
                            }
                            try {
                                _wide.setText("W: " + response.body().get(0).getWide());
                            } catch (Exception e) {
                            }
                            try {
                                _noball.setText("N: " + response.body().get(0).getNoBall());
                            } catch (Exception e) {
                            }
                            try {
                                _bye.setText("B: " + response.body().get(0).getBye());
                            } catch (Exception e) {
                            }
                            try {
                                _legbye.setText("LB: " + response.body().get(0).getLegBye());
                            } catch (Exception e) {
                            }
                            try {
                                _penalty.setText("P: " + response.body().get(0).getPenalty());
                            } catch (Exception e) {
                            }
                            try {
                                _totalscore.setText("Total: " + response.body().get(0).getScore() + "/" +
                                        response.body().get(0).getWickets() + "(" +
                                        response.body().get(0).getOvers() + ")");
                            } catch (Exception e) {
                            }

                            try {
                                D = response.body().get(0).getBattingLine().get(i).getName().toString();
                            } catch (Exception e) {
                            }
                            try {
                                I = response.body().get(0).getBattingLine().get(i).getFowScore();
                            } catch (Exception e) {
                            }
                            try {
                                J = response.body().get(0).getBattingLine().get(i).getFowOver();
                            } catch (Exception e) {
                            }
                            try {
                                if (I != 0) {
                                    mWicketlist1.add(new HomeMatch_inning_wicket(D, i + 1, I, J));
                                    mWicketlist1.sort(new SortMode1());
                                }
                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        for (int j = 0; j < response.body().get(0).getBowlingLine().size(); j++) {
                            try {
                                mBowlinglist.add(new HomeMatch_inning_bowling(response.body().get(0).getBowlingLine().get(j).getName().toString(),
                                        response.body().get(0).getBowlingLine().get(j).getMaiden(),
                                        response.body().get(0).getBowlingLine().get(j).getRun(),
                                        response.body().get(0).getBowlingLine().get(j).getWicket(),
                                        response.body().get(0).getBowlingLine().get(j).getNoBall(),
                                        response.body().get(0).getBowlingLine().get(j).getWide(),
                                        10.33,
                                        response.body().get(0).getBowlingLine().get(j).getOver()));
                            } catch (Exception e) {
                                Log.d("INNINGS", e.toString());
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        for (int k = 0; k < response.body().get(0).getPartnerships().size(); k++) {
                            try {
                                E = response.body().get(0).getPartnerships().get(k).getPlayer1().getName().toString();
                            } catch (Exception e) {
                            }
                            try {
                                F = response.body().get(0).getPartnerships().get(k).getPlayer2().getName().toString();
                            } catch (Exception e) {
                            }
                            try {
                                K = response.body().get(0).getPartnerships().get(k).getScore();
                            } catch (Exception e) {
                            }
                            try {
                                L = response.body().get(0).getPartnerships().get(k).getBalls();
                            } catch (Exception e) {
                            }
                            try {
                                M = response.body().get(0).getPartnerships().get(k).getId();
                            } catch (Exception e) {
                            }
                            try {
                                mPartnershiplist.add(new HomeMatch_inning_partnership(E, F, M, K, L));
                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e) {
                    }


                    try {
                        for (int i = 0; i < response.body().get(1).getBattingLine().size(); i++) {

                            try {
                                A = response.body().get(1).getBattingLine().get(i).getName().toString();
                            } catch (Exception e) {
                                A = null;
                            }
                            try {
                                B = response.body().get(1).getBattingLine().get(i).getWicketCatch().getName().toString();
                            } catch (Exception e) {
                                B = null;
                            }
                            try {
                                C = response.body().get(1).getBattingLine().get(i).getWicketBowler().getName().toString();
                            } catch (Exception e) {
                                C = null;
                            }
                            try {
                                O = response.body().get(1).getBattingLine().get(i).getWicketTypeId();
                            } catch (Exception e) {
                                O = 0;
                            }


                            try {
                                mBattinglist1.add(new HomeMatch_inning_batting(A, B, C, null,
                                        response.body().get(1).getBattingLine().get(i).getScore(),
                                        response.body().get(1).getBattingLine().get(i).getBalls(),
                                        response.body().get(1).getBattingLine().get(i).getS4(),
                                        response.body().get(1).getBattingLine().get(i).getS6(),
                                        O));
                            } catch (Exception e) {
                                Log.d("INNINGS", e.toString());
                            }

                            try {
                                _extra.setText("Extra: " + response.body().get(1).getExtra());
                            } catch (Exception e) {
                            }
                            try {
                                _wide.setText("W: " + response.body().get(1).getWide());
                            } catch (Exception e) {
                            }
                            try {
                                _noball.setText("N: " + response.body().get(1).getNoBall());
                            } catch (Exception e) {
                            }
                            try {
                                _bye.setText("B: " + response.body().get(1).getBye());
                            } catch (Exception e) {
                            }
                            try {
                                _legbye.setText("LB: " + response.body().get(1).getLegBye());
                            } catch (Exception e) {
                            }
                            try {
                                _penalty.setText("P: " + response.body().get(1).getPenalty());
                            } catch (Exception e) {
                            }
                            try {
                                _totalscore.setText("Total: " + response.body().get(1).getScore() + "/" +
                                        response.body().get(1).getWickets() + "(" +
                                        response.body().get(1).getOvers() + ")");
                            } catch (Exception e) {
                            }


                            try {
                                D = response.body().get(1).getBattingLine().get(i).getName().toString();
                            } catch (Exception e) {
                            }
                            try {
                                I = response.body().get(1).getBattingLine().get(i).getFowScore();
                            } catch (Exception e) {
                            }
                            try {
                                J = response.body().get(1).getBattingLine().get(i).getFowOver();
                            } catch (Exception e) {
                            }
                            try {
                                if (I != 0) {
                                    mWicketlist2.add(new HomeMatch_inning_wicket(D, i + 1, I, J));
                                    mWicketlist2.sort(new SortMode1());
                                }
                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        for (int j = 0; j < response.body().get(1).getBowlingLine().size(); j++) {
                            try {
                                mBowlinglist1.add(new HomeMatch_inning_bowling(response.body().get(1).getBowlingLine().get(j).getName().toString(),
                                        response.body().get(1).getBowlingLine().get(j).getMaiden(),
                                        response.body().get(1).getBowlingLine().get(j).getRun(),
                                        response.body().get(1).getBowlingLine().get(j).getWicket(),
                                        response.body().get(1).getBowlingLine().get(j).getNoBall(),
                                        response.body().get(1).getBowlingLine().get(j).getWide(),
                                        10.33,
                                        response.body().get(1).getBowlingLine().get(j).getOver()));
                            } catch (Exception e) {
                                Log.d("INNINGS", e.toString());
                            }
                        }
                    } catch (Exception e) {
                    }
                    try {
                        for (int k = 0; k < response.body().get(1).getPartnerships().size(); k++) {
                            try {
                                E = response.body().get(1).getPartnerships().get(k).getPlayer1().getName().toString();
                            } catch (Exception e) {
                            }
                            try {
                                F = response.body().get(1).getPartnerships().get(k).getPlayer2().getName().toString();
                            } catch (Exception e) {
                            }
                            try {
                                K = response.body().get(1).getPartnerships().get(k).getScore();
                            } catch (Exception e) {
                            }
                            try {
                                L = response.body().get(1).getPartnerships().get(k).getBalls();
                            } catch (Exception e) {
                            }
                            try {
                                M = response.body().get(1).getPartnerships().get(k).getId();
                            } catch (Exception e) {
                            }
                            try {
                                mPartnershiplist1.add(new HomeMatch_inning_partnership(E, F, M, K, L));
                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e) {
                    }


                    loadata();


                    SpiinerBind(view);


                } else {

                    _linearLayout.setVisibility(View.GONE);

                }

            }

            @Override
            public void onFailure(Call<List<Innings_Example>> call, Throwable t) {
                Log.d("INNINGS", t.toString());

            }
        });


    }

    private void SpiinerBind(View view) {
        RepeaterSpin adapter = new RepeaterSpin(mContext, mCategories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {


                        HomeMatch_Inning_items clickedItem = (HomeMatch_Inning_items) parent.getItemAtPosition(position);
                        String name = clickedItem.getName();
                        if (name.equals(G)) {

                            loadata();
                        } else {

                            loadata1();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
    }

    private void loadata() {

        RepeaterBatting recyclerAdapter = new RepeaterBatting(mContext, mBattinglist);
        recyclerAdapter.notifyDataSetChanged();
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        _batting_rv.setLayoutManager(llm);

        _batting_rv.setAdapter(recyclerAdapter);


        RepeaterBowling recyclerAdapter1 = new RepeaterBowling(mContext, mBowlinglist);
        recyclerAdapter1.notifyDataSetChanged();
        LinearLayoutManager llm1 = new LinearLayoutManager(getContext());
        llm1.setOrientation(LinearLayoutManager.VERTICAL);
        _bowling_rv.setLayoutManager(llm1);

        _bowling_rv.setAdapter(recyclerAdapter1);


        RepeaterWicket recyclerAdapter2 = new RepeaterWicket(mContext, mWicketlist1);
        recyclerAdapter2.notifyDataSetChanged();
        LinearLayoutManager llm2 = new LinearLayoutManager(getContext());
        llm2.setOrientation(LinearLayoutManager.VERTICAL);
        _wicket_rv.setLayoutManager(llm2);

        _wicket_rv.setAdapter(recyclerAdapter2);


        RepeaterPartners recyclerAdapter3 = new RepeaterPartners(mContext, mPartnershiplist);
        recyclerAdapter3.notifyDataSetChanged();
        LinearLayoutManager llm3 = new LinearLayoutManager(getContext());
        llm3.setOrientation(LinearLayoutManager.VERTICAL);
        _partnership_rv.setLayoutManager(llm3);

        _partnership_rv.setAdapter(recyclerAdapter3);
    }

    private void loadata1() {

        RepeaterBatting recyclerAdapter = new RepeaterBatting(mContext, mBattinglist1);
        recyclerAdapter.notifyDataSetChanged();
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        _batting_rv.setLayoutManager(llm);

        _batting_rv.setAdapter(recyclerAdapter);


        RepeaterBowling recyclerAdapter1 = new RepeaterBowling(mContext, mBowlinglist1);
        recyclerAdapter1.notifyDataSetChanged();
        LinearLayoutManager llm1 = new LinearLayoutManager(getContext());
        llm1.setOrientation(LinearLayoutManager.VERTICAL);
        _bowling_rv.setLayoutManager(llm1);

        _bowling_rv.setAdapter(recyclerAdapter1);


        RepeaterWicket recyclerAdapter2 = new RepeaterWicket(mContext, mWicketlist2);
        recyclerAdapter2.notifyDataSetChanged();
        LinearLayoutManager llm2 = new LinearLayoutManager(getContext());
        llm2.setOrientation(LinearLayoutManager.VERTICAL);
        _wicket_rv.setLayoutManager(llm2);

        _wicket_rv.setAdapter(recyclerAdapter2);


        RepeaterPartners recyclerAdapter3 = new RepeaterPartners(mContext, mPartnershiplist1);
        recyclerAdapter3.notifyDataSetChanged();
        LinearLayoutManager llm3 = new LinearLayoutManager(getContext());
        llm3.setOrientation(LinearLayoutManager.VERTICAL);
        _partnership_rv.setLayoutManager(llm3);

        _partnership_rv.setAdapter(recyclerAdapter3);
    }

}