package com.score.cricket.SubScreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Helper.FavoriteDBHelp;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.Models.HomeMain.HomeMatcheFavoriteInfo;
import com.score.cricket.Models.AppMatches.Matches_Example;
import com.score.cricket.R;
import com.score.cricket.Repeater.RepeaterFavorite;
import com.score.cricket.databinding.FragMatchFavoriteBinding;
import com.score.cricket.databinding.FragMatchHomeBinding;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragMatchFavorite extends Fragment {
    ArrayList<ListnerItems> mItems = new ArrayList<>();
    ImageView _ivBack;
    ImageView _ivRemove;
    RepeaterFavorite repeaterFav;
    RecyclerView _recyclerView;
    ArrayList<Integer> mDatabaseleageid = new ArrayList<Integer>();
    SwipeRefreshLayout mSwipeRefreshLayout;
    LocalDate mLocalDate1;
    Activity mContext;
    FavoriteDBHelp mMyDb;
    SharedPreferences sharedPreferences;
    String s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11 = null, s12, mCurrentteam;
    String  mCompare;
    int a, a1, a2, a3, a4;
    String mName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        Log.d("CHECK", "onCreate: ");
        getDataFromDatabase(mContext);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragMatchFavoriteBinding binding = FragMatchFavoriteBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();
        setHasOptionsMenu(true);
        
        Log.d("CHECK", "onCreateview: ");

        _ivBack = binding.ivBack;
        _ivRemove = binding.ivRemove;
        _ivBack.setOnClickListener(view1 -> getActivity().onBackPressed());
        _ivRemove.setOnClickListener(view12 -> {
            mMyDb.deleteAllData();
            mItems.clear();
            repeaterFav.notifyDataSetChanged();
        });

        _recyclerView = binding.favoriteRv;
        _recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        _recyclerView.setLayoutManager(layoutManager);

        repeaterFav = new RepeaterFavorite(mItems, mContext);
        _recyclerView.setAdapter(repeaterFav);

        mSwipeRefreshLayout = binding.swiperefresh;

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            getDataFromDatabase(mContext);
            new Handler().postDelayed(() -> mSwipeRefreshLayout.setRefreshing(false), 1000);
        });
        return view;
    }


    @Override
    public void onPause() {
        Log.d("CHECK", "on pause: ");
        super.onPause();
    }

    @Override
    public void onResume() {
        Log.d("CHECK", "on resume: ");
        super.onResume();
    }

    public void getDataFromDatabase(Context mContext) {

        mItems.clear();
        Log.d("DATABASE", "getDataFromDatabase:  ");
        mMyDb = new FavoriteDBHelp(mContext);
        Cursor res = mMyDb.getAllData();

        if (res == null && res.getCount() == 0) {
            Log.d("DATABASE", "getDataFromDatabase: empty ");
        }

        while (res.moveToNext()) {
            LoadMatchInfo(Integer.parseInt(res.getString(1)), Integer.parseInt(res.getString(2)), res.getLong(4), mContext);
            mDatabaseleageid.add(Integer.valueOf(res.getString(1)));
            Log.d("DATABASE", "Id: " + res.getString(0));
            Log.d("DATABASE", "Name: " + res.getString(1));
        }
    }

    public void LoadMatchInfo(int id, int position, long timeStamp, Context mContext) {

        Date date = new Date(timeStamp * 1000);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy K:mm:ss aa ");
        sdf.setTimeZone(TimeZone.getDefault());

        String match_time = sdf.format(date);

        sharedPreferences = mContext.getSharedPreferences("mName", Context.MODE_PRIVATE);
        mLocalDate1 = LocalDate.parse(sharedPreferences.getString("localdate", "1999-06-21"));
        Log.d("CRICKETSCORE", "nwe date..." + mLocalDate1.toString());

        APIService service = RestManager.getPlayer().create(APIService.class);
        Call<Matches_Example> call = service.getPlayer("cricket", id, mLocalDate1);

        call.enqueue(new Callback<Matches_Example>() {
            @Override
            public void onResponse(@NonNull Call<Matches_Example> call, @NonNull Response<Matches_Example> response) {

                try {
                    if (response.body() != null) {
                        for (int j = 0; j < response.body().getTournaments().size(); j++) {

                            mItems.add(new HomeHeader(response.body().getTournaments().get(j).getTournament().getName(),
                                    response.body().getTournaments().get(j).getCategory().getFlag()));

                            try {


                                long time = response.body().getTournaments().get(j).getEvents().get(position).getStartTimestamp();

                                Date date = new Date(time * 1000); // *1000 is to convert seconds to milliseconds
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd.MMM"); // the format of your date
                                sdf.setTimeZone(TimeZone.getDefault());

                                Log.d("CRICKETSCORE", "time" + sdf.format(date));

                                Date date1 = new Date(time * 1000); // *1000 is to convert seconds to milliseconds
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("K:mm aa"); // the format of your date
                                sdf1.setTimeZone(TimeZone.getDefault());
                                Log.d("CRICKETSCORE", "time" + sdf1.format(date1));

                                try {
                                    s = response.body().getTournaments().get(j).getEvents().get(position).getHomeTeam().getName();
                                    Log.d("CRICKETSCORE", "time" + s);
                                } catch (Exception e) {
                                    s = "null";
                                }

                                try {
                                    s1 = response.body().getTournaments().get(j).getEvents().get(position).getAwayTeam().getName();
                                    Log.d("CRICKETSCORE", "time" + s1);
                                } catch (Exception e) {
                                    s1 = "null";
                                }

                                try {
                                    s2 = response.body().getTournaments().get(j).getEvents().get(position).getHomeScore().getDisplay().toString();
                                    Log.d("CRICKETSCORE", "time" + s2);
                                } catch (Exception e) {
                                    s2 = "null";
                                }

                                try {
                                    s3 = response.body().getTournaments().get(j).getEvents().get(position).getAwayScore().getDisplay().toString();
                                    Log.d("CRICKETSCORE", "time" + s3);
                                } catch (Exception e) {
                                    s3 = "null";
                                }

                                try {
                                    s4 = response.body().getTournaments().get(j).getEvents().get(position).getHomeScore().getInnings().getInning1().getWickets().toString();
                                    Log.d("CRICKETSCORE", "time" + s4);
                                } catch (Exception e) {
                                    s4 = "null";
                                }

                                try {
                                    s5 = response.body().getTournaments().get(j).getEvents().get(position).getAwayScore().getInnings().getInning1().getWickets().toString();
                                    Log.d("CRICKETSCORE", "time" + s5);
                                } catch (Exception e) {
                                    s5 = "null";
                                }

                                try {
                                    s6 = response.body().getTournaments().get(j).getEvents().get(position).getHomeScore().getInnings().getInning1().getOvers().toString();
                                    Log.d("CRICKETSCORE", "time" + s6);
                                } catch (Exception e) {
                                    s6 = "null";
                                }

                                try {
                                    s7 = response.body().getTournaments().get(j).getEvents().get(position).getAwayScore().getInnings().getInning1().getOvers().toString();
                                    Log.d("CRICKETSCORE", "time" + s7);
                                } catch (Exception e) {
                                    s7 = "null";
                                }

                                try {
                                    s8 = sdf.format(date);
                                } catch (Exception e) {
                                    s8 = "null";
                                }

                                try {
                                    s9 = sdf1.format(date1);
                                } catch (Exception e) {
                                    s9 = "null";
                                }

                                try {
                                    s10 = response.body().getTournaments().get(j).getEvents().get(position).getNote();
                                    Log.d("CRICKETSCORE", "time" + s10);
                                } catch (Exception e) {
                                    s10 = "null";
                                }

                                try {
                                    if (s11 == null) {
                                        Log.d("COUNTRY..1", "time" + mName);
                                        s11 = response.body().getTournaments().get(j).getTournament().getName();
                                        mCompare = s11;
                                    } else if (response.body().getTournaments().get(j).getTournament().getName().equals(mCompare)) {
                                        Log.d("COUNTRY..2", "time" + mName);
                                        s11 = "null";
                                    } else {
                                        Log.d("COUNTRY..3", "time" + mName);
                                        s11 = response.body().getTournaments().get(j).getTournament().getName();
                                        mCompare = s11;
                                    }

                                    Log.d("CRICKETSCORE", "time" + s11);
                                } catch (Exception e) {
                                    s11 = "null";
                                }

                                try {
                                    a1 = response.body().getTournaments().get(j).getEvents().get(position).getId();
                                    Log.d("CRICKETSCORE", "time" + a1);
                                } catch (Exception e) {
                                }


                                try {
                                    a2 = response.body().getTournaments().get(j).getSeason().getId();
                                    Log.d("CRICKETSCORE", "time" + a2);
                                } catch (Exception e) {
                                }

                                try {
                                    a3 = response.body().getTournaments().get(j).getTournament().getUniqueId();
                                    Log.d("CRICKETSCORE", "time" + a2);
                                } catch (Exception e) {
                                }

                                try {
                                    a4 = response.body().getTournaments().get(j).getEvents().get(position).getWinnerCode();
                                    Log.d("CRICKETSCORE", "time" + a2);
                                } catch (Exception e) {
                                }

                                try {
                                    s12 = response.body().getTournaments().get(j).getEvents().get(position).getStatus().getType();
                                    Log.d("CRICKETSCORE", "time" + a2);
                                } catch (Exception e) {
                                }

                                try {
                                    mCurrentteam = response.body().getTournaments().get(j).getEvents().get(position).getCurrentBattingTeam().getName();
                                    Log.d("CRICKETSCORE", "time" + a2);
                                } catch (Exception e) {
                                    mCurrentteam = null;
                                }
                                mItems.add(new HomeMatcheFavoriteInfo(s11, s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s12, mCurrentteam, a1, a2, a3, a4));
                                repeaterFav.notifyDataSetChanged();

                            } catch (Exception e) {
                                Log.d("CRICKETSCORE", "Error---------" + e);
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.d("CRICKETSCORE", "Error---------" + e);
                }
            }

            @Override
            public void onFailure(Call<Matches_Example> call, Throwable t) {
                Log.d("CRICKETSCORE", "Error---------" + t);
            }
        });
    }
}