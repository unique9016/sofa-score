package com.score.cricket.SubScreen;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.Models.TeamDetails.TeamSquad.TeamSquadExample;
import com.score.cricket.MyApplication;
import com.score.cricket.Repeater.RapaeterSquad;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragSquadsBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragSquads extends Fragment implements IEventListener {
    FrameLayout fl_adplaceholder;
    private int mTeamid;
    private RecyclerView _recyclerView;
    private Activity mContext;
    private ArrayList<TeamSquadExample> mExample = new ArrayList<>();
    private SwipeRefreshLayout _swipeRefreshLayout;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        mTeamid = sharedPreferences.getInt("team_id", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragSquadsBinding binding = FragSquadsBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();
        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();
        mContext = getActivity();
        _recyclerView = binding.squadRv;
        _recyclerView.setHasFixedSize(true);
        ApiCall();
        _swipeRefreshLayout = binding.swiperefresh;
        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCall();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1);
    }

    private void ApiCall() {
        APIService apiService20 = RestManager.getPlayer().create(APIService.class);
        Call<List<TeamSquadExample>> call20 = apiService20.getPlayerInfo(mTeamid);

        call20.enqueue(new Callback<List<TeamSquadExample>>() {
            @Override
            public void onResponse(@NonNull Call<List<TeamSquadExample>> call, @NonNull Response<List<TeamSquadExample>> response) {
                mExample.clear();
                mExample = (ArrayList<TeamSquadExample>) response.body();
                LoadData();
            }

            @Override
            public void onFailure(@NonNull Call<List<TeamSquadExample>> call, @NonNull Throwable t) {
            }
        });
    }

    private void LoadData() {

        RapaeterSquad recyclerAdapter = new RapaeterSquad(mContext, mExample);
        recyclerAdapter.notifyDataSetChanged();
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        _recyclerView.setLayoutManager(llm);
        _recyclerView.setAdapter(recyclerAdapter);
    }
}