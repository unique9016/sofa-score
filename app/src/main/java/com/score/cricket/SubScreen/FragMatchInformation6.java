package com.score.cricket.SubScreen;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.Models.HomeMain.HomePlayer_Staticstics_Spinner;
import com.score.cricket.Models.HomeMain.HomePlayer_Statistic_BattingList;
import com.score.cricket.Models.PlayerDetails.PlayerStatistics.Player_Example;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Repeater.RepeaterStaticBating;
import com.score.cricket.Repeater.RepeaterStaticSpin;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragMatchInformation5Binding;
import com.score.cricket.databinding.FragMatchInformation6Binding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragMatchInformation6 extends Fragment implements IEventListener {


    FrameLayout fl_adplaceholder;
    FrameLayout fl_adplaceholder1;
    private Context mContext;
    private int mPlayer_id;
    private RepeaterStaticBating repeater;
    private RepeaterStaticBating repeater2;
    private ArrayList<HomePlayer_Staticstics_Spinner> mCategories = new ArrayList<HomePlayer_Staticstics_Spinner>();
    private ArrayList<Object> mBattinglist = new ArrayList<>();
    private ArrayList<Object> mBowling = new ArrayList<>();
    private Spinner spinner;
    private RecyclerView _recyclerView, _recyclerView1;
    private SwipeRefreshLayout _swipeRefreshLayout;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1), 500));
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(fl_adplaceholder1, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mContext = getActivity();
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("name", MODE_PRIVATE);
        mPlayer_id = sharedPreferences.getInt("mPlayer_id", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragMatchInformation6Binding binding = FragMatchInformation6Binding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        fl_adplaceholder = binding.flAdplaceholder;
        fl_adplaceholder1 = binding.flAdplaceholder1;
        registerAdsListener();

        spinner = binding.spinner;
        _recyclerView = binding.playerBattingRv;
        _recyclerView.setHasFixedSize(true);

        _recyclerView1 = binding.playerBowlingRv;
        _recyclerView1.setHasFixedSize(true);

        ApiCall();
        _swipeRefreshLayout = binding.swiperefresh;

        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCall();

            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1);
        MyApplication.getInstance().loadNativeAds(fl_adplaceholder1, requireActivity(), 1);
    }

    private void ApiCall() {
        APIService service = RestManager.getPlayer().create(APIService.class);
        Call<List<Player_Example>> call22 = service.getPlayerStatistic(mPlayer_id);

        call22.enqueue(new Callback<List<Player_Example>>() {
            @Override
            public void onResponse(@NonNull Call<List<Player_Example>> call, @NonNull Response<List<Player_Example>> response) {

                mBattinglist = new ArrayList<>();
                mBowling = new ArrayList<>();
                mBattinglist.clear();
                mBowling.clear();
                mCategories.clear();
                try {
                    for (int i = 0; i < response.body().size(); i++) {
                        if (response.body().get(i).getGroupName().equals("Batting")) {
                        } else if (response.body().get(i).getGroupName().equals("Bowling")) {

                        } else {
                            mCategories.add(new HomePlayer_Staticstics_Spinner(response.body().get(i).getGroupName().toString(), i));
                        }
                    }
                } catch (Exception e) {

                }
                
                RepeaterStaticSpin spinnerAdapter = new RepeaterStaticSpin(mContext, mCategories);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(spinnerAdapter);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        mBattinglist.clear();
                        mBowling.clear();

                        HomePlayer_Staticstics_Spinner clickedItem = (HomePlayer_Staticstics_Spinner) parent.getItemAtPosition(position);
                        String name = clickedItem.getName();
                        int pos = clickedItem.getPosition();
                        // Toast.makeText(mContext, "position"+pos, Toast.LENGTH_SHORT).show();
                        for (int j = 0; j < response.body().get(pos + 1).getStatisticsItems().size(); j++) {
                            Log.d("POSITION", "onItemSelected: " + response.body().get(pos + 1).getStatisticsItems().get(j).getName());

                            mBattinglist.add(new HomePlayer_Statistic_BattingList(response.body().get(pos + 1).getStatisticsItems().get(j).getName(),
                                    response.body().get(pos + 1).getStatisticsItems().get(j).getValue()));

                        }

                        for (int j = 0; j < response.body().get(pos + 2).getStatisticsItems().size(); j++) {
                            Log.d("POSITION", "onItemSelected: " + response.body().get(pos + 2).getStatisticsItems().get(j).getName());

                            mBowling.add(new HomePlayer_Statistic_BattingList(response.body().get(pos + 2).getStatisticsItems().get(j).getName(),
                                    response.body().get(pos + 2).getStatisticsItems().get(j).getValue()));
                        }
                        loadata();
                        repeater.notifyDataSetChanged();
                        repeater2.notifyDataSetChanged();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onFailure(@NonNull Call<List<Player_Example>> call, @NonNull Throwable t) {

            }
        });
    }

    private void loadata() {

        repeater = new RepeaterStaticBating(mContext, mBattinglist);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        _recyclerView.setLayoutManager(llm);

        _recyclerView.setAdapter(repeater);


        repeater2 = new RepeaterStaticBating(mContext, mBowling);
        LinearLayoutManager llm1 = new LinearLayoutManager(getContext());
        llm1.setOrientation(LinearLayoutManager.VERTICAL);
        _recyclerView1.setLayoutManager(llm1);

        _recyclerView1.setAdapter(repeater2);
    }
}