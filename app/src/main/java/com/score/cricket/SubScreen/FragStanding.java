package com.score.cricket.SubScreen;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.Models.AppTournaments.TournamentsExample;
import com.score.cricket.Models.HomeMain.HomeFormList;
import com.score.cricket.Models.HomeMain.HomeFullList;
import com.score.cricket.Models.HomeMain.HomeShortList;
import com.score.cricket.Models.HomeMain.HomeYearData;
import com.score.cricket.Models.Standings.StandingsExample;
import com.score.cricket.MyApplication;
import com.score.cricket.Repeater.RapaeterSpinYear2;
import com.score.cricket.Repeater.RepeaterStading;
import com.score.cricket.Repeater.RepeaterStading2;
import com.score.cricket.Repeater.RepeaterStading3;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragStandingBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragStanding extends Fragment implements IEventListener {

    Spinner mySpinner;
    FrameLayout fl_adplaceholder;
    private ImageButton mFilter;
    private int mCheckedItem = 0;
    private RecyclerView mRecyclerView;
    private ArrayList<Object> mShortlist = new ArrayList<>();
    private ArrayList<Object> mFulllist = new ArrayList<>();
    private ArrayList<Object> mFormlist = new ArrayList<>();
    private int mSeasonid, mId;
    private String A, B, C, D, E, F, G;
    private Activity mContext;
    private RepeaterStading recyclerRepeater;
    private LinearLayout _shortlayout, _fulllayout, _formlayout;
    private SwipeRefreshLayout _swipeRefreshLayout;
    private ArrayList<HomeYearData> mYear = new ArrayList<>();

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragStandingBinding binding = FragStandingBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        mSeasonid = sharedPreferences.getInt("mSeasonid", 0);
        mId = sharedPreferences.getInt("mId", 0);

        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();

        mContext = getActivity();
        mySpinner = (Spinner) binding.spinnerr;

        mFilter = binding.filter;

        mRecyclerView = binding.recyclerView;

        mRecyclerView.setHasFixedSize(true);

        _shortlayout = binding.shortlayout;
        _fulllayout = binding.fulllayout;
        _formlayout = binding.formlayout;

        ApiCall1();
        _swipeRefreshLayout = binding.swiperefresh;

        _swipeRefreshLayout.setOnRefreshListener(() -> {
            mCheckedItem = 0;
            mFilter.setOnClickListener(v -> showdialog());
            ApiCall1();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    _swipeRefreshLayout.setRefreshing(false);
                }
            }, 2000);
        });

        mFilter.setOnClickListener(v -> showdialog());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1);
    }

    private void ApiCall1() {
        APIService service = RestManager.getMatch().create(APIService.class);
        Call<TournamentsExample> call25 = service.getYearly(mId);

        call25.enqueue(new Callback<TournamentsExample>() {
            @Override
            public void onResponse(Call<TournamentsExample> call, Response<TournamentsExample> response) {
                mYear = new ArrayList<>();

                mYear.clear();
                for (int i = 0; i < response.body().getSeasons().size(); i++) {
                    mYear.add(new HomeYearData(response.body().getSeasons().get(i).getYear(), response.body().getSeasons().get(i).getId()));
                }
                RapaeterSpinYear2 spinnerAdapter = new RapaeterSpinYear2(mContext, mYear);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mySpinner.setAdapter(spinnerAdapter);

                mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id2) {


                        HomeYearData clickedItem = (HomeYearData) parent.getItemAtPosition(position);
                        String name = clickedItem.getYear();
                        int id1 = clickedItem.getId();

                        ApiCall(mId, id1);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<TournamentsExample> call, Throwable t) {

            }
        });

    }

    private void ApiCall(int mId, int id1) {
        new Handler().postDelayed(() -> {
            APIService service = RestManager.getPlayer().create(APIService.class);
            Call<List<StandingsExample>> call9 = service.getStanding(mId, id1);
            call9.enqueue(new Callback<List<StandingsExample>>() {
                @Override
                public void onResponse(@NonNull Call<List<StandingsExample>> call, @NonNull Response<List<StandingsExample>> response) {
                    mShortlist = new ArrayList<>();
                    mFulllist = new ArrayList<>();
                    mFormlist = new ArrayList<>();
                    assert response.body() != null;


                    for (int i = 0; i < response.body().get(0).getTableRows().size(); i++) {

                        mShortlist.add(new HomeShortList(response.body().get(0).getTableRows().get(i).getPosition().toString(),
                                response.body().get(0).getTableRows().get(i).getTeam().getName().toString(),
                                response.body().get(0).getTableRows().get(i).getTotalFields().getMatchesTotal().toString(),
                                response.body().get(0).getTableRows().get(i).getTotalFields().getWinsLossesTotal().toString(),
                                response.body().get(0).getTableRows().get(i).getPoints().toString(),
                                response.body().get(0).getTableRows().get(i).getTeam().getId()));

                        mFulllist.add(new HomeFullList(response.body().get(0).getTableRows().get(i).getPosition().toString(),
                                response.body().get(0).getTableRows().get(i).getTeam().getName().toString(),
                                response.body().get(0).getTableRows().get(i).getTotalFields().getMatchesTotal().toString(),
                                response.body().get(0).getTableRows().get(i).getTotalFields().getWinsTotal().toString(),
                                response.body().get(0).getTableRows().get(i).getTotalFields().getDrawsTotal().toString(),
                                response.body().get(0).getTableRows().get(i).getTotalFields().getLossesTotal().toString(),
                                response.body().get(0).getTableRows().get(i).getPoints().toString(),
                                response.body().get(0).getTableRows().get(i).getTotalFields().getNetRunRateTotal().toString(),
                                response.body().get(0).getTableRows().get(i).getTotalFields().getNoResultTotal().toString(),
                                response.body().get(0).getTableRows().get(i).getTeam().getId()));


                        try {
                            A = response.body().get(0).getTableRows().get(i).getTotalForm().get(0).toString();
                        } catch (Exception e) {
                            A = null;
                        }

                        try {
                            B = response.body().get(0).getTableRows().get(i).getTotalForm().get(1).toString();
                        } catch (Exception e) {
                            B = null;
                        }

                        try {
                            C = response.body().get(0).getTableRows().get(i).getTotalForm().get(2).toString();
                        } catch (Exception e) {
                            C = null;

                        }

                        try {
                            D = response.body().get(0).getTableRows().get(i).getTotalForm().get(3).toString();
                        } catch (Exception e) {
                            D = null;

                        }

                        try {
                            E = response.body().get(0).getTableRows().get(i).getTotalForm().get(4).toString();
                        } catch (Exception e) {
                            E = null;

                        }
                        try {
                            mFormlist.add(new HomeFormList(response.body().get(0).getTableRows().get(i).getPosition().toString(),
                                    response.body().get(0).getTableRows().get(i).getTeam().getName().toString(),
                                    A,
                                    B,
                                    C,
                                    D,
                                    E,
                                    response.body().get(0).getTableRows().get(i).getTeam().getId()));
                        } catch (Exception e) {

                        }

                    }

                    loadata();

                }

                @Override
                public void onFailure(@NonNull Call<List<StandingsExample>> call, @NonNull Throwable t) {
                    Log.d("STANDINGS", t.toString());
                }
            });
        }, 1000);
    }

    private void showdialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(requireContext());
        alertDialog.setTitle("Filter");
        String[] items = {"Short", "Full", "Form"};

        alertDialog.setSingleChoiceItems(items, mCheckedItem, (dialog, which) -> {
            switch (which) {
                case 0:
                    mCheckedItem = 0;

                    _fulllayout.setVisibility(View.GONE);
                    _formlayout.setVisibility(View.GONE);
                    loadata();
                    break;
                case 1:

                    _shortlayout.setVisibility(View.GONE);
                    _formlayout.setVisibility(View.GONE);
                    _fulllayout.setVisibility(View.VISIBLE);
                    RepeaterStading2 recyclerRepeater = new RepeaterStading2(mContext, mFulllist);
                    recyclerRepeater.notifyDataSetChanged();
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerView.setLayoutManager(llm);

                    mRecyclerView.setAdapter(recyclerRepeater);
                    mCheckedItem = 1;
                    break;
                case 2:

                    _shortlayout.setVisibility(View.GONE);
                    _fulllayout.setVisibility(View.GONE);
                    _formlayout.setVisibility(View.VISIBLE);
                    RepeaterStading3 recyclerAdapter4 = new RepeaterStading3(mContext, mFormlist);
                    recyclerAdapter4.notifyDataSetChanged();
                    LinearLayoutManager llm2 = new LinearLayoutManager(getContext());
                    llm2.setOrientation(LinearLayoutManager.VERTICAL);
                    mRecyclerView.setLayoutManager(llm2);

                    mRecyclerView.setAdapter(recyclerAdapter4);
                    mCheckedItem = 2;

                    break;
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }

    public void loadata() {
        _shortlayout.setVisibility(View.GONE);
        _formlayout.setVisibility(View.GONE);
        _formlayout.setVisibility(View.GONE);

        if (mCheckedItem == 1) {
            _shortlayout.setVisibility(View.GONE);
            _formlayout.setVisibility(View.GONE);
            _fulllayout.setVisibility(View.VISIBLE);
            RepeaterStading2 recyclerRepeater = new RepeaterStading2(mContext, mFulllist);
            recyclerRepeater.notifyDataSetChanged();
            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            mRecyclerView.setLayoutManager(llm);

            mRecyclerView.setAdapter(recyclerRepeater);
        } else if (mCheckedItem == 2) {
            _shortlayout.setVisibility(View.GONE);
            _fulllayout.setVisibility(View.GONE);
            _formlayout.setVisibility(View.VISIBLE);
            RepeaterStading3 recyclerAdapter4 = new RepeaterStading3(mContext, mFormlist);
            recyclerAdapter4.notifyDataSetChanged();
            mRecyclerView.setAdapter(recyclerAdapter4);
            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            mRecyclerView.setLayoutManager(llm);

        } else {

            _shortlayout.setVisibility(View.VISIBLE);
            recyclerRepeater = new RepeaterStading(mContext, mShortlist);
            recyclerRepeater.notifyDataSetChanged();
            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            mRecyclerView.setLayoutManager(llm);

            mRecyclerView.setAdapter(recyclerRepeater);
        }
    }
}