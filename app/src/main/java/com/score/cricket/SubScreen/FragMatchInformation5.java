package com.score.cricket.SubScreen;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.Models.HomeMain.HomeMatches_Matche;
import com.score.cricket.Models.PlayerDetails.PlayerMatches.PlayerExample;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Repeater.RepeaterMatchList;
import com.score.cricket.databinding.FragMatchInformation4Binding;
import com.score.cricket.databinding.FragMatchInformation5Binding;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragMatchInformation5 extends Fragment implements IEventListener {


    private int mPlayerId;
    private Activity mContext;
    private ArrayList<ListnerItems> mItems = new ArrayList<>();
    private RepeaterMatchList repeater;
    private RecyclerView _recyclerView;
    private String s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12 = null, s13, s14;
    private int a, b;
    private SwipeRefreshLayout _swipeRefreshLayout;


    FrameLayout fl_adplaceholder;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mContext = getActivity();
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("name", MODE_PRIVATE);
        mPlayerId = sharedPreferences.getInt("mPlayerId", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragMatchInformation5Binding binding = FragMatchInformation5Binding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();

        _recyclerView = binding.matcheRv1;
        _recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        _recyclerView.setLayoutManager(layoutManager);

        repeater = new RepeaterMatchList(mItems, mContext);
        _recyclerView.setAdapter(repeater);

        _swipeRefreshLayout = binding.swiperefresh;

        ApiCall();

        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCall();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1);
    }


    private void ApiCall() {
        APIService service = RestManager.getMatch().create(APIService.class);
        Call<PlayerExample> call23 = service.getPlayerMatches(mPlayerId);

        call23.enqueue(new Callback<PlayerExample>() {
            @Override
            public void onResponse(@NonNull Call<PlayerExample> call, @NonNull Response<PlayerExample> response) {
                Log.d("XYZZZ", "response");

                mItems.clear();

                for (int i = 0; i < response.body().getEvents().size(); i++) {
                    s13 = response.body().getEvents().get(i).getTournament().getName().toString();
                    if (s12 == null) {
                        try {


                            s11 = response.body().getEvents().get(i).getTournament().getName().toString();
                            s14 = response.body().getEvents().get(i).getTournament().getCategory().getFlag();
                            mItems.add(new HomeHeader(s11, s14));
                            s12 = s11;
                        } catch (Exception e) {
                            s11 = null;
                        }
                    } else if (!s12.equals(s13)) {
                        mItems.add(new HomeHeader(response.body().getEvents().get(i).getTournament().getName().toString(),
                                response.body().getEvents().get(i).getTournament().getCategory().getFlag()));
                        s12 = s13;
                    }
                    try {
                        long time = response.body().getEvents().get(i).getStartTimestamp();

                        Date date = new Date(time * 1000); // *1000 is to convert seconds to milliseconds
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd.MMM"); // the format of your date
                        sdf.setTimeZone(TimeZone.getDefault());

                        Log.d("XYZZZ", "time" + sdf.format(date));

                        Date date1 = new Date(time * 1000); // *1000 is to convert seconds to milliseconds
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("K:mm aa"); // the format of your date
                        sdf1.setTimeZone(TimeZone.getDefault());
                        Log.d("XYZZZ", "time" + sdf1.format(date1));


                        try {
                            s = response.body().getEvents().get(i).getHomeTeam().getName().toString();
                        } catch (Exception e) {
                            s = null;
                        }

                        try {
                            s1 = response.body().getEvents().get(i).getAwayTeam().getName().toString();

                        } catch (Exception e) {
                            s1 = null;

                        }

                        try {
                            s2 = response.body().getEvents().get(i).getHomeScore().getDisplay().toString();

                        } catch (Exception e) {
                            s2 = null;

                        }

                        try {
                            s3 = response.body().getEvents().get(i).getAwayScore().getDisplay().toString();

                        } catch (Exception e) {
                            s3 = null;

                        }

                        try {
                            s4 = response.body().getEvents().get(i).getHomeScore().getInnings().getInning1().getWickets().toString();

                        } catch (Exception e) {
                            s4 = null;

                        }

                        try {
                            s5 = response.body().getEvents().get(i).getAwayScore().getInnings().getInning1().getWickets().toString();


                        } catch (Exception e) {
                            s5 = null;

                        }

                        try {
                            s6 = response.body().getEvents().get(i).getHomeScore().getInnings().getInning1().getOvers().toString();


                        } catch (Exception e) {
                            s6 = null;

                        }

                        try {
                            s7 = response.body().getEvents().get(i).getAwayScore().getInnings().getInning1().getOvers().toString();

                        } catch (Exception e) {
                            s7 = null;

                        }

                        try {
                            s8 = sdf.format(date);

                        } catch (Exception e) {
                            s8 = null;
                        }

                        try {
                            s9 = sdf1.format(date1);

                        } catch (Exception e) {
                            s9 = null;

                        }

                        try {
                            s10 = response.body().getEvents().get(i).getNote().toString();

                        } catch (Exception e) {
                            s10 = null;

                        }

                        try {
                            a = response.body().getEvents().get(i).getId();

                        } catch (Exception e) {
                            a = 0;

                        }
                        try {
                            b = response.body().getEvents().get(i).getWinnerCode();
                        } catch (Exception e) {

                        }
                        
                        mItems.add(new HomeMatches_Matche(s, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, a, b));
                        repeater.notifyDataSetChanged();
                    } catch (Exception e) {
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PlayerExample> call, @NonNull Throwable t) {
                Log.d("XYZZZ", t.toString());
            }
        });
    }
}