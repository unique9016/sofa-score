package com.score.cricket.SubScreen;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.AppMatches.AppMatches_Lineup.Lineup_Example;
import com.score.cricket.Models.HomeMain.HomePlayer_List;
import com.score.cricket.MyApplication;
import com.score.cricket.Repeater.RepeaterLineUps;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragLineupBinding;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragLineup extends Fragment implements IEventListener {
    RecyclerView _recyclerView, _recyclerView1;
    RepeaterLineUps mRepeaterLineup;
    RepeaterLineUps mRepeaterLineup1;
    Activity mContext;
    int mEvenid, id, mHometeamid, mAwayteamid;
    String mHometeam, mAwayteam;
    TextView _hometeamname, _awayteamname;
    ImageView _homelogo, _awaylogo;
    FrameLayout _fl_adplaceholder;
    FrameLayout fl_adplaceholder1;
    private ArrayList<Object> mHomeplayerlist = new ArrayList<>();
    private ArrayList<Object> mAwayplayerlist = new ArrayList<>();
    private SwipeRefreshLayout _swipeRefreshLayout;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(_fl_adplaceholder, requireActivity(), 1), 500));
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(fl_adplaceholder1, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        mEvenid = sharedPreferences.getInt("eventid", 0);
        mHometeam = sharedPreferences.getString("HomeTeam", null);
        mAwayteam = sharedPreferences.getString("AwayTeam", null);
        mHometeamid = sharedPreferences.getInt("hometeamid", 0);
        mAwayteamid = sharedPreferences.getInt("awayteamid", 0);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragLineupBinding binding = FragLineupBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        _fl_adplaceholder = binding.flAdplaceholder;
        fl_adplaceholder1 = binding.flAdplaceholder1;
        registerAdsListener();

        mContext = getActivity();
        _recyclerView = binding.hometeamRv;
        _recyclerView1 = binding.awayteamRv;
        _recyclerView.setNestedScrollingEnabled(false);
        _recyclerView1.setNestedScrollingEnabled(false);

        _hometeamname = binding.hometeamName;
        _awayteamname = binding.awayteamName;

        _homelogo = binding.homelogo;
        _awaylogo = binding.awaylogo;

        _swipeRefreshLayout = binding.swiperefresh;

        _hometeamname.setText(mHometeam);
        _awayteamname.setText(mAwayteam);

        _recyclerView.setHasFixedSize(true);
        _recyclerView1.setHasFixedSize(true);

        ApiCall();

        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCall();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);

        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(_fl_adplaceholder, requireActivity(), 1);
        MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder1, requireActivity(), 1);
    }


    private void ApiCall() {
        APIService service = RestManager.getMatch().create(APIService.class);
        Call<Lineup_Example> call14 = service.getPlayer(mEvenid);

        call14.enqueue(new Callback<Lineup_Example>() {
            @Override
            public void onResponse(@NonNull Call<Lineup_Example> call, @NonNull Response<Lineup_Example> response) {
                mHomeplayerlist = new ArrayList<>();
                mAwayplayerlist = new ArrayList<>();

                for (int i = 0; i < response.body().getHome().getPlayers().size(); i++) {
                    mHomeplayerlist.add(new HomePlayer_List(response.body().getHome().getPlayers().get(i).getPlayer().getName().toString(),
                            response.body().getHome().getPlayers().get(i).getPlayer().getId()));
                }

                for (int j = 0; j < response.body().getAway().getPlayers().size(); j++) {
                    mAwayplayerlist.add(new HomePlayer_List(response.body().getAway().getPlayers().get(j).getPlayer().getName().toString(),
                            response.body().getAway().getPlayers().get(j).getPlayer().getId()));
                }

                loadata();
                Loadlogo();
            }

            @Override
            public void onFailure(@NonNull Call<Lineup_Example> call, @NonNull Throwable t) {
            }
        });
    }

    private void Loadlogo() {

        String team2 = ActivityLaunch.LogoLink_2 + mHometeamid + "/logo";
        String team1 = ActivityLaunch.LogoLink_2 + mAwayteamid + "/logo";

        Glide.with(mContext)
                .load(team2)
                .centerCrop()
                .into(_homelogo);

        Glide.with(mContext)
                .load(team1)
                .centerCrop()
                .into(_awaylogo);

    }

    private void loadata() {

        mRepeaterLineup = new RepeaterLineUps(mContext, mHomeplayerlist);
        mRepeaterLineup.notifyDataSetChanged();
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        _recyclerView.setLayoutManager(llm);
        _recyclerView.setAdapter(mRepeaterLineup);

        mRepeaterLineup1 = new RepeaterLineUps(mContext, mAwayplayerlist);
        mRepeaterLineup1.notifyDataSetChanged();
        LinearLayoutManager llm1 = new LinearLayoutManager(getContext());
        llm1.setOrientation(LinearLayoutManager.VERTICAL);
        _recyclerView1.setLayoutManager(llm1);
        _recyclerView1.setAdapter(mRepeaterLineup1);
    }
}