package com.score.cricket.SubScreen;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.score.cricket.R;

public class Frag_Base extends Fragment {

    public Activity mActivity;
    public Dialog mLoadingDialog;

    public void showProgressDialog(Activity mActivity) {
        mActivity.runOnUiThread(() -> {
            if (mLoadingDialog != null && !mActivity.isFinishing()) {
                mLoadingDialog.setCancelable(false);
                mLoadingDialog.setCanceledOnTouchOutside(false);
                mLoadingDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                mLoadingDialog.setContentView(R.layout.dialog_loading);

                LottieAnimationView animationView = mLoadingDialog.findViewById(R.id.animationView);
                animationView.setAnimation(R.raw.loading);
                animationView.animate();

                if (!mLoadingDialog.isShowing() && !mActivity.isFinishing())
                    mLoadingDialog.show();
            }
        });
    }

    public void dismissProgressDialog(Activity mActivity) {
        mActivity.runOnUiThread(() -> {
            if (!mActivity.isFinishing()) {
                if (mLoadingDialog.isShowing() && !mActivity.isFinishing())
                    mLoadingDialog.dismiss();
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mLoadingDialog == null) {
            mLoadingDialog = new Dialog(mActivity);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mActivity = (Activity) context;
    }
}
