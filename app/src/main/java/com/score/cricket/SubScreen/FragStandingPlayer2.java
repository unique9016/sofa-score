package com.score.cricket.SubScreen;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.Models.HomeMain.HomeFormList;
import com.score.cricket.Models.HomeMain.HomeFullList;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.Models.HomeMain.HomeShortList;
import com.score.cricket.Models.Standings.StandingsExample;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Repeater.RepeaterStading4;
import com.score.cricket.Repeater.RepeaterStading5;
import com.score.cricket.Repeater.RepeaterStading6;
import com.score.cricket.databinding.FragStandingPlayer2Binding;
import com.score.cricket.databinding.FragStandingPlayerBinding;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragStandingPlayer2 extends Fragment implements IEventListener {


    FrameLayout fl_adplaceholder;
    private ImageButton mFilter;
    private int mCheckedItem = 0;
    private RecyclerView _recyclerView;
    private ArrayList<Object> mShortlist = new ArrayList<>();
    private ArrayList<Object> mFormlist = new ArrayList<>();
    private String A, B, C, D, E, F, G;
    private int mSeasonid, mId;
    private Activity mContext;
    private RepeaterStading6 recyclerRepeater;
    private SwipeRefreshLayout _swipeRefreshLayout;
    private TextView seasonname;
    private ArrayList<ListnerItems> mFull_items = new ArrayList<>();
    private ArrayList<ListnerItems> mForm_items = new ArrayList<>();
    private ArrayList<ListnerItems> mShort_items = new ArrayList<>();

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragStandingPlayer2Binding binding = FragStandingPlayer2Binding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();
        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();
        mFilter = binding.filter;
        mContext = getActivity();
        _recyclerView = binding.recyclerView;
        _recyclerView.setHasFixedSize(true);
        _swipeRefreshLayout = binding.swiperefresh;
        _swipeRefreshLayout.setOnRefreshListener(() -> {
            mCheckedItem = 0;
            mFilter.setOnClickListener(v -> showdialog());
            ApiCall();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 2000);
        });

        new Handler().postDelayed(() -> ApiCall(), 1000);
        mFilter.setOnClickListener(v -> showdialog());
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1);
    }

    private void ApiCall() {
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        mSeasonid = sharedPreferences.getInt("mSeasonid", 0);
        mId = sharedPreferences.getInt("mId", 0);

        APIService service = RestManager.getPlayer().create(APIService.class);
        Call<List<StandingsExample>> call9 = service.getStanding(mId, mSeasonid);
        call9.enqueue(new Callback<List<StandingsExample>>() {
            @Override
            public void onResponse(@NonNull Call<List<StandingsExample>> call, @NonNull Response<List<StandingsExample>> response) {
                mShortlist = new ArrayList<>();
                mFormlist = new ArrayList<>();
                mFull_items.clear();
                mForm_items.clear();
                mShort_items.clear();
                mShortlist.clear();
                mFormlist.clear();
                assert response.body() != null;

                try {

                    for (int j = 0; j < response.body().size(); j++) {

                        mFull_items.add(new HomeHeader(response.body().get(j).getName().toString(), response.body().get(j).getCategory().getFlag()));
                        mForm_items.add(new HomeHeader(response.body().get(j).getName().toString(), response.body().get(j).getCategory().getFlag()));
                        mShort_items.add(new HomeHeader(response.body().get(j).getName().toString(), response.body().get(j).getCategory().getFlag()));

                        for (int i = 0; i < response.body().get(j).getTableRows().size(); i++) {

                            mShort_items.add(new HomeShortList(response.body().get(j).getTableRows().get(i).getPosition().toString(),
                                    response.body().get(j).getTableRows().get(i).getTeam().getName().toString(),
                                    response.body().get(j).getTableRows().get(i).getTotalFields().getMatchesTotal().toString(),
                                    response.body().get(j).getTableRows().get(i).getTotalFields().getWinsLossesTotal().toString(),
                                    response.body().get(j).getTableRows().get(i).getPoints().toString(),
                                    response.body().get(j).getTableRows().get(i).getTeam().getId()));

                            mFull_items.add(new HomeFullList(
                                    response.body().get(j).getTableRows().get(i).getPosition().toString(),
                                    response.body().get(j).getTableRows().get(i).getTeam().getName().toString(),
                                    response.body().get(j).getTableRows().get(i).getTotalFields().getMatchesTotal().toString(),
                                    response.body().get(j).getTableRows().get(i).getTotalFields().getWinsTotal().toString(),
                                    response.body().get(j).getTableRows().get(i).getTotalFields().getDrawsTotal().toString(),
                                    response.body().get(j).getTableRows().get(i).getTotalFields().getLossesTotal().toString(),
                                    response.body().get(j).getTableRows().get(i).getPoints().toString(),
                                    response.body().get(j).getTableRows().get(i).getTotalFields().getNetRunRateTotal().toString(),
                                    response.body().get(j).getTableRows().get(i).getTotalFields().getNoResultTotal().toString(),
                                    response.body().get(j).getTableRows().get(i).getTeam().getId()));

                            try {
                                A = response.body().get(j).getTableRows().get(i).getTotalForm().get(0).toString();
                            } catch (Exception e) {
                                A = null;
                            }

                            try {
                                B = response.body().get(j).getTableRows().get(i).getTotalForm().get(1).toString();
                            } catch (Exception e) {
                                B = null;
                            }
                            try {
                                C = response.body().get(j).getTableRows().get(i).getTotalForm().get(2).toString();
                            } catch (Exception e) {
                                C = null;
                            }
                            try {
                                D = response.body().get(j).getTableRows().get(i).getTotalForm().get(3).toString();
                            } catch (Exception e) {
                                D = null;
                            }
                            try {
                                E = response.body().get(j).getTableRows().get(i).getTotalForm().get(4).toString();
                            } catch (Exception e) {
                                E = null;
                            }
                            try {
                                mForm_items.add(new HomeFormList(response.body().get(j).getTableRows().get(i).getPosition().toString(),
                                        response.body().get(j).getTableRows().get(i).getTeam().getName().toString(),
                                        A,
                                        B,
                                        C,
                                        D,
                                        E,
                                        response.body().get(j).getTableRows().get(i).getTeam().getId()));
                            } catch (Exception e) {
                            }
                        }
                    }
                } catch (Exception e) {
                }
                loadata();
            }
            @Override
            public void onFailure(Call<List<StandingsExample>> call, Throwable t) {
                Log.d("STANDINGS", t.toString());

            }
        });


    }

    private void showdialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Filter");
        String[] full_itemss = {"Short", "Full", "Form"};

        alertDialog.setSingleChoiceItems(full_itemss, mCheckedItem, (dialog, which) -> {
            switch (which) {
                case 0:
                    mCheckedItem = 0;

                    loadata();
                    break;
                case 1:

                    RepeaterStading4 recyclerRepeater = new RepeaterStading4(mContext, mFull_items);
                    recyclerRepeater.notifyDataSetChanged();
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    _recyclerView.setLayoutManager(llm);

                    _recyclerView.setAdapter(recyclerRepeater);
                    mCheckedItem = 1;
                    break;
                case 2:
                    RepeaterStading5 recyclerAdapter4 = new RepeaterStading5(mContext, mForm_items);
                    recyclerAdapter4.notifyDataSetChanged();
                    _recyclerView.setAdapter(recyclerAdapter4);
                    mCheckedItem = 2;

                    break;
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
    }


    public void loadata() {

        if (mCheckedItem == 1) {


            RepeaterStading4 recyclerRepeater = new RepeaterStading4(mContext, mFull_items);
            recyclerRepeater.notifyDataSetChanged();
            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            _recyclerView.setLayoutManager(llm);

            _recyclerView.setAdapter(recyclerRepeater);
        } else if (mCheckedItem == 2) {

            RepeaterStading5 recyclerAdapter4 = new RepeaterStading5(mContext, mForm_items);
            recyclerAdapter4.notifyDataSetChanged();
            _recyclerView.setAdapter(recyclerAdapter4);

        } else {


            recyclerRepeater = new RepeaterStading6(mContext, mShort_items);
            recyclerRepeater.notifyDataSetChanged();
            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            _recyclerView.setLayoutManager(llm);

            _recyclerView.setAdapter(recyclerRepeater);
        }

    }

}