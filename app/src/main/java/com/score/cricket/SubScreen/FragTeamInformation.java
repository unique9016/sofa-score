package com.score.cricket.SubScreen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.MainScreen.ActivityCrickrtMatchInformation;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.TeamDetails.FeaturedMatch.FeaturedExample;
import com.score.cricket.Models.TeamDetails.TeamInfo.TeamInfoExample;
import com.score.cricket.Models.TeamDetails.Team_Example;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragTeamInformationBinding;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragTeamInformation extends Fragment implements IEventListener {

    FrameLayout fl_adplaceholder;
    private Context mContext;
    private String mCountryname, mSeasonname, mHometeam, mAwayteam;
    private long mTimestamp;
    private int mHome_score, mAway_score, mEvent_id;
    private int mTeamid;
    private int A;
    private int B;
    private ImageView _img, _img1, _img2, _img3, _img4, _img5, _img6, _img7, _img8, _img9;
    private ImageView _teamlogo1, _teamlogo2, _imgturnament;
    private TextView _win, _win1, _win2, _win3, _win4, _win5, _win6, _win7, _win8, _win9;
    private TextView _loss, _loss1, _loss2, _loss3, _loss4, _loss5, _loss6, _loss7, _loss8, _loss9;
    private TextView _season_name, _teamname, _matchtime, _homescore, _awayscore;
    private LinearLayout _matchlayout, _scorelayout, _venue_layout, _county_layout, _tournament_layout, _total_layout, _divider, _divider1, _divider2, _linearLayout,
            _linearLayout1, _linearLayout2, _linearLayout3, _linearLayout4, _linearLayout5, _linearLayout6, _linearLayout7, _linearLayout8, _linearLayout9;
    private Animation a, b;
    private TextView _totalplayer, _tournamentname, _country_name, _stadiumname, _cityname, _capacity;
    private SwipeRefreshLayout _swipeRefreshLayout;
    private CardView _cardView;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
        mTeamid = sharedPreferences.getInt("team_id", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragTeamInformationBinding binding = FragTeamInformationBinding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();

        mContext = getActivity();


        UIInitialize(binding);

        RunAnimation();

        ApiCall();

        _swipeRefreshLayout = binding.swiperefresh;
        _swipeRefreshLayout.setOnRefreshListener(() -> {
            RunAnimation();
            ApiCall();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);


        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeAds(fl_adplaceholder, requireActivity(), 1);
    }

    private void UIInitialize(FragTeamInformationBinding binding) {

        _total_layout = binding.totalPLayout;
        _tournament_layout = binding.tournamentLayout;
        _county_layout = binding.countyLayout;
        _venue_layout = binding.venueLayout;
        _cardView = binding.cardview;
        _divider = binding.divider;
        _divider1 = binding.divider1;
        _divider2 = binding.divider2;
        _totalplayer = binding.totalplayer;
        _tournamentname = binding.tournamentname;
        _country_name = binding.countryname;
        _stadiumname = binding.stadiumname;
        _cityname = binding.cityname;
        _capacity = binding.capacity;
        _linearLayout = binding.linearLayout;
        _linearLayout1 = binding.linearLayout1;
        _linearLayout2 = binding.linearLayout2;
        _linearLayout3 = binding.linearLayout3;
        _linearLayout4 = binding.linearLayout4;
        _linearLayout5 = binding.linearLayout5;
        _linearLayout6 = binding.linearLayout6;
        _linearLayout7 = binding.linearLayout7;
        _linearLayout8 = binding.linearLayout8;
        _linearLayout9 = binding.linearLayout9;
        _img = binding.img;
        _img1 = binding.img1;
        _img2 = binding.img2;
        _img3 = binding.img3;
        _img4 = binding.img4;
        _img5 = binding.img5;
        _img6 = binding.img6;
        _img7 = binding.img7;
        _img8 = binding.img8;
        _img9 = binding.img9;
        _win = binding.win;
        _win1 = binding.win1;
        _win2 = binding.win2;
        _win3 = binding.win3;
        _win4 = binding.win4;
        _win5 = binding.win5;
        _win6 = binding.win6;
        _win7 = binding.win7;
        _win8 = binding.win8;
        _win9 = binding.win9;
        _loss = binding.loss;
        _loss1 = binding.loss1;
        _loss2 = binding.loss2;
        _loss3 = binding.loss3;
        _loss4 = binding.loss4;
        _loss5 = binding.loss5;
        _loss6 = binding.loss6;
        _loss7 = binding.loss7;
        _loss8 = binding.loss8;
        _loss9 = binding.loss9;
        _teamlogo1 = binding.teamlogo1;
        _teamlogo2 = binding.teamlogo2;
        _imgturnament = binding.imgtornament;
        _season_name = binding.seasonname;
        _teamname = binding.teamname;
        _matchlayout = binding.matchlayout;
        _scorelayout = binding.scorelayout;
        _matchtime = binding.matchtime;
        _homescore = binding.homescore;
        _awayscore = binding.awayscore;
    }

    private void RunAnimation() {
        a = AnimationUtils.loadAnimation(requireActivity(), R.anim.fade_up);
        a.reset();
        b = AnimationUtils.loadAnimation(requireActivity(), R.anim.fade_down);
        b.reset();


    }

    private void ApiCall() {

        APIService service = RestManager.getPlayer().create(APIService.class);
        Call<List<Team_Example>> call17 = service.getTeamInfo(mTeamid);

        call17.enqueue(new Callback<List<Team_Example>>() {
            @Override
            public void onResponse(@NonNull Call<List<Team_Example>> call, @NonNull Response<List<Team_Example>> response) {
                Log.d("TEAMINFO", "winlos...s" + response.body().get(0).getWinFlag());
                String team = ActivityLaunch.LogoLink_2;

                try {

                    String team0 = team + response.body().get(0).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img);

                    _linearLayout.setVisibility(View.VISIBLE);


                    if (response.body().get(0).getWinFlag().equals("W")) {
                        _win.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win.setVisibility(View.VISIBLE);
                        _loss.setVisibility(View.VISIBLE);
                        _win.clearAnimation();
                        _win.startAnimation(a);
                    } else if (response.body().get(0).getWinFlag().equals("L")) {
                        _loss.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss.setVisibility(View.VISIBLE);
                        _win.setVisibility(View.VISIBLE);
                        _loss.clearAnimation();
                        _loss.startAnimation(b);
                    } else {
                        _win.setVisibility(View.INVISIBLE);
                        _loss.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout.setVisibility(View.GONE);
                    _img.setVisibility(View.GONE);
                    _win.setVisibility(View.GONE);
                    _loss.setVisibility(View.GONE);
                }

                try {

                    String team0 = team + response.body().get(1).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img1);

                    _linearLayout1.setVisibility(View.VISIBLE);

                    if (response.body().get(1).getWinFlag().equals("W")) {
                        _win1.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss1.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win1.setVisibility(View.VISIBLE);
                        _loss1.setVisibility(View.VISIBLE);
                        _win1.clearAnimation();
                        _win1.startAnimation(a);
                    } else if (response.body().get(1).getWinFlag().equals("L")) {
                        _loss1.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win1.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss1.setVisibility(View.VISIBLE);
                        _win1.setVisibility(View.VISIBLE);
                        _loss1.clearAnimation();
                        _loss1.startAnimation(b);
                    } else {
                        _win1.setVisibility(View.INVISIBLE);
                        _loss1.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout1.setVisibility(View.GONE);
                    _img1.setVisibility(View.GONE);
                    _win1.setVisibility(View.GONE);
                    _loss1.setVisibility(View.GONE);
                }

                try {

                    String team0 = team + response.body().get(2).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img2);

                    _linearLayout2.setVisibility(View.VISIBLE);

                    if (response.body().get(2).getWinFlag().equals("W")) {
                        _win2.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss2.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win2.setVisibility(View.VISIBLE);
                        _loss2.setVisibility(View.VISIBLE);
                        _win2.clearAnimation();
                        _win2.startAnimation(a);
                    } else if (response.body().get(2).getWinFlag().equals("L")) {
                        _loss2.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win2.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss2.setVisibility(View.VISIBLE);
                        _win2.setVisibility(View.VISIBLE);
                        _loss2.clearAnimation();
                        _loss2.startAnimation(b);
                    } else {
                        _win2.setVisibility(View.INVISIBLE);
                        _loss2.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout2.setVisibility(View.GONE);
                    _img2.setVisibility(View.GONE);
                    _win2.setVisibility(View.GONE);
                    _loss2.setVisibility(View.GONE);
                }

                try {

                    String team0 = team + response.body().get(3).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img3);

                    _linearLayout3.setVisibility(View.VISIBLE);

                    if (response.body().get(3).getWinFlag().equals("W")) {
                        _win3.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss3.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win3.setVisibility(View.VISIBLE);
                        _loss3.setVisibility(View.VISIBLE);
                        _win3.clearAnimation();
                        _win3.startAnimation(a);
                    } else if (response.body().get(3).getWinFlag().equals("L")) {
                        _loss3.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win3.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss3.setVisibility(View.VISIBLE);
                        _win3.setVisibility(View.VISIBLE);
                        _loss3.clearAnimation();
                        _loss3.startAnimation(b);
                    } else {
                        _win3.setVisibility(View.INVISIBLE);
                        _loss3.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout3.setVisibility(View.GONE);
                    _img3.setVisibility(View.GONE);
                    _win3.setVisibility(View.GONE);
                    _loss3.setVisibility(View.GONE);
                }

                try {

                    String team0 = team + response.body().get(4).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img4);

                    _linearLayout4.setVisibility(View.VISIBLE);

                    if (response.body().get(4).getWinFlag().equals("W")) {
                        _win4.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss4.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win4.setVisibility(View.VISIBLE);
                        _loss4.setVisibility(View.VISIBLE);
                        _win4.clearAnimation();
                        _win4.startAnimation(a);
                    } else if (response.body().get(4).getWinFlag().equals("L")) {
                        _loss4.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win4.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss4.setVisibility(View.VISIBLE);
                        _win4.setVisibility(View.VISIBLE);
                        _loss4.clearAnimation();
                        _loss4.startAnimation(b);
                    } else {
                        _win4.setVisibility(View.INVISIBLE);
                        _loss4.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout4.setVisibility(View.GONE);
                    _img4.setVisibility(View.GONE);
                    _win4.setVisibility(View.GONE);
                    _loss4.setVisibility(View.GONE);
                }

                try {

                    String team0 = team + response.body().get(5).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img5);

                    _linearLayout5.setVisibility(View.VISIBLE);

                    if (response.body().get(5).getWinFlag().equals("W")) {
                        _win5.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss5.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win5.setVisibility(View.VISIBLE);
                        _loss5.setVisibility(View.VISIBLE);
                        _win5.clearAnimation();
                        _win5.startAnimation(a);
                    } else if (response.body().get(5).getWinFlag().equals("L")) {
                        _loss5.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win5.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss5.setVisibility(View.VISIBLE);
                        _win5.setVisibility(View.VISIBLE);
                        _loss5.clearAnimation();
                        _loss5.startAnimation(b);
                    } else {
                        _win5.setVisibility(View.INVISIBLE);
                        _loss5.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout5.setVisibility(View.GONE);
                    _img5.setVisibility(View.GONE);
                    _win5.setVisibility(View.GONE);
                    _loss5.setVisibility(View.GONE);
                }

                try {

                    String team0 = team + response.body().get(6).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img6);

                    _linearLayout6.setVisibility(View.VISIBLE);

                    if (response.body().get(6).getWinFlag().equals("W")) {
                        _win6.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss6.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win6.setVisibility(View.VISIBLE);
                        _loss6.setVisibility(View.VISIBLE);
                        _win6.clearAnimation();
                        _win6.startAnimation(a);
                    } else if (response.body().get(6).getWinFlag().equals("L")) {
                        _loss6.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win6.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss6.setVisibility(View.VISIBLE);
                        _win6.setVisibility(View.VISIBLE);
                        _loss6.clearAnimation();
                        _loss6.startAnimation(b);
                    } else {
                        _win6.setVisibility(View.INVISIBLE);
                        _loss6.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout6.setVisibility(View.GONE);
                    _img6.setVisibility(View.GONE);
                    _win6.setVisibility(View.GONE);
                    _loss6.setVisibility(View.GONE);
                }

                try {


                    String team0 = team + response.body().get(7).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img7);

                    _linearLayout7.setVisibility(View.VISIBLE);

                    if (response.body().get(7).getWinFlag().equals("W")) {
                        _win7.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss7.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win7.setVisibility(View.VISIBLE);
                        _loss7.setVisibility(View.VISIBLE);
                        _win7.clearAnimation();
                        _win7.startAnimation(a);
                    } else if (response.body().get(7).getWinFlag().equals("L")) {
                        _loss7.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win7.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss7.setVisibility(View.VISIBLE);
                        _win7.setVisibility(View.VISIBLE);
                        _loss7.clearAnimation();
                        _loss7.startAnimation(b);
                    } else {
                        _win7.setVisibility(View.INVISIBLE);
                        _loss7.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout7.setVisibility(View.GONE);
                    _img7.setVisibility(View.GONE);
                    _win7.setVisibility(View.GONE);
                    _loss7.setVisibility(View.GONE);
                }

                try {

                    String team0 = team + response.body().get(8).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img8);


                    _linearLayout8.setVisibility(View.VISIBLE);

                    if (response.body().get(8).getWinFlag().equals("W")) {
                        _win8.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss8.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win8.setVisibility(View.VISIBLE);
                        _loss8.setVisibility(View.VISIBLE);
                        _win8.clearAnimation();
                        _win8.startAnimation(a);
                    } else if (response.body().get(8).getWinFlag().equals("L")) {
                        _loss8.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win8.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss8.setVisibility(View.VISIBLE);
                        _win8.setVisibility(View.VISIBLE);
                        _loss8.clearAnimation();
                        _loss8.startAnimation(b);
                    } else {
                        _win8.setVisibility(View.INVISIBLE);
                        _loss8.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout8.setVisibility(View.GONE);
                    _img8.setVisibility(View.GONE);
                    _win8.setVisibility(View.GONE);
                    _loss8.setVisibility(View.GONE);
                }

                try {

                    String team0 = team + response.body().get(9).getOpponent().getId() + "/logo";

                    Glide.with(mContext)
                            .load(team0)
                            .centerCrop()
                            .into(_img9);

                    _linearLayout9.setVisibility(View.VISIBLE);

                    if (response.body().get(9).getWinFlag().equals("W")) {
                        _win9.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));
                        _loss9.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _win9.setVisibility(View.VISIBLE);
                        _loss9.setVisibility(View.VISIBLE);
                        _win9.clearAnimation();
                        _win9.startAnimation(a);
                    } else if (response.body().get(9).getWinFlag().equals("L")) {
                        _loss9.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                        _win9.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.grey));
                        _loss9.setVisibility(View.VISIBLE);
                        _win9.setVisibility(View.VISIBLE);
                        _loss9.clearAnimation();
                        _loss9.startAnimation(b);
                    } else {
                        _win9.setVisibility(View.INVISIBLE);
                        _loss9.setVisibility(View.INVISIBLE);
                    }
                } catch (Exception e) {
                    _linearLayout9.setVisibility(View.GONE);
                    _img9.setVisibility(View.GONE);
                    _win9.setVisibility(View.GONE);
                    _loss9.setVisibility(View.GONE);
                }


            }

            @Override
            public void onFailure(@NonNull Call<List<Team_Example>> call, @NonNull Throwable t) {
                Log.d("TEAMINFO", "fail.." + t.toString());
            }
        });

        APIService service1 = RestManager.getPlayer().create(APIService.class);
        Call<FeaturedExample> call18 = service1.getFeaturedMatch(mTeamid);

        call18.enqueue(new Callback<FeaturedExample>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<FeaturedExample> call, @NonNull Response<FeaturedExample> response) {

                for (int i = 0; i < response.body().getTournaments().size(); i++) {
                    for (int j = 0; j < response.body().getTournaments().get(i).getEvents().size(); j++) {
                        int teamid1 = response.body().getTournaments().get(i).getEvents().get(j).getHomeTeam().getId();
                        int teamid2 = response.body().getTournaments().get(i).getEvents().get(j).getAwayTeam().getId();
                        String team1 = ActivityLaunch.LogoLink_2 + teamid1 + "/logo";

                        Glide.with(mContext)
                                .load(team1)
                                .centerCrop()
                                .into(_teamlogo1);

                        String team2 = ActivityLaunch.LogoLink_2 + teamid2 + "/logo";

                        Glide.with(mContext)
                                .load(team2)
                                .centerCrop()
                                .into(_teamlogo2);

                        mHometeam = response.body().getTournaments().get(i).getEvents().get(j).getHomeTeam().getName().toString();
                        mAwayteam = response.body().getTournaments().get(i).getEvents().get(j).getAwayTeam().getName().toString();
                        try {
                            mHome_score = response.body().getTournaments().get(i).getEvents().get(j).getHomeScore().getDisplay();
                        } catch (Exception e) {
                            mHome_score = 0;
                        }

                        try {
                            mAway_score = response.body().getTournaments().get(i).getEvents().get(j).getAwayScore().getDisplay();
                        } catch (Exception e) {
                            mAway_score = 0;
                        }

                        try {
                            mEvent_id = response.body().getTournaments().get(i).getEvents().get(j).getId();
                        } catch (Exception e) {
                            mEvent_id = 0;
                        }

                        mTimestamp = response.body().getTournaments().get(i).getEvents().get(j).getStartTimestamp();
                    }
                    mCountryname = response.body().getTournaments().get(i).getCategory().getName();
                    mSeasonname = response.body().getTournaments().get(i).getTournament().getName();
                }

                _season_name.setText(mCountryname + "," + mSeasonname);
                _teamname.setText(mHometeam + " - " + mAwayteam);

                getDateandTime(mTimestamp);

                try {
                    SharedPreferences sharedPreferencess = requireActivity().getSharedPreferences("name", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferencess.edit();
                    editor.putInt("eventid", mEvent_id);
                    editor.apply();
                } catch (Exception e) {
                    Log.d("Formation", "shareprefrencees error: " + e.toString());

                }

                _cardView.setOnClickListener(v -> {

                    Intent intent = new Intent(requireActivity(), ActivityCrickrtMatchInformation.class);
                    if (AdsHelper.isNetworkConnected(requireActivity())) {
                        MyApplication.getInstance().displayInterstitialAds(requireActivity(), intent, false);
                    } else {
                        startActivity(intent);
                    }

                });

                if (mHome_score == 0) {


                } else {
                    Log.d("ABCDEF", "else condition");
                    _scorelayout.setVisibility(View.VISIBLE);
                    _matchlayout.setVisibility(View.INVISIBLE);


                    _homescore.setText(String.valueOf(mHome_score));
                    _awayscore.setText(String.valueOf(mAway_score));

                    A = mHome_score;
                    B = mAway_score;


                    if (A > B) {

                        _homescore.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));

                    } else {

                        _awayscore.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                    }
                }


            }

            @Override
            public void onFailure(@NonNull Call<FeaturedExample> call, @NonNull Throwable t) {
                Log.d("TEAMINFO", "winlos...sfail..." + t.toString());
            }
        });

        APIService service2 = RestManager.getPlayer().create(APIService.class);
        Call<TeamInfoExample> call19 = service2.getMatchInfo1(mTeamid);

        call19.enqueue(new Callback<TeamInfoExample>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<TeamInfoExample> call, @NonNull Response<TeamInfoExample> response) {

                try {
                    int t_id = response.body().getTournamentInfo().get(0).getUniqueId();
                    String tournament = ActivityLaunch.LogoLink + t_id + "/logo";

                    Glide.with(mContext)
                            .load(tournament)
                            .centerCrop()
                            .into(_imgturnament);
                } catch (Exception e) {

                }

                try {
                    _totalplayer.setText(response.body().getTotalPlayers().toString());
                } catch (Exception e) {
                    _total_layout.setVisibility(View.GONE);
                }
                try {
                    _tournamentname.setText(response.body().getTournamentInfo().get(0).getName().toString());
                } catch (Exception e) {
                    _tournament_layout.setVisibility(View.GONE);
                    _divider.setVisibility(View.GONE);

                }
                try {
                    _country_name.setText(response.body().getCountry().toString());
                } catch (Exception e) {
                    _county_layout.setVisibility(View.GONE);
                    _divider1.setVisibility(View.GONE);

                }
                try {
                    _stadiumname.setText(response.body().getVenue().getStadium().getName().toString());
                } catch (Exception e) {
                    _venue_layout.setVisibility(View.GONE);
                    _divider2.setVisibility(View.GONE);

                }
                try {
                    _capacity.setText(response.body().getVenue().getStadium().getCapacity().toString());
                } catch (Exception e) {
                    _venue_layout.setVisibility(View.GONE);
                    _divider2.setVisibility(View.GONE);
                }

                try {
                    _cityname.setText(response.body().getVenue().getCity().getName().toString());
                } catch (Exception e) {
                    _venue_layout.setVisibility(View.GONE);
                    _divider2.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<TeamInfoExample> call, @NonNull Throwable t) {

            }
        });


    }

    @SuppressLint("SetTextI18n")
    private String getDate(long time) {
        Date date = new Date(time * 1000);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("K:mm aa ");
        sdf.setTimeZone(TimeZone.getDefault());

        Log.d("TIME", "time" + sdf.format(date));
        _matchlayout.setVisibility(View.VISIBLE);
        _matchtime.setText("Today, " + sdf.format(date));
        return sdf.format(date);


    }

    @SuppressLint("SetTextI18n")
    private void getDateandTime(long time) {

        Date date5 = new Date(time * 1000);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf5 = new SimpleDateFormat("K:mm aa ");
        sdf5.setTimeZone(TimeZone.getDefault());

        Date date2 = new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyy K:mm:ss aa ");
        sdf1.setTimeZone(TimeZone.getDefault());

        String currenttime = sdf1.format(date2);

        Log.d("PRASHANT", "simplmethod - time" + sdf1.format(date2));


        Date date = new Date(time * 1000);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy K:mm:ss aa ");
        sdf.setTimeZone(TimeZone.getDefault());

        String match_time = sdf.format(date);
        Log.d("PRASHANT", "match time" + match_time);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy K:mm:ss aa");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(currenttime);
            d2 = format.parse(match_time);

            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d2);


            int a = Hours.hoursBetween(dt1, dt2).getHours() % 24;
            int b = Minutes.minutesBetween(dt1, dt2).getMinutes() % 60;
            int c = Seconds.secondsBetween(dt1, dt2).getSeconds() % 60;
            int d = Days.daysBetween(dt1, dt2).getDays();

            if (d == 0) {
                _matchlayout.setVisibility(View.VISIBLE);
                _matchtime.setText("Today, " + sdf5.format(date5));
            } else if (d == 1) {
                _matchlayout.setVisibility(View.VISIBLE);
                _matchtime.setText("Tomorrow, " + sdf5.format(date5));
            } else {
                _matchlayout.setVisibility(View.VISIBLE);
                _matchtime.setText("In " + d + " days");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}