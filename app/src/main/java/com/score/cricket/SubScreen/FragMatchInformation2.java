package com.score.cricket.SubScreen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.AppMatches.AppMatches_Details.Details_Example;
import com.score.cricket.Models.Match_DetailViewModel;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.FragMatchInformation2Binding;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragMatchInformation2 extends Fragment implements IEventListener {

    private static int id;
    int mDelay = 0;
    int mPeriod = 500;
    Timer mTimer = new Timer();
    Match_DetailViewModel mNewsViewModel;
    List<Details_Example> mNewsArticles;
    View view;
    FrameLayout fl_adplaceholder;
    private Context mContext;
    private int A;
    private String a;
    private String b;
    private String c;
    private String mUrl;
    private String mTeam1;
    private String mTeam2;
    private int mSportid, mHomeiconid, mAwayiconid;
    private TextView _header, _teamname, _matchtime, _matchcountdown, _h_score, _dash, _a_score, _homeform1,
            _homeform2, _homeform3, _homeform4, _homeform5, _awayform1, _awayform2, _awayform3, _awayform4,
            _awayform5, _position1, _position2, _points1, _points2, _datetime, _homesscore, _awayscore,
            _scorestatus, _refereename, _tosswin, _toss, _umpire1, _umpire2, _tvumpire, _menofthematch,
            _venuename, _venuelocation;
    private ImageView _leageicon, _homeicon, _awayicon, _homeicon1, _awayicon1;
    private LinearLayout _homescoreboard, _awayscoreboard, _score_board_layout, _scorelayout,
            _scoreboardlayout, _matchinfolayout, _rnamelayout, _twlayout, _tdlayout, _umpirelayout1,
            _umpirelayout2, _tvumpirelayout, _momlayout, _v_namelayout, _locationlayout, _divider;
    private LinearLayout _refree_divider, _refree_layout, _venue_divider, _venue_main_layout;
    private CardView _leagename_layout;
    private SwipeRefreshLayout _swipeRefreshLayout;
    private int status = 1;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            requireActivity().runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("name", Context.MODE_PRIVATE);
        id = sharedPreferences.getInt("eventid", 0);
        mSportid = sharedPreferences.getInt("id", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragMatchInformation2Binding binding = FragMatchInformation2Binding.inflate(LayoutInflater.from(requireActivity()), container, false);
        View view = binding.getRoot();

        fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();

        mContext = getActivity();

        UIInitilialize(view);
        ApiCalll();
        loadlogo();
        checkstatus();

        _swipeRefreshLayout.setOnRefreshListener(() -> {
            ApiCalll();
            checkstatus();
            new Handler().postDelayed(() -> _swipeRefreshLayout.setRefreshing(false), 1000);
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getInstance().loadNativeFullAds(fl_adplaceholder, requireActivity(), 1);
    }

    private void checkstatus() {
        ApiCalll();
        loadlogo();

        if (status == 1) {
            loadlogo();
        } else if (status == 0) {
            loadlogo();
        } else if (status == 100) {
            loadlogo();
        } else {
            loadlogo();
            LiveAPIData();
        }
    }

    private void LiveAPIData() {
        loadlogo();

        mTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                ApiCalll();
            }
        }, mDelay, mPeriod);
    }

    private void LiveData() {
        Log.d("LIVEDATA", "LiveData: ");

        mNewsViewModel = new ViewModelProvider(this).get(Match_DetailViewModel.class);
        mNewsViewModel.init();
        mNewsViewModel.getNewsRepository().observe(getViewLifecycleOwner(), newsResponse -> {
            mNewsArticles = Collections.singletonList(newsResponse);
            ApiCalll();


        });
    }

    private void UIInitilialize(View view) {

        _leagename_layout = view.findViewById(R.id.leagename_layout);
        _score_board_layout = view.findViewById(R.id.score_board_layout);
        _refree_divider = view.findViewById(R.id.refree_divider);
        _refree_layout = view.findViewById(R.id.refree_layout);
        _venue_divider = view.findViewById(R.id.venue_divider);
        _venue_main_layout = view.findViewById(R.id.venue_main_layout);

        _swipeRefreshLayout = view.findViewById(R.id.swiperefresh);

        _leageicon = view.findViewById(R.id.leaguelogo);
        _homeicon = view.findViewById(R.id.homeicon);
        _awayicon = view.findViewById(R.id.awayicon);
        _homeicon1 = view.findViewById(R.id.homeicon1);
        _awayicon1 = view.findViewById(R.id.awayicon1);

        _header = view.findViewById(R.id.header);
        _teamname = view.findViewById(R.id.teamname);

        _matchtime = view.findViewById(R.id.matchtime);
        _matchcountdown = view.findViewById(R.id.matchcountdown);

        _scorelayout = view.findViewById(R.id.scorelayout);
        _scoreboardlayout = view.findViewById(R.id.scoreboardlayout);
        _matchinfolayout = view.findViewById(R.id.matchinfolayout);
        _homescoreboard = view.findViewById(R.id.homescoreboard);
        _awayscoreboard = view.findViewById(R.id.awayscoreboard);
        _rnamelayout = view.findViewById(R.id.rnamelayout);
        _twlayout = view.findViewById(R.id.twlayout);
        _tdlayout = view.findViewById(R.id.tdlayout);
        _umpirelayout1 = view.findViewById(R.id.umpirelayout1);
        _umpirelayout2 = view.findViewById(R.id.umpirelayout2);
        _tvumpirelayout = view.findViewById(R.id.tvumpirelayout);
        _momlayout = view.findViewById(R.id.momlayout);
        _v_namelayout = view.findViewById(R.id.v_namelayout);
        _locationlayout = view.findViewById(R.id.locationlayout);
        _divider = view.findViewById(R.id.divider);

        _h_score = view.findViewById(R.id.h_score);
        _a_score = view.findViewById(R.id.a_score);

        _dash = view.findViewById(R.id.dash);

        _homeform1 = view.findViewById(R.id.homeform1);
        _homeform2 = view.findViewById(R.id.homeform2);
        _homeform3 = view.findViewById(R.id.homeform3);
        _homeform4 = view.findViewById(R.id.homeform4);
        _homeform5 = view.findViewById(R.id.homeform5);

        _awayform1 = view.findViewById(R.id.awayform1);
        _awayform2 = view.findViewById(R.id.awayform2);
        _awayform3 = view.findViewById(R.id.awayform3);
        _awayform4 = view.findViewById(R.id.awayform4);
        _awayform5 = view.findViewById(R.id.awayform5);

        _position1 = view.findViewById(R.id.position1);
        _position2 = view.findViewById(R.id.position2);
        _points1 = view.findViewById(R.id.points1);
        _points2 = view.findViewById(R.id.points2);

        _datetime = view.findViewById(R.id.datetime);
        _homesscore = view.findViewById(R.id.homescore);
        _awayscore = view.findViewById(R.id.awayscore);
        _scorestatus = view.findViewById(R.id.scorestatus);

        _refereename = view.findViewById(R.id.refereename);
        _tosswin = view.findViewById(R.id.tosswin);
        _toss = view.findViewById(R.id.toss);
        _umpire1 = view.findViewById(R.id.umpire1);
        _umpire2 = view.findViewById(R.id.umpire2);
        _tvumpire = view.findViewById(R.id.tvumpire);
        _menofthematch = view.findViewById(R.id.mom);

        _venuename = view.findViewById(R.id.venuename);
        _venuelocation = view.findViewById(R.id.venuelocation);
    }

    @SuppressLint("SetTextI18n")
    private void ApiCalll() {
        APIService apiService8 = RestManager.getPlayer().create(APIService.class);
        Call<Details_Example> call8 = apiService8.getMatchInfo(id);

        call8.enqueue(new Callback<Details_Example>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<Details_Example> call, @NonNull Response<Details_Example> response) {
                assert response.body() != null;

                try {
                    status = response.body().getGame().getTournaments().get(0).getEvents().get(0).getStatus().getCode();
                    mHomeiconid = response.body().getGame().getTournaments().get(0).getEvents().get(0).getHomeTeam().getId();
                    mAwayiconid = response.body().getGame().getTournaments().get(0).getEvents().get(0).getAwayTeam().getId();
                } catch (Exception e) {
                }
                try {
                    _leagename_layout.setVisibility(View.VISIBLE);
                    _header.setText(response.body().getGame().getSport().getName() + ", " +
                            response.body().getGame().getTournaments().get(0).getCategory().getName() + ", " +
                            response.body().getGame().getTournaments().get(0).getTournament().getName());


                } catch (Exception e) {
                    _leagename_layout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {

                    _score_board_layout.setVisibility(View.VISIBLE);
                    _teamname.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getHomeTeam().getName() + " - " +
                            response.body().getGame().getTournaments().get(0).getEvents().get(0).getAwayTeam().getName());

                    SharedPreferences sharedPreferences = getContext().getSharedPreferences("name", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("HomeTeam", response.body().getGame().getTournaments().get(0).getEvents().get(0).getHomeTeam().getName());
                    editor.putString("AwayTeam", response.body().getGame().getTournaments().get(0).getEvents().get(0).getAwayTeam().getName());

                    editor.putInt("hometeamid", response.body().getGame().getTournaments().get(0).getEvents().get(0).getHomeTeam().getId());
                    editor.putInt("awayteamid", response.body().getGame().getTournaments().get(0).getEvents().get(0).getAwayTeam().getId());
                    editor.apply();

                } catch (Exception e) {
                    _score_board_layout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }


                try {
                    b = response.body().getGame().getTournaments().get(0).getEvents().get(0).getAwayTeam().getName();
                    c = response.body().getGame().getTournaments().get(0).getEvents().get(0).getHomeTeam().getName();
                    _h_score.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getHomeScore().getCurrent().toString());
                    _a_score.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getAwayScore().getCurrent().toString());
                    _scorelayout.setVisibility(View.VISIBLE);
                    if (status == 1) {
                        try {
                            A = response.body().getGame().getTournaments().get(0).getEvents().get(0).getWinnerCode();
                            if (A == 1) {
                                _h_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            } else {
                                _a_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            }
                        } catch (Exception e) {
                            _h_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            _a_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                        }
                    } else if (status == 0) {
                        try {
                            A = response.body().getGame().getTournaments().get(0).getEvents().get(0).getWinnerCode();
                            if (A == 1) {
                                _h_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            } else {
                                _a_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            }
                        } catch (Exception e) {
                            _h_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            _a_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));

                        }
                    } else if (status == 100) {
                        try {
                            A = response.body().getGame().getTournaments().get(0).getEvents().get(0).getWinnerCode();

                            if (A == 1) {

                                _h_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));

                            } else {

                                _a_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            }
                        } catch (Exception e) {
                            _h_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            _a_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));

                        }
                    } else {
                        try {

                            _h_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.red));
                            _dash.setTextColor(ContextCompat.getColor(requireActivity(), R.color.red));
                            _a_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.red));

                            try {
                                a = response.body().getGame().getTournaments().get(0).getEvents().get(0).getCurrentBattingTeam().getName();
                            } catch (Exception e) {

                                Log.d("FGFDGDFG", "onResponse: " + e);
                            }

                            if (a.equals(b)) {
                                _awayscore.setTextColor(ContextCompat.getColor(requireActivity(), R.color.red));
                            } else if (a.equals(c)) {
                                _homesscore.setTextColor(ContextCompat.getColor(requireActivity(), R.color.red));
                            }


                        } catch (Exception e) {
                            _h_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            _a_score.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));
                            _dash.setTextColor(ContextCompat.getColor(requireActivity(), R.color.black));

                        }
                    }

                } catch (Exception e) {
                    _matchtime.setVisibility(View.VISIBLE);
                    _matchcountdown.setVisibility(View.VISIBLE);
                    _scorelayout.setVisibility(View.GONE);

                    Log.d("VENUE", e.toString());

                }
                try {
                } catch (Exception e) {
                    Log.d("VENUE", e.toString());

                }
                try {
                } catch (Exception e) {
                    Log.d("VENUE", e.toString());

                }
                try {
                    getDate(response.body().getGame().getTournaments().get(0).getEvents().get(0).getStartTimestamp());
                    getDateandTime(response.body().getGame().getTournaments().get(0).getEvents().get(0).getStartTimestamp());
                    try {
                        _homesscore.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getHomeScore().getCurrent().toString() + "-" +
                                response.body().getGame().getTournaments().get(0).getEvents().get(0).getHomeScore().getInnings().getInning1().getWickets().toString() + "(" +
                                response.body().getGame().getTournaments().get(0).getEvents().get(0).getHomeScore().getInnings().getInning1().getOvers().toString() + ")");
                        _scoreboardlayout.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        Log.d("ERRROE", "onResponse: " + e);
                        _homescoreboard.setVisibility(View.INVISIBLE);

                    }

                    try {
                        _awayscore.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getAwayScore().getCurrent().toString() + "-" +
                                response.body().getGame().getTournaments().get(0).getEvents().get(0).getAwayScore().getInnings().getInning1().getWickets().toString() + "(" +
                                response.body().getGame().getTournaments().get(0).getEvents().get(0).getAwayScore().getInnings().getInning1().getOvers().toString() + ")");
                        _scoreboardlayout.setVisibility(View.VISIBLE);
                    } catch (Exception e) {
                        Log.d("ERRROE", "onResponse22: " + e);

                        _awayscoreboard.setVisibility(View.INVISIBLE);

                    }

                    _scorestatus.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getNote());

                } catch (Exception e) {
                    _scoreboardlayout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {
                    _position1.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getPosition());
                    _position2.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getPosition());
                    _points1.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getPoints());
                    _points2.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getPoints());
                    _matchinfolayout.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    _matchinfolayout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {
                    _refereename.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getReferee().getName());
                    _rnamelayout.setVisibility(View.VISIBLE);
                    _refree_divider.setVisibility(View.VISIBLE);
                    _refree_layout.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    _rnamelayout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {
                    _tosswin.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTossWin());
                    _twlayout.setVisibility(View.VISIBLE);
                    _refree_divider.setVisibility(View.VISIBLE);
                    _refree_layout.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    _twlayout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {
                    _toss.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTossDecision());
                    _tdlayout.setVisibility(View.VISIBLE);
                    _refree_divider.setVisibility(View.VISIBLE);
                    _refree_layout.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    _tdlayout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {
                    _umpire1.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getUmpire1());
                    _umpirelayout1.setVisibility(View.VISIBLE);
                    _refree_divider.setVisibility(View.VISIBLE);
                    _refree_layout.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    _umpirelayout1.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {
                    _umpire2.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getUmpire2());
                    _umpirelayout2.setVisibility(View.VISIBLE);
                    _refree_divider.setVisibility(View.VISIBLE);
                    _refree_layout.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    _umpirelayout2.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {
                    _tvumpire.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTvUmpire());
                    _tvumpirelayout.setVisibility(View.VISIBLE);
                    _refree_divider.setVisibility(View.VISIBLE);
                    _refree_layout.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    _tvumpirelayout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {
                    _menofthematch.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getManOfMatch().getName());
                    _momlayout.setVisibility(View.VISIBLE);
                    _refree_divider.setVisibility(View.VISIBLE);
                    _refree_layout.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    _momlayout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());
                }
                try {
                    _venuename.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getVenue().getStadium().getName());
                    _v_namelayout.setVisibility(View.VISIBLE);
                    _venue_divider.setVisibility(View.VISIBLE);
                    _venue_main_layout.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    _v_namelayout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }
                try {

                    _venuelocation.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getVenue().getCity().getName() + "," +
                            response.body().getGame().getTournaments().get(0).getEvents().get(0).getVenue().getCountry().getName());
                    _locationlayout.setVisibility(View.VISIBLE);
                    _venue_divider.setVisibility(View.VISIBLE);
                    _venue_main_layout.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    _locationlayout.setVisibility(View.GONE);
                    Log.d("VENUE", e.toString());

                }


                try {

                    _homeform1.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(0));

                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(0).equals("W")) {
                        _homeform1.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _homeform1.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }
                try {

                    _homeform2.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(1));
                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(1).equals("W")) {
                        _homeform2.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _homeform2.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }
                try {
                    _homeform3.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(2));
                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(2).equals("W")) {
                        _homeform3.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _homeform3.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }
                try {

                    _homeform4.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(3));
                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(3).equals("W")) {
                        _homeform4.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _homeform4.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }
                try {

                    _homeform5.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(4));
                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getHomeTeam().getForm().get(4).equals("W")) {
                        _homeform5.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _homeform5.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }

                try {

                    _awayform1.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(0));
                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(0).equals("W")) {
                        _awayform1.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _awayform1.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }
                try {

                    _awayform2.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(1));
                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(1).equals("W")) {
                        _awayform2.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _awayform2.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }
                try {

                    _awayform3.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(2));
                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(2).equals("W")) {
                        _awayform3.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _awayform3.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }
                try {

                    _awayform4.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(3));
                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(3).equals("W")) {
                        _awayform4.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _awayform4.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }
                try {

                    _awayform5.setText(response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(4));
                    if (response.body().getGame().getTournaments().get(0).getEvents().get(0).getTeamsForm().getAwayTeam().getForm().get(4).equals("W")) {
                        _awayform5.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.green));

                    } else {
                        _awayform5.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.red));
                    }
                } catch (Exception e) {
                }

                if (status == 1) {
                    loadlogo();
                } else if (status == 0) {
                    loadlogo();
                } else if (status == 100) {
                    loadlogo();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Details_Example> call, @NonNull Throwable t) {
                Log.d("VENUE", "Faile" + t);
            }
        });
    }

    private void loadlogo() {
        mUrl = ActivityLaunch.LogoLink + mSportid + "/logo";
        mTeam1 = ActivityLaunch.LogoLink_2 + mHomeiconid + "/logo";
        mTeam2 = ActivityLaunch.LogoLink_2 + mAwayiconid + "/logo";

        try {
            Glide.with(mContext)
                    .load(mUrl)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(_leageicon);

            Glide.with(mContext)
                    .load(mTeam1)
                    .centerCrop()
                    .into(_homeicon);

            Glide.with(mContext)
                    .load(mTeam2)
                    .centerCrop()
                    .into(_awayicon);

            Glide.with(mContext)
                    .load(mTeam2)
                    .centerCrop()
                    .into(_homeicon1);

            Glide.with(mContext)
                    .load(mTeam1)
                    .centerCrop()
                    .into(_awayicon1);
        } catch (Exception e) {
        }
    }

    private void getDate(long time) {
        Date date = new Date(time * 1000); // *1000 is to convert seconds to milliseconds
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy . K:mm aa "); // the format of your date
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf6 = new SimpleDateFormat("K:mm aa "); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault());

        _datetime.setText(sdf.format(date));
        Log.d("TIME", "time" + sdf.format(date));

        sdf.format(date);
    }

    @SuppressLint("SetTextI18n")
    private void getDateandTime(long time) {

        Date date5 = new Date(time * 1000); // *1000 is to convert seconds to milliseconds
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf5 = new SimpleDateFormat("dd-MM-yyyy K:mm aa "); // the format of your date
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf6 = new SimpleDateFormat("K:mm aa "); // the format of your date
        sdf5.setTimeZone(TimeZone.getDefault());


        Date date2 = new Date(); // *1000 is to convert seconds to milliseconds
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyy K:mm:ss aa "); // the format of your date
        sdf1.setTimeZone(TimeZone.getDefault());

        String currenttime = sdf1.format(date2);

        Log.d("PRASHANT", "simplmethod - time" + sdf1.format(date2));


        Date date = new Date(time * 1000); // *1000 is to convert seconds to milliseconds
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy K:mm:ss aa "); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault());

        String match_time = sdf.format(date);
        Log.d("PRASHANT", "match time" + match_time);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy K:mm:ss aa");

        Date d1 = null;
        Date d2 = null;

        try {
            d1 = format.parse(currenttime);
            d2 = format.parse(match_time);

            DateTime dt1 = new DateTime(d1);
            DateTime dt2 = new DateTime(d2);


            int a = Hours.hoursBetween(dt1, dt2).getHours() % 24;
            int b = Minutes.minutesBetween(dt1, dt2).getMinutes() % 60;
            int c = Seconds.secondsBetween(dt1, dt2).getSeconds() % 60;
            int d = Days.daysBetween(dt1, dt2).getDays();


            if (d == 0) {
                int daytosecond = d * 24 * 60 * 60;
                int hourtosecond = a * 60 * 60;
                int minutestosecond = b * 60;
                int second = c + daytosecond + hourtosecond + minutestosecond;

                new CountDownTimer(second * 1000, 1000) {

                    @SuppressLint({"SetTextI18n", "DefaultLocale"})
                    public void onTick(long millisUntilFinished) {
                        long sec = millisUntilFinished / 1000;
                        long min = sec / 60;
                        long hours = (sec / 60) / 60;
                        long minutes = min - (hours * 60);
                        long s = (minutes * 60) + (hours * 60 * 60);
                        long seconds = sec - s;
                        long days = hours / 24;

                        if (seconds == 30) {
                            Log.d("SECONDS", "onTick: ");
                        }

                        if (d == 0) {
                            _matchtime.setText("Today " + sdf6.format(date5));
                            _matchcountdown.setText(String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                        } else if (d == 1) {
                            _matchtime.setText(sdf5.format(date5));
                            _matchcountdown.setText("Tomorrow");
                        } else {
                            _matchtime.setText("sdf5.format(date5)");
                            _matchcountdown.setText("In " + d + " days");
                        }
                    }

                    public void onFinish() {
                        Log.d("LIVEDATA", "onFinish countdown: ");
                        _matchcountdown.setVisibility(View.GONE);
                        _matchtime.setVisibility(View.GONE);
                    }

                }.start();
            } else {
                if (d == 1) {
                    _matchtime.setText(sdf5.format(date5));
                    _matchcountdown.setText("Tomorrow");
                } else {
                    _matchtime.setText(sdf5.format(date5));
                    _matchcountdown.setText("In " + d + " days");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
    }

    public static class Match_DetailRepository {
        private static Match_DetailRepository newRepository;
        Call<Details_Example> call8;
        private APIService newsApi;


        public Match_DetailRepository() {
            newsApi = RestManager.getPlayer().create(APIService.class);
            call8 = newsApi.getMatchInfo(id);

            Log.d("LIVEDATA", "Match_DetailRepository: ");
            int mDelay = 0; // mDelay for 0 sec.
            int mPeriod = 10000; // repeat every 10 sec.
            Timer mTimer = new Timer();
            mTimer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    Log.d("LIVEDATA", "LiveData every 10 sec: ");
                    newsApi = RestManager.getPlayer().create(APIService.class);
                    call8 = newsApi.getMatchInfo(id);
                    getNews();
                }
            }, mDelay, mPeriod);


        }

        public static Match_DetailRepository getInstance() {
            Log.d("LIVEDATA", "getInstance: ");

            if (newRepository == null) {
                Log.d("LIVEDATA", "getInstance..if..: ");
                newRepository = new Match_DetailRepository();
            }


            return newRepository;
        }

        public MutableLiveData<Details_Example> getNews() {
            Log.d("LIVEDATA", "getNews: ");
            MutableLiveData<Details_Example> matchdata = new MutableLiveData<>();

            call8.enqueue(new Callback<Details_Example>() {
                @Override
                public void onResponse(Call<Details_Example> call, Response<Details_Example> response) {
                    if (response.isSuccessful()) {
                        Log.d("LIVEDATA", "onResponse: ");
                        matchdata.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<Details_Example> call, Throwable t) {
                    Log.d("LIVEDATA", "onFailure: ");
                }
            });

            return matchdata;
        }
    }
}