package com.score.cricket.Repeater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.Models.HomeMain.HomeMatch_inning_bowling;
import com.score.cricket.databinding.LayoutBowlingRvItemsBinding;

import java.util.ArrayList;
import java.util.List;

public class RepeaterBowling extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private ArrayList<Object> mDataList;

    public RepeaterBowling(Context mContext, ArrayList<Object> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        LayoutBowlingRvItemsBinding binding = LayoutBowlingRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PostHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        List<Object> list = this.mDataList;
        if (list == null || list.isEmpty()) {
            return 0;
        }
        return this.mDataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;
            HomeMatch_inning_bowling data = (HomeMatch_inning_bowling) mDataList.get(position);
            viewHolder.setData(data);
        }
    }

    @Override
    public int getItemCount() {
        if (mDataList == null) {
            return 0;
        }
        Log.d("MYTAG", "getitemcount" + mDataList.size());
        return mDataList.size();
    }

    private class PostHolder extends RecyclerView.ViewHolder {

        TextView _bowlername, _over, _maiden, _run, _wicket, _noball, _wide, _er;

        PostHolder(LayoutBowlingRvItemsBinding binding) {
            super(binding.getRoot());
            _bowlername = binding.bowlername;
            _over = binding.over;
            _maiden = binding.maiden;
            _run = binding.run;
            _wicket = binding.wicket;
            _noball = binding.noball;
            _wide = binding.wide;
        }

        @SuppressLint({"ResourceAsColor", "SetTextI18n", "UseValueOf"})
        public void setData(HomeMatch_inning_bowling data) {
            double dataa = data.getOver();
            int value = (int) dataa;
            _bowlername.setText(data.getBowlername());
            _over.setText(Integer.toString(value));
            _maiden.setText(String.valueOf(data.getMaiden()));
            _run.setText(String.valueOf(data.getRun()));
            _wicket.setText(String.valueOf(data.getWicket()));
            _noball.setText(String.valueOf(data.getNoball()));
            _wide.setText(String.valueOf(data.getWide()));
        }
    }
}
