package com.score.cricket.Repeater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.Models.HomeMain.HomeMatch_inning_batting;
import com.score.cricket.databinding.LayoutBattingRvItemsBinding;

import java.util.ArrayList;
import java.util.List;

public class RepeaterBatting extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private ArrayList<Object> mDataList;

    public RepeaterBatting(Context mContext, ArrayList<Object> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        LayoutBattingRvItemsBinding binding = LayoutBattingRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PostHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        List<Object> list = this.mDataList;
        if (list == null || list.isEmpty()) {
            return 0;
        }
        return this.mDataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;
            HomeMatch_inning_batting data = (HomeMatch_inning_batting) mDataList.get(position);
            viewHolder.setData(data);
        }

    }

    @Override
    public int getItemCount() {
        if (mDataList == null) {
            return 0;
        }
        Log.d("MYTAG", "getitemcount" + mDataList.size());
        return mDataList.size();
    }

    private class PostHolder extends RecyclerView.ViewHolder {

        TextView _batsmaname, _catchername, _bowlername, _status, _score, _ball, _four, _six, _sr;

        PostHolder(LayoutBattingRvItemsBinding binding) {
            super(binding.getRoot());
            _batsmaname = binding.batsmaname;
            _catchername = binding.catchername;
            _bowlername = binding.bowlername;
            _status = binding.status;
            _score = binding.score;
            _ball = binding.ball;
            _four = binding.four;
            _six = binding.six;
        }

        @SuppressLint({"ResourceAsColor", "SetTextI18n"})
        public void setData(HomeMatch_inning_batting data) {

            if (data.getBatsmaname() != null) {
                _batsmaname.setVisibility(View.VISIBLE);
                _batsmaname.setText(data.getBatsmaname());
            }
            if (data.getWicketid() == 1) {
                _status.setVisibility(View.VISIBLE);
                _status.setText("Bowled");
            } else if (data.getWicketid() == 3) {
                if (data.getCathername() != null) {
                    _catchername.setVisibility(View.VISIBLE);
                    _catchername.setText("c " + data.getCathername());
                }
            } else if (data.getWicketid() == 2) {
                if (data.getCathername() != null) {
                    _catchername.setVisibility(View.VISIBLE);
                    _catchername.setText("c " + data.getCathername());
                }
            } else if (data.getWicketid() == 8) {
                _status.setVisibility(View.VISIBLE);
                _status.setText("Not Out");
            } else if (data.getWicketid() == 14) {
                if (data.getCathername() != null) {
                    _catchername.setVisibility(View.VISIBLE);
                    _catchername.setText("c(sub) " + data.getCathername());
                }
            } else if (data.getWicketid() == 5) {
                if (data.getCathername() != null) {
                    _catchername.setVisibility(View.VISIBLE);
                    _catchername.setText("c " + data.getCathername());
                }
                _status.setVisibility(View.VISIBLE);
                _status.setText("Run Out");
            }
            if (data.getBowlername() != null) {
                _bowlername.setVisibility(View.VISIBLE);
                _bowlername.setText("b " + data.getBowlername());
            }
            _score.setText(String.valueOf(data.getRun()));
            _ball.setText(String.valueOf(data.getBall()));
            _four.setText(String.valueOf(data.getFour()));
            _six.setText(String.valueOf(data.getSix()));
        }
    }
}
