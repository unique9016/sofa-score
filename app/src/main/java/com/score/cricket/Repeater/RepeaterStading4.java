package com.score.cricket.Repeater;

import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.MainScreen.ActivityCricketTeamInformation;
import com.score.cricket.Models.HomeMain.HomeFullList;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.MyApplication;
import com.score.cricket.databinding.LayoutFullRvItemsBinding;
import com.score.cricket.databinding.LayoutStandingFullHeaderBinding;

import java.util.ArrayList;

public class RepeaterStading4 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mContext;
    private ArrayList<ListnerItems> mDataList;

    public RepeaterStading4(Activity mContext, ArrayList<ListnerItems> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        if (viewType == ListnerItems.TYPE_HEADER) {
            LayoutStandingFullHeaderBinding binding = LayoutStandingFullHeaderBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new VHHeader(binding);
        } else if (viewType == ListnerItems.TYPE_ITEM) {
            LayoutFullRvItemsBinding binding = LayoutFullRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new PostHolder(binding);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            HomeHeader header = (HomeHeader) mDataList.get(position);
            VHHeader VHheader = (VHHeader) holder;
            VHheader.tvName.setText(header.getName());
        } else if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;
            HomeFullList data = (HomeFullList) mDataList.get(position);
            viewHolder.setData(data);
        }
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mDataList.get(position).getItemType();
    }

    static class VHHeader extends RecyclerView.ViewHolder {
        TextView tvName;

        public VHHeader(LayoutStandingFullHeaderBinding binding) {
            super(binding.getRoot());
            this.tvName = binding.seasonname;
        }
    }

    private class PostHolder extends RecyclerView.ViewHolder {

        TextView index, name, total, win, draw, loss, points, netrunrate, noresult;


        PostHolder(LayoutFullRvItemsBinding binding) {
            super(binding.getRoot());
            index = binding.index;
            name = binding.teamname;
            total = binding.matchtotal;
            win = binding.win;
            draw = binding.drawtotal;
            loss = binding.loss;
            points = binding.points;
            netrunrate = binding.netrunrate;
            noresult = binding.noresult;

            binding.llMain.setOnClickListener(v -> {
                int pos = getAdapterPosition();

                if (pos != RecyclerView.NO_POSITION) {
                    HomeFullList clickedDataItem = (HomeFullList) mDataList.get(pos);

                    SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("team_id", clickedDataItem.getId());
                    editor.putString("team_name", clickedDataItem.getName());
                    editor.apply();

                    Intent intent = new Intent(v.getContext(), ActivityCricketTeamInformation.class);
                    if (AdsHelper.isNetworkConnected(mContext)) {
                        MyApplication.getInstance().displayInterstitialAds(mContext, intent, false);
                    } else {
                        v.getContext().startActivity(intent);
                    }
                }
            });
        }

        public void setData(HomeFullList data) {
            index.setText(data.getIndex());
            name.setText(data.getName());
            total.setText(data.getTotal());
            win.setText(data.getWin());
            draw.setText(data.getDraw());
            loss.setText(data.getLoss());
            points.setText(data.getPoints());
            netrunrate.setText(data.getNetrunrate());
            noresult.setText(data.getNoresult());
        }
    }
}
