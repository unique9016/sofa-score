package com.score.cricket.Repeater;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Helper.WorldCountryIdList;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.MainScreen.ActivityCrickrtMatchInformation;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.Models.HomeMain.HomeTeam_Matche;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.databinding.LayoutHeaderLayoutBinding;
import com.score.cricket.databinding.LayoutMatchScoreItemsBinding;

import java.util.ArrayList;

public class RapaeterMatch extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mContext;
    private final ArrayList<ListnerItems> mItems;
    String mTeamName;

    public RapaeterMatch(Activity mContext, ArrayList<ListnerItems> items) {
        this.mItems = items;
        this.mContext = mContext;
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("name", MODE_PRIVATE);
        mTeamName = sharedPreferences.getString("team_name", null);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ListnerItems.TYPE_HEADER) {
            LayoutHeaderLayoutBinding binding = LayoutHeaderLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new VHHeader(binding);
        } else if (viewType == ListnerItems.TYPE_ITEM) {
            LayoutMatchScoreItemsBinding binding = LayoutMatchScoreItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new VHItem(binding);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");

    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getItemType();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            HomeHeader header = (HomeHeader) mItems.get(position);
            VHHeader VHheader = (VHHeader) holder;

            try {
                WorldCountryIdList country = new WorldCountryIdList();
                int abs = country.Createarray().get(header.getFlage());
                Log.d("FLAGE", "loadlogo: " + abs);
                String flaage = ActivityLaunch.LogoLink_2 + abs + "/logo";
                Glide.with(mContext)
                        .load(flaage)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            } catch (Exception e) {

            }

            if (header.getFlage().equals("asia")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_asia)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }

            if (header.getFlage().equals("international")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_world)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }

            if (header.getFlage().equals("europe")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_europe)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }

            if (header.getFlage().equals("africa")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_africa)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }

            if (header.getFlage().equals("oceania")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_oceania)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }

            if (header.getFlage().equals("north-and-central-america")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_south_america)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }

            if (header.getFlage().equals("south-america")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_south_america)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }

            VHheader.tvName.setText(header.getName());
        } else if (holder instanceof VHItem) {
            HomeTeam_Matche person = (HomeTeam_Matche) mItems.get(position);
            VHItem VHitem = (VHItem) holder;
            VHitem.setData(person);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    static class VHHeader extends RecyclerView.ViewHolder {
        TextView tvName;
        ImageView imageView;

        public VHHeader(LayoutHeaderLayoutBinding binding) {
            super(binding.getRoot());
            this.tvName = binding.tvName;
            this.imageView = binding.leaguelogo;
        }
    }

    private class VHItem extends RecyclerView.ViewHolder {

        TextView _hometeamname, _homescore, _awayteamname, _awayscore, _date, _status, _winloassstatus;

        VHItem(LayoutMatchScoreItemsBinding binding) {
            super(binding.getRoot());
            _hometeamname = binding.hometeamname;
            _homescore = binding.homescore;
            _awayteamname = binding.awayteamname;
            _awayscore = binding.awayscore;
            _date = binding.date;
            _status = binding.status;
            _winloassstatus = binding.winlossstatus;

            binding.clMain.setOnClickListener(v -> {
                int pos = getAdapterPosition();
                if (pos != RecyclerView.NO_POSITION) {
                    HomeTeam_Matche clickedDataItem = (HomeTeam_Matche) mItems.get(pos);
                    Toast.makeText(v.getContext(), "You clicked " + clickedDataItem.getWinnercode(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(v.getContext(), ActivityCrickrtMatchInformation.class);
                    if (AdsHelper.isNetworkConnected(mContext)) {
                        MyApplication.getInstance().displayInterstitialAds(mContext, intent, false);
                    } else {
                        v.getContext().startActivity(intent);
                    }

                    SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("eventid", clickedDataItem.getId());
                    editor.apply();
                }
            });
        }

        @SuppressLint({"ResourceAsColor", "SetTextI18n", "ResourceType"})
        public void setData(HomeTeam_Matche data) {

            _hometeamname.setText(data.getHometeam());
            _homescore.setText(data.getHomescore() + "-" + data.getHomewicket() + "(" + data.getHomeover() + ")");
            _awayteamname.setText(data.getAwayteam());
            _awayscore.setText(data.getAwayscore() + "-" + data.getAwaywicket() + "(" + data.getAwayover() + ")");
            _date.setText(data.getDate());

            _hometeamname.setTextColor(Color.parseColor("#000000"));
            _awayteamname.setTextColor(Color.parseColor("#000000"));
            _homescore.setTextColor(Color.parseColor("#000000"));
            _awayscore.setTextColor(Color.parseColor("#000000"));
            _status.setTextColor(Color.parseColor("#939393"));
            _date.setTextColor(Color.parseColor("#939393"));

            if (data.getHomescore() != null) {
                int a = Integer.parseInt(data.getHomescore());
                int b = Integer.parseInt(data.getAwayscore());

                if (data.getHomescore() != null) {
                    if (data.getWinnercode() == 2) {
                        _hometeamname.setTextColor(Color.parseColor("#939393"));
                        _homescore.setTextColor(Color.parseColor("#939393"));
                        _winloassstatus.setText("L");
                        _winloassstatus.setBackgroundResource(R.drawable.ic_loss_bg);
                    } else if (data.getWinnercode() == 3) {
                        _hometeamname.setTextColor(Color.parseColor("#000000"));
                        _awayteamname.setTextColor(Color.parseColor("#000000"));
                        _homescore.setTextColor(Color.parseColor("#000000"));
                        _awayscore.setTextColor(Color.parseColor("#000000"));
                        _winloassstatus.setText("D");
                        _winloassstatus.setBackgroundResource(R.drawable.ic_draw_bg);
                    } else {
                        _awayteamname.setTextColor(Color.parseColor("#939393"));
                        _awayscore.setTextColor(Color.parseColor("#939393"));
                        _winloassstatus.setText("W");
                        _winloassstatus.setBackgroundResource(R.drawable.ic_win_bg);
                    }
                }
            }

            if (data.getStatus() == null) {
                _status.setText(data.getTime());
            } else {
                _status.setText(data.getStatus());
            }
            if (data.getAwayscore() == null) {
                _awayscore.setVisibility(View.GONE);
            } else {
                _awayscore.setVisibility(View.VISIBLE);
            }

            if (data.getHomescore() == null) {
                _homescore.setVisibility(View.GONE);
            } else {
                _homescore.setVisibility(View.VISIBLE);
            }
        }
    }
}
