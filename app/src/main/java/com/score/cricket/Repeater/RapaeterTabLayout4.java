package com.score.cricket.Repeater;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class RapaeterTabLayout4 extends FragmentPagerAdapter {
    Context mContext;
    private ArrayList<Fragment> mData;

    public RapaeterTabLayout4(Context c, FragmentManager fm, ArrayList<Fragment> data) {
        super(fm);
        mContext = c;
        this.mData = data;
    }

    @Override
    public Fragment getItem(int position) {
        return mData.get(position);
    }


    @Override
    public CharSequence getPageTitle(int position) {
        String mTitle = "";
        switch (position) {
            case 0:
                mTitle = "DETAILS";
                break;
            case 1:
                mTitle = "MATCHES";
                break;
            case 2:
                mTitle = "STANDINGS";
                break;
            case 3:
                mTitle = "SQUAD";
                break;

            default:
                return null;
        }
        return mTitle;
    }



    @Override
    public int getCount() {
        return mData.size();
    }
}