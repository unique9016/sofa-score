package com.score.cricket.Repeater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.score.cricket.Listener.ListnerOnClick;
import com.score.cricket.Models.HomeMain.HomeYearData;
import com.score.cricket.R;

import java.util.ArrayList;

public class RapaeterSpinYear2 extends ArrayAdapter<HomeYearData> {
    Context mContext;
    ArrayList<String> mCategories;
    private ListnerOnClick mClickListener;

    public RapaeterSpinYear2(Context mContext,
                             ArrayList<HomeYearData> algorithmList) {
        super(mContext, 0, algorithmList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    public void setClickListener(ListnerOnClick itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    private View initView(int position, View convertView,
                          ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_spinner_items_2, parent, false);
        }

        TextView textViewName = convertView.findViewById(R.id.teaamname);
        HomeYearData currentItem = getItem(position);

        if (currentItem != null) {
            textViewName.setText(currentItem.getYear());
        }
        return convertView;
    }
}
