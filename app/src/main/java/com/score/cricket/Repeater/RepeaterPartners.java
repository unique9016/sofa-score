package com.score.cricket.Repeater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.Models.HomeMain.HomeMatch_inning_partnership;
import com.score.cricket.databinding.LayoutPartnershipRvItemsBinding;

import java.util.ArrayList;
import java.util.List;

public class RepeaterPartners extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private ArrayList<HomeMatch_inning_partnership> mDataList;

    public RepeaterPartners(Context mContext, ArrayList<HomeMatch_inning_partnership> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        LayoutPartnershipRvItemsBinding binding = LayoutPartnershipRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PostHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        List<HomeMatch_inning_partnership> list = this.mDataList;
        if (list == null || list.isEmpty()) {
            return 0;
        }
        return this.mDataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;
            HomeMatch_inning_partnership data = (HomeMatch_inning_partnership) mDataList.get(position);
            viewHolder.setData(data);
        }
    }


    @Override
    public int getItemCount() {
        if (mDataList == null) {
            return 0;
        }
        Log.d("MYTAG", "getitemcount" + mDataList.size());
        return mDataList.size();
    }

    private class PostHolder extends RecyclerView.ViewHolder {
        TextView _index, _player, _score, _ball;

        PostHolder(LayoutPartnershipRvItemsBinding binding) {
            super(binding.getRoot());
            _index = binding.index;
            _player = binding.player;
            _score = binding.score;
            _ball = binding.ball;
        }

        @SuppressLint({"ResourceAsColor", "SetTextI18n", "UseValueOf"})
        public void setData(HomeMatch_inning_partnership data) {
            _index.setText(Integer.toString(data.getIndex()));
            _player.setText(data.getPlayer1() + " / " + data.getPlayer2());
            _score.setText(Integer.toString(data.getScore()));
            _ball.setText(Integer.toString(data.getBall()));
        }
    }
}
