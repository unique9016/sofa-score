package com.score.cricket.Repeater;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Helper.WorldCountryIdList;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.MainScreen.ActivityCrickrtMatchInformation;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.Models.HomeMain.HomeMatcheFavoriteInfo;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.databinding.LayoutHeaderLayoutBinding;
import com.score.cricket.databinding.LayoutMatcheItemsBinding;

import java.util.ArrayList;

public class RepeaterFavorite extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<ListnerItems> mItems;
    private Activity mContext;

    public RepeaterFavorite(ArrayList<ListnerItems> items, Activity mContext) {
        this.mItems = items;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ListnerItems.TYPE_HEADER) {
            LayoutHeaderLayoutBinding binding = LayoutHeaderLayoutBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new VHHeader(binding);
        } else if (viewType == ListnerItems.TYPE_ITEM) {
            LayoutMatcheItemsBinding binding = LayoutMatcheItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new VHItem(binding);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            HomeHeader header = (HomeHeader) mItems.get(position);
            VHHeader VHheader = (VHHeader) holder;

            try {
                WorldCountryIdList country = new WorldCountryIdList();
                int abs = country.Createarray().get(header.getFlage());
                Log.d("FLAGE", "loadlogo: " + abs);
                String flaage = ActivityLaunch.LogoLink_2 + abs + "/logo";
                Glide.with(mContext)
                        .load(flaage)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            } catch (Exception e) {
            }
            if (header.getFlage().equals("asia")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_asia)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }
            if (header.getFlage().equals("international")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_world)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }
            if (header.getFlage().equals("europe")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_europe)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }
            if (header.getFlage().equals("africa")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_africa)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }
            if (header.getFlage().equals("oceania")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_oceania)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }

            if (header.getFlage().equals("north-and-central-america")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_south_america)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }
            if (header.getFlage().equals("south-america")) {
                Glide.with(mContext)
                        .load(R.drawable.ic_south_america)
                        .centerCrop()
                        .placeholder(R.drawable.ic_baseline_error_24)
                        .into(VHheader.imageView);
            }
            VHheader.tvName.setText(header.getName());
        } else if (holder instanceof VHItem) {
            HomeMatcheFavoriteInfo person = (HomeMatcheFavoriteInfo) mItems.get(position);
            VHItem VHitem = (VHItem) holder;
            VHitem.setData(person);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getItemType();
    }

    static class VHHeader extends RecyclerView.ViewHolder {
        TextView tvName;
        ImageView imageView;

        public VHHeader(LayoutHeaderLayoutBinding binding) {
            super(binding.getRoot());
            this.tvName = binding.tvName;
            this.imageView = binding.leaguelogo;
        }
    }

    class VHItem extends RecyclerView.ViewHolder {
        TextView hometeamname, homescore, awayteamname, awayscore, date, status;

        public VHItem(LayoutMatcheItemsBinding binding) {
            super(binding.getRoot());
            hometeamname = binding.hometeamname;
            homescore = binding.homescore;
            awayteamname = binding.awayteamname;
            awayscore = binding.awayscore;
            date = binding.date;
            status = binding.status;

            binding.llMain.setOnClickListener(v -> {
                int pos = getAdapterPosition();

                if (pos != RecyclerView.NO_POSITION) {
                    HomeMatcheFavoriteInfo clickedDataItem = (HomeMatcheFavoriteInfo) mItems.get(pos);
                    Intent intent = new Intent(mContext, ActivityCrickrtMatchInformation.class);
                    if (AdsHelper.isNetworkConnected(mContext)) {
                        MyApplication.getInstance().displayInterstitialAds(mContext, intent, false);
                    } else {
                        mContext.startActivity(intent);
                    }

                    SharedPreferences sharedPreferences = mContext.getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("eventid", clickedDataItem.getId());
                    editor.apply();
                }
            });
        }

        @SuppressLint({"ResourceAsColor", "SetTextI18n", "ResourceType"})
        public void setData(HomeMatcheFavoriteInfo detailInfo) {
            {
                hometeamname.setText(detailInfo.getHometeam());
                if (detailInfo.getHomewicket().equals("null")) {
                    homescore.setText(detailInfo.getHomescore());
                } else if (detailInfo.getHomeover().equals("null")) {
                    homescore.setText(detailInfo.getHomescore());
                } else {
                    homescore.setText(detailInfo.getHomescore() + "-" + detailInfo.getHomewicket() + "(" + detailInfo.getHomeover() + ")");
                }

                if (detailInfo.getAwaywicket().equals("null")) {
                    awayscore.setText(detailInfo.getAwayscore());
                } else if (detailInfo.getAwaywicket().equals("null")) {
                    awayscore.setText(detailInfo.getAwayscore());
                } else {
                    awayscore.setText(detailInfo.getAwayscore() + "-" + detailInfo.getAwaywicket() + "(" + detailInfo.getAwayover() + ")");
                }
                awayteamname.setText(detailInfo.getAwayteam());
                date.setText(detailInfo.getDate());

                hometeamname.setTextColor(Color.parseColor("#000000"));
                awayteamname.setTextColor(Color.parseColor("#000000"));
                homescore.setTextColor(Color.parseColor("#000000"));
                awayscore.setTextColor(Color.parseColor("#000000"));
                status.setTextColor(Color.parseColor("#939393"));
                date.setTextColor(Color.parseColor("#939393"));

                if (!detailInfo.getHomescore().equals("null")) {
                    if (detailInfo.getWinnercode() == 2) {
                        hometeamname.setTextColor(Color.parseColor("#939393"));
                        homescore.setTextColor(Color.parseColor("#939393"));

                    } else if (detailInfo.getWinnercode() == 1) {
                        awayteamname.setTextColor(Color.parseColor("#939393"));
                        awayscore.setTextColor(Color.parseColor("#939393"));

                    } else {
                    }
                }

                if (TextUtils.isEmpty(detailInfo.getStatus()) || detailInfo.getStatus().equals("null")) {
                    status.setText(detailInfo.getTime());
                } else {
                    status.setText(detailInfo.getStatus());
                }

                if (detailInfo.getAwayscore().equals("null")) {
                    awayscore.setVisibility(View.GONE);
                } else {
                    awayscore.setVisibility(View.VISIBLE);
                }

                if (detailInfo.getHomescore().equals("null")) {
                    homescore.setVisibility(View.GONE);
                } else {
                    homescore.setVisibility(View.VISIBLE);
                }
            }
            if (detailInfo.getMatchstatus().equals("inprogress")) {
                status.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            } else {
                Log.d("LIVEDATA", "getChildView: not red");
            }

            Log.d("LIVEDATA", "getChildView match score : not red" + detailInfo.getHometeam());
            Log.d("LIVEDATA", "getChildView match score : not red" + detailInfo.getAwayteam());
            Log.d("LIVEDATA", "getChildView match score : not red" + detailInfo.getCurrentBattingTeam());

            if (detailInfo.getHometeam().equals(detailInfo.getCurrentBattingTeam())) {
                homescore.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            } else if (detailInfo.getAwayteam().equals(detailInfo.getCurrentBattingTeam())) {
                awayscore.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            } else {
                Log.d("LIVEDATA", "getChildView match score : not red");
            }
        }
    }
}