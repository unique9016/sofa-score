package com.score.cricket.Repeater;

import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.MainScreen.ActivityCricketTeamInformation;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.Models.HomeMain.HomeShortList;
import com.score.cricket.MyApplication;
import com.score.cricket.databinding.LayoutShortRvItemsBinding;
import com.score.cricket.databinding.LayoutStandingShortHeaderBinding;

import java.util.ArrayList;

public class RepeaterStading6 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mContext;
    private ArrayList<ListnerItems> mDataList;

    public RepeaterStading6(Activity mContext, ArrayList<ListnerItems> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());

        if (viewType == ListnerItems.TYPE_HEADER) {
            LayoutStandingShortHeaderBinding binding = LayoutStandingShortHeaderBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new VHHeader(binding);
        } else if (viewType == ListnerItems.TYPE_ITEM) {
            LayoutShortRvItemsBinding binding = LayoutShortRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new PostHolder(binding);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mDataList.get(position).getItemType();
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            HomeHeader header = (HomeHeader) mDataList.get(position);
            VHHeader VHheader = (VHHeader) holder;

            VHheader.tvName.setText(header.getName());
        } else if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;


            HomeShortList data = (HomeShortList) mDataList.get(position);
            viewHolder.setData(data);
        }
    }

    static class VHHeader extends RecyclerView.ViewHolder {
        TextView tvName;

        public VHHeader(LayoutStandingShortHeaderBinding binding) {
            super(binding.getRoot());
            this.tvName = binding.seasonname;
        }
    }


    private class PostHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        TextView name, live, total;
        TextView index, teamname, totalmatch, winloss, points;


        PostHolder(LayoutShortRvItemsBinding binding) {
            super(binding.getRoot());
            index = binding.index;
            teamname = binding.teamname;
            totalmatch = binding.totalmatch;
            winloss = binding.winloass;
            points = binding.points;
            image = binding.teamlogo;

            binding.llMain.setOnClickListener(v -> {
                int pos = getAdapterPosition();

                if (pos != RecyclerView.NO_POSITION) {
                    HomeShortList clickedDataItem = (HomeShortList) mDataList.get(pos);

                    SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("team_id", clickedDataItem.getId());
                    editor.putString("team_name", clickedDataItem.getName());
                    editor.apply();

                    Intent intent = new Intent(v.getContext(), ActivityCricketTeamInformation.class);
                    if (AdsHelper.isNetworkConnected(mContext)) {
                        MyApplication.getInstance().displayInterstitialAds(mContext, intent, false);
                    } else {
                        v.getContext().startActivity(intent);
                    }
                }
            });
        }

        public void setData(HomeShortList data) {
            index.setText(data.getIndex());
            teamname.setText(data.getName());
            totalmatch.setText(data.getTotal());
            winloss.setText(data.getWl());
            points.setText(data.getPoint());
            String url = ActivityLaunch.LogoLink_2 + data.getId() + "/logo";
            Glide.with(itemView.getContext())
                    .load(url)
                    .centerCrop()
                    .into(image);

        }
    }
}
