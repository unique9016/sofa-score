package com.score.cricket.Repeater;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.MainScreen.ActivityHomeMain;
import com.score.cricket.Models.NavigationDrawerModel;
import com.score.cricket.databinding.LayoutNavDrawerBinding;

import java.util.ArrayList;

public class RepeaterDrawer extends RecyclerView.Adapter<RepeaterDrawer.MyHolder> {

    private final Activity mActivity;
    private final ArrayList<NavigationDrawerModel> mModelArrayList;

    public RepeaterDrawer(Activity activity, ArrayList<NavigationDrawerModel> modelArrayList) {
        this.mActivity = activity;
        this.mModelArrayList = modelArrayList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutNavDrawerBinding binding = LayoutNavDrawerBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MyHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        holder.tvTitle.setText(mModelArrayList.get(position).getItem_name());
        holder.ivThumb.setImageResource(mModelArrayList.get(position).getItem_icon());
    }

    @Override
    public int getItemCount() {
        return mModelArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        private final ImageView ivThumb;

        public MyHolder(@NonNull LayoutNavDrawerBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            ivThumb = binding.ivThumb;

            binding.rlMain.setOnClickListener(v -> ((ActivityHomeMain) mActivity).onDrawerItemSelected(getLayoutPosition()));
        }
    }
}
