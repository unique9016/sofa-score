package com.score.cricket.Repeater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.score.cricket.Listener.ListnerOnClick;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.HomeMain.HomeMatch_Inning_items;
import com.score.cricket.R;

import java.util.ArrayList;

public class RepeaterSpin extends ArrayAdapter<HomeMatch_Inning_items> {
    Context mContext;
    ArrayList<String> mCategories;
    private ListnerOnClick mClickListener;

    public RepeaterSpin(Context mContext,
                        ArrayList<HomeMatch_Inning_items> algorithmList) {
        super(mContext, 0, algorithmList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    public void setClickListener(ListnerOnClick itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    private View initView(int position, View convertView,
                          ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_inning_spinner, parent, false);
        }

        TextView textViewName = convertView.findViewById(R.id.teaamname);
        ImageView imageView = convertView.findViewById(R.id.logo);
        HomeMatch_Inning_items currentItem = getItem(position);

        if (currentItem != null) {
            textViewName.setText(currentItem.getName());
        }
        String team2 = ActivityLaunch.LogoLink_2 + currentItem.getId() + "/logo";

        Glide.with(convertView.getContext())
                .load(team2)
                .centerCrop()
                .into(imageView);
        return convertView;
    }
}
