package com.score.cricket.Repeater;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.score.cricket.SubScreen.FragMatchInformation;
import com.score.cricket.SubScreen.FragMatchInformation4;
import com.score.cricket.SubScreen.FragStandingPlayer2;

public class RapaeterTabLayout2 extends FragmentPagerAdapter {
    Context mContext;
    int mTotalTabs;

    public RapaeterTabLayout2(Context c, FragmentManager fm, int totalTabs) {
        super(fm);
        mContext = c;
        this.mTotalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragMatchInformation mDetails = new FragMatchInformation();
                return mDetails;
            case 1:
                FragStandingPlayer2 mStandings = new FragStandingPlayer2();
                return mStandings;
            case 2:
                FragMatchInformation4 mMatche = new FragMatchInformation4();
                return mMatche;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mTotalTabs;
    }
}