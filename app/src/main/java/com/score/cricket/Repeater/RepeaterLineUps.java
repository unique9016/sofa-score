package com.score.cricket.Repeater;

import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.MainScreen.ActivityCricketPlayerInfomation;
import com.score.cricket.Models.HomeMain.HomePlayer_List;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.databinding.LayoutLineupRvItemsBinding;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RepeaterLineUps extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mContext;
    private final ArrayList<Object> mDataList;

    public RepeaterLineUps(Activity mContext, ArrayList<Object> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        LayoutLineupRvItemsBinding binding = LayoutLineupRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PostHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        List<Object> list = this.mDataList;
        if (list == null || list.isEmpty()) {
            return 0;
        }
        return this.mDataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;
            HomePlayer_List data = (HomePlayer_List) mDataList.get(position);
            viewHolder.setData(data);
        }
    }

    @Override
    public int getItemCount() {
        if (mDataList == null) {
            return 0;
        }
        Log.d("MYTAG", "getitemcount" + mDataList.size());
        return mDataList.size();
    }

    private class PostHolder extends RecyclerView.ViewHolder {

        public CircleImageView image;
        TextView name;

        PostHolder(LayoutLineupRvItemsBinding binding) {
            super(binding.getRoot());
            name = binding.playerName;
            image = binding.imageView2;

            binding.clMain.setOnClickListener(v -> {
                int pos = getAdapterPosition();
                if (pos != RecyclerView.NO_POSITION) {
                    HomePlayer_List clickedDataItem = (HomePlayer_List) mDataList.get(pos);
                    Intent intent = new Intent(mContext, ActivityCricketPlayerInfomation.class);
                    intent.putExtra("id", clickedDataItem.getId());
                    if (AdsHelper.isNetworkConnected(mContext)) {
                        MyApplication.getInstance().displayInterstitialAds(mContext, intent, false);
                    } else {
                        mContext.startActivity(intent);
                    }

                    SharedPreferences sharedPreferences = mContext.getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("player_id", clickedDataItem.getId());
                    editor.putString("player_name", clickedDataItem.getName());
                    editor.apply();
                }
            });
        }

        public void setData(HomePlayer_List data) {
            name.setText(data.getName());
            String team2 = "https://api.sofascore.com/mobile/v4/player/" + data.getId() + "/image";
            Glide.with(itemView.getContext())
                    .load(team2)
                    .placeholder(mContext.getResources().getDrawable(R.drawable.ic_player_default))
                    .centerCrop()
                    .into(image);
        }
    }
}
