package com.score.cricket.Repeater;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.MainScreen.ActivityCricketPlayerInfomation;
import com.score.cricket.Models.TeamDetails.TeamSquad.TeamSquadExample;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.databinding.LayoutSquadRvItemsBinding;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RapaeterSquad extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mContext;
    private ArrayList<TeamSquadExample> mDataList;

    public RapaeterSquad(Activity mContext, ArrayList<TeamSquadExample> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        LayoutSquadRvItemsBinding binding = LayoutSquadRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PostHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        List<TeamSquadExample> list = this.mDataList;
        if (list == null || list.isEmpty()) {
            return 0;
        }
        return this.mDataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;
            TeamSquadExample data = (TeamSquadExample) mDataList.get(position);
            viewHolder.setData(data);
        }
    }

    @Override
    public int getItemCount() {
        if (mDataList == null) {
            return 0;
        }
        Log.d("MYTAG", "getitemcount" + mDataList.size());
        return mDataList.size();
    }

    private class PostHolder extends RecyclerView.ViewHolder {

        public CircleImageView _image;
        TextView _name, _age;


        PostHolder(LayoutSquadRvItemsBinding binding) {
            super(binding.getRoot());
            _name = binding.playerName;
            _age = binding.age;
            _image = binding.imageView2;

            binding.clMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();

                    if (pos != RecyclerView.NO_POSITION) {
                        TeamSquadExample clickedDataItem = (TeamSquadExample) mDataList.get(pos);

                        SharedPreferences sharedPreferences = mContext.getSharedPreferences("name", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("player_id", clickedDataItem.getId());
                        editor.putString("player_name", clickedDataItem.getName());
                        editor.apply();

                        Intent intent = new Intent(mContext, ActivityCricketPlayerInfomation.class);
                        intent.putExtra("name", clickedDataItem.getName().toString());
                        if (AdsHelper.isNetworkConnected(mContext)) {
                            MyApplication.getInstance().displayInterstitialAds(mContext, intent, false);
                        } else {
                            mContext.startActivity(intent);
                        }
                    }
                }
            });
        }

        @SuppressLint("SetTextI18n")
        public void setData(TeamSquadExample data) {

            try {
                String country = data.getNationality().toString();
                String agee = data.getAge().toString() + " yrs";
                _name.setText(data.getName());
                _age.setText(country + " " + agee);

                String team2 = "https://api.sofascore.com/mobile/v4/player/" + data.getId() + "/image";

                Glide.with(itemView.getContext())
                        .load(team2)
                        .centerCrop()
                        .placeholder(mContext.getResources().getDrawable(R.drawable.ic_player_default))
                        .into(_image);
            } catch (Exception e) {
                _name.setText(data.getName());
                _age.setVisibility(View.INVISIBLE);
            }
        }
    }
}
