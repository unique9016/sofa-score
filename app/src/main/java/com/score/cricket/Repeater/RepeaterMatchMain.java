package com.score.cricket.Repeater;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.WorldCountryIdList;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.HomeMain.HomeMatcheGroupInfo;
import com.score.cricket.R;
import com.score.cricket.databinding.LayoutMatchBinding;

import java.util.ArrayList;

public class RepeaterMatchMain extends RecyclerView.Adapter<RepeaterMatchMain.MyHolder> {

    private final Activity mActivity;
    private ArrayList<HomeMatcheGroupInfo> mDeptList = new ArrayList<>();

    public RepeaterMatchMain(Activity activity, ArrayList<HomeMatcheGroupInfo> deptList) {
        this.mActivity = activity;
        this.mDeptList = deptList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutMatchBinding binding = LayoutMatchBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MyHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        HomeMatcheGroupInfo headerInfo = mDeptList.get(position);

        try {
            WorldCountryIdList country = new WorldCountryIdList();
            int abs = country.Createarray().get(headerInfo.getFlage());
            String flaage = ActivityLaunch.LogoLink_2 + abs + "/logo";
            Glide.with(mActivity)
                    .load(flaage)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(holder.ivThumb);
        } catch (Exception e) {
        }

        if (headerInfo.getFlage().equals("asia")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_asia)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(holder.ivThumb);
        }

        if (headerInfo.getFlage().equals("international")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_world)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(holder.ivThumb);
        }

        if (headerInfo.getFlage().equals("europe")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_europe)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(holder.ivThumb);
        }

        if (headerInfo.getFlage().equals("africa")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_africa)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(holder.ivThumb);
        }

        if (headerInfo.getFlage().equals("oceania")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_oceania)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(holder.ivThumb);
        }

        if (headerInfo.getFlage().equals("north-and-central-america")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_south_america)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(holder.ivThumb);
        }

        if (headerInfo.getFlage().equals("south-america")) {
            Glide.with(mActivity)
                    .load(R.drawable.ic_south_america)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(holder.ivThumb);
        }
        holder.tvTitle.setText(headerInfo.getName().trim());
        RepeaterMatchChild adapter = new RepeaterMatchChild(mActivity, mDeptList.get(position).getProductList());
        holder.recyclerView.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return mDeptList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        private final ImageView ivThumb;
        private final TextView tvTitle;
        private final RecyclerView recyclerView;

        public MyHolder(@NonNull LayoutMatchBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            ivThumb = binding.ivThumb;
            recyclerView = binding.recyclerView;
            recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        }
    }
}
