package com.score.cricket.Repeater;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.score.cricket.SubScreen.FragLeaguesHome;
import com.score.cricket.SubScreen.FragLiveMatcheHome;

public class RapaeterTabLayout extends FragmentPagerAdapter {
    Context mContext;

    public RapaeterTabLayout(Context c, FragmentManager fm) {
        super(fm);
        mContext = c;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragLeaguesHome mLeagues = new FragLeaguesHome();
                return mLeagues;
            case 1:
                FragLiveMatcheHome mMatches = new FragLiveMatcheHome();
                return mMatches;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}