package com.score.cricket.Repeater;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Listener.ListnerItems;
import com.score.cricket.MainScreen.ActivityCricketTeamInformation;
import com.score.cricket.Models.HomeMain.HomeFormList;
import com.score.cricket.Models.HomeMain.HomeHeader;
import com.score.cricket.MyApplication;
import com.score.cricket.databinding.LayoutFormRvItemsBinding;
import com.score.cricket.databinding.LayoutStandingFormHeaderBinding;

import java.util.ArrayList;

public class RepeaterStading5 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity mContext;
    private ArrayList<ListnerItems> mDataList;

    public RepeaterStading5(Activity mContext, ArrayList<ListnerItems> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        if (viewType == ListnerItems.TYPE_HEADER) {
            LayoutStandingFormHeaderBinding binding = LayoutStandingFormHeaderBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new VHHeader(binding);
        } else if (viewType == ListnerItems.TYPE_ITEM) {
            LayoutFormRvItemsBinding binding = LayoutFormRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new PostHolder(binding);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mDataList.get(position).getItemType();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof RepeaterStading4.VHHeader) {
            HomeHeader header = (HomeHeader) mDataList.get(position);
            VHHeader VHheader = (VHHeader) holder;

            VHheader.tvName.setText(header.getName());
        } else if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;


            HomeFormList data = (HomeFormList) mDataList.get(position);
            viewHolder.setData(data);
        }


    }

    static class VHHeader extends RecyclerView.ViewHolder {
        TextView tvName;

        public VHHeader(LayoutStandingFormHeaderBinding binding) {
            super(binding.getRoot());
            this.tvName = binding.seasonname;
        }
    }


    private class PostHolder extends RecyclerView.ViewHolder {

        TextView _index, _name, _awayform1, _awayform2, _awayform3, _awayform4, _awayform5;

        PostHolder(LayoutFormRvItemsBinding binding) {
            super(binding.getRoot());
            _index = binding.index;
            _name = binding.teamname;
            _awayform1 = binding.awayform1;
            _awayform2 = binding.awayform2;
            _awayform3 = binding.awayform3;
            _awayform4 = binding.awayform4;
            _awayform5 = binding.awayform5;

            binding.llMain.setOnClickListener(v -> {
                int pos = getAdapterPosition();

                if (pos != RecyclerView.NO_POSITION) {
                    HomeFormList clickedDataItem = (HomeFormList) mDataList.get(pos);

                    SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("team_id", clickedDataItem.getId());
                    editor.putString("team_name", clickedDataItem.getName());
                    editor.apply();

                    Intent intent = new Intent(v.getContext(), ActivityCricketTeamInformation.class);
                    if (AdsHelper.isNetworkConnected(mContext)) {
                        MyApplication.getInstance().displayInterstitialAds(mContext, intent, false);
                    } else {
                        v.getContext().startActivity(intent);
                    }
                }
            });
        }

        @SuppressLint("ResourceAsColor")
        public void setData(HomeFormList data) {
            _index.setText(data.getIndex());
            _name.setText(data.getName());

            if (data.getS1() != null) {
                _awayform1.setText(data.getS1());

                if (data.getS1().toString().equals("W")) {
                    _awayform1.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS1().toString().equals("D")) {
                    _awayform1.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    _awayform1.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                _awayform1.setVisibility(View.INVISIBLE);
            }

            if (data.getS2() != null) {
                _awayform2.setText(data.getS2());
                if (data.getS2().toString().equals("W")) {
                    _awayform2.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS2().toString().equals("D")) {
                    _awayform2.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    _awayform2.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                _awayform2.setVisibility(View.INVISIBLE);
            }
            if (data.getS3() != null) {
                _awayform3.setText(data.getS3());
                if (data.getS3().toString().equals("W")) {

                    _awayform3.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS3().toString().equals("D")) {
                    _awayform3.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    _awayform3.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                _awayform3.setVisibility(View.INVISIBLE);
            }

            if (data.getS4() != null) {
                _awayform4.setText(data.getS4());

                if (data.getS4().toString().equals("W")) {
                    _awayform4.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS4().toString().equals("D")) {
                    _awayform4.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    _awayform4.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                _awayform4.setVisibility(View.INVISIBLE);
            }

            if (data.getS5() != null) {
                _awayform5.setText(data.getS5());
                if (data.getS5().toString().equals("W")) {
                    _awayform5.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS5().toString().equals("D")) {
                    _awayform5.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    _awayform5.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                _awayform5.setVisibility(View.INVISIBLE);
            }

        }
    }
}
