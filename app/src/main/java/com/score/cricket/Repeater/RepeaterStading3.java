package com.score.cricket.Repeater;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.MainScreen.ActivityCricketTeamInformation;
import com.score.cricket.Models.HomeMain.HomeFormList;
import com.score.cricket.MyApplication;
import com.score.cricket.databinding.LayoutFormRvItemsBinding;

import java.util.ArrayList;
import java.util.List;

public class RepeaterStading3 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Activity mContext;
    private ArrayList<Object> mDataList;


    public RepeaterStading3(Activity mContext, ArrayList<Object> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        LayoutFormRvItemsBinding binding = LayoutFormRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PostHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        List<Object> list = this.mDataList;
        if (list == null || list.isEmpty()) {
            return 0;
        }
        return this.mDataList.size();

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;

            HomeFormList data = (HomeFormList) mDataList.get(position);
            viewHolder.setData(data);
        }
    }

    @Override
    public int getItemCount() {
        if (mDataList == null) {
            return 0;
        }
        Log.d("MYTAG", "getitemcount" + mDataList.size());
        return mDataList.size();
    }

    private class PostHolder extends RecyclerView.ViewHolder {

        TextView index, name, awayform1, awayform2, awayform3, awayform4, awayform5;


        PostHolder(LayoutFormRvItemsBinding binding) {
            super(binding.getRoot());
            index = binding.index;
            name = binding.teamname;
            awayform1 = binding.awayform1;
            awayform2 = binding.awayform2;
            awayform3 = binding.awayform3;
            awayform4 = binding.awayform4;
            awayform5 = binding.awayform5;

            binding.llMain.setOnClickListener(v -> {
                int pos = getAdapterPosition();

                if (pos != RecyclerView.NO_POSITION) {
                    HomeFormList clickedDataItem = (HomeFormList) mDataList.get(pos);

                    SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("name", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("team_id", clickedDataItem.getId());
                    editor.putString("team_name", clickedDataItem.getName());
                    editor.apply();

                    Intent intent = new Intent(v.getContext(), ActivityCricketTeamInformation.class);
                    if (AdsHelper.isNetworkConnected(mContext)) {
                        MyApplication.getInstance().displayInterstitialAds(mContext, intent, false);
                    } else {
                        v.getContext().startActivity(intent);
                    }
                }
            });
        }

        @SuppressLint("ResourceAsColor")
        public void setData(HomeFormList data) {
            index.setText(data.getIndex());
            name.setText(data.getName());

            if (data.getS1() != null) {
                awayform1.setText(data.getS1());

                if (data.getS1().toString().equals("W")) {
                    awayform1.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS1().toString().equals("D")) {
                    awayform1.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    awayform1.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                awayform1.setVisibility(View.INVISIBLE);
            }

            if (data.getS2() != null) {
                awayform2.setText(data.getS2());
                if (data.getS2().toString().equals("W")) {
                    awayform2.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS2().toString().equals("D")) {
                    awayform2.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    awayform2.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                awayform2.setVisibility(View.INVISIBLE);
            }

            if (data.getS3() != null) {
                awayform3.setText(data.getS3());
                if (data.getS3().toString().equals("W")) {

                    awayform3.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS3().toString().equals("D")) {
                    awayform3.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    awayform3.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                awayform3.setVisibility(View.INVISIBLE);
            }

            if (data.getS4() != null) {
                awayform4.setText(data.getS4());

                if (data.getS4().toString().equals("W")) {
                    awayform4.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS4().toString().equals("D")) {
                    awayform4.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    awayform4.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                awayform4.setVisibility(View.INVISIBLE);
            }

            if (data.getS5() != null) {
                awayform5.setText(data.getS5());
                if (data.getS5().toString().equals("W")) {
                    awayform5.setBackgroundColor(Color.parseColor("#01BF04"));
                } else if (data.getS5().toString().equals("D")) {
                    awayform5.setBackgroundColor(Color.parseColor("#838383"));
                } else {
                    awayform5.setBackgroundColor(Color.parseColor("#C80202"));
                }
            } else {
                awayform5.setVisibility(View.INVISIBLE);
            }

        }
    }
}
