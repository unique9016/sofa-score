package com.score.cricket.Repeater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.score.cricket.Models.HomeMain.HomePlayer_Staticstics_Spinner;
import com.score.cricket.Listener.ListnerOnClick;
import com.score.cricket.R;

import java.util.ArrayList;

public class RepeaterStaticSpin extends ArrayAdapter<HomePlayer_Staticstics_Spinner> {
    private ListnerOnClick mClickListener;
    Context mContext;
    ArrayList<String> mCategories;

    public RepeaterStaticSpin(Context mContext,
                              ArrayList<HomePlayer_Staticstics_Spinner> algorithmList)
    {
        super(mContext, 0, algorithmList);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable
            View convertView, @NonNull ViewGroup parent)
    {
        return initView(position, convertView, parent);
    }

    public void setClickListener(ListnerOnClick itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    private View initView(int position, View convertView,
                          ViewGroup parent)
    {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_inning_spinner, parent, false);
        }

        TextView textViewName = convertView.findViewById(R.id.teaamname);
        ImageView imageView = convertView.findViewById(R.id.logo);
        HomePlayer_Staticstics_Spinner currentItem = getItem(position);

        if (currentItem != null) {
            textViewName.setText(currentItem.getName());
            imageView.setVisibility(View.GONE);
        }
        return convertView;
    }
}
