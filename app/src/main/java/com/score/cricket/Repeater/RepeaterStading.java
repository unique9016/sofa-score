package com.score.cricket.Repeater;

import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.MainScreen.ActivityCricketTeamInformation;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.HomeMain.HomeShortList;
import com.score.cricket.MyApplication;
import com.score.cricket.databinding.LayoutShortRvItemsBinding;

import java.util.ArrayList;
import java.util.List;

public class RepeaterStading extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mContext;
    private final ArrayList<Object> mDataList;

    public RepeaterStading(Activity mContext, ArrayList<Object> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        LayoutShortRvItemsBinding binding = LayoutShortRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PostHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        List<Object> list = this.mDataList;
        if (list == null || list.isEmpty()) {
            return 0;
        }
        return this.mDataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;
            HomeShortList data = (HomeShortList) mDataList.get(position);
            viewHolder.setData(data);
        }
    }


    @Override
    public int getItemCount() {
        if (mDataList == null) {
            return 0;
        }
        Log.d("MYTAG", "getitemcount" + mDataList.size());
        return mDataList.size();
    }

    private class PostHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        TextView name, live, total;
        TextView index, teamname, totalmatch, winloss, points;

        PostHolder(LayoutShortRvItemsBinding binding) {
            super(binding.getRoot());
            index = binding.index;
            teamname = binding.teamname;
            totalmatch = binding.totalmatch;
            winloss = binding.winloass;
            points = binding.points;
            image = binding.teamlogo;

            binding.llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();

                    if (pos != RecyclerView.NO_POSITION) {
                        HomeShortList clickedDataItem = (HomeShortList) mDataList.get(pos);

                        SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("name", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("team_id", clickedDataItem.getId());
                        editor.putString("team_name", clickedDataItem.getName());
                        editor.apply();

                        Intent intent = new Intent(v.getContext(), ActivityCricketTeamInformation.class);
                        if (AdsHelper.isNetworkConnected(mContext)) {
                            MyApplication.getInstance().displayInterstitialAds(mContext, intent, false);
                        } else {
                            v.getContext().startActivity(intent);
                        }
                    }
                }
            });
        }

        public void setData(HomeShortList data) {
            index.setText(data.getIndex());
            teamname.setText(data.getName());
            totalmatch.setText(data.getTotal());
            winloss.setText(data.getWl());
            points.setText(data.getPoint());
            String url = ActivityLaunch.LogoLink_2 + data.getId() + "/logo";

            Glide.with(itemView.getContext())
                    .load(url)
                    .centerCrop()
                    .into(image);
        }
    }
}
