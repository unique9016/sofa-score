package com.score.cricket.Repeater;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Helper.FavoriteDBHelp;
import com.score.cricket.Helper.Logger;
import com.score.cricket.MainScreen.ActivityCrickrtMatchInformation;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.Models.HomeMain.HomeMatcheChildInfo;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.SubScreen.FragMatchFavorite;
import com.score.cricket.databinding.LayoutMatchesBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class RepeaterMatchChild extends RecyclerView.Adapter<RepeaterMatchChild.MyHolder> {

    private final Activity activity;
    ArrayList<HomeMatcheChildInfo> arrayList;
    FavoriteDBHelp myDb;
    long timestamp;

    public RepeaterMatchChild(Activity activity, ArrayList<HomeMatcheChildInfo> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
        myDb = new FavoriteDBHelp(activity);
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutMatchesBinding binding = LayoutMatchesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MyHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        HomeMatcheChildInfo detailInfo = arrayList.get(position);
        String Compare = detailInfo.getName();

        Cursor res = myDb.getAllData1(String.valueOf(detailInfo.getId()));
        holder.checkBox.setChecked(res.getCount() > 0);

        Date date2 = new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd-yyyy");
        sdf1.setTimeZone(TimeZone.getDefault());

        String currenttime = sdf1.format(date2);
        try {
            Date d1 = sdf1.parse(currenttime);
            long output = d1.getTime() / 1000L;
            String str = Long.toString(output);
            timestamp = Long.parseLong(str);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.checkBox.setOnClickListener(v -> {
            if (((CheckBox) v).isChecked()) {
                if (detailInfo.getTimestamp() >= timestamp) {
                    Toast.makeText(activity, "This Match is inserted in Favorite", Toast.LENGTH_LONG).show();
                    myDb.insertData(String.valueOf(detailInfo.getMain_id()), String.valueOf(detailInfo.getPosition()), String.valueOf(detailInfo.getId()), detailInfo.getTimestamp());
                    new FragMatchFavorite();
                } else {
                    Toast.makeText(activity, "You will not be able to add past matches to your Favorite", Toast.LENGTH_LONG).show();
                    holder.checkBox.setChecked(false);
                }
            } else {
                myDb.deleteData(String.valueOf(detailInfo.getMain_id()));

            }
        });

        holder.cnt_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeMatcheChildInfo detailInfo = arrayList.get(position);

                SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("name", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("eventid", detailInfo.getId());
                editor.putInt("seasonid", detailInfo.getSeasonid());
                editor.putInt("id", detailInfo.getTournamentid());
                editor.apply();

                Intent intent = new Intent(v.getContext(), ActivityCrickrtMatchInformation.class);
                if (AdsHelper.isNetworkConnected(activity)) {
                    MyApplication.getInstance().displayInterstitialAds(activity, intent, false);
                } else {
                    activity.startActivity(intent);
                }
            }
        });

        if (!Compare.equals("null")) {
            holder.header.setVisibility(View.VISIBLE);
            String url = ActivityLaunch.LogoLink + detailInfo.getTournamentid() + "/logo";
            Glide.with(activity)
                    .load(url)
                    .centerCrop()
                    .placeholder(R.drawable.ic_baseline_error_24)
                    .into(holder.imageView);

            holder.headertext.setText(detailInfo.getName());
            holder.hometeamname.setText(detailInfo.getHometeam());
            if (detailInfo.getHomewicket().equals("null")) {
                holder.homescore.setText(detailInfo.getHomescore());
            } else if (detailInfo.getHomeover().equals("null")) {
                holder.homescore.setText(detailInfo.getHomescore());
            } else {
                holder.homescore.setText(detailInfo.getHomescore() + "-" + detailInfo.getHomewicket() + "(" + detailInfo.getHomeover() + ")");
            }

            if (detailInfo.getAwaywicket().equals("null")) {
                holder.awayscore.setText(detailInfo.getAwayscore());
            } else if (detailInfo.getAwaywicket().equals("null")) {
                holder.awayscore.setText(detailInfo.getAwayscore());
            } else {
                holder.awayscore.setText(detailInfo.getAwayscore() + "-" + detailInfo.getAwaywicket() + "(" + detailInfo.getAwayover() + ")");
            }

            holder.awayteamname.setText(detailInfo.getAwayteam());
            holder.date.setText(detailInfo.getDate());
            holder.hometeamname.setTextColor(Color.parseColor("#000000"));
            holder.awayteamname.setTextColor(Color.parseColor("#000000"));
            holder.homescore.setTextColor(Color.parseColor("#000000"));
            holder.awayscore.setTextColor(Color.parseColor("#000000"));
            holder.status.setTextColor(Color.parseColor("#939393"));
            holder.date.setTextColor(Color.parseColor("#939393"));

            if (!detailInfo.getHomescore().equals("null")) {
                if (detailInfo.getWinnercode() == 2) {
                    holder.hometeamname.setTextColor(Color.parseColor("#939393"));
                    holder.homescore.setTextColor(Color.parseColor("#939393"));
                } else if (detailInfo.getWinnercode() == 1) {
                    holder.awayteamname.setTextColor(Color.parseColor("#939393"));
                    holder.awayscore.setTextColor(Color.parseColor("#939393"));
                }
            }


            if (TextUtils.isEmpty(detailInfo.getStatus()) || detailInfo.getStatus().equals("null")) {
                if (detailInfo.getMatchstatus().equals("notstarted")) {
                    holder.status.setText(detailInfo.getTime());
                } else {
                    holder.status.setText(detailInfo.getMatchstatus());
                }
            } else {
                holder.status.setText(detailInfo.getStatus());
            }

            if (detailInfo.getAwayscore().equals("null")) {
                holder.awayscore.setVisibility(View.GONE);
            } else {
                holder.awayscore.setVisibility(View.VISIBLE);
            }

            if (detailInfo.getHomescore().equals("null")) {
                holder.homescore.setVisibility(View.GONE);
            } else {
                holder.homescore.setVisibility(View.VISIBLE);
            }

            if (detailInfo.getMatchstatus().equals("inprogress")) {
                holder.status.setTextColor(ContextCompat.getColor(activity, R.color.red));
            } else {
                Logger.AppLog("LIVEDATA", "getChildView: not red");
            }

            if (detailInfo.getHometeam().equals(detailInfo.getCurrentBattingTeam())) {
                holder.homescore.setTextColor(ContextCompat.getColor(activity, R.color.red));
            } else if (detailInfo.getAwayteam().equals(detailInfo.getCurrentBattingTeam())) {
                holder.awayscore.setTextColor(ContextCompat.getColor(activity, R.color.red));
            }
        } else {
            holder.header.setVisibility(View.GONE);
            Logger.AppLog("COUNTRY", "adapter ..elese  " + detailInfo.getName());

            holder.hometeamname.setText(detailInfo.getHometeam());
            if (detailInfo.getHomewicket().equals("null")) {
                holder.homescore.setText(detailInfo.getHomescore());
            } else if (detailInfo.getHomeover().equals("null")) {
                holder.homescore.setText(detailInfo.getHomescore());
            } else {
                holder.homescore.setText(detailInfo.getHomescore() + "-" + detailInfo.getHomewicket() + "(" + detailInfo.getHomeover() + ")");
            }
            if (detailInfo.getAwaywicket().equals("null")) {
                holder.awayscore.setText(detailInfo.getAwayscore());
            } else if (detailInfo.getAwaywicket().equals("null")) {
                holder.awayscore.setText(detailInfo.getAwayscore());
            } else {
                holder.awayscore.setText(detailInfo.getAwayscore() + "-" + detailInfo.getAwaywicket() + "(" + detailInfo.getAwayover() + ")");
            }

            holder.awayteamname.setText(detailInfo.getAwayteam());
            holder.date.setText(detailInfo.getDate());
            holder.hometeamname.setTextColor(Color.parseColor("#000000"));
            holder.awayteamname.setTextColor(Color.parseColor("#000000"));
            holder.homescore.setTextColor(Color.parseColor("#000000"));
            holder.awayscore.setTextColor(Color.parseColor("#000000"));
            holder.status.setTextColor(Color.parseColor("#939393"));
            holder.date.setTextColor(Color.parseColor("#939393"));

            if (!detailInfo.getHomescore().equals("null")) {
                if (detailInfo.getWinnercode() == 2) {
                    holder.hometeamname.setTextColor(Color.parseColor("#939393"));
                    holder.homescore.setTextColor(Color.parseColor("#939393"));

                } else if (detailInfo.getWinnercode() == 1) {
                    holder.awayteamname.setTextColor(Color.parseColor("#939393"));
                    holder.awayscore.setTextColor(Color.parseColor("#939393"));
                }
            }

            if (TextUtils.isEmpty(detailInfo.getStatus()) || detailInfo.getStatus().equals("null")) {
                if (detailInfo.getMatchstatus().equals("notstarted")) {
                    holder.status.setText(detailInfo.getTime());
                } else {
                    holder.status.setText(detailInfo.getMatchstatus());
                }
            } else {
                holder.status.setText(detailInfo.getStatus());
            }

            if (detailInfo.getAwayscore().equals("null")) {
                holder.awayscore.setVisibility(View.GONE);
            } else {
                holder.awayscore.setVisibility(View.VISIBLE);
            }
            if (detailInfo.getHomescore().equals("null")) {
                holder.homescore.setVisibility(View.GONE);
            } else {
                holder.homescore.setVisibility(View.VISIBLE);
            }
        }
        if (detailInfo.getMatchstatus().equals("inprogress")) {
            holder.status.setTextColor(ContextCompat.getColor(activity, R.color.red));
        } else {
            Logger.AppLog("LIVEDATA", "getChildView: not red");
        }

        if (detailInfo.getHometeam().equals(detailInfo.getCurrentBattingTeam())) {
            holder.homescore.setTextColor(ContextCompat.getColor(activity, R.color.red));
        } else if (detailInfo.getAwayteam().equals(detailInfo.getCurrentBattingTeam())) {
            holder.awayscore.setTextColor(ContextCompat.getColor(activity, R.color.red));
        } else {
            Logger.AppLog("LIVEDATA", "getChildView match score : not red");
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class MyHolder extends RecyclerView.ViewHolder {
        private final TextView hometeamname, homescore, awayteamname, awayscore,
                date, status, headertext;
        private final CardView header;
        private final CheckBox checkBox;
        private final ConstraintLayout cnt_layout;
        private final ImageView imageView;

        public MyHolder(@NonNull LayoutMatchesBinding binding) {
            super(binding.getRoot());
            checkBox = binding.notification;
            header = binding.header;
            cnt_layout = binding.cntLayout;
            headertext = binding.tvName;
            imageView = binding.leagelogo;
            hometeamname = binding.hometeamname;
            homescore = binding.homescore;
            awayteamname = binding.awayteamname;
            awayscore = binding.awayscore;
            date = binding.date;
            status = binding.status;

        }
    }
}
