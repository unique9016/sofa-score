package com.score.cricket.Repeater;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.Models.HomeMain.HomeChildInfo;
import com.score.cricket.Models.HomeMain.HomeGroupInfo;
import com.score.cricket.databinding.LayoutLayoutLeaguesBinding;

import java.util.ArrayList;

public class RepeaterLeaguesMain extends RecyclerView.Adapter<RepeaterLeaguesMain.MyHolder> {

    private final Activity mActivity;
    private ArrayList<HomeGroupInfo> mArrayListParent = new ArrayList<>();

    public RepeaterLeaguesMain(Activity activity, ArrayList<HomeGroupInfo> arrayListParent) {
        this.mActivity = activity;
        this.mArrayListParent = arrayListParent;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutLayoutLeaguesBinding binding = LayoutLayoutLeaguesBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MyHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        HomeGroupInfo headerInfo = mArrayListParent.get(position);
        holder.tvTitle.setText(headerInfo.getName().trim());

        ArrayList<HomeChildInfo> childInfos = headerInfo.getProductList();

        RepeaterLeaguesChild adapter = new RepeaterLeaguesChild(mActivity, childInfos, mArrayListParent.get(position));
        holder.recyclerView.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return mArrayListParent.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        private final RecyclerView recyclerView;

        public MyHolder(@NonNull LayoutLayoutLeaguesBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            recyclerView = binding.recyclerView;
            recyclerView.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        }
    }
}
