package com.score.cricket.Repeater;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class RapaeterTabLayout5 extends FragmentPagerAdapter {
    Context mContext;
    private ArrayList<Fragment> mData;

    public RapaeterTabLayout5(Context c, FragmentManager fm, ArrayList<Fragment> data) {
        super(fm);
        mContext = c;
        this.mData = data;
    }

    @Override
    public Fragment getItem(int position) {
        return mData.get(position);
    }


    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position) {
            case 0:
                title = " DETAILS ";
                break;
            case 1:
                title = "STATISTICS";
                break;
            case 2:
                title = "MATCHES";
                break;
            case 3:
                title = "SQUAD";
                break;

            default:
                return null;
        }
        return title;
    }



    @Override
    public int getCount() {
        return mData.size();
    }
}