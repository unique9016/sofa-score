package com.score.cricket.Repeater;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.Models.HomeMain.HomePlayer_Statistic_BattingList;
import com.score.cricket.databinding.LayoutPlayerStatisticsRvItemsBinding;

import java.util.ArrayList;
import java.util.List;

public class RepeaterStaticBating extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private ArrayList<Object> mDataList;

    public RepeaterStaticBating(Context mContext, ArrayList<Object> dataList) {
        this.mContext = mContext;
        this.mDataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + mDataList.size());
        LayoutPlayerStatisticsRvItemsBinding binding = LayoutPlayerStatisticsRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PostHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        List<Object> list = this.mDataList;
        if (list == null || list.isEmpty()) {
            return 0;
        }
        return this.mDataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;
            HomePlayer_Statistic_BattingList data = (HomePlayer_Statistic_BattingList) mDataList.get(position);
            viewHolder.setData(data);
        }
    }

    @Override
    public int getItemCount() {
        if (mDataList == null) {
            return 0;
        }
        Log.d("MYTAG", "getitemcount" + mDataList.size());
        return mDataList.size();
    }

    private class PostHolder extends RecyclerView.ViewHolder {

        TextView _name, _value;

        PostHolder(LayoutPlayerStatisticsRvItemsBinding binding) {
            super(binding.getRoot());
            _name = binding.name;
            _value = binding.value;
        }

        public void setData(HomePlayer_Statistic_BattingList data) {
            _name.setText(data.getName());
            _value.setText(data.getValue());
        }
    }
}
