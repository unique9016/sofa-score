package com.score.cricket.Repeater;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.MainScreen.ActivityLaunch;
import com.score.cricket.MainScreen.ActivityLeagueInfo;
import com.score.cricket.Models.HomeMain.HomeChildInfo;
import com.score.cricket.Models.HomeMain.HomeGroupInfo;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.databinding.LayoutLeaguesChildBinding;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RepeaterLeaguesChild extends RecyclerView.Adapter<RepeaterLeaguesChild.MyHolder> {

    private final Activity mActivity;
    private final ArrayList<HomeChildInfo> mArrayListChild;
    private final HomeGroupInfo mGroupInfo;

    public RepeaterLeaguesChild(Activity activity, ArrayList<HomeChildInfo> arrayListChild, HomeGroupInfo groupInfo) {
        this.mActivity = activity;
        this.mArrayListChild = arrayListChild;
        this.mGroupInfo = groupInfo;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutLeaguesChildBinding binding = LayoutLeaguesChildBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new MyHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        HomeChildInfo childInfo = mArrayListChild.get(position);


        String url = ActivityLaunch.LogoLink + childInfo.getId() + "/logo";
        Glide.with(mActivity)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_baseline_error_24)
                .into(holder.ivThumb);

        holder.tvTitle.setText(childInfo.getName().trim());

    }

    @Override
    public int getItemCount() {
        return mArrayListChild.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        private final CircleImageView ivThumb;

        public MyHolder(@NonNull LayoutLeaguesChildBinding binding) {
            super(binding.getRoot());
            tvTitle = binding.tvTitle;
            ivThumb = binding.ivThumb;

            binding.rlMain.setOnClickListener(v -> {
                HomeChildInfo detailInfo = mArrayListChild.get(getLayoutPosition());

                SharedPreferences sharedPreferences = v.getContext().getSharedPreferences("name", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("id", detailInfo.getId());
                editor.apply();

                Intent intent = new Intent(mActivity, ActivityLeagueInfo.class);
                intent.putExtra("leage_name", detailInfo.getName());
                intent.putExtra("country", mGroupInfo.getName());
                intent.putExtra("id", detailInfo.getId());

                if (AdsHelper.isNetworkConnected(mActivity)) {
                    MyApplication.getInstance().displayInterstitialAds(mActivity, intent, false);
                } else {
                    mActivity.startActivity(intent);
                }
            });
        }
    }
}
