package com.score.cricket.Repeater;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.score.cricket.Models.HomeMain.HomeMatch_inning_wicket;
import com.score.cricket.databinding.LayoutWicketRvItemsBinding;

import java.util.ArrayList;
import java.util.List;

public class RepeaterWicket extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private ArrayList<HomeMatch_inning_wicket> dataList;

    public RepeaterWicket(Context mContext, ArrayList<HomeMatch_inning_wicket> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d("MYTAG", "oncreate" + dataList.size());
        LayoutWicketRvItemsBinding binding = LayoutWicketRvItemsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new PostHolder(binding);
    }

    @Override
    public int getItemViewType(int position) {
        List<HomeMatch_inning_wicket> list = this.dataList;
        if (list == null || list.isEmpty()) {
            return 0;
        }
        return this.dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof PostHolder) {
            PostHolder viewHolder = (PostHolder) holder;
            HomeMatch_inning_wicket data = (HomeMatch_inning_wicket) dataList.get(position);
            viewHolder.setData(data);
        }
    }

    @Override
    public int getItemCount() {
        if (dataList == null) {
            return 0;
        }
        Log.d("MYTAG", "getitemcount" + dataList.size());
        return dataList.size();
    }

    private class PostHolder extends RecyclerView.ViewHolder {

        TextView _index, _batsmen, _score, _over;

        PostHolder(LayoutWicketRvItemsBinding binding) {
            super(binding.getRoot());
            _index = binding.index;
            _batsmen = binding.batsmen;
            _score = binding.score;
            _over = binding.over;
        }

        @SuppressLint({"ResourceAsColor", "SetTextI18n", "UseValueOf"})
        public void setData(HomeMatch_inning_wicket data) {
            _index.setText(Integer.toString(data.getIndex()));
            _batsmen.setText(data.getBatsmen());
            _score.setText(Integer.toString(data.getScore()));
            _over.setText(Double.toString(data.getOver()));

        }
    }
}
