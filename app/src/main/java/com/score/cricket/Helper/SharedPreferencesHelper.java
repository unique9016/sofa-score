package com.score.cricket.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.score.cricket.MyApplication;

public class SharedPreferencesHelper {

    private static SharedPreferencesHelper instance;
    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor editor;

    private SharedPreferencesHelper() {
        sharedPreferences= MyApplication.getInstance().getSharedPreferences("MY_PREFERANCE", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public static SharedPreferencesHelper getInstance() {
        if (instance == null)
            instance = new SharedPreferencesHelper();
        return instance;
    }

    public static SharedPreferencesHelper getInstance(Context context) {
        if (instance == null)
            instance = new SharedPreferencesHelper();
        return instance;
    }

    public String getString(String key, String defValue) {

        return sharedPreferences.getString(key, defValue);
    }

    public SharedPreferencesHelper setString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
        return this;
    }

    public SharedPreferencesHelper setStatus(String key, boolean value) {

        editor.putBoolean(key, value);
        editor.commit();

        return this;
    }

    public int getInt(String key, int defValue) {

        return sharedPreferences.getInt(key, defValue);
    }

    public SharedPreferencesHelper setInt(String key, int value) {

        editor.putInt(key, value);
        editor.commit();

        return this;
    }

    public boolean getBoolean(String key, boolean defValue) {

        return sharedPreferences.getBoolean(key, defValue);
    }

    public SharedPreferencesHelper setBoolean(String key, boolean value) {

        editor.putBoolean(key, value);
        editor.commit();

        return this;
    }

    public SharedPreferencesHelper setLong(String key, long value) {

        editor.putLong(key, value);
        editor.commit();

        return this;
    }

    public long getLong(String key, long defValue) {

        return sharedPreferences.getLong(key, defValue);
    }

    public void clearData() {

        editor.clear();
        editor.commit();
    }

}
