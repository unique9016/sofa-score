package com.score.cricket.Helper.Sorter;

import com.score.cricket.Models.HomeMain.HomeMatch_inning_wicket;

import java.util.Comparator;

public class SortMode1 implements Comparator<HomeMatch_inning_wicket> {

    @Override
    public int compare(HomeMatch_inning_wicket inningWicket, HomeMatch_inning_wicket inning_wicket) {
        return inningWicket.getScore().compareTo(inning_wicket.getScore());
    }
}
