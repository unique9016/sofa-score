package com.score.cricket.Helper;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;

public class AdsHelper {

    public static final String AD_TYPE_ADMOB = "admob";
    public static final String AD_TYPE_ADX = "adx";
    public static final String ID = "id";
    public static final String IS_AD_ENABLE = "is_ad_enable";
    public static final String AD_TYPE = "ad_type";
    public static final String BANNER_APP = "banner_ad";
    public static final String REVIEW = "review";
    public static final String REVIEW_COUNT = "review_count";
    public static final String NATIVE_SHOW_LIST_POSITION = "native_show_list_position";
    public static final String NATIVE_SHOW_GRID_POSITION = "native_show_grid_position";
    public static final String REVIEW_CHECK = "review_check";
    public static final String IN_APP_REVIEW = "in_appreview";
    public static final String SHOW_APP_OPEN = "show_app_open";
    public static final String SHOW_APP_OPEN_SPLASH = "show_app_open_splash";
    public static final String SHOW_BANNER = "show_banner";
    public static final String SHOW_INTERSTITIAL = "show_interstitial";
    public static final String SHOW_NATIVE = "show_native";
    public static final String APP_OPEN_AD = "app_open_ad";
    public static final String INTERSTITIAL_AD = "interstitial_ad";
    public static final String NATIVE_ADD_1 = "native_ad_1";
    private static final String INTERSTITIAL_ADS_COUNT = "interstitial_ads_count";
    private static final String INTERSTITIAL_ADS_CLICK = "interstitial_ads_click";
    private static final String TRANSFER_LINK = "transfer_link";
    private static final String ADS_PER_SESSION = "ads_per_session";
    private static final String EXIT_AD_ENABLE = "exit_ad_enable";
    public static boolean isShowingFullScreenAd = false;
    public static int ads_per_session = 0;
    public static String splash_screen_wait_count = "splash_screen_wait_count";
    public static String app_open_ad_show = "app_open_ad_show";
    public static String app_open_ad_count = "app_open_ad_count";

    public static final String USER_IN_SPLASH_INTRO = "USER_IN_SPLASH_INTRO";

    public static final String FAVORITE = "favorite";

    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public static String rateInApp = "rateInApp";

    public static boolean isInAppShow() {
        return SharedPreferencesHelper.getInstance().getBoolean(rateInApp, false);
    }

    public static void setInAppShow(boolean rate) {
        SharedPreferencesHelper.getInstance().setBoolean(rateInApp, rate);
    }

    public static String rateAppDone = "rateAppDone";

    public static boolean isRateDone() {
        return SharedPreferencesHelper.getInstance().getBoolean(rateAppDone, false);
    }

    public static void setRateDone(boolean rate) {
        SharedPreferencesHelper.getInstance().setBoolean(rateAppDone, rate);
    }


    public static int getInterstitialAdsCount() {
        int mData;
        mData = SharedPreferencesHelper.getInstance().getInt(INTERSTITIAL_ADS_COUNT, 0);
        return mData;
    }

    public static void setInterstitialAdsCount(int i) {
        SharedPreferencesHelper.getInstance().setInt(INTERSTITIAL_ADS_COUNT, i);
    }

    public static int getIsAdEnable() {
        return SharedPreferencesHelper.getInstance().getInt(IS_AD_ENABLE, 0);
    }

    public static void setIsAdEnable(int key) {
        SharedPreferencesHelper.getInstance().setInt(IS_AD_ENABLE, key);
    }

    public static String getAdType() {
        return SharedPreferencesHelper.getInstance().getString(AD_TYPE, "");
    }

    public static void setAdType(String key) {
        SharedPreferencesHelper.getInstance().setString(AD_TYPE, key);
    }

    public static int getId() {
        return SharedPreferencesHelper.getInstance().getInt(ID, 0);
    }

    public static void setId(int key) {
        SharedPreferencesHelper.getInstance().setInt(ID, key);
    }

    public static void setInAppreview(int key) {
        SharedPreferencesHelper.getInstance().setInt(IN_APP_REVIEW, key);
    }
    public static int getInAppreview() {
        int type = SharedPreferencesHelper.getInstance().getInt(IN_APP_REVIEW, 0);
        return type;
    }


    public static int getInterstitialAdsClick() {
        return SharedPreferencesHelper.getInstance().getInt(INTERSTITIAL_ADS_CLICK, 0);
    }
    public static void updateReviewCount() {
        int lastCount = getReviewCount();
        SharedPreferencesHelper.getInstance().setInt(REVIEW_COUNT, lastCount + 1);
    }

    //            review
    public static int getReview() {
        int type = SharedPreferencesHelper.getInstance().getInt(REVIEW, 0);
        return type;
    }

    public static void setReview(int num) {
        SharedPreferencesHelper.getInstance().setInt(REVIEW, num);
    }

    public static void setInterstitialAdsClick(int i) {
        SharedPreferencesHelper.getInstance().setInt(INTERSTITIAL_ADS_CLICK, i);
    }

    public static int getAdsPerSession() {
        return SharedPreferencesHelper.getInstance().getInt(ADS_PER_SESSION, 0);
    }

    public static void setAdsPerSession(int i) {
        SharedPreferencesHelper.getInstance().setInt(ADS_PER_SESSION, i);
    }

    public static String getTransferLink() {
        return SharedPreferencesHelper.getInstance().getString(TRANSFER_LINK, "");
    }

    public static void setTransferLink(String i) {
        SharedPreferencesHelper.getInstance().setString(TRANSFER_LINK, i);
    }

    public static int getShowAppOpen() {
        return SharedPreferencesHelper.getInstance().getInt(SHOW_APP_OPEN, 0);
    }

    public static void setShowAppOpen(int key) {
        SharedPreferencesHelper.getInstance().setInt(SHOW_APP_OPEN, key);
    }

    public static int getOpenAdsSplash() {
        return SharedPreferencesHelper.getInstance().getInt(SHOW_APP_OPEN_SPLASH, 0);
    }

    public static void setOpenAdsSplash(int key) {
        SharedPreferencesHelper.getInstance().setInt(SHOW_APP_OPEN_SPLASH, key);
    }

    public static int getShowBanner() {
        return SharedPreferencesHelper.getInstance().getInt(SHOW_BANNER, 0);
    }

    public static void setShowBanner(int key) {
        SharedPreferencesHelper.getInstance().setInt(SHOW_BANNER, key);
    }

    public static int getShowInterstitial() {
        return SharedPreferencesHelper.getInstance().getInt(SHOW_INTERSTITIAL, 0);
    }

    public static void setShowInterstitial(int key) {
        SharedPreferencesHelper.getInstance().setInt(SHOW_INTERSTITIAL, key);
    }

    public static int getExitAdEnable() {
        return SharedPreferencesHelper.getInstance().getInt(EXIT_AD_ENABLE, 0);
    }

    public static void setExitAdEnable(int key) {
        SharedPreferencesHelper.getInstance().setInt(EXIT_AD_ENABLE, key);
    }

    public static int getShowNative() {
        return SharedPreferencesHelper.getInstance().getInt(SHOW_NATIVE, 0);
    }

    public static void setShowNative(int key) {
        SharedPreferencesHelper.getInstance().setInt(SHOW_NATIVE, key);
    }

    public static String getBannerAd() {
        return SharedPreferencesHelper.getInstance().getString(BANNER_APP, "");
    }

    public static void setBannerAd(String key) {
        SharedPreferencesHelper.getInstance().setString(BANNER_APP, key);
    }

    public static String getAppOpenAd() {
        return SharedPreferencesHelper.getInstance().getString(APP_OPEN_AD, "");
    }

    public static void setAppOpenAd(String key) {
        SharedPreferencesHelper.getInstance().setString(APP_OPEN_AD, key);
    }

    public static String getInterstitialAd() {
        return SharedPreferencesHelper.getInstance().getString(INTERSTITIAL_AD, "");
    }

    public static void setInterstitialAd(String key) {
        SharedPreferencesHelper.getInstance().setString(INTERSTITIAL_AD, key);
    }

    public static String getNativeAd() {
        return SharedPreferencesHelper.getInstance().getString(NATIVE_ADD_1, "");
    }

    public static void setNativeAd(String key) {
        SharedPreferencesHelper.getInstance().setString(NATIVE_ADD_1, key);
    }

    public static boolean getUserInSplashIntro() {
        return SharedPreferencesHelper.getInstance().getBoolean(USER_IN_SPLASH_INTRO, false);
    }

    public static void setUserInSplashIntro(boolean rate) {
        SharedPreferencesHelper.getInstance().setBoolean(USER_IN_SPLASH_INTRO, rate);
    }

    public static int getSplashScreenWaitCount() {
        int type = SharedPreferencesHelper.getInstance().getInt(splash_screen_wait_count, 10);
        return type;
    }

    public static void setSplashScreenWaitCount(int str) {
        SharedPreferencesHelper.getInstance().setInt(splash_screen_wait_count, str);
    }

    public static int getOpenAdsCount() {
        int type = SharedPreferencesHelper.getInstance().getInt(app_open_ad_count, 3);
        return type;
    }

    public static void setOpenAdsCount(int str) {
        SharedPreferencesHelper.getInstance().setInt(app_open_ad_count, str);
    }

    public static int getOpenAdsShowCount() {
        int type = SharedPreferencesHelper.getInstance().getInt(app_open_ad_show, 1);
        return type;
    }

    public static void setOpenAdsShowCount(int str) {
        SharedPreferencesHelper.getInstance().setInt(app_open_ad_show, str);
    }

    public static String review_popup_count = "review_popup_count";
    public static int getReviewPopupCount() {
        int type = SharedPreferencesHelper.getInstance().getInt(review_popup_count, 1);
        return type;
    }

    public static void setReviewPopupCount(int str) {
        SharedPreferencesHelper.getInstance().setInt(review_popup_count, str);
    }

    //            review
    public static int getReviewCount() {
        int type = SharedPreferencesHelper.getInstance().getInt(REVIEW_COUNT, 0);
        return type;
    }
    public static void setReviewCount(int num) {
        SharedPreferencesHelper.getInstance().setInt(REVIEW_COUNT, num);
    }

    public static boolean getReviewCheck() {
        boolean b =SharedPreferencesHelper.getInstance().getBoolean(REVIEW_CHECK, false);
        return b;
    }

    public static void setReviewCheck(boolean rate) {
        SharedPreferencesHelper.getInstance().setBoolean(REVIEW_CHECK, rate);
    }

    public static boolean getFavorite() {
        return SharedPreferencesHelper.getInstance().getBoolean(FAVORITE, false);
    }

    public static void setFavorite(boolean rate) {
        SharedPreferencesHelper.getInstance().setBoolean(FAVORITE, rate);
    }

    public static String direct_review_enable = "direct_review_enable";
    public static int getDirectReviewEnable() {
        int type = SharedPreferencesHelper.getInstance().getInt(direct_review_enable, 1);
        return type;
    }

    public static void setDirectReviewEnable(int str) {
        SharedPreferencesHelper.getInstance().setInt(direct_review_enable, str);
    }

    //nativeAd show List position
    public static int getNativeShowListPosition() {
        int type = SharedPreferencesHelper.getInstance().getInt(NATIVE_SHOW_LIST_POSITION, 0);
        return type;
    }
    public static void setNativeShowListPosition(int num) {
        SharedPreferencesHelper.getInstance().setInt(NATIVE_SHOW_LIST_POSITION, num);
    }

    //nativeAd show Grid position
    public static int getNativeShowGridPosition() {
        int type = SharedPreferencesHelper.getInstance().getInt(NATIVE_SHOW_GRID_POSITION, 0);
        return type;
    }
    public static void setNativeShowGridPosition(int num) {
        SharedPreferencesHelper.getInstance().setInt(NATIVE_SHOW_GRID_POSITION, num);
    }



    public static String LogoLink = "logo_link";
    public static String getLogo_Link() {
        String type = SharedPreferencesHelper.getInstance().getString(LogoLink, "");
        return type;
    }
    public static void setLogo_Link(String str) {
        SharedPreferencesHelper.getInstance().setString(LogoLink, str);
    }

    public static String LOGO_LINK_2 = "logo_link_2";
    public static String getLogo_Link_2() {
        String type = SharedPreferencesHelper.getInstance().getString(LOGO_LINK_2, "");
        return type;
    }
    public static void setLogo_Link_2(String str) {
        SharedPreferencesHelper.getInstance().setString(LOGO_LINK_2, str);
    }

    public static String LeaguesLink = "leagues_link";
    public static String getLeagues_Link() {
        String type = SharedPreferencesHelper.getInstance().getString(LeaguesLink, "");
        return type;
    }
    public static void setLeagues_Link(String str) {
        SharedPreferencesHelper.getInstance().setString(LeaguesLink, str);
    }

    public static String GetPlayer = "get_player";
    public static String getGet_Player() {
        String type = SharedPreferencesHelper.getInstance().getString(GetPlayer, "");
        return type;
    }
    public static void setGet_Player(String str) {
        SharedPreferencesHelper.getInstance().setString(GetPlayer, str);
    }

    public static String GetMatch = "get_match";
    public static String getGet_Match() {
        String type = SharedPreferencesHelper.getInstance().getString(GetMatch, "");
        return type;
    }
    public static void setGet_Match(String str) {
        SharedPreferencesHelper.getInstance().setString(GetMatch, str);
    }

    public static String LeagueName = "league_name";
    public static String getLeague_Name() {
        String type = SharedPreferencesHelper.getInstance().getString(LeagueName, "");
        return type;
    }
    public static void setGetLeague_Name(String str) {
        SharedPreferencesHelper.getInstance().setString(LeagueName, str);
    }
}
