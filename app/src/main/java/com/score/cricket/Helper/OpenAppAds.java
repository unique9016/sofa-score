package com.score.cricket.Helper;

import static androidx.lifecycle.Lifecycle.Event.ON_START;
import static com.score.cricket.MyApplication.isShowingAd;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.appopen.AppOpenAd;
import com.score.cricket.BuildConfig;
import com.score.cricket.MyApplication;
import com.score.cricket.R;

public class OpenAppAds implements LifecycleObserver, Application.ActivityLifecycleCallbacks {


    private final MyApplication myApplication;
    private AppOpenAd appOpenAd = null;
    private AppOpenAd.AppOpenAdLoadCallback loadCallback;
    private Activity currentActivity;

    public OpenAppAds(MyApplication myApplication) {
        this.myApplication = myApplication;
        this.myApplication.registerActivityLifecycleCallbacks(this);
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    @OnLifecycleEvent(ON_START)
    public void onStart() {
        showAdIfAvailable();
        Log.e(MyApplication.ADMOB_TAG, "Open AD ==> " + " onStart");
    }

    public void showAdIfAvailable() {

        if (AdsHelper.getIsAdEnable() != 1) {
            return;
        }

        if (!isShowingAd && !AdsHelper.isShowingFullScreenAd && isAdAvailable() && AdsHelper.getShowAppOpen() == 1 && !AdsHelper.getUserInSplashIntro()) {
            Log.e(MyApplication.ADMOB_TAG, "Open AD ==> " + " Will show Ad");
            FullScreenContentCallback fullScreenContentCallback =
                    new FullScreenContentCallback() {
                        @Override
                        public void onAdDismissedFullScreenContent() {
                            appOpenAd = null;
                            isShowingAd = false;
                            fetchAd();
                            Log.e(MyApplication.ADMOB_TAG, "Open AD ===> The ad was dismissed.");
                        }

                        @Override
                        public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                            Log.e(MyApplication.ADMOB_TAG, "Open AD ===> The ad failed to show.");
                            isShowingAd = true;
                        }

                        @Override
                        public void onAdShowedFullScreenContent() {
                            isShowingAd = true;
                            Log.e(MyApplication.ADMOB_TAG, "Open AD ===> The ad was shown.");
                        }
                    };

            appOpenAd.setFullScreenContentCallback(fullScreenContentCallback);
            int count  = AdsHelper.getOpenAdsShowCount();
            AdsHelper.setOpenAdsShowCount(count+1);
            appOpenAd.show(currentActivity);
            isShowingAd = true;
        } else {
            if (AdsHelper.getShowAppOpen() == 1) {
                Log.e(MyApplication.ADMOB_TAG, "Open App ID ==> " + " Enable");
                fetchAd();
            } else {
                Log.e(MyApplication.ADMOB_TAG, "Open App ID ==> " + " Disabled");
            }
        }
    }


    public void fetchAd() {
        if (appOpenAd != null) {
            return;
        }
        if (AdsHelper.getIsAdEnable() != 1) {
            return;
        }
        if (AdsHelper.getShowAppOpen() != 1) {
            return;
        }

        int showCount  = AdsHelper.getOpenAdsShowCount();
        int totalLimit  = AdsHelper.getOpenAdsCount();
        if (showCount >= totalLimit) {
            return;
        }

        loadCallback =
                new AppOpenAd.AppOpenAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull  AppOpenAd ad) {
                        appOpenAd = ad;
                        Log.e(MyApplication.ADMOB_TAG, "Open App ID Load == > onAdLoaded");
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        Log.e(MyApplication.ADMOB_TAG, "Open App ID Fail Load == > " + loadAdError.getMessage());
                    }
                };
        AdRequest request = getAdRequest();
        String adUnitId = "";
        if (BuildConfig.DEBUG ) {
            if (AdsHelper.getAdType().equalsIgnoreCase(AdsHelper.AD_TYPE_ADMOB)) {
                adUnitId = myApplication.getString(R.string.app_open_ads_id);
            } else if (AdsHelper.getAdType().equalsIgnoreCase(AdsHelper.AD_TYPE_ADX)) {
                adUnitId = myApplication.getString(R.string.adx_app_open_ads_id);
            } else {
                return;
            }
        }else {
            adUnitId = AdsHelper.getAppOpenAd();
        }
        if (TextUtils.isEmpty(adUnitId)) {
            return;
        }
        Log.e(MyApplication.ADMOB_TAG, "Open App ID ==> " + adUnitId);
        AppOpenAd.load(myApplication, adUnitId, request,
                AppOpenAd.APP_OPEN_AD_ORIENTATION_PORTRAIT, loadCallback);
    }

    private AdRequest getAdRequest() {
        return new AdRequest.Builder().build();
    }


    public boolean isAdAvailable() {
        return appOpenAd != null && checkOpenAdsCondition();
    }

    private boolean checkOpenAdsCondition() {
        if (AdsHelper.getIsAdEnable() != 1) {
            return false;
        }

        if (AdsHelper.getShowAppOpen() != 1) {
            return false;
        }

        int showCount  = AdsHelper.getOpenAdsShowCount();
        int totalLimit  = AdsHelper.getOpenAdsCount();
        if (showCount >= totalLimit) {
            return false;
        }

        if (!AdsHelper.getUserInSplashIntro() && !isShowingAd) {
            return !AdsHelper.isShowingFullScreenAd;
        } else {
            return false;
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        currentActivity = activity;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        currentActivity = activity;
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        currentActivity = null;
    }
}