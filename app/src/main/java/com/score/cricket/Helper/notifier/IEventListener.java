package com.score.cricket.Helper.notifier;

public interface IEventListener {

    public int eventNotify(int eventType, Object eventObject);
}