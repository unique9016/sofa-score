package com.score.cricket.Listener;

public interface ListnerItems {
    int TYPE_ITEM = 0;
    int TYPE_HEADER = 1;

    int getItemType();
}
