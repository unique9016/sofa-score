package com.score.cricket.MainScreen;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.score.cricket.Helper.Utility;
import com.score.cricket.Helper.notifier.EventNotifier;
import com.score.cricket.Helper.notifier.EventState;
import com.score.cricket.Helper.notifier.IEventListener;
import com.score.cricket.Helper.notifier.NotifierFactory;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.databinding.ActivityStringInformationBinding;

public class ActivityStringInfomation extends BaseActivity implements IEventListener {
    ActivityStringInformationBinding binding;
    ImageView _ivBack;
    FrameLayout _fl_adplaceholder;
    int mCurrPos;
    TextView _tvDescription;
    TextView _tvTitle;

    @Override
    public int eventNotify(int eventType, final Object eventObject) {
        Utility.printLog("Update: ", "eventNotify");
        int eventState = EventState.EVENT_IGNORED;
        if (eventType == EventState.EVENT_AD_LOADED_NATIVE) {
            Utility.printLog("Update: ", "Case");
            eventState = EventState.EVENT_PROCESSED;
            runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> MyApplication.getInstance().loadNativeFullAds(_fl_adplaceholder, ActivityStringInfomation.this, 1), 500));
        }
        return eventState;
    }

    private void registerAdsListener() {
        EventNotifier
                notifier = NotifierFactory.getInstance().getNotifier(NotifierFactory.EVENT_NOTIFIER_AD_STATUS);
        notifier.registerListener(this, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityStringInformationBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);

        _tvDescription = binding.tvDescription;
        _tvTitle = binding.tvTitle;

        mCurrPos = getIntent().getIntExtra("currPos", 0);
        if (mCurrPos == 1 || mCurrPos == 4) {
            _tvTitle.setText(getResources().getString(R.string.app_info_title1));
            _tvDescription.setText(getResources().getString(R.string.app_info_text1));
        } else if (mCurrPos == 2 || mCurrPos == 5) {
            _tvTitle.setText(getResources().getString(R.string.app_info_title2));
            _tvDescription.setText(getResources().getString(R.string.app_info_text2));
        } else {
            _tvTitle.setText(getResources().getString(R.string.app_info_title3));
            _tvDescription.setText(getResources().getString(R.string.app_info_text3));
        }
        _fl_adplaceholder = binding.flAdplaceholder;
        registerAdsListener();

        _ivBack = binding.ivBack;
        _ivBack.setOnClickListener(view -> onBackPressed());

        RelativeLayout adViewBanner = binding.adViewBanner;
        MyApplication.getInstance().loadBanner(adViewBanner, ActivityStringInfomation.this);

        MyApplication.getInstance().loadNativeFullAds(_fl_adplaceholder, ActivityStringInfomation.this, 1);
    }
}