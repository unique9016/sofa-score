package com.score.cricket.MainScreen;

import static com.score.cricket.MyApplication.appOpenAd;
import static com.score.cricket.MyApplication.isShowingAd;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.mindinventory.midrawer.MIDrawerView;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Helper.OpenAppAds;
import com.score.cricket.Models.NavigationDrawerModel;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Repeater.RapaeterTabLayout;
import com.score.cricket.Repeater.RepeaterDrawer;
import com.score.cricket.databinding.ActivityHomeMainBinding;

import java.util.ArrayList;

public class ActivityHomeMain extends BaseActivity {
    ActivityHomeMainBinding binding;
    ImageView _ivLeague;
    ImageView _ivLiveMatches;
    TextView _tvLeague;
    TextView _tvLiveMatches;
    ViewPager _viewPager;
    LinearLayout _llLeague;
    LinearLayout _llLiveMatches;
    private MIDrawerView _drawer_ly;
    private ImageView _iv_drawer;
    private ImageView _iv_favorite;
    private boolean mIsDoubleClick = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeMainBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setTheme(R.style.AppTheme_NoActionBar);
        setContentView(view1);

        if (appOpenAd != null && AdsHelper.isNetworkConnected(this) && AdsHelper.getOpenAdsSplash() == 1) {
            FullScreenContentCallback callback =
                    new FullScreenContentCallback() {
                        @Override
                        public void onAdDismissedFullScreenContent() {
                            AdsHelper.isShowingFullScreenAd = false;
                            isShowingAd = false;
                            appOpenAd = null;
                            Log.e(MyApplication.ADMOB_TAG, "Open AD ===> The ad was dismissed.");
                            OpenAppAds appLifecycleObserver = new OpenAppAds(MyApplication.getInstance());
                            ProcessLifecycleOwner.get().getLifecycle().addObserver(appLifecycleObserver);
                        }

                        @Override
                        public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                            Log.e(MyApplication.ADMOB_TAG, "Open AD ===> The ad failed to show.");
                            isShowingAd = true;
                        }

                        @Override
                        public void onAdShowedFullScreenContent() {
                            AdsHelper.isShowingFullScreenAd = true;
                            isShowingAd = true;
                            Log.e(MyApplication.ADMOB_TAG, "Open AD ===> The ad was shown.");
                        }
                    };
            appOpenAd.setFullScreenContentCallback(callback);
            appOpenAd.show(this);
            isShowingAd = true;
        }

        RelativeLayout adViewBanner = binding.adViewBanner;
        MyApplication.getInstance().loadBanner(adViewBanner, ActivityHomeMain.this);

        initToolbar();

        _llLeague.setOnClickListener(view -> {
            _viewPager.setCurrentItem(0, true);
            _ivLeague.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            _ivLiveMatches.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
            _tvLeague.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            _tvLiveMatches.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
        });
        _llLiveMatches.setOnClickListener(view -> {
            _viewPager.setCurrentItem(1, true);
            _ivLeague.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
            _ivLiveMatches.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            _tvLeague.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
            _tvLiveMatches.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        });

        _ivLeague.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        _ivLiveMatches.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
        _tvLeague.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        _tvLiveMatches.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
        initTabBar();

        _iv_drawer.setOnClickListener(v -> _drawer_ly.openDrawer(GravityCompat.START));
        _iv_favorite.setOnClickListener(v -> {
            Intent intent = new Intent(ActivityHomeMain.this, ActivityFavoriteMatch.class);
            if (AdsHelper.isNetworkConnected(ActivityHomeMain.this)) {
                MyApplication.getInstance().displayInterstitialAds(ActivityHomeMain.this, intent, false);
            } else {
                startActivity(intent);
            }
        });
    }

    private void initTabBar() {
        _viewPager = binding.viewPager;
        final RapaeterTabLayout rapaeterTabLayout = new RapaeterTabLayout(this, getSupportFragmentManager());
        _viewPager.setAdapter(rapaeterTabLayout);
        _viewPager.setOffscreenPageLimit(0);
        _viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        _viewPager.setCurrentItem(0, true);
                        _ivLeague.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                        _ivLiveMatches.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
                        _tvLeague.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                        _tvLiveMatches.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
                        break;
                    case 1:
                        _viewPager.setCurrentItem(1, true);
                        _ivLeague.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
                        _ivLiveMatches.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                        _tvLeague.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
                        _tvLiveMatches.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initToolbar() {
        _ivLeague = binding.ivLeague;
        _ivLiveMatches = binding.ivLiveMatches;
        _tvLeague = binding.tvLeague;
        _tvLiveMatches = binding.tvLiveMatches;
        _iv_drawer = binding.ivDrawer;
        _drawer_ly = binding.drawerLayout;
        _iv_favorite = binding.ivFavorite;
        _llLeague = binding.llLeague;
        _llLiveMatches = binding.llLiveMatches;
        RecyclerView mDrawerRecyclerView = binding.drawerRecyclerView;
        mDrawerRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, _drawer_ly,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        _drawer_ly.addDrawerListener(toggle);
        toggle.syncState();

        ArrayList<NavigationDrawerModel> drawerList = new ArrayList<>();

        drawerList.add(new NavigationDrawerModel("Leagues", R.drawable.ic_league));
        drawerList.add(new NavigationDrawerModel("Live Score", R.drawable.ic_matches));
        drawerList.add(new NavigationDrawerModel("Share App", R.drawable.ic_share));
        drawerList.add(new NavigationDrawerModel("Rate Us", R.drawable.ic_rateus));
        drawerList.add(new NavigationDrawerModel("Privacy Policy", R.drawable.ic_policy));

        RepeaterDrawer adapter = new RepeaterDrawer(this, drawerList);
        mDrawerRecyclerView.setAdapter(adapter);
    }

    public void onDrawerItemSelected(int position) {
        if (position == 0) {
            _viewPager.setCurrentItem(0, true);
            _ivLeague.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            _ivLiveMatches.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
            _tvLeague.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            _tvLiveMatches.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
        } else if (position == 1) {
            _viewPager.setCurrentItem(1, true);
            _ivLeague.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
            _ivLiveMatches.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
            _tvLeague.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.grey_40)));
            _tvLiveMatches.setTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        } else if (position == 2) {
            Intent sharintent = new Intent(Intent.ACTION_SEND);
            sharintent.setType("text/plain");
            sharintent.putExtra(Intent.EXTRA_SUBJECT, "Cricket");
            String sharemsg = "\nLet me recommend you this application\n\n";
            sharemsg = sharemsg + "http://play.google.com/store/apps/details?id=" + getPackageName() + "\n\n";
            sharintent.putExtra(Intent.EXTRA_TEXT, sharemsg);
            startActivity(Intent.createChooser(sharintent, "choose one"));
        } else if (position == 3) {
            Uri uri = Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName());

            Intent gotomarket = new Intent(Intent.ACTION_VIEW, uri);
            gotomarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(gotomarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
            }
        } else if (position == 4) {
            Intent intent = new Intent(ActivityHomeMain.this, ActivityAppPolicy.class);
            if (AdsHelper.isNetworkConnected(ActivityHomeMain.this)) {
                MyApplication.getInstance().displayInterstitialAds(ActivityHomeMain.this, intent, false);
            } else {
                startActivity(intent);
            }
        }
        if (_drawer_ly.isDrawerOpen(GravityCompat.START)) {
            _drawer_ly.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        if (_drawer_ly.isDrawerOpen(GravityCompat.START)) {
            _drawer_ly.closeDrawer(GravityCompat.START);
        } else {
            doubleBackToExit();
        }
    }

    private void doubleBackToExit() {
        if (mIsDoubleClick) {
            Intent intent = new Intent(ActivityHomeMain.this, ActivityThankYou.class);
            MyApplication.getInstance().displayInterstitialAdsExit(ActivityHomeMain.this, intent, true);
            return;
        }
        this.mIsDoubleClick = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> mIsDoubleClick = false, 2000);
    }

}