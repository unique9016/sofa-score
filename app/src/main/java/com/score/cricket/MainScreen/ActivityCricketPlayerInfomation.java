package com.score.cricket.MainScreen;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.score.cricket.R;
import com.score.cricket.Repeater.RapaeterTabLayout5;
import com.score.cricket.SubScreen.FragMatchInformation5;
import com.score.cricket.SubScreen.FragMatchInformation6;
import com.score.cricket.SubScreen.FragPlayerInformation;
import com.score.cricket.databinding.ActivityCricketPlayerInformataionBinding;

import java.util.ArrayList;

public class ActivityCricketPlayerInfomation extends BaseActivity {
    private final ArrayList<Fragment> mData = new ArrayList<>();
    ActivityCricketPlayerInformataionBinding binding;
    TabLayout _tabLayout;
    ViewPager _viewPager;
    ImageView _playerlogo;
    int mPlayerId;
    String mPlayerName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCricketPlayerInformataionBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);

        SharedPreferences sharedPreferences = getSharedPreferences("name", MODE_PRIVATE);
        mPlayerId = sharedPreferences.getInt("player_id", 0);
        mPlayerName = sharedPreferences.getString("player_name", null);

        _tabLayout = binding.tabLayout;
        _viewPager = binding.viewPager;

        CollapsingToolbarLayout collapsingToolbarLayout = binding.toolbarLayout;
        collapsingToolbarLayout.setTitleEnabled(false);

        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        toolbar.setTitle(mPlayerName);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        mData.add(new FragPlayerInformation());
        mData.add(new FragMatchInformation6());
        mData.add(new FragMatchInformation5());

        final RapaeterTabLayout5 adapter = new RapaeterTabLayout5(this, getSupportFragmentManager(), mData);

        _tabLayout.addTab(_tabLayout.newTab().setText(adapter.getPageTitle(0)));
        _tabLayout.addTab(_tabLayout.newTab().setText(adapter.getPageTitle(1)));
        _tabLayout.addTab(_tabLayout.newTab().setText(adapter.getPageTitle(2)));

        _tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        _tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        ViewGroup slidingTabStrip = (ViewGroup) _tabLayout.getChildAt(0);
        for (int i = 0; i < _tabLayout.getTabCount(); i++) {
            View tab = slidingTabStrip.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            layoutParams.weight = 2;
            tab.setLayoutParams(layoutParams);
        }

        _viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        _viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(_tabLayout));
        _tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                _viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        _playerlogo = binding.playerlogo;

        String team2 = "https://api.sofascore.com/mobile/v4/player/" + mPlayerId + "/image";

        Glide.with(this)
                .load(team2)
                .centerCrop()
                .into(_playerlogo);
    }
}