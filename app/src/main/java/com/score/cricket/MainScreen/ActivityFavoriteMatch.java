package com.score.cricket.MainScreen;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import androidx.fragment.app.FragmentTransaction;

import com.score.cricket.R;
import com.score.cricket.SubScreen.FragMatchFavorite;
import com.score.cricket.databinding.ActivityFavoriteMatchBinding;

public class ActivityFavoriteMatch extends BaseActivity {
    ActivityFavoriteMatchBinding binding;
    FrameLayout _fl_favorite;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFavoriteMatchBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);

        _fl_favorite = binding.flFavorite;

        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fl_favorite, new FragMatchFavorite());
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}