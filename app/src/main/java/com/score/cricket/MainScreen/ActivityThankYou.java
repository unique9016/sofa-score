package com.score.cricket.MainScreen;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.score.cricket.Helper.Utility;
import com.score.cricket.R;
import com.score.cricket.databinding.ActivityThankYouBinding;

public class ActivityThankYou extends BaseActivity {
    ActivityThankYouBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = ActivityThankYouBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);

        Bitmap bitmapLocal1 = Utility.decodeSampledBitmapFromResource(getResources(), R.drawable.ic_thanks_you, 500, 500);
        binding.ivThankyou.setImageBitmap(bitmapLocal1);

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            if (android.os.Build.VERSION.SDK_INT >= 21) {
                finishAndRemoveTask();
            } else {
                finishAffinity();
            }
        }, 1500);
    }

    @Override
    public void onBackPressed() {

    }
}