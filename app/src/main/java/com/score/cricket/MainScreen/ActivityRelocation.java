package com.score.cricket.MainScreen;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.score.cricket.Helper.Utility;
import com.score.cricket.R;
import com.score.cricket.databinding.ActivityRelocationBinding;

public class ActivityRelocation extends BaseActivity {
    ActivityRelocationBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRelocationBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);

        init();
        binding.linPlaystore.setOnClickListener(v -> {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("https://play.google.com/store/apps/details?id="
                                + getIntent().getStringExtra("link"))));
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage());
            }
        });
    }

    private void init() {
        ImageView mIvplaystoreicon = binding.ivplaystoreicon;
        Bitmap bitmapLocal = Utility.decodeSampledBitmapFromResource(getResources(), R.drawable.ic_playstore, 256, 256);
        mIvplaystoreicon.setImageBitmap(bitmapLocal);

    }
}