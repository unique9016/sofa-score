package com.score.cricket.MainScreen;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.score.cricket.R;
import com.score.cricket.Repeater.RapaeterTabLayout4;
import com.score.cricket.SubScreen.FragMatchInformation7;
import com.score.cricket.SubScreen.FragSquads;
import com.score.cricket.SubScreen.FragStanding;
import com.score.cricket.SubScreen.FragTeamInformation;
import com.score.cricket.databinding.ActivityCricketTeamInformationBinding;

import java.util.ArrayList;

public class ActivityCricketTeamInformation extends BaseActivity {
    private final ArrayList<Fragment> mData = new ArrayList<>();
    ActivityCricketTeamInformationBinding binding;
    TabLayout _tabLayout;
    ViewPager _viewPager;
    ImageView _playerlogo;
    int mTeamid;
    String mTeamname;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCricketTeamInformationBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);

        SharedPreferences sharedPreferences = getSharedPreferences("name", Context.MODE_PRIVATE);
        mTeamid = sharedPreferences.getInt("team_id", 0);
        mTeamname = sharedPreferences.getString("team_name", null);

        _tabLayout = binding.tabLayout;
        _viewPager = binding.viewPager;

        CollapsingToolbarLayout collapsingToolbarLayout = binding.toolbarLayout;
        collapsingToolbarLayout.setTitleEnabled(false);

        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        toolbar.setTitle(mTeamname);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        mData.add(new FragTeamInformation());
        mData.add(new FragMatchInformation7());
        mData.add(new FragStanding());
        mData.add(new FragSquads());

        final RapaeterTabLayout4 adapter = new RapaeterTabLayout4(this, getSupportFragmentManager(), mData);

        _tabLayout.addTab(_tabLayout.newTab().setText(adapter.getPageTitle(0)));
        _tabLayout.addTab(_tabLayout.newTab().setText(adapter.getPageTitle(1)));
        _tabLayout.addTab(_tabLayout.newTab().setText(adapter.getPageTitle(2)));
        _tabLayout.addTab(_tabLayout.newTab().setText(adapter.getPageTitle(3)));

        _tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        _tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        ViewGroup slidingTabStrip = (ViewGroup) _tabLayout.getChildAt(0);
        for (int i = 0; i < _tabLayout.getTabCount(); i++) {
            View tab = slidingTabStrip.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            layoutParams.weight = 1;
            tab.setLayoutParams(layoutParams);
        }

        _viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        _viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(_tabLayout));
        _tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                _viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        _playerlogo = binding.playerlogo;
        String team2 = ActivityLaunch.LogoLink_2 + mTeamid + "/logo";
        Glide.with(this)
                .load(team2)
                .centerCrop()
                .into(_playerlogo);
    }
}