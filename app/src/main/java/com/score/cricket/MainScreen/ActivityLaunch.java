package com.score.cricket.MainScreen;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.lifecycle.ProcessLifecycleOwner;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.score.cricket.BuildConfig;
import com.score.cricket.Helper.AdsHelper;
import com.score.cricket.Helper.Logger;
import com.score.cricket.Helper.OpenAppAds;
import com.score.cricket.Helper.Utility;
import com.score.cricket.Models.AppDataModel;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.databinding.ActivityLaunchBinding;

public class ActivityLaunch extends BaseActivity {
ActivityLaunchBinding binding;

    public static String LogoLink = "";
    public static String LogoLink_2 = "";
    public static String LeaguesLink = "";
    private boolean isAppOpenAdLoad = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLaunchBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(view1);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        AdsHelper.setReviewCount(0);
        Bitmap bitmapLocal1 = Utility.decodeSampledBitmapFromResource(getResources(), R.drawable.ic_splash_bg, 500, 500);
        binding.ivSplashBg.setImageBitmap(bitmapLocal1);

        AdsHelper.setUserInSplashIntro(true);
        AdsHelper.setOpenAdsShowCount(0);
        AdsHelper.setInAppShow(false);
        AdsHelper.ads_per_session = 0;

        if (AdsHelper.isNetworkConnected(ActivityLaunch.this)) {
            remoteConfig();
        }
    }

    public void remoteConfig() {
        FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600).build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);

        mFirebaseRemoteConfig.fetchAndActivate().addOnCompleteListener(this, task -> {
            if (task.isSuccessful()) {
                String response = mFirebaseRemoteConfig.getString("cricket_data");
                if (!response.isEmpty()) {
                    if (BuildConfig.DEBUG) {
                        Logger.AppLog("RemoteData: ", response);
                    }
                    doNext(response);
                } else {
                    toHome();
                }
            }
        }).addOnFailureListener(e -> Logger.AppLog("Splash: ", "RemoteData: " + e.getMessage()));
    }

    public void doNext(String response) {
        AppDataModel appData = parseAppUserListModel(response);
        setAppData(appData);

        String link = AdsHelper.getTransferLink();
        if (!TextUtils.isEmpty(link) && !link.equalsIgnoreCase("null")) {
            startActivity(new Intent(this, ActivityRelocation.class)
                    .putExtra("link", link));
            finish();
            return;
        }

        {
            if (!AdsHelper.isNetworkConnected(ActivityLaunch.this)) {
                AdsHelper.setIsAdEnable(0);
                toHome();
            } else {
                if (AdsHelper.getIsAdEnable() == 1) {
                    Log.e(MyApplication.ADMOB_TAG, "All Ads ===> Enable");
                    if (AdsHelper.getShowInterstitial() == 1) {
                        MyApplication.getInstance().loadInterstitialAd(ActivityLaunch.this);
                    }
                    if (AdsHelper.getShowNative() == 1) {
                        MyApplication.getInstance().loadAdmobNativeAd(0, ActivityLaunch.this);
                    }
                    if (AdsHelper.getShowAppOpen() == 1) {
                        loadAds();
                        new Handler(Looper.myLooper()).postDelayed(() -> {
                            if (!isAppOpenAdLoad) {
                                Log.e("Ads Next ", "From OpenApp Time Out");
                                toHome();
                            }
                        }, AdsHelper.getSplashScreenWaitCount() * 1000L);

                    } else {
                        toHome();
                        Log.e(MyApplication.ADMOB_TAG, "OpenApps Ads ===> Disable");
                    }
                } else {
                    toHome();
                    Log.e(MyApplication.ADMOB_TAG, "All Ads ===> Disable");
                }
            }
        }
    }

    public void loadAds() {
        if (AdsHelper.getIsAdEnable() == 1) {
            OpenAppAds appLifecycleObserver = new OpenAppAds(MyApplication.getInstance());
            ProcessLifecycleOwner.get().getLifecycle().addObserver(appLifecycleObserver);

            MyApplication.getInstance().loadOpenAppAdsOnSplash(isLoaded -> {
                if (isLoaded && !isFinishing() && !isDestroyed()) {
                    Log.e("Ads Next", "From OpenApp Load");
                    isAppOpenAdLoad = true;
                    toHome();
                }
            }, ActivityLaunch.this);
        }
    }

    public AppDataModel parseAppUserListModel(String jsonObject) {
        try {
            Gson gson = new Gson();
            TypeToken<AppDataModel> token = new TypeToken<AppDataModel>() {
            };
            return gson.fromJson(jsonObject, token.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setAppData(AppDataModel appData) {

        try {
            AdsHelper.setBannerAd(String.valueOf(appData.getBannerid()));
            AdsHelper.setShowBanner(appData.getBanner());
            AdsHelper.setInterstitialAd(String.valueOf(appData.getInterstitialid()));
            AdsHelper.setShowInterstitial(appData.getInterstitial());
            AdsHelper.setNativeAd(String.valueOf(appData.getNativeid()));
            AdsHelper.setShowNative(appData.getNative());
            AdsHelper.setAppOpenAd(String.valueOf(appData.getOpenadid()));
            AdsHelper.setOpenAdsSplash(appData.getIs_splash_on());
            AdsHelper.setSplashScreenWaitCount(appData.getSplash_time());
            AdsHelper.setOpenAdsCount(appData.getApp_open_count());
            AdsHelper.setShowAppOpen(appData.getOpenad());
            AdsHelper.setAdType(String.valueOf(appData.getAdtype()));
            AdsHelper.setIsAdEnable(appData.getIsAdEnable());
            AdsHelper.setExitAdEnable(appData.getExit_ad_enable());
            AdsHelper.setAdsPerSession(appData.getAds_per_session());
            AdsHelper.setInterstitialAdsClick(appData.getAds_per_click());
            AdsHelper.setDirectReviewEnable(appData.getDirect_review_enable());
            AdsHelper.setFavorite(appData.getShowFavorite());
            AdsHelper.setReviewPopupCount(appData.getReview_popup_count());
//            AdsHelper.setInAppreview(appData.getInAppreview());
//            AdsHelper.setReview(appData.getReview());
            AdsHelper.setTransferLink(appData.getTransfer_link());

            AdsHelper.setLogo_Link(appData.getLogoLink());
            AdsHelper.setLogo_Link_2(appData.getLogoLink_2());
            AdsHelper.setLeagues_Link(appData.getLeaguesLink());
            AdsHelper.setGet_Player(appData.getGetPlayer());
            AdsHelper.setGet_Match(appData.getGetMatch());
            AdsHelper.setGetLeague_Name(appData.getGetLeagueName());

            LogoLink = AdsHelper.getLogo_Link();
            LogoLink_2 = AdsHelper.getLogo_Link_2();
            LeaguesLink = AdsHelper.getLeagues_Link();
//            Test
            AdsHelper.setReview(1);
            AdsHelper.setShowBanner(1);
            AdsHelper.setShowInterstitial(1);
            AdsHelper.setAdsPerSession(2);
            AdsHelper.setShowAppOpen(1);
            AdsHelper.setExitAdEnable(1);
            AdsHelper.setInterstitialAdsClick(1);
            AdsHelper.setIsAdEnable(1);
            AdsHelper.setOpenAdsSplash(1);
            AdsHelper.setAdType("adx");
            /////////
            AdsHelper.setReviewPopupCount(2);


        } catch (Exception e) {
            Logger.AppLog("Ads:: Exception", e.getMessage());
        }
    }

    private void toHome() {
        startActivity(new Intent(ActivityLaunch.this, ActivityHomeMain.class));
        finish();
    }


}