package com.score.cricket.MainScreen;

import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.score.cricket.MyApplication;
import com.score.cricket.databinding.ActivityAppPolicyBinding;

public class ActivityAppPolicy extends BaseActivity {
    ActivityAppPolicyBinding binding;
    ImageView ivBack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAppPolicyBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);

        ivBack = binding.ivBack;
        ivBack.setOnClickListener(view -> onBackPressed());

        RelativeLayout adViewBanner = binding.adViewBanner;
        MyApplication.getInstance().loadBanner(adViewBanner, ActivityAppPolicy.this);

        WebView webview = binding.wvPrivacyPolicy;
        String url = "https://pages.flycricket.io/live-cricket-score-4/privacy.html";

        webview.getSettings().setJavaScriptEnabled(true);
        webview.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
        webview.getSettings().setAppCacheEnabled(true);
        webview.getSettings().setDatabaseEnabled(true);
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        webview.setWebViewClient(new WebViewClient() {

            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                String e = error.toString();
                System.out.println("ERROR " + e);
                handler.cancel();
            }
        });
        webview.loadUrl(url);
    }
}