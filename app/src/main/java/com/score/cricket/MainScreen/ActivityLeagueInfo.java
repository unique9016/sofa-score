package com.score.cricket.MainScreen;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.score.cricket.Models.HomeMain.HomeYearData;
import com.score.cricket.Models.AppTournaments.TournamentsExample;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Repeater.RapaeterSpinYear;
import com.score.cricket.Repeater.RapaeterTabLayout2;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.databinding.ActivityLeagueInfoBinding;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLeagueInfo extends BaseActivity {
    ActivityLeagueInfoBinding binding;
    int mSportid;
    TabLayout _tabLayout;
    ViewPager _viewPager;
    Toolbar _myToolbar;
    Spinner _mySpinner;
    private ArrayList<HomeYearData> mYear = new ArrayList<>();

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLeagueInfoBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);

        SharedPreferences sharedPreferences = getSharedPreferences("name", Context.MODE_PRIVATE);
        mSportid = sharedPreferences.getInt("id", 0);

        RelativeLayout adViewBanner = binding.adViewBanner;
        MyApplication.getInstance().loadBanner(adViewBanner, ActivityLeagueInfo.this);

        _myToolbar = binding.tb;
        setSupportActionBar(_myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        _myToolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        _myToolbar.setNavigationOnClickListener(v -> onBackPressed());

        _mySpinner = binding.spinnerr;
        ApiCalling();
    }

    private void ApiCalling() {
        APIService service = RestManager.getMatch().create(APIService.class);
        Call<TournamentsExample> call25 = service.getYearly(mSportid);

        call25.enqueue(new Callback<TournamentsExample>() {
            @Override
            public void onResponse(@NonNull Call<TournamentsExample> call, @NonNull Response<TournamentsExample> response) {
                mYear = new ArrayList<>();

                if (response.body() != null) {
                    for (int i = 0; i < response.body().getSeasons().size(); i++) {
                        mYear.add(new HomeYearData(response.body().getSeasons().get(i).getYear(), response.body().getSeasons().get(i).getId()));
                    }
                }
                RapaeterSpinYear rapaeterSpinYear = new RapaeterSpinYear(ActivityLeagueInfo.this, mYear);
                rapaeterSpinYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                _mySpinner.setAdapter(rapaeterSpinYear);

                _mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        HomeYearData clickedItem = (HomeYearData) parent.getItemAtPosition(position);
                        int id1 = clickedItem.getId();

                        SharedPreferences sharedPreferences = getSharedPreferences("name", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("seasonid", id1);
                        if (position == 0) {
                            editor.putInt("position", 0);
                        } else {
                            editor.putInt("position", 1);
                        }
                        editor.apply();

                        TabInitialize();

                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
            @Override
            public void onFailure(@NonNull Call<TournamentsExample> call, @NonNull Throwable t) {

            }
        });

    }

    private void TabInitialize() {
        _tabLayout = binding.tabLayout;
        _viewPager = binding.viewPager;
        if (_tabLayout.getTabCount() != 0) {
            _tabLayout.removeAllTabs();
        }
        _tabLayout.addTab(_tabLayout.newTab().setText("Details"));
        _tabLayout.addTab(_tabLayout.newTab().setText("Standings"));
        _tabLayout.addTab(_tabLayout.newTab().setText("All Matches"));
        _tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final RapaeterTabLayout2 adapter = new RapaeterTabLayout2(this, getSupportFragmentManager(),
                _tabLayout.getTabCount());
        _viewPager.setAdapter(adapter);
        _viewPager.setCurrentItem(0, true);
        _viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(_tabLayout));
        _tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                _viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });
    }
}