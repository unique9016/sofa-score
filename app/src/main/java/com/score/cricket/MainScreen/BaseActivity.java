package com.score.cricket.MainScreen;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.score.cricket.NetworkStateReceiver;
import com.score.cricket.R;

public class BaseActivity extends AppCompatActivity {
    public static BroadcastReceiver mNetworkReceiver;
    public static Dialog dialog;

    private static void registerNetworkBroadcast(Context context) {
        context.registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    public static void noInternetDialog(boolean value, Context context) {
        dialog.setContentView(R.layout.dialog_no_net);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;

        ImageView ivimageView = dialog.findViewById(R.id.imageView2);
        TextView ivRetry = dialog.findViewById(R.id.dialogRetryBtn);


        ivimageView.setImageResource(R.drawable.ic_no_internet);

        ivRetry.setOnClickListener(view -> {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
                registerNetworkBroadcast(context);
            }
        });


        if (value) {
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                Log.i("ShowManager", "Dismiss");
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                    ((Activity) context).finish();
                    Intent intent = ((Activity) context).getIntent();
                    ((Activity) context).overridePendingTransition(0, 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    ((Activity) context).finish();
                    ((Activity) context).overridePendingTransition(0, 0);
                    context.startActivity(intent);
                }
            }, 10);
        } else {
            try {
                if (dialog != null && !dialog.isShowing()) {
                    dialog.show();
                    Log.i("ShowManager", "Show");
                }

            } catch (WindowManager.BadTokenException e) {
                //use a log message
                Log.i("WindowManager", e.getMessage());
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialog = new Dialog(BaseActivity.this);
        mNetworkReceiver = new NetworkStateReceiver();
        registerNetworkBroadcast(BaseActivity.this);
    }

    private void unregisterNetworkChanges() {
        try {
            unregisterReceiver(mNetworkReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterNetworkChanges();
    }
}
