package com.score.cricket.MainScreen;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.score.cricket.Models.AppMatches.AppMatches_Details.Details_Example;
import com.score.cricket.MyApplication;
import com.score.cricket.R;
import com.score.cricket.Repeater.RapaeterTabLayout3;
import com.score.cricket.Retrofit.APIService;
import com.score.cricket.Retrofit.RestManager;
import com.score.cricket.SubScreen.FragLineup;
import com.score.cricket.SubScreen.FragMatchInformation2;
import com.score.cricket.SubScreen.FragMatchInformation3;
import com.score.cricket.SubScreen.FragMatchInningsInfomation;
import com.score.cricket.SubScreen.FragStandingPlayer;
import com.score.cricket.databinding.ActivityCricketMatchInfomationBinding;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCrickrtMatchInformation extends BaseActivity {
    private final ArrayList<Fragment> mData = new ArrayList<>();
    ActivityCricketMatchInfomationBinding binding;
    TabLayout _tabLayout;
    ViewPager _viewPager;
    int mId;
    RapaeterTabLayout3 rapaeterTabLayout3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCricketMatchInfomationBinding.inflate(getLayoutInflater());
        View view1 = binding.getRoot();
        setContentView(view1);
        SharedPreferences sharedPreferences1 = getSharedPreferences("name", Context.MODE_PRIVATE);
        mId = sharedPreferences1.getInt("eventid", 0);

        RelativeLayout adViewBanner = binding.adViewBanner;
        MyApplication.getInstance().loadBanner(adViewBanner, ActivityCrickrtMatchInformation.this);

        Toolbar toolbar = binding.tb;
        setSupportActionBar(toolbar);
        toolbar.setTitle("Match Information");
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        TabInotialize();
        ApiCalling(mId);
    }

    private void TabInotialize() {
        _tabLayout = binding.tabLayout;
        _viewPager = binding.viewPager;

        mData.add(new FragMatchInformation2());
        rapaeterTabLayout3 = new RapaeterTabLayout3(this, getSupportFragmentManager(), mData);
        _tabLayout.addTab(_tabLayout.newTab().setText(rapaeterTabLayout3.getPageTitle(0)));

        _tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        _tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        ViewGroup slidingTabStrip = (ViewGroup) _tabLayout.getChildAt(0);
        for (int i = 0; i < _tabLayout.getTabCount(); i++) {
            View tab = slidingTabStrip.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            layoutParams.weight = 1;
            tab.setLayoutParams(layoutParams);
        }
        _viewPager.setAdapter(rapaeterTabLayout3);
        rapaeterTabLayout3.notifyDataSetChanged();

        _viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(_tabLayout));
        _tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                _viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void ApiCalling(int mId) {

        APIService apiService8 = RestManager.getPlayer().create(APIService.class);
        Call<Details_Example> call8 = apiService8.getMatchInfo(mId);

        call8.enqueue(new Callback<Details_Example>() {
            @Override
            public void onResponse(@NonNull Call<Details_Example> call, @NonNull Response<Details_Example> response) {
                if (response.body().getHasInnings()) {
                    mData.add(new FragMatchInningsInfomation());
                    _tabLayout.addTab(_tabLayout.newTab().setText(rapaeterTabLayout3.getPageTitle(1)));
                    _tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                    _tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                    ViewGroup slidingTabStrip = (ViewGroup) _tabLayout.getChildAt(0);
                    for (int i = 0; i < _tabLayout.getTabCount(); i++) {
                        View tab = slidingTabStrip.getChildAt(i);
                        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                        layoutParams.weight = 1;
                        tab.setLayoutParams(layoutParams);
                    }
                    _viewPager.setAdapter(rapaeterTabLayout3);
                    rapaeterTabLayout3.notifyDataSetChanged();
                }
                if (response.body().getHasLineups()) {
                    mData.add(new FragLineup());

                    _tabLayout.addTab(_tabLayout.newTab().setText(rapaeterTabLayout3.getPageTitle(2)));
                    _tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                    _tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                    ViewGroup slidingTabStrip = (ViewGroup) _tabLayout.getChildAt(0);
                    for (int i = 0; i < _tabLayout.getTabCount(); i++) {
                        View tab = slidingTabStrip.getChildAt(i);
                        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                        layoutParams.weight = 1;
                        tab.setLayoutParams(layoutParams);
                    }
                    _viewPager.setAdapter(rapaeterTabLayout3);
                    rapaeterTabLayout3.notifyDataSetChanged();
                }
                if (response.body().getHasStandings()) {
                    mData.add(new FragStandingPlayer());

                    _tabLayout.addTab(_tabLayout.newTab().setText(rapaeterTabLayout3.getPageTitle(3)));

                    _tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                    _tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                    ViewGroup slidingTabStrip = (ViewGroup) _tabLayout.getChildAt(0);
                    for (int i = 0; i < _tabLayout.getTabCount(); i++) {
                        View tab = slidingTabStrip.getChildAt(i);
                        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                        layoutParams.weight = 1;
                        tab.setLayoutParams(layoutParams);
                    }
                    _viewPager.setAdapter(rapaeterTabLayout3);
                    rapaeterTabLayout3.notifyDataSetChanged();
                }
                mData.add(new FragMatchInformation3());
                _tabLayout.addTab(_tabLayout.newTab().setText(rapaeterTabLayout3.getPageTitle(4)));

                _tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                _tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                ViewGroup slidingTabStrip = (ViewGroup) _tabLayout.getChildAt(0);
                for (int i = 0; i < _tabLayout.getTabCount(); i++) {
                    View tab = slidingTabStrip.getChildAt(i);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                    layoutParams.weight = 1;
                    tab.setLayoutParams(layoutParams);
                }
                _viewPager.setAdapter(rapaeterTabLayout3);
                rapaeterTabLayout3.notifyDataSetChanged();
            }

            @Override
            public void onFailure(@NonNull Call<Details_Example> call, @NonNull Throwable t) {
            }
        });
    }
}